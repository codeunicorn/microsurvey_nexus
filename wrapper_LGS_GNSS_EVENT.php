<?php

// Demo Nexus v2.0 - Jareb Coupe 2013
// Demo Nexus v2.2 - Joseph Santos 2014
// ### Form Builder (core) ###
// The Leica Geosystems NAFTA demo wrapper

// Common initialization
require_once('common_init.php');
$page = $_SERVER['PHP_SELF']."?".$querystring;

?>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Leica Geosystems Demo Nexus</title>
<link rel="icon" href="http://gis.leica-geosystems.us/media/favicon/websites/6/IDR_MAINFRAME.ico" type="image/x-icon" />
<link rel="shortcut icon" href="http://gis.leica-geosystems.us/media/favicon/websites/6/IDR_MAINFRAME.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/fortis/default/css/styles.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/fortis/gis/css/skin.css" media="all" />
<script type="text/javascript" src="scripts/prototype/prototype.js"></script>
<script type="text/javascript" src="scripts/prototype/validation.js"></script>
<script type="text/javascript" src="scripts/varien/form.js"></script>
<script type="text/javascript" src="scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript">jQuery.noConflict();</script>

<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/base/default/css/styles-ie.css" media="all" />
<![endif]-->
<!--[if lt IE 7]>
<script type="text/javascript" src="http://gis.leica-geosystems.us/js/lib/ds-sleight.js"></script>
<script type="text/javascript" src="http://gis.leica-geosystems.us/skin/frontend/base/default/js/ie6.js"></script>
<![endif]-->
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/fortis/default/css/styles-ie-all.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/fortis/default/css/skin-ie-all.css" media="all" />
<![endif]-->
<!--[if lte IE 7]>
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/fortis/default/css/styles-ie7.css" media="all" />
<![endif]-->

<style>
  body{text-align:left;}
  .container .header{
    padding-left:15px;padding-right: 15px;height:inherit;width:inherit;}
  .container .header h1{
    font-weight:700;}
  .container .event{
    margin-top:30px;}
  .fieldset, .multiple-checkout .col2-set, .multiple-checkout .col3-set, .info-set { border:none;padding:0;}
  .header-container{background:none; text-align:center;}
  .main-container{background-position-y: -15px;}
  .main-container .main{width:inherit;}
  .col-main{
    float: none;
    width:inherit;}
  form label { color:#777; font-weight:400;}
  .form-postamble{
    margin-left:15px;}
  form .form-control{border-radius:0px;}
  #x_udf_event443_fieldpair{display:none;}
  .footer-container{
    background: none;}
  .footer-container .footer{width:inherit;padding-left:15px;}
  p.required{
    text-align: left;padding-left: 15px;font-size:14px;}
  .buttons-set button.button{float:none;margin-left:0px;}
  .buttons-set button.button span{font-size:14px;}

  @media (min-width:992px) and (max-width: 1200px){
    #x_udf_main_application121__ss_label{margin-top:20px}
  }
  @media (max-width: 992px){
    #x_udf_main_application121__ss_label{margin-top:0px}
  }
</style>
</head>

<body class="customer-account-create">
<div class="container">
  <div class="header">
    <h1>Register Today to Reserve Your Space!</h1>
    <?php echo isset($_GET['event']) ?'<h4 class="event">'. $_GET['event'] .'</h4>' : ''; ?>
    <?php echo isset($_GET['date']) ? '<h5>'. $_GET['date'] .'</h5>' : ''; ?>
    <?php echo isset($_GET['address']) ? '<h5>'. $_GET['address'] .'</h5>' : ''; ?>
    <?php echo isset($_GET['description']) ? '<p>'. $_GET['description'] .'</p>' : ''; ?>
  </div>
  <div class="main-container col2-right-layout">
    <div class="main">
      <div class="breadcrumbs">
        <ul>
          <li class="home"><a href="<?php echo $config_back; ?>"><?php echo isset($config_back_label) ? $config_back_label : ''; ?></a></li>
        </ul>
      </div>
      <div class="col-main">
        <div class="account-create">
          <?php require_once('core.php'); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-container">
    <div class="footer">
      <div class="footer-text">© 2016 Leica Geosystems. All Rights Reserved.</div>
    </div>
  </div>
</div>
<script>
  jQuery(document).ready(function(){

    jQuery('p.required').insertBefore( jQuery("form .fieldset") );
    jQuery('#x_udf_privacy_policy_version143_fieldpair_fieldpair').addClass("form-group col-md-12");
  });
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-33624115-18', 'auto', {'allowLinker': true});
  ga('require', 'linker');
  ga('linker:autoLink', ['puresurveying.com'] );
  ga('send', 'pageview');

</script>
</body>
</html>