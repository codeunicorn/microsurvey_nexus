<?php

/**

 *  This is where we can easily enable or disable each module throughout the site/app.

 *  To Enable, set the value of the module to 1.

 *  To Disable, set the value of the module to 0.

 */



/*

 * CLICKATELL SMS

 */

defined('CLICKATELL_SMS') or define('CLICKATELL_SMS', 0);



/*

 * EMAIL NOTIFICATION

 */

defined('EMAIL_NOTIFICATION') or define('EMAIL_NOTIFICATION', 1);



/*

 * EMAIL AUTO RESPONDER

 */

defined('EMAIL_AUTO_RESPONDER') or define('EMAIL_AUTO_RESPONDER', 1);



/*

 * SAVE FORM SUBMISSION

 */

defined('SAVE_FORM_SUBMISSION') or define('SAVE_FORM_SUBMISSION', 1);