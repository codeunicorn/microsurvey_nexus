[body]
<table border=0 cellpadding=0 cellspacing=0 width=548 align=center>
    <tr>
		<td width=225><a href='".$c2a_logo."'><img src='[[emailLogo]]' width='230' height='50' border='0'></a></td>
		<td width=318 align=right valign=bottom>&nbsp;</td>
    </tr>
    <tr>
        <td colspan='3'><img src='http://www.microsurvey.com/email/spacers/spacer.gif' style='display: block;' width=548 height=6 /></td>
    </tr>
</table>

<table border=0 cellpadding=0 cellspacing=0 width=548 align=center>
	<tr>
		<td colspan=10>
			<table border=0 cellpadding=0 cellspacing=0 width=548>
				<tr>
					<td colspan=3>
						<img src='[[productPeamble]]' style='display: block;' width=548 />
					</td>
				</tr>
				<tr>
					<td colspan=3>
						<img src='[[productTagLine]]' style='display: block;' width=548 height=241 />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr>
        <td width=309 bgcolor=#".$colorlight." valign=top>
            <img src='[[productSubject1]]' style='display: block;' />
			<p style='font-family:Verdana,Arial,Helvetica,sans-serif;color:#000;font-size:20px;letter-spacing:0px;margin:0;margin-bottom:18px;'><strong>[[emailBody0]]</strong></p>
			<p style='font-family:Verdana,Arial,Helvetica,sans-serif;color:#000;font-size:16px;letter-spacing:0px;margin:0;margin-bottom:0;'><strong>[[emailBody1]]</strong></p>
			<p style='font-family:Verdana,Arial,Helvetica,sans-serif;color:#000;font-size:12px;letter-spacing:0px;margin:0;margin-bottom:18px;'>[[emailBody2]]</strong></p>
			<p style='font-family:Verdana,Arial,Helvetica,sans-serif;color:#000;font-size:12px;letter-spacing:0px;margin:0;margin-bottom:18px;'><strong>[[emailBody3]]</p>
			<p style='font-family:Verdana,Arial,Helvetica,sans-serif;color:#000;font-size:12px;letter-spacing:0px;margin:0;margin-bottom:18px;'><strong>[[emailBody4]]</strong></p>
			<p style='font-family:Verdana,Arial,Helvetica,sans-serif;color:#000;font-size:12px;letter-spacing:0px;margin:0;margin-bottom:18px;'>[[emailBody5]]</p>
			<p style='font-family:Verdana,Arial,Helvetica,sans-serif;color:#000;font-size:16px;letter-spacing:0px;margin:0;margin-bottom:0px;'><strong>[[emailBody6]]</strong></p>
		</td>
	</tr>
</table>
[/body]