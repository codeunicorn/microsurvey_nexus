<?php

// Demo Nexus v2.0
// ### Form Builder (core) ###
// Wrapper for hexagon forms

// Common initialization

require_once('common_init.php');
$page = $_SERVER['PHP_SELF']."?".$querystring;
?>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Leica Geosystems</title>
<link rel="icon" href="http://gis.leica-geosystems.us/media/favicon/websites/6/IDR_MAINFRAME.ico" type="image/x-icon" />
<link rel="shortcut icon" href="http://gis.leica-geosystems.us/media/favicon/websites/6/IDR_MAINFRAME.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="http://www.microsurvey.com/tradeup/css/bootstrap.min.css" media="all" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
jQuery.noConflict();
function reloadparent() {
	parent.location.reload();
}
jQuery(window).load(function() {
	styleNexus();
	function styleNexus() {
		jQuery( "#etrReg" ).addClass("contact-form");
		jQuery( "#etrReg" ).attr('role','form');
		jQuery( "#etrReg .input-text" ).addClass("form-control input-box");
		jQuery( "#etrReg select, #etrReg textarea" ).addClass("form-control");
		jQuery( "#etrReg label" ).addClass("control-label");
		jQuery( "#etrSubmit" ).addClass("btn btn-primary standard-button2 ladda-button btn-block");
		jQuery( "#x_udf_privacy_policy_version143_fieldpair" ).wrap( '<div class="col-sm-12"></div>' );
		jQuery( "#form-results" ).wrap( '<div class="col-sm-12"></div>' );
		jQuery( "#x_udf_privacy_policy_version143_fieldpair" ).wrap( '<div class="checkbox"></div>' );	
		jQuery( "#x_emailaddress_fieldpair" ).wrap( '<div class="col-sm-6 field-container"></div>' );
		jQuery( "#x_firstname_fieldpair" ).wrap( '<div class="col-sm-6 field-container"></div>' );
		jQuery( "#x_lastname_fieldpair" ).wrap( '<div class="col-sm-6 field-container"></div>' );
		jQuery( "#x_title_fieldpair" ).wrap( '<div class="col-sm-6 field-container"></div>' );
		jQuery( "#x_companyname_fieldpair" ).wrap( '<div class="col-sm-6 field-container"></div>' );
		jQuery( "#x_address_fieldpair" ).wrap( '<div class="col-sm-6 field-container"></div>' );
		jQuery( "#x_address2_fieldpair" ).wrap( '<div class="col-sm-6 field-container"></div>' );
		jQuery( "#x_city_fieldpair" ).wrap( '<div class="col-sm-6 field-container"></div>' );
		jQuery( "#x_country_fieldpair" ).wrap( '<div class="col-sm-6 field-container"></div>' );
		jQuery( "#x_state_fieldpair" ).wrap( '<div class="col-sm-6 field-container"></div>' );
		jQuery( "#x_zip_fieldpair" ).wrap( '<div class="col-sm-6 field-container"></div>' );
		jQuery( "#x_phone_fieldpair" ).wrap( '<div class="col-sm-6 field-container"></div>' );
		jQuery( "#x_phone2_fieldpair" ).wrap( '<div class="col-sm-6 field-container"></div>' );
		jQuery( "#x_udf_ext_purchase_question405_fieldpair" ).wrap( '<div class="col-sm-6 field-container"></div>' );
		jQuery( "#x_udf_extcustomer_fieldpair" ).wrap( '<div class="col-sm-6 field-container"></div>' );
		jQuery( "#x_show_products_fieldpair" ).wrap( '<div class="col-sm-6"></div>' );
		jQuery( "#x_udf_extpurchasetime_fieldpair" ).wrap( '<div class="col-sm-6"></div>' );
		jQuery( "#x_udf_ext_last_topic364_fieldpair" ).wrap( '<div class="col-sm-6"></div>' );
		jQuery( "#x_udf_ext_interest__2_415_fieldpair" ).wrap( '<div class="col-sm-6 field-container"></div>' );
		jQuery( "#x_udf_ext_last_topic364_fieldpair" ).wrap( '<div class="col-sm-6"></div>' );
		jQuery( "#x_udf_main_application121__ss_fieldpair" ).wrap( '<div class="col-sm-6"></div>' );
		jQuery( "#x_udf_ext_contact_question441_fieldpair" ).wrap( '<div class="col-sm-6"></div>' );
		jQuery( "#x_udf_ext_surv_ground_question442_fieldpair" ).wrap( '<div class="col-sm-6"></div>' );
		jQuery( "#x_udf_comments126_fieldpair" ).wrap( '<div class="col-sm-12"></div>' );
		jQuery( "#x_udf_event443" ).wrap( '<div class="col-sm-12"></div>' );
		jQuery( ".buttons-set" ).wrap( '<div class="col-xs-12"></div>' );
		jQuery('.required-entry').closest('div').addClass('required');
	};
	
	if ($('#x_udf_event443').length) {
		$('#x_udf_event443').prop('readonly', true);
	 }

});

</script>

<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/base/default/css/styles-ie.css" media="all" />
<![endif]-->
<!--[if lt IE 7]>
<script type="text/javascript" src="http://gis.leica-geosystems.us/js/lib/ds-sleight.js"></script>
<script type="text/javascript" src="http://gis.leica-geosystems.us/skin/frontend/base/default/js/ie6.js"></script>
<![endif]-->
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/fortis/default/css/styles-ie-all.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/fortis/default/css/skin-ie-all.css" media="all" />
<![endif]-->
<!--[if lte IE 7]>
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/fortis/default/css/styles-ie7.css" media="all" />
<![endif]-->

<style>
/* --------------------------------------
=========================================
GLOBAL STYLES
=========================================
-----------------------------------------*/

html {
    font-size: 100%;
}

body {
	background-color:transparent;
    font-size: 16px;
    font-weight: 300;
    color: #313131;
    line-height: 28px;
    text-align: center;
    overflow-x: hidden !important;
    margin: auto !important;
    font-family: Arial, sans-serif,Helvetica Neue, Helvetica;
}

/* Internet Explorer 10 in Windows 8 and Windows Phone 8 Bug fix */

@-webkit-viewport {
    width: device-width;
}

@-moz-viewport {
    width: device-width;
}

@-ms-viewport {
    width: device-width;
}

@-o-viewport {
    width: device-width;
}

@viewport {
    width: device-width;
}

a {
	color:#0099bb;
    -webkit-transition: all ease 0.25s;
            transition: all ease 0.25s;
}

a:hover {
    text-decoration: none;
    color:#007a95;
}

.btn:focus,
.btn:active {
    outline: inherit;
}

/* Other fixes*/
*,
*:before,
*:after {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
            box-sizing: border-box;
}

ul,
ol {
    padding-left: 0 !important;
}

li {
    list-style: none;
}
.contact-form {
    margin-bottom: 30px;
}

.contact-form .success,
.contact-form .error {
    display: none;
}


.contact-form .textarea-box {
    margin-top: 10px;
    margin-bottom: 20px;
    -webkit-box-shadow: none;
            box-shadow: none;
}

.contact-form .textarea-box:active {
    color: #000;
}

.contact-form .input-box, .contact-form select {
    margin-bottom: 10px;
    height: 30px;
    -webkit-box-shadow: none;
            box-shadow: none;
}

.contact-form .textarea-box {
    margin-top: 10px;
    margin-bottom: 10px;
    -webkit-box-shadow: none;
            box-shadow: none;
}

.contact-form .textarea-box:active {
    color: #000;
}
.contact-form .control-label, .buttons-set p {
    font-size:14px;
    margin-bottom: 0;
    font-weight: normal;
}
.contact-form .col-sm-6, .contact-form .col-sm-12, .buttons-set p {
    text-align: left;
}
#x_udf_privacy_policy_version143_label {
	font-weight: normal; 
	font-size: 12px;
}
.standard-button,
.standard-button2 {
    font-size: 14px;
    font-weight: 400 !important;
    border-radius: 4px !important;
    text-shadow: 0 !important;
    color: #ffffff;
    min-width: 150px;
    border: none;
    padding: 10px 25px 10px 25px;
    margin: 5px;
    -webkit-transition: all ease 0.25s;
            transition: all ease 0.25s;
}

.standard-button:hover,
.standard-button2:hover {
    border: none;
}

.standard-button i,
.standard-button2 i {
    vertical-align: inherit;
    margin-right: 8px;
    font-size: 20px;
}

.btn-primary {background-color: #0099bb;}
.btn-primary:hover {background-color: #007a95;}

 .text-label{
font-family: Arial, sans-serif,Helvetica Neue, Helvetica;
font-size:11px;
line-height:1px;
margin-top:-2px;

}
.text-field{
width:100%;
height:20px;
border: 1px solid #dddddd;
margin-top:0px;
margin-bottom:10px;
padding:0px;
}
.submit-button-section{
  width:8%;
  font-family: Arial, sans-serif,Helvetica Neue, Helvetica;
  font-size:12px;
  font-weight:bold;
  text-transform: uppercase;
  text-align: center;
  margin-left:3px;
  background-color: #0099bb;
  padding:10px 10px 10px 8px;
  color: #fff;
  border:none;
 }

#x_state {
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    color: #555;
    display: block;
    font-size: 14px;
    line-height: 1.42857;
    padding: 6px 12px;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
    width: 100%;
    box-shadow: none;
    height: 30px;
    margin-bottom: 10px;
    margin-top: 0;
}
#x_state:focus {
    border-color: #66afe9;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(102, 175, 233, 0.6);
    outline: 0 none;
                }
textarea.form-control {height: 50px !important;}
.contact-form .btn-block {width:50% !important;}

.contact-form .standard-button, .contact-form .standard-button2 {border-radius: 0 !important;}
</style>

</head>

<body onload="location.href='#jumptop'">

<a href="#jumptop" style="line-height:4px;font-size:4px;">&nbsp;</a>

      <?php require_once('core.php'); ?>

<?php if ($form=="9999") { ?>
	<a href="<?php echo $config_back; ?>"><?php echo isset($config_back_label) ? $config_back_label : ''; ?></a>
<?php } ?>


</body>
</html>