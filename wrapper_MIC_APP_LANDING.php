<?php

// Demo Nexus v2.0 - Jareb Coupe 2013
// ### Form Builder (core) ###
// The MicroSurvey demo wrapper

// Common initialization
require_once('common_init.php');
$page = "/wrapper_MIC_DEMO.php?".$querystring;

?>

<style>
label {width:200px;text-align:right;padding-right:15px;}
input, select, textarea {margin-left:12px;}
.fields {text-align:left;}
.form-results {float:right;margin: 9px 8px 0 0;}

body {margin:0;padding:0;text-align:left;}
a {color: #c09700;}
a:visited {color: #c09700;}
a:hover {color: #000;}
.header-container {background-image: url(images/wrapper-images/MIC-demo/header-gray-vd.png);}
.header-container2 {background: url(images/wrapper-images/MIC-demo/top-bg-plus.png) center 0 no-repeat;}
.header {width: 960px;margin: 0 auto;padding: 0;height: 126px;position: relative;}
.header-top {height: 40px;}
.header-mid {height: 86px;}
.header-mid a.logo {
	position: absolute;
	top: 38px;
	left: -33px;
	float: none;
	text-decoration: none;
	margin: 0;
	max-width: 215px;
	height: 85px;
	padding-top: 1px;
}
.nav {width: 960px;margin: 0 auto;padding: 0;height: 51px;}
.navbar-bg {background-image: url(images/wrapper-images/MIC-demo/navbar-gray-l.png);}
.navbar-left {background-position: 0 -51px;width: 8px;height: 51px;float: left;}
.navbar {background-position: top left;background-repeat: repeat-x;float: left;width: 944px;height: 41px;padding-top: 10px;}
.navbar-right {
	background-position: 100% -51px;
	width: 8px;
	height: 51px;
	float: left;
}

.header-text {
	width:300px;
	color: #333;
	margin: 6px 0 0 10px;
	font-family: 'Open Sans', sans-serif;
	font-weight: 600;
	font-size: 14px;
	text-transform: uppercase;
	float:left;
}

.show-home-img {
	width: 40px;
	height: 41px;
	padding: 0;
	background: url(images/wrapper-images/MIC-demo/pix.png) 0 -335px no-repeat;
	text-indent: -9999px;
	overflow: hidden;
	float: left;
	border: none;
	text-decoration: none;
	text-align: left;
	list-style: none;
}

.show-home-img:hover {
	background-position: -46px -335px;
}

.nav-home-link {
	height: 41px;
	display: inline-block;
	list-style: none;
	float: left;
	text-align: left;
}

.footer-container {
	margin: 0 auto;
	padding: 0;
	background: url(images/wrapper-images/MIC-demo/footer-gray-vd.png);
	height: 154px;
}

.footer {
	width: 960px;
	margin: 0 auto;
	padding: 0;
    text-align: center;
}

.footer-text {
    font-size: 11px;
    display: inline-block;
    width: 505px;
    margin: 86px auto 0;
    line-height: 15px;
    text-align: center;
    color: #555;
    line-height: 15px;
}
.form-list {
	display:none;
}
.form-preamble, .form-postamble {
	margin:0 0 17px 0;	
}
.col-main {
	width:960px;
}
.legend, .buttons-set, .form-preamble {
	display:none;
}
.fieldset {
	border: 0;
	margin: 71px 0 0 720px;
	padding: 0 20px 0 0;
	position: absolute;
}
.legend-lazy {
	font-family: 'Open Sans', sans-serif;
	font-weight: 600;
	background-color: #fff;
	float: left;
	font-size: 14px;
	margin: -36px 0 0 1px;
	padding: 0 15px;
	color: #B0A08E;
}
.fieldset-lazy {
	border: 15px solid #eee;
	margin: 25px 0 20px;
	padding: 20px 20px 20px;
}
.demo-column-left{ float: left; width: 215px; margin:10px 0 0 10px;}
.demo-column-center{ display: inline-block; width: 475px; margin:20px 0 20px 10px;}
.demo-column-right{ float: right; width: 110px; margin:40px 40px 20px 20px;}

button.button.btn-cart span {
	background-color: #f5c714;
	color: #000;
}
.outer-div-greywhite {
	margin:0 0 20px 0;
}
.page-dark-lightgrey-bg {
	min-height:110px;
}


</style>



                    <?php require_once('core.php'); ?>

                    <fieldset>
                        <div class='fieldset-lazy'>
                        <h2 class='legend-lazy'><?php echo isset($config_select_product) ? $nexus->insertStrings($config_select_product) : ''; ?></h2>

                        <!-- ### SN ### -->
                        <div class="page-dark-lightgrey-bg outer-div-greywhite">
                            <div class="inner-div-greywhite">
                            	<div class="page-dark-lightgrey-bg">
                                    <div class="demo-column-center">
                                    <p style="font-weight:bold;">MicroSurvey STAR*NET</p>
                                	<p><?php echo isset($mic_sn_desc) ? $nexus->insertStrings($mic_sn_desc) : ''; ?></p>
                                    </div>
                                    <div class="demo-column-left"><a href="/?form=4106" class="product-demo-link"><img src="https://assets.microsurvey.com/media/images/sn/product_emblem.jpg" alt="" width="212" height="90" border="0" usemap="#hidden_details"></a></div>
                                    <div class="demo-column-right"><a href="/?form=4106" class="product-demo-link"><button type="button" title="Request Demo" class="button btn-cart"><span><span>Request Demo</span></span></button></a></div>
                            	</div>
                            </div>
                        </div>
                        
                        <!-- ### CAD ### -->
                        <div class="page-dark-lightgrey-bg outer-div-greywhite">
                            <div class="inner-div-greywhite">
                            	<div class="page-dark-lightgrey-bg">
                                    <div class="demo-column-center">
                                    <p style="font-weight:bold;">MicroSurvey CAD</p>
                                	<p><?php echo isset($mic_cad_desc) ? $nexus->insertStrings($mic_cad_desc) : ''; ?></p>
                                    </div>
                                    <div class="demo-column-left"><a href="/?form=4100"  class="product-demo-link"><img src="https://assets.microsurvey.com/media/images/cad/product_emblem.jpg" alt="" width="212" height="90" border="0" usemap="#hidden_details"></a></div>
                                    <div class="demo-column-right"><a href="/?form=4100"  class="product-demo-link"><button type="button" title="Request Demo" class="button btn-cart"><span><span>Request Demo</span></span></button></a></div>
                            	</div>
                            </div>
                        </div>

                        
                        <!-- ### CAD ### -->
                        <div class="page-dark-lightgrey-bg outer-div-greywhite">
                            <div class="inner-div-greywhite">
                            	<div class="page-dark-lightgrey-bg">
                                    <div class="demo-column-center">
                                    <p style="font-weight:bold;">MicroSurvey inCAD</p>
                                	<p><?php echo isset($mic_in_desc) ? $nexus->insertStrings($mic_in_desc) : ''; ?></p>
                                    </div>
                                    <div class="demo-column-left"><a href="/?form=4102"  class="product-demo-link"><img src="https://assets.microsurvey.com/media/images/in/product_emblem.jpg" alt="" width="212" height="90" border="0" usemap="#hidden_details"></a></div>
                                    <div class="demo-column-right"><a href="/?form=4102"  class="product-demo-link"><button type="button" title="Request Demo" class="button btn-cart"><span><span>Request Demo</span></span></button></a></div>
                            	</div>
                            </div>
                        </div>

                        
                        <!-- ### EM ### -->
                        <div class="page-dark-lightgrey-bg outer-div-greywhite">
                            <div class="inner-div-greywhite">
                            	<div class="page-dark-lightgrey-bg">
                                    <div class="demo-column-center">
                                    <p style="font-weight:bold;">MicroSurvey embeddedCAD</p>
                                	<p><?php echo isset($mic_em_desc) ? $nexus->insertStrings($mic_em_desc) : ''; ?></p>
                                    </div>
                                    <div class="demo-column-left"><a href="/?form=4103"  class="product-demo-link"><img src="https://assets.microsurvey.com/media/images/em/product_emblem.jpg" alt="" width="212" height="90" border="0" usemap="#hidden_details"></a></div>
                                    <div class="demo-column-right"><a href="/?form=4103"  class="product-demo-link"><button type="button" title="Request Demo" class="button btn-cart"><span><span>Request Demo</span></span></button></a></div>
                            	</div>
                            </div>
                        </div>

                        
                        <!-- ### PP ### -->
                        <div class="page-dark-lightgrey-bg outer-div-greywhite">
                            <div class="inner-div-greywhite">
                            	<div class="page-dark-lightgrey-bg">
                                    <div class="demo-column-center">
                                    <p style="font-weight:bold;">MicroSurvey Point Prep</p>
                                	<p><?php echo isset($mic_pp_desc) ? $nexus->insertStrings($mic_pp_desc) : ''; ?></p>
                                    </div>
                                    <div class="demo-column-left"><a href="/?form=4108"  class="product-demo-link"><img src="https://assets.microsurvey.com/media/images/pp/product_emblem.jpg" alt="" width="212" height="90" border="0" usemap="#hidden_details"></a></div>
                                    <div class="demo-column-right"><a href="/?form=4108"  class="product-demo-link"><button type="button" title="Request Demo" class="button btn-cart"><span><span>Request Demo</span></span></button></a></div>
                            	</div>
                            </div>
                        </div>
                        
                        <!-- ### LO ### -->
                        <div class="page-dark-lightgrey-bg outer-div-greywhite">
                            <div class="inner-div-greywhite">
                            	<div class="page-dark-lightgrey-bg">
                                    <div class="demo-column-center">
                                    <p style="font-weight:bold;">MicroSurvey Layout</p>
                                	<p><?php echo isset($mic_lo_desc) ? $nexus->insertStrings($mic_lo_desc) : ''; ?></p>
                                    </div>
                                    <div class="demo-column-left"><a href="/?form=4107"  class="product-demo-link"><img src="https://assets.microsurvey.com/media/images/lo/product_emblem.jpg" alt="" width="212" height="90" border="0" usemap="#hidden_details"></a></div>
                                    <div class="demo-column-right"><a href="/?form=4107"  class="product-demo-link"><button type="button" title="Request Demo" class="button btn-cart"><span><span>Request Demo</span></span></button></a></div>
                            	</div>
                            </div>
                        </div>

                        <!-- ### FG ### -->
                        <div class="page-dark-lightgrey-bg outer-div-greywhite">
                            <div class="inner-div-greywhite">
                            	<div class="page-dark-lightgrey-bg">
                                    <div class="demo-column-center">
                                    <p style="font-weight:bold;">MicroSurvey FieldGenius</p>
                                	<p><?php echo isset($mic_fg_desc) ? $nexus->insertStrings($mic_fg_desc) : ''; ?></p>
                                    </div>
                                    <div class="demo-column-left"><a href="/?form=4104" class="product-demo-link"><img src="https://assets.microsurvey.com/media/images/fg/product_emblem.jpg" alt="" width="212" height="90" border="0" usemap="#hidden_details"></a></div>
                                    <div class="demo-column-right"><a href="/?form=4104" class="product-demo-link"><button type="button" title="Request Demo" class="button btn-cart"><span><span>Request Demo</span></span></button></a></div>
                            	</div>
                            </div>
                        </div>

                        <!-- ### MSDX ### -->
                        <div class="page-dark-lightgrey-bg outer-div-greywhite" style="margin: 60px 0 20px 0 !important;">
                            <div class="inner-div-greywhite">
                            	<div class="page-dark-lightgrey-bg">
                                    <div class="demo-column-center">
                                    <p style="font-weight:bold;">MicroSurvey Data Exchange (MSDX)</p>
                                	<p><?php echo isset($mic_msdx_desc) ? $nexus->insertStrings($mic_msdx_desc) : ''; ?></p>
                                    </div>
                                    <div class="demo-column-left"><a href="/?form=4110" class="product-demo-link"><img src="https://assets.microsurvey.com/media/images/msdx/product_emblem.jpg" alt="" width="212" height="90" border="0" usemap="#hidden_details"></a></div>
                                    <div class="demo-column-right"><a href="/?form=4110" class="product-demo-link"><button type="button" title="Request Demo" class="button btn-cart"><span><span>Request Demo</span></span></button></a></div>
                            	</div>
                            </div>
                        </div>

                        <!-- ### Calc ### -->
                        <div class="page-dark-lightgrey-bg outer-div-greywhite" style="margin:0 0 0 0 !important;">
                            <div class="inner-div-greywhite">
                            	<div class="page-dark-lightgrey-bg">
                                    <div class="demo-column-center">
                                    <p style="font-weight:bold;">SurveyCalc</p>
                                	<p><?php echo isset($mic_calc_desc) ? $nexus->insertStrings($mic_calc_desc) : ''; ?></p>
                                    </div>
                                    <div class="demo-column-left"><a href="/?form=4109" class="product-demo-link"><img src="https://assets.microsurvey.com/media/images/calc/product_emblem.jpg" alt="" width="212" height="90" border="0" usemap="#hidden_details"></a></div>
                                    <div class="demo-column-right"><a href="/?form=4109" class="product-demo-link"><button type="button" title="Request Demo" class="button btn-cart"><span><span>Request Demo</span></span></button></a></div>
                            	</div>
                            </div>
                        </div>
                        

                        </div>
                    </fieldset>
