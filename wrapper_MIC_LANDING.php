<?php

// Demo Nexus v2.0 - Jareb Coupe 2013
// ### Form Builder (core) ###
// The MicroSurvey demo wrapper

// Common initialization
require_once('common_init.php');
$page = "/wrapper_MIC_DEMO.php?".$querystring;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MicroSurvey Demo Nexus</title>

<link rel="icon" href="http://store.microsurvey.com/media/favicon/websites/6/IDR_MAINFRAME.ico" type="image/x-icon" />
<link rel="shortcut icon" href="http://store.microsurvey.com/media/favicon/websites/6/IDR_MAINFRAME.ico" type="image/x-icon" />

<link rel="stylesheet" type="text/css" href="http://store.microsurvey.com/skin/frontend/fortis/default/css/styles.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://store.microsurvey.com/skin/frontend/fortis/microsurvey/css/skin.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://assets.microsurvey.com/media/css/magento.css" media="all" />
<script type="text/javascript" src="scripts/prototype/prototype.js"></script>
<script type="text/javascript" src="scripts/prototype/validation.js"></script>
<script type="text/javascript" src="scripts/varien/form.js"></script>
<script type="text/javascript" src="scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript">jQuery.noConflict();</script>


<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" href="http://store.microsurvey.com/skin/frontend/base/default/css/styles-ie.css" media="all" />
<![endif]-->
<!--[if lt IE 7]>
<script type="text/javascript" src="http://store.microsurvey.com/js/lib/ds-sleight.js"></script>
<script type="text/javascript" src="http://store.microsurvey.com/skin/frontend/base/default/js/ie6.js"></script>
<![endif]-->
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="http://store.microsurvey.com/skin/frontend/fortis/default/css/styles-ie-all.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://store.microsurvey.com/skin/frontend/fortis/default/css/skin-ie-all.css" media="all" />
<![endif]-->
<!--[if lte IE 7]>
<link rel="stylesheet" type="text/css" href="http://store.microsurvey.com/skin/frontend/fortis/default/css/styles-ie7.css" media="all" />
<![endif]-->
<style>
label {width:200px;text-align:right;padding-right:15px;}
input, select, textarea {margin-left:12px;}
.fields {text-align:left;}
.form-results {float:right;margin: 9px 8px 0 0;}

body {margin:0;padding:0;text-align:left;}
a {color: #c09700;}
a:visited {color: #c09700;}
a:hover {color: #000;}
.header-container {background-image: url(images/wrapper-images/MIC-demo/header-gray-vd.png);}
.header-container2 {background: url(images/wrapper-images/MIC-demo/top-bg-plus.png) center 0 no-repeat;}
.header {width: 960px;margin: 0 auto;padding: 0;height: 126px;position: relative;}
.header-top {height: 40px;}
.header-mid {height: 86px;}
.header-mid a.logo {
	position: absolute;
	top: 38px;
	left: -33px;
	float: none;
	text-decoration: none;
	margin: 0;
	max-width: 215px;
	height: 85px;
	padding-top: 1px;
}
.nav {width: 960px;margin: 0 auto;padding: 0;height: 51px;}
.navbar-bg {background-image: url(images/wrapper-images/MIC-demo/navbar-gray-l.png);}
.navbar-left {background-position: 0 -51px;width: 8px;height: 51px;float: left;}
.navbar {background-position: top left;background-repeat: repeat-x;float: left;width: 944px;height: 41px;padding-top: 10px;}
.navbar-right {
	background-position: 100% -51px;
	width: 8px;
	height: 51px;
	float: left;
}

.header-text {
	width:300px;
	color: #333;
	margin: 6px 0 0 10px;
	font-family: 'Open Sans', sans-serif;
	font-weight: 600;
	font-size: 14px;
	text-transform: uppercase;
	float:left;
}

.show-home-img {
	width: 40px;
	height: 41px;
	padding: 0;
	background: url(images/wrapper-images/MIC-demo/pix.png) 0 -335px no-repeat;
	text-indent: -9999px;
	overflow: hidden;
	float: left;
	border: none;
	text-decoration: none;
	text-align: left;
	list-style: none;
}

.show-home-img:hover {
	background-position: -46px -335px;
}

.nav-home-link {
	height: 41px;
	display: inline-block;
	list-style: none;
	float: left;
	text-align: left;
}

.footer-container {
	margin: 0 auto;
	padding: 0;
	background: url(images/wrapper-images/MIC-demo/footer-gray-vd.png);
	height: 154px;
}

.footer {
	width: 960px;
	margin: 0 auto;
	padding: 0;
    text-align: center;
}

.footer-text {
    font-size: 11px;
    display: inline-block;
    width: 505px;
    margin: 86px auto 0;
    line-height: 15px;
    text-align: center;
    color: #555;
    line-height: 15px;
}
.form-list {
	display:none;
}
.form-preamble, .form-postamble {
	margin:0 0 17px 0;	
}
.col-main {
	width:960px;
}
.legend, .buttons-set, .form-preamble {
	display:none;
}
.fieldset {
	border: 0;
	margin: 71px 0 0 720px;
	padding: 0 20px 0 0;
	position: absolute;
}
.legend-lazy {
	font-family: 'Open Sans', sans-serif;
	font-weight: 600;
	background-color: #fff;
	float: left;
	font-size: 14px;
	margin: -36px 0 0 1px;
	padding: 0 15px;
	color: #B0A08E;
}
.fieldset-lazy {
	border: 15px solid #eee;
	margin: 25px 0 20px;
	padding: 20px 20px 20px;
}
.demo-column-left{ float: left; width: 215px; margin:10px 0 0 10px;}
.demo-column-center{ display: inline-block; width: 475px; margin:20px 0 20px 10px;}
.demo-column-right{ float: right; width: 110px; margin:40px 40px 20px 20px;}

button.button.btn-cart span {
	background-color: #f5c714;
	color: #000;
}
.outer-div-greywhite {
	margin:0 0 20px 0;
}
.page-dark-lightgrey-bg {
	min-height:110px;
}


</style>

</head>

<body class="customer-account-create">

<div class="wrapper">
	<div class="header-container">
		<div class="header-container2">
			<div class="header">
				<div class="header-top">
				</div>
				<div class="header-mid">
                <?php 
				if (substr($origin_code,0,11)!="microsurvey") { ?>
					<div class="logo" style="float:left;"><a href="<?php echo $config_back; ?>"><img src="http://assets.microsurvey.com/apps/nexus/images/origin/<?php echo $origin_code; ?>_wrapper_logo.png" border="0"></a></div>
                	<div class="logo" style="float:right;"><a href="http://www.microsurvey.com"><img src="images/wrapper-images/MIC-demo/logo-default-sm.png" border="0"></a></div>
				<?php } else { ?>
                	<div><a href="http://www.microsurvey.com" class="logo"><img src="images/wrapper-images/MIC-demo/logo-default.png" border="0"></a></div>
				<?php } ?>
				</div>
			</div>
			<div class="nav-container">
			  <div class="nav">
				<div class="navbar-bg navbar-left"></div>
			    <div class="navbar-bg navbar">
                    <div class="nav-home-link"><a class="show-home-img" href="<?php echo $config_back; ?>"><?php echo isset($config_back_label) ? $config_back_label : ''; ?></a></div>
					<div class="form-search"></div>
                    <div class="header-text"><?php echo isset($config_navbar1_label) ? $config_navbar1_label : ''; ?></div>

                </div>
                <div class="navbar-bg navbar-right"></div>
			  </div>
			</div>
		</div>
	</div>

    <div class="main-container col2-right-layout">
        <div class="main">
        	<div class="breadcrumbs">
            	<ul>
                	<li class="home"><a href="<?php echo $config_back; ?>"><?php echo isset($config_back_label) ? $config_back_label : ''; ?></a></li>
                </ul>
            </div>                   
            <div class="col-main">
                <div class="account-create">

                    <?php require_once('core.php'); ?>

                    <fieldset>
                        <div class='fieldset-lazy'>
                        <h2 class='legend-lazy'><?php echo isset($config_select_product) ? $nexus->insertStrings($config_select_product) : ''; ?></h2>

                        <!-- ### SN ### -->
                        <div class="page-dark-lightgrey-bg outer-div-greywhite">
                            <div class="inner-div-greywhite">
                            	<div class="page-dark-lightgrey-bg">
                                    <div class="demo-column-center">
                                    <p style="font-weight:bold;">MicroSurvey STAR*NET</p>
                                	<p><?php echo isset($mic_sn_desc) ? $nexus->insertStrings($mic_sn_desc) : ''; ?></p>
                                    </div>
                                    <div class="demo-column-left"><a href="<?php echo Utility::mergeQuerystring($page,"?form=4106"); ?>"><img src="http://assets.microsurvey.com/media/images/sn/product_emblem.jpg" alt="" width="212" height="90" border="0" usemap="#hidden_details"></a></div>
                                    <div class="demo-column-right"><button type="button" title="Request Demo" class="button btn-cart" onclick="window.location.href='<?php echo Utility::mergeQuerystring($page,"?form=4106"); ?>'"><span><span>Request Demo</span></span></button></div>
                            	</div>
                            </div>
                        </div>
                        
                        <!-- ### CAD ### -->
                        <div class="page-dark-lightgrey-bg outer-div-greywhite">
                            <div class="inner-div-greywhite">
                            	<div class="page-dark-lightgrey-bg">
                                    <div class="demo-column-center">
                                    <p style="font-weight:bold;">MicroSurvey CAD</p>
                                	<p><?php echo isset($mic_cad_desc) ? $nexus->insertStrings($mic_cad_desc) : ''; ?></p>
                                    </div>
                                    <div class="demo-column-left"><a href="<?php echo Utility::mergeQuerystring($page,"?form=4100"); ?>"><img src="http://assets.microsurvey.com/media/images/cad/product_emblem.jpg" alt="" width="212" height="90" border="0" usemap="#hidden_details"></a></div>
                                    <div class="demo-column-right"><button type="button" title="Request Demo" class="button btn-cart" onclick="window.location.href='<?php echo Utility::mergeQuerystring($page,"?form=4100"); ?>'"><span><span>Request Demo</span></span></button></div>
                            	</div>
                            </div>
                        </div>

                        
                        <!-- ### CAD ### -->
                        <div class="page-dark-lightgrey-bg outer-div-greywhite">
                            <div class="inner-div-greywhite">
                            	<div class="page-dark-lightgrey-bg">
                                    <div class="demo-column-center">
                                    <p style="font-weight:bold;">MicroSurvey inCAD</p>
                                	<p><?php echo isset($mic_in_desc) ? $nexus->insertStrings($mic_in_desc) : ''; ?></p>
                                    </div>
                                    <div class="demo-column-left"><a href="<?php echo Utility::mergeQuerystring($page,"?form=4102"); ?>"><img src="http://assets.microsurvey.com/media/images/in/product_emblem.jpg" alt="" width="212" height="90" border="0" usemap="#hidden_details"></a></div>
                                    <div class="demo-column-right"><button type="button" title="Request Demo" class="button btn-cart" onclick="window.location.href='<?php echo Utility::mergeQuerystring($page,"?form=4102"); ?>'"><span><span>Request Demo</span></span></button></div>
                            	</div>
                            </div>
                        </div>

                        
                        <!-- ### EM ### -->
                        <div class="page-dark-lightgrey-bg outer-div-greywhite">
                            <div class="inner-div-greywhite">
                            	<div class="page-dark-lightgrey-bg">
                                    <div class="demo-column-center">
                                    <p style="font-weight:bold;">MicroSurvey embeddedCAD</p>
                                	<p><?php echo isset($mic_em_desc) ? $nexus->insertStrings($mic_em_desc) : ''; ?></p>
                                    </div>
                                    <div class="demo-column-left"><a href="<?php echo Utility::mergeQuerystring($page,"?form=4103"); ?>"><img src="http://assets.microsurvey.com/media/images/em/product_emblem.jpg" alt="" width="212" height="90" border="0" usemap="#hidden_details"></a></div>
                                    <div class="demo-column-right"><button type="button" title="Request Demo" class="button btn-cart" onclick="window.location.href='<?php echo Utility::mergeQuerystring($page,"?form=4103"); ?>'"><span><span>Request Demo</span></span></button></div>
                            	</div>
                            </div>
                        </div>

                        
                        <!-- ### PP ### -->
                        <div class="page-dark-lightgrey-bg outer-div-greywhite">
                            <div class="inner-div-greywhite">
                            	<div class="page-dark-lightgrey-bg">
                                    <div class="demo-column-center">
                                    <p style="font-weight:bold;">MicroSurvey Point Prep</p>
                                	<p><?php echo isset($mic_pp_desc) ? $nexus->insertStrings($mic_pp_desc) : ''; ?></p>
                                    </div>
                                    <div class="demo-column-left"><a href="<?php echo Utility::mergeQuerystring($page,"?form=4108"); ?>"><img src="http://assets.microsurvey.com/media/images/pp/product_emblem.jpg" alt="" width="212" height="90" border="0" usemap="#hidden_details"></a></div>
                                    <div class="demo-column-right"><button type="button" title="Request Demo" class="button btn-cart" onclick="window.location.href='<?php echo Utility::mergeQuerystring($page,"?form=4108"); ?>'"><span><span>Request Demo</span></span></button></div>
                            	</div>
                            </div>
                        </div>
                        
                        <!-- ### LO ### -->
                        <div class="page-dark-lightgrey-bg outer-div-greywhite">
                            <div class="inner-div-greywhite">
                            	<div class="page-dark-lightgrey-bg">
                                    <div class="demo-column-center">
                                    <p style="font-weight:bold;">MicroSurvey Layout</p>
                                	<p><?php echo isset($mic_lo_desc) ? $nexus->insertStrings($mic_lo_desc) : ''; ?></p>
                                    </div>
                                    <div class="demo-column-left"><a href="<?php echo Utility::mergeQuerystring($page,"?form=4107"); ?>"><img src="http://assets.microsurvey.com/media/images/lo/product_emblem.jpg" alt="" width="212" height="90" border="0" usemap="#hidden_details"></a></div>
                                    <div class="demo-column-right"><button type="button" title="Request Demo" class="button btn-cart" onclick="window.location.href='<?php echo Utility::mergeQuerystring($page,"?form=4107"); ?>'"><span><span>Request Demo</span></span></button></div>
                            	</div>
                            </div>
                        </div>

                        <!-- ### FG ### -->
                        <div class="page-dark-lightgrey-bg outer-div-greywhite">
                            <div class="inner-div-greywhite">
                            	<div class="page-dark-lightgrey-bg">
                                    <div class="demo-column-center">
                                    <p style="font-weight:bold;">MicroSurvey FieldGenius</p>
                                	<p><?php echo isset($mic_fg_desc) ? $nexus->insertStrings($mic_fg_desc) : ''; ?></p>
                                    </div>
                                    <div class="demo-column-left"><a href="<?php echo Utility::mergeQuerystring($page,"?form=4104"); ?>"><img src="http://assets.microsurvey.com/media/images/fg/product_emblem.jpg" alt="" width="212" height="90" border="0" usemap="#hidden_details"></a></div>
                                    <div class="demo-column-right"><button type="button" title="Request Demo" class="button btn-cart" onclick="window.location.href='<?php echo Utility::mergeQuerystring($page,"?form=4104"); ?>'"><span><span>Request Demo</span></span></button></div>
                            	</div>
                            </div>
                        </div>

                        <!-- ### EZSurv ### -->
                        <div class="page-dark-lightgrey-bg outer-div-greywhite" style="margin: 60px 0 20px 0 !important;">
                            <div class="inner-div-greywhite">
                                <div class="page-dark-lightgrey-bg">
                                    <div class="demo-column-center">
                                        <p style="font-weight:bold;">Effigis EZSurv</p>
                                        <p><?php echo isset($mic_ezsurv_desc) ? $nexus->insertStrings($mic_ezsurv_desc) : ''; ?></p>
                                    </div>
                                    <div class="demo-column-left"><a href="<?php echo Utility::mergeQuerystring($page,"?form=4222"); ?>"><img src="http://assets.microsurvey.com/media/images/ezsurv/product_emblem.jpg" alt="" width="212" height="90" border="0" usemap="#hidden_details"></a></div>
                                    <div class="demo-column-right"><button type="button" title="Request Demo" class="button btn-cart" onclick="window.location.href='<?php echo Utility::mergeQuerystring($page,"?form=4222"); ?>'"><span><span>Request Demo</span></span></button></div>
                                </div>
                            </div>
                        </div>

                        <!-- ### MSDX ### -->
                        <div class="page-dark-lightgrey-bg outer-div-greywhite">
                            <div class="inner-div-greywhite">
                            	<div class="page-dark-lightgrey-bg">
                                    <div class="demo-column-center">
                                    <p style="font-weight:bold;">MicroSurvey Data Exchange (MSDX)</p>
                                	<p><?php echo isset($mic_msdx_desc) ? $nexus->insertStrings($mic_msdx_desc) : ''; ?></p>
                                    </div>
                                    <div class="demo-column-left"><a href="<?php echo Utility::mergeQuerystring($page,"?form=4110"); ?>"><img src="http://assets.microsurvey.com/media/images/msdx/product_emblem.jpg" alt="" width="212" height="90" border="0" usemap="#hidden_details"></a></div>
                                    <div class="demo-column-right"><button type="button" title="Request Demo" class="button btn-cart" onclick="window.location.href='<?php echo Utility::mergeQuerystring($page,"?form=4110"); ?>'"><span><span>Request Demo</span></span></button></div>
                            	</div>
                            </div>
                        </div>

                        <!-- ### Calc ### -->
                        <div class="page-dark-lightgrey-bg outer-div-greywhite" style="margin:0 0 0 0 !important;">
                            <div class="inner-div-greywhite">
                            	<div class="page-dark-lightgrey-bg">
                                    <div class="demo-column-center">
                                    <p style="font-weight:bold;">SurveyCalc</p>
                                	<p><?php echo isset($mic_calc_desc) ? $nexus->insertStrings($mic_calc_desc) : ''; ?></p>
                                    </div>
                                    <div class="demo-column-left"><a href="<?php echo Utility::mergeQuerystring($page,"?form=4109"); ?>"><img src="http://assets.microsurvey.com/media/images/calc/product_emblem.jpg" alt="" width="212" height="90" border="0" usemap="#hidden_details"></a></div>
                                    <div class="demo-column-right"><button type="button" title="Request Demo" class="button btn-cart" onclick="window.location.href='<?php echo Utility::mergeQuerystring($page,"?form=4109"); ?>'"><span><span>Request Demo</span></span></button></div>
                            	</div>
                            </div>
                        </div>
                        

                        </div>
                    </fieldset>




                </div>
            </div>
		</div>
    </div>

	<div class="footer-container">
		<div class="footer">
            <div class="footer-text">
                MicroSurvey, FieldGenius, OfficeSync, and MapScenes are registered with the U.S. Patent and Trademark Office by MicroSurvey Software Inc. Powered by Autodesk Technology, and AutoCAD are registered trademarks of Autodesk, Inc. © 2013 MicroSurvey Software Inc.
            </div>
		</div>
	</div>
</div>


</body>
</html>