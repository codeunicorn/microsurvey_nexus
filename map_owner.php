<?php
// testing this stuff
// Demo Nexus v2.0 - Jareb Coupe 2013
// ### Owner Mapper ###
// This is a function within the js form submission - it looks up the owner by territory, and assigns
// Remember:
//   $country = Country
//   $region = State
//   $subregion = County (derived from city)

// Common initialization
defined('NEXUS_INTERNAL_ROOT') or define('NEXUS_INTERNAL_ROOT', realpath(dirname(__FILE__)) .'/');
require_once(NEXUS_INTERNAL_ROOT .'/common_init.php');

// grab other essential variables
$country = isset($_REQUEST['x_country']) ? $_REQUEST['x_country'] : '';
$state = isset($_REQUEST['x_state']) ? $_REQUEST['x_state'] : '';
$city = isset($_REQUEST['x_city']) ? $_REQUEST['x_city'] : '';

$nexus->addToGlobals($_REQUEST);
$form = new Forms();

$debugMode = false || $_REQUEST['debug'];
/*************************************
 * Get Owner By Territory Map
 *
 * Ownership assignment is as follows:
 *    > Load Owner by country
 *       > Load owner by region (override above)
 *          > Load owner by subregion (override above)
 ************************************/
$owner = new Owner();

// If owner or origin has been overriden, get the owner by Element
if($nexus->isOwnerOverridden() || $nexus->isOriginOverridden())
{
    if($debugMode){Utility::logMessage("owner override", "Debug");}
    $owner->getOwnerByElement($nexus->owner->code);
}
// This entire section need only run if a country value and territory map is supplied.
else if(!empty($config_territory_map) && !empty($country))
{
    if($debugMode){Utility::logMessage("territory lookup", "Debug");}
    $owner_shortcode = $nexus->mapTerritoryAndCountryToOwner($config_territory_map, $country, $state, $city, $debugMode);

    if(!$owner_shortcode) {
        if($debugMode){Utility::logMessage("territory lookup failed. Lookup by original owner: ". $nexus->owner->shortcode, "Debug");}
        $owner->getOwnerByShortcode($nexus->owner->shortcode);
    }
    else {
        if($debugMode){Utility::logMessage("territory lookup succeeded. Owner: ". $owner_shortcode, "Debug");}
        $owner->getOwnerByShortcode($owner_shortcode);
    }
}
// Default case, assign owner from default x_udf_form_owner148 / espUsers
else
{
    if($debugMode){Utility::logMessage("Default fallback. Lookup by original owner", "Debug");}
    $owner->getOwnerByShortcode($nexus->owner->shortcode);
}

// add owner data to global variables
$ownerData = $owner->get();
$nexus->addToGlobals($ownerData);
extract($ownerData);

/*************************************
 * End Get Owner By Territory Map
 ************************************/

// Construct our return data
$data = array(
    'owner' => $ownerData
);

// additional debug values
if(isset($_REQUEST['debug']))
{
    $data['debug'] = true;
    $data['submitted'] = $_REQUEST;
    $data['submitted']['espUsers'] = $ownerData['owner_id'];
    $data['submitted']['x_udf_form_owner148'] = $ownerData['owner_shortcode'];

    $data['additional'] = array(
        'profile: ' => $nexus->prof->code,
        'territory map' => isset($config_territory_map) ? $config_territory_map : '',
        'territory map vertical' => isset($seg_column_header) ? $seg_column_header : '',
        'origin_value' => $origin_value,
        'origin_code' => $origin_code,
        'sms' => isset($_REQUEST['sms']) ? true : false,
        'email_notify' => isset($_REQUEST['email_notify']) ? true : false,
        'auto_responder' => isset($_REQUEST['auto_responder']) ? true : false,
        'save_to_db' => isset($_REQUEST['save_to_db']) ? true : false
    );

    unset($data['submitted']['debug']);
}
else if(isset($_REQUEST['import']))
{
    $data['submitted'] = $_REQUEST;
    $data['submitted']['espUsers'] = $ownerData['owner_id'];
    $data['submitted']['x_udf_form_owner148'] = $ownerData['owner_shortcode'];
}

/************************************
 * END Load Owner Information from Accounts
 ************************************/

/*****************************
 * SEND EMAIL NOTIFICATION
 *****************************/
if(
    ( (defined('EMAIL_NOTIFICATION') && EMAIL_NOTIFICATION == 1 && !isset($_REQUEST['email_notify'])) || (isset($_REQUEST['email_notify']) && $_REQUEST['email_notify'] == true) )
    && $nexus->notification_email->code !== false && ($nexus->status->code !== 'Utility' || (isset($_REQUEST['nstatus']) && $_REQUEST['nstatus'] !== 7) || (isset($_REQUEST['nstatus_label']) && $_REQUEST['nstatus_label'] !== 'Utility') )
)
{
    // additional placeholders we can pass to the templates
    $placeholders = isset($nexus->lang->code) && isset($nexus->prod->shortcode) ? array_merge($nexus->loadEmailLabels($nexus->lang->code, $nexus->prod->shortcode), $_REQUEST) : $_REQUEST;
    $placeholders['product_label'] = isset($_REQUEST['x_udf_form_product149']) ? $nexus->getProductLabelFromProductShortcode($_REQUEST['x_udf_form_product149']) : '';
    $placeholders['preset'] = $nexus->getPresetFromSegmentAndDivision($segment_code, $division_code, $origin_code);

    // if we're sending notifications with accept/reject buttons
    if((isset($nexus->crm->code) && $nexus->crm->code == true) || $ownerData['owner_shortcode'] == 'joseph.santos')
    {
        // create our unique ID for authentication
        include_once(NEXUS_INTERNAL_ROOT. 'inc/PasswordHash.php');
        $isAuthKeyInQuerystring = isset($_REQUEST['auth_key']) ? true : false;

        if($isAuthKeyInQuerystring)
        {
            $form->auth_key = $_REQUEST['auth_key'];
            $form->loadFormFromAuthKey($_REQUEST['auth_key']);

            // update the form owner
            $form->loadOwnerData($ownerData);

            // add the form data to the placeholders array
            $placeholders = array_merge($form->getFormData(), $placeholders);
        }
        else
        {
            $isAuthKeyInDb = true;
            while($isAuthKeyInDb)
            {
                $form->loadAuthKey(create_random_password(32));
                $isAuthKeyInDb = $form->doesAuthKeyExist();
            }
        }

        $placeholders['auth_key'] = isset($_REQUEST['auth_key']) ? $_REQUEST['auth_key'] : $form->auth_key;

        // hack for x_udf_main_application121__ss_label
        if(isset($_REQUEST['x_udf_main_application121__ss'])) {
            $placeholders['x_udf_main_application121__ss_label'] = $form->getApplicationLabelFromApplicationValue($_REQUEST['x_udf_main_application121__ss']);
        }

        // hack for x_udf_ext_bim_hds_question462__ss
        if(isset($_REQUEST['x_udf_ext_bim_hds_question462__ss'])) {
            $placeholders['x_udf_ext_bim_hds_question462__ss_label'] = $form->getApplication2LabelFromValues($_REQUEST['x_udf_ext_bim_hds_question462__ss'], ",");
        }

        // hack for ext_hds_question_cyclone451__ms
        if(isset($_REQUEST['x_udf_ext_hds_question_cyclone451__ms'])) {
            $placeholders['x_udf_ext_hds_question_cyclone451__ms_label'] = $form->getCycloneLabelsFromValues($_REQUEST['x_udf_ext_hds_question_cyclone451__ms'], ",");
        }

        // hack for ext_hds_question_cyclone_2452__ms
        if(isset($_REQUEST['x_udf_ext_hds_question_cyclone_2452__ms'])) {
            $placeholders['x_udf_ext_hds_question_cyclone_2452__ms_label'] = $form->getCycloneLabelsFromValues($_REQUEST['x_udf_ext_hds_question_cyclone_2452__ms'], ",");
        }

        // hack for x_udf_accuracy_needs486 and x_udf_gnss_receiver485
        if(isset($_REQUEST['x_udf_accuracy_needs486']) && isset($_REQUEST['x_udf_gnss_receiver485'])) {
            $placeholderComments = isset($_REQUEST['x_udf_comments126']) ? $_REQUEST['x_udf_comments126'] : '';
            $_REQUEST['x_udf_comments126'] = 'GNSS Receiver: '. $_REQUEST['x_udf_gnss_receiver485'] . '<br><br>' . 'Accuracy Needs: '. $_REQUEST['x_udf_accuracy_needs486'] . '<br><br>'. $placeholderComments;
            $placeholders['x_udf_comments126'] = 'GNSS Receiver: '. $_REQUEST['x_udf_gnss_receiver485'] . '<br><br>' . 'Accuracy Needs: '. $_REQUEST['x_udf_accuracy_needs486'] . '<br><br>'. $placeholderComments;
        }

        // add a debug placeholder in querystring
        $placeholders['debug'] = isset($_REQUEST['debug']) ? '&debug=' : '';
        $placeholders['debugto'] = isset($_REQUEST['debugto']) ? '&debugto='. $_REQUEST['debugto'] : '';
    }

    // get html template
    $htmlTemplate = NEXUS_INTERNAL_ROOT . 'templates/'. $nexus->notification_email->code .'.php';
    if(file_exists($htmlTemplate))
    {
        $msgTemplate = $nexus->load_template($htmlTemplate, $placeholders);
        $sender = $msgTemplate['sender'];
        $subject = $msgTemplate['subject'];
        $body = $nexus->replaceAtSignWithHTML($msgTemplate['body']);

        if((isset($nexus->crm->code) && $nexus->crm->code == true) || $ownerData['owner_shortcode'] == 'joseph.santos')
        {
            foreach($placeholders as $key => $val)
            {
                if($key !== 'debug' && $key !== 'debugto')
                    $placeholders[$key] = urlencode($val);
            }

            $placeholders['nexus_external_root'] = NEXUS_EXTERNAL_ROOT;

            // requested change only for form 4145 from the Leica Lead Portal to only show Accept button
            $buttonTemplate = $_REQUEST['espFormID'] == 4145 ? NEXUS_INTERNAL_ROOT . 'templates/notify_STATUS_ACCEPT_ONLY.php' : NEXUS_INTERNAL_ROOT . 'templates/notify_STATUS.php';

            $btnTemplate = $nexus->load_template($buttonTemplate, $placeholders);
            $body .= $btnTemplate['body'];
        }

        //Create a new PHPMailer instance
        $mail = new PHPMailer();

        // Set PHPMailer to use UTF-8 charset
        $mail->CharSet = 'UTF-8';

        // Set PHPMailer to use base64 encoding
        $mail->Encoding = 'base64';

        $mail->IsSendmail();
        $mail->SetFrom($sender, $sender);

        //Set who the message is to be sent to
        if(isset($_REQUEST['debug']))
        {
            $o = new Owner();
            $debugTo = isset($_REQUEST['debugto']) ? $_REQUEST['debugto'] : 'joseph.santos';
            $debugOwner = $o->getOwnerByShortcode($debugTo);

            // if the owner doesn't exist, fallback to me
            if(empty($debugOwner))
                $debugOwner = $o->getOwnerByShortcode('joseph.santos');


            $mail->AddAddress($debugOwner['owner_email'], $debugOwner['owner_label']);
        }
        else
        {
            $mail->AddAddress($owner_email, $owner_label);
        }

        // hack for form 4232 - Surveying Pipeline to send lead alert to both Tony Wilson and Steve Crowfoot
        if($_REQUEST['espFormID'] == 4232 && $owner_email == 'steve.crowfoot@leicaus.com') {
            $otherOwner = new Owner();
            $TonyWilson = $otherOwner->getOwnerByShortcode('tony.wilson');
            $mail->AddCC($mail->AddAddress($TonyWilson['owner_email'], $TonyWilson['owner_label']));
        }

        /*
         * send to owner's manager(s)
         */

        // first, let's create an Owner class and get our owner by email
        $o = new Owner();
        $owner = $o->getOwnerByEmail($owner_email);

        // this array will hold our owner's managers to send the reports to
        $managerArray = array();

        // counter for how many managers we're sending it to
        $counter = 0;

        // we're only sending it 3 levels up the manager chain
        while($counter < 3 && ! empty($owner['owner_manager'])) {

            // check if our owner has 'real-time' lead summary
            $lead_summary_array = explode( "||", strtolower($owner['owner_lead_summary']));
            if( in_array( "real-time", $lead_summary_array ) ) {

                // this guy can have multiple managers, so we split them via the delimiter
                $managers = explode("||", $owner['owner_manager']);

                // and go through each of them
                foreach($managers as $manager) {

                    // let's create a new Owner class for this guy's manager(s)
                    $subOwner = new Owner();
                    $owner2 = $subOwner->getOwnerByShortcode($owner['owner_manager']);

                    // if this owner isn't in our $managerArray (we check by email),
                    // let's add them so we can send the report to them
                    if(!in_array($owner2['owner_email'], $managerArray))
                        $managerArray[$owner2['owner_label']] = $owner2['owner_email'];
                }
            }

            // we increase our counter to indicate 1 level up the manager chain
            $counter++;
        }


        // now we go through each manager and add them as CC
        foreach($managerArray as $managerLabel => $managerEmail)
        {
            $mail->addCC($managerEmail, $managerLabel);
        }


        // BCC admins or the type specified in 'notify_type' in querystring (admin, editor, etc)
        $u = new User();
        $notifyType = isset($_REQUEST['notify_type']) ? $_REQUEST['notify_type'] : 'admin';
        $users = $u->getAllUsers($notifyType);
        if(is_array($users)) {
            foreach( $users as $user ) {
                $o = new Owner();
                $owner = $o->getOwnerByShortcode( $user[ 'username' ] );

                if( ! empty( $owner ) && $owner[ 'owner_id' ] != -1 && $owner[ 'owner_email' ] !== $owner_email ) {
                    $mail->AddBCC( $owner[ 'owner_email' ], $owner[ 'owner_label' ] );
                }
            }
        }

        // send to admins
        if($notifyType !== 'admins')
        {
            $users = $u->getAllUsers('admin');
            if(is_array($users)) {
                foreach( $users as $user ) {
                    $o = new Owner();
                    $owner = $o->getOwnerByShortcode( $user[ 'username' ] );

                    if( ! empty( $owner ) && $owner[ 'owner_id' ] != -1 && $owner[ 'owner_email' ] !== $owner_email ) {
                        $mail->AddBCC( $owner[ 'owner_email' ], $owner[ 'owner_label' ] );
                    }
                }
            }

            // by request, send Microsurvey leads to Jason and Geordie
            $microsurvey = array('jason.poitras', 'geordie.helm');

            if($origin_code == 'microsurvey') {

                foreach ($microsurvey as $ms) {
                    $o = new Owner();
                    $owner = $o->getOwnerByShortcode($ms);

                    if (!empty($owner) && $owner['owner_email'] !== $owner_email) {
                        $mail->AddBCC($owner['owner_email'], $owner['owner_label']);
                    }
                }
            }

            // by request, send Leica leads to Aasha
            $leica = array('aasha.ostridge');

            if($origin_code == 'leicaus') {

                foreach ($leica as $lgs) {
                    $o = new Owner();
                    $owner = $o->getOwnerByShortcode($lgs);

                    if (!empty($owner) && $owner['owner_email'] !== $owner_email) {
                        $mail->AddBCC($owner['owner_email'], $owner['owner_label']);
                    }
                }
            }
        }

        //Set the subject line
        $mail->Subject = $subject;

        //Read an HTML message body from an external file, convert referenced images to embedded, convert HTML into a basic plain-text alternative body
        $mail->MsgHTML($body);

        //Replace the plain text body with one created manually
        $mail->AltBody = $mail->html2text($body, true);

        //Send the message, check for errors. Log if there's error
        if(!$mail->Send())
        {
            error_log("Mailer Error: " . $mail->ErrorInfo);
            $_REQUEST['notif'] = 0;
        }
        else
        {
            $_REQUEST['notif'] = 1;
        }
    }

}
/*****************************
 * SEND EMAIL NOTIFICATION
 *****************************/

/*****************************
 * SEND EMAIL AUTO RESPONDER NOTIFICATION
 *****************************/

if(
    ( (defined('EMAIL_AUTO_RESPONDER') && EMAIL_AUTO_RESPONDER == 1 && !isset($_REQUEST['auto_responder'])) || (isset($_REQUEST['auto_responder']) && $_REQUEST['auto_responder'] == true) )
    && !empty($owner_email)
    && $nexus->responder_email->code !== false
)
{
    // get html template
    $htmlTemplate = NEXUS_INTERNAL_ROOT . 'templates/'. $nexus->responder_email->code .'.php';

    // additional placeholders we can pass to the templates
    $placeholders = isset($nexus->lang->code) && isset($nexus->prod->shortcode) ? array_merge($nexus->loadEmailLabels($nexus->lang->code, $nexus->prod->shortcode), $_REQUEST) : $_REQUEST;
    $placeholders['preset'] = $nexus->getPresetFromSegmentAndDivision($segment_code, $division_code, $origin_code);
    if(isset($nexus->prod->shortcode)){$placeholders['product'] = $nexus->prod->shortcode;}

    if(file_exists($htmlTemplate))
    {
        $msgTemplate = $nexus->load_template($htmlTemplate, $placeholders);
        $sender = $msgTemplate['sender'];
        $subject = $msgTemplate['subject'];
        $body = $nexus->replaceAtSignWithHTML($msgTemplate['body']);

        // Create a new PHPMailer instance
        $mail = new PHPMailer();

        // Set PHPMailer to use UTF-8 charset
        $mail->CharSet = 'UTF-8';

        // Set PHPMailer to use base64 encoding
        $mail->Encoding = 'base64';

        // Set PHPMailer to use the sendmail transport
        $mail->IsSendmail();
        $mail->SetFrom($sender, $sender);

        //Set who the message is to be sent to
        $emailTo = isset($_REQUEST['x_emailaddress']) ? $_REQUEST['x_emailaddress'] : '';
        $firstname = isset($_REQUEST['x_firstname']) ? $_REQUEST['x_firstname'] : '';
        $lastname = isset($_REQUEST['x_lastname']) ? $_REQUEST['x_lastname'] : '';

        $mail->AddAddress($emailTo, $firstname .' '. $lastname);

        //Set the subject line
        $mail->Subject = $subject;

        //Read an HTML message body from an external file, convert referenced images to embedded, convert HTML into a basic plain-text alternative body
        $mail->MsgHTML($body);

        //Replace the plain text body with one created manually
        $mail->AltBody = $mail->html2text($body, true);

        //Send the message, check for errors. Log if there's error
        if(!$mail->Send())
        {
            Utility::logMessage("Mailer Error: " . $mail->ErrorInfo, 'auto_responder_notifications');
            $_REQUEST['respond'] = 0;
        }
        else
        {
            $_REQUEST['respond'] = 1;
        }
    }
}
/*****************************
 * END SEND EMAIL AUTO RESPONDER NOTIFICATION
 *****************************/

/********************************
 * FORM SUBMISSION INTO DB
 ********************************/
if( (defined('SAVE_FORM_SUBMISSION') && SAVE_FORM_SUBMISSION == 1 && !isset($_REQUEST['save_to_db'])) || (isset($_REQUEST['save_to_db']) && $_REQUEST['save_to_db'] == true))
{
    // load post data
    $_REQUEST['origin_id'] = $origin_value;
    $_REQUEST['origin_label'] = $origin_code;

    // set status
    if(isset($nexus->status->code) && $nexus->status->code == 'Utility' && !isset($_REQUEST['nstatus']))
    {
        $_REQUEST['nstatus'] = 7;
        $_REQUEST['nstatus_label'] = 'Utility';
    }

    if(!isset($_REQUEST['map']))
        $_REQUEST['map'] = isset($config_territory_map) ? $config_territory_map : '';

    if(!isset($_REQUEST['auth_key']))
        $form->loadPostData($_REQUEST);

    // load owner data
    $form->loadOwnerData($ownerData);

    // save form submission
    $ret = isset($_REQUEST['auth_key']) ? $form->update() : $form->saveSubmission();
}
/********************************
 * END FORM SUBMISSION INTO DB
 ********************************/

// get coupon code for Allen Precision newsletter signup
if(isset($_REQUEST['coupon_code'])) {
    $coupon_url = "https://www.allenprecision.com/coupon-generator.php?x_emailaddress=".$_REQUEST['x_emailaddress'];
    $data['coupon'] = json_decode(file_get_contents($coupon_url));
}

// Pass the mapped owner array data into the js that called this routine
echo json_encode($data);
die();
?>
