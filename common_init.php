<?php
// Demo Nexus v2.0 - Jareb Coupe 2013
// ### Common Initialization ###
// ### Serves as the bootstrap file ###

/**************************************
 * THESE LINES OF CODE ARE ALWAYS ON TOP!
 * NO OTHER CODE SHOULD BE ON TOP OF THESE!
 **************************************/
// error reporting
if (isset($_GET['debug'])) {
    error_reporting(E_ALL);

    if($_SERVER['HTTP_HOST'] == 'nexus.microsurvey.staging' || $_SERVER['HTTP_HOST'] == 'nexus.microsurvey.local')
    {
        $phpConsole = dirname(dirname(__FILE__)) . '/libraries/php-console-master/src/PhpConsole/__autoload.php';

        if(file_exists($phpConsole))
        {
            require_once($phpConsole);
            $PC = PhpConsole\Handler::getInstance();
            $PC->start();
        }
    }
}
else {
    error_reporting(0);
}

// set our default timezone to Los Angeles
date_default_timezone_set('America/Los_Angeles');

// let's set our charset to utf-8 just to make sure we're working with the right charset
header('Content-Type: text/html; charset=utf-8');

// define all of our project specific directories
defined('NEXUS_INTERNAL_ROOT') or define('NEXUS_INTERNAL_ROOT', realpath(dirname(__FILE__)) .'/');
defined('NEXUS_CLASSES_DIR') or define('NEXUS_CLASSES_DIR', NEXUS_INTERNAL_ROOT .'classes/');
defined('NEXUS_DATA_DIR') or define('NEXUS_DATA_DIR', NEXUS_INTERNAL_ROOT .'/data/');

$externalRoot = $_SERVER['SERVER_NAME'] == 'assets.microsurvey.com' ? $_SERVER['SERVER_NAME'] . '/apps/nexus' : $_SERVER['SERVER_NAME'];
defined('NEXUS_EXTERNAL_ROOT') or define("NEXUS_EXTERNAL_ROOT", $externalRoot ."/");

session_start();

// include our plugins file, which defines site wide constants to enable or disable each module
include_once(NEXUS_INTERNAL_ROOT .'module_definitions.php');

// include our main database abstraction class, which also loads all other classes
include_once(NEXUS_INTERNAL_ROOT .'DB.class.php');

DB::Get(array('MySQL'));

/****************************************
 * OK, CONTINUE WITH ALL OTHER CODE BELOW
 * **************************************/
/**
 * Our main Nexus class.
 * This will be our main object to interface with
 */
$nexus = new Nexus();

// Access Control Origin
$nexus->setAccessControl();

// ban the user if their ip is in our ban list
$ip = Utility::GrabIP();
if ($nexus->isBanned($ip)) {
	die("An unexpected error has occurred. Error Code: 2.1.14"); // 2 = B, 1 = A, 14 = N (BAN)
}

// load form profiles
$nexus->loadProfiles();

/**
 * Create variables from nexus profile. Useful for Wrapper files so they can use
 * variables like $espUsers instead of $nexus->espUsers.
 */
$nexusVars = $nexus->createVariablesFromProfile();
extract($nexusVars);
foreach($nexusVars as $key => $val)
{
    $nexusVars[$key] = $nexus->insertStrings($val);
}

// Run sneaky bot checker >>>
// Since the country select field is dynamically generated, any bot that scans the page
// and then attempts to auto-fill and submit will end up sending an email address without
// a country value, making it easy to spot the bots - only works when country is required
if (isset($nexusCode->confirmation->code) && $nexusCode->confirmation->code) {
    if (in_array('x_country',$nexus->required_array)) {
        if (empty($_REQUEST['x_country'])) {die("Bot");}
    }
}
?>
