<?php

// Demo Nexus v2.2 - Joseph Santos, Jareb Coupe 2015
// Leica Geosystems eBook wrapper

// Common initialization
require_once('common_init.php');
$page = $_SERVER['PHP_SELF']."?".$querystring;
$img = isset($_GET['img']) ? urldecode($_GET['img']) : '';
$t1 = isset($_GET['t1']) ? urldecode($_GET['t1']) : '';
$t2 = isset($_GET['t2']) ? urldecode($_GET['t2']) : '';
$dl = isset($_GET['dl']) ? urldecode($_GET['dl']) : '';
$ex = isset($_GET['ex']) ? urldecode($_GET['ex']) : '';
$status = isset($_GET['status']) ? urldecode($_GET['status']) : '';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="icon" href="http://assets.leica-geosystems.us/wrapper/ebook/favicon.ico">
<link href="http://assets.leica-geosystems.us/wrapper/ebook/style/template.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $t1; ?></title>
<style>
.bgheader {background-image:url(http://assets.leica-geosystems.us/wrapper/ebook/images/bg_header/<?php echo $img; ?>.jpg);background-repeat:no-repeat; width:100%; height:680px; position:relative;}
</style>
<script>
var _etgq=_etgq||[];_etgq.push(['_etg_o',979]);
(function(e,t,r,i,g){g=e.createElement(t);i=e.getElementsByTagName(t)[0];g.async=true;g.src=r;
i.parentNode.insertBefore(g,i);})(document,'script','//trk.etrigue.com/etriguelive.js');
</script>
</head>

<body>
<div class="container">
	<div class="header">
		<div class="logo"></div>
		<div class="clr"></div>
	</div>
	<div class="bgheader">
		<h1><?php echo $t1; ?></h1>
		<div class="li_di"></div>
		<h2><?php echo $t2; ?></h2>
		<div class="book"><img src="http://assets.leica-geosystems.us/wrapper/ebook/images/book/<?php echo $img; ?>.png" width="328" height="468" /></div>
	</div>
    
    
<?php if ($status=="confirmation") { ?>

    <div id="content">
        <h3>You're one click away.<br />
        Click below to download!</h3>
        <div class="bdown-a"><a href="http://assets.leica-geosystems.us/wrapper/ebook/download/<?php echo $dl; ?>" target="_blank">DOWNLOAD</a></div>
    </div>

<?php } else { ?>
    
	<div id="form_s">
		<h3>Fill out the form below to download this E-Book.</h3>
		<?php require_once('core.php'); ?>
		<div class="clr"></div>
		<div class="lg_s"></div>
        <?php if (!empty($ex)) { ?>
		<div class="clr"></div>
		<iframe src="http://assets.leica-geosystems.us/wrapper/ebook/iframes/<?php echo $ex; ?>.html" align="center" height="200" width="768" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" seamless style="margin:50px 0 0 0;"></iframe>
        <?php } ?>
<div class="clr"></div>
	</div>
    
<?php } ?>

	<div id="footer">
		<div>© Copyright <?php echo date("Y");?> Leica Geosystems - Part of Hexagon</div>
		<img src="http://assets.leica-geosystems.us/wrapper/ebook/images/leica_logo.png" style="position: absolute;top: 25px;right: 30px;">
	</div>
</div>
</body>
</html>
