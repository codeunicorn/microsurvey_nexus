<?php

// Demo Nexus v2.0 - Jareb Coupe 2013
// ### Form Builder (core) ###
// The MicroSurvey demo wrapper when used in Magento

// Common initialization
require_once('common_init.php');
$page = "/nexus/?".$querystring;

?>

<style>
label {width:200px;text-align:right;padding-right:15px;} 
.fields input, .fields select, .fields textarea {margin-left:12px;}
.fields {text-align:left;}
.form-results {float:right;margin: 9px 8px 0 0;}
.header-languages {
	float:right;
	margin:-60px 0 0 0;
	width:800px;
}
.header-languages-flag {
	margin:0 9px 0 0 ;
}
.sidebar {
	background: url(https://assets.microsurvey.com/apps/nexus/images/wrapper-images/MIC-demo/rightbar.png) 0 0 no-repeat;
	margin:11px 0 0 -1px;
}
.sidebar-top {
	padding: 10px 20px 15px 17px;
	border-left: 1px #fff solid;
	border-right: 1px #fff solid;
}
.sidebar-bottom {
	margin:0 0 0 2px;
	border-left: 1px #fff solid;
	border-right: 1px #fff solid;
}
.form-preamble, .form-postamble {
	margin:0 0 17px 0;	
}
.col-main-nx {
	float: left;
	width: 720px;
	padding: 0;
}
.col-main-nx {
	float: left;
	width: 720px;
	padding: 0;
}
.col-right-nx {
	float: right;
	width: 220px;
	padding: 0 0 0 20px;
}

</style>

<div class="header-languages">
<?php
    /*
    $i=0;
    foreach ($querystring_codes as $val) {
        if ($querystring_codes[$i]['Querystring Variable']=="lang") {
            $language_array = explode("||",$querystring_codes[$i]['New Value']);
            $language_select_flag = $querystring_codes[$i]['Querystring Value'];
            $language_select_code = $language_array[0];
            $language_select_label = $language_array[1];
            $language_select_native = $language_array[2];
            echo "<a href='". Utility::mergeQuerystring($page,"?lang=".$language_select_code)."'><img src='https://assets.microsurvey.com/apps/nexus/images/flag-".$language_select_flag.".png' class='header-languages-flag' border='0' title='".$language_select_label." / ".$language_select_native."'></a>";
        }
        $i++;
    }
    */
?>
</div>
   
<div class="col-main-nx">
    <div class="account-create">

        <img src="https://assets.microsurvey.com/apps/nexus/images/wrapper-images/MIC-demo/header_mic_demo_<?php echo $product_shortcode; ?>.jpg" style="height:80px;width:739px;margin:6px 0 24px -9px;" />

        <?php require_once('core.php'); ?>

        <script type="text/javascript">
        //<![CDATA[
            // Disabled for now
            // var dataForm = new VarienForm('notset', true);
        //]]>
        </script>
    </div>
</div>
<div class="col-right-nx">
    <div class="sidebar">
        <div class="sidebar-top">
            <div class="block block-layered-nav" style="margin-bottom: 12px; margin-top: 10px;">
                <div class="block-title"><strong><span><?php echo isset($config_sidebar_header1_label) ? $nexus->insertStrings($config_sidebar_header1_label) : ''; ?></span></strong></div>
            </div>
            <span><?php echo isset($config_sidebar_content1_label) ? $nexus->insertStrings($config_sidebar_content1_label) : ''; ?></span>
        </div>
        <div class="sidebar-bottom">
            <a href="<?php echo Utility::mergeQuerystring($page,"?form=4100"); ?>"><img src="https://assets.microsurvey.com/media/images/cad/product_emblem.jpg" alt="" width="212" height="90" border="0" /></a>
            <a href="<?php echo Utility::mergeQuerystring($page,"?form=4102"); ?>"><img src="https://assets.microsurvey.com/media/images/in/product_emblem.jpg" alt="" width="212" height="90" border="0" /></a>
            <a href="<?php echo Utility::mergeQuerystring($page,"?form=4103"); ?>"><img src="https://assets.microsurvey.com/media/images/em/product_emblem.jpg" alt="" width="212" height="90" border="0" /></a>
            <a href="<?php echo Utility::mergeQuerystring($page,"?form=4104"); ?>"><img src="https://assets.microsurvey.com/media/images/fg/product_emblem.jpg" alt="" width="212" height="90" border="0" /></a>
            <a href="<?php echo Utility::mergeQuerystring($page,"?form=4106"); ?>"><img src="https://assets.microsurvey.com/media/images/sn/product_emblem.jpg" alt="" width="212" height="90" border="0" /></a>
            <a href="<?php echo Utility::mergeQuerystring($page,"?form=4107"); ?>"><img src="https://assets.microsurvey.com/media/images/lo/product_emblem.jpg" alt="" width="212" height="90" border="0" /></a>
            <a href="<?php echo Utility::mergeQuerystring($page,"?form=4108"); ?>"><img src="https://assets.microsurvey.com/media/images/pp/product_emblem.jpg" alt="" width="212" height="90" border="0" /></a>
            <a href="<?php echo Utility::mergeQuerystring($page,"?form=4109"); ?>"><img src="https://assets.microsurvey.com/media/images/calc/product_emblem.jpg" alt="" width="212" height="90" border="0" /></a>
            <a href="<?php echo Utility::mergeQuerystring($page,"?form=4110"); ?>"><img src="https://assets.microsurvey.com/media/images/msdx/product_emblem.jpg" alt="" width="212" height="90" border="0" /></a>
        </div>
    </div>
</div>