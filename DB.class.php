<?php
/**
 * Database connection layer
 * 
 * Connects the database and other reuqired APIs (implemented MySQL DB)
 * 
 */


/**
 * Database connection class. This is a singleton class, which cannot me instantiated by itself.
 * Useage: $Database = Db::Get('MySQL');  -> this instantiates the class and returns it (if already instantiated, just returns a reference to it)
 * 
 * @package arbed.admin
 * @subpackage classes
 */
 class Db 
{
	/**
	 * The internal instance holder variable
	 * @access private
	 */
	private static $instance;

	/**
	 * The DSN, Username and Password variables for the PDO database connection. 
	 * These get initialized differently depending on the environment that runs the script (devel, test, live)
	 * @access private
	 */
	private $DSN;
	private $DBUser;
	private $DBPass;

	/**
	 * MySQL database connection (PDO instance)
	 * @access public
	 */
	public $Db;

	/**
	 * Private constructor class. Being a singleton, this is never called from outside this class
	 * @param Array $CreateWhat an array of objects to create. Currently implemented: MySQL
	 */
	private function __construct($CreateWhat)
	{
		/*
		 * init vars based on the environment the script runs in
		 */
		$this->InitVars();

		/*
		 * create the various connections requested
		 */
		if (count($CreateWhat) > 0)
			$this->CreateAPIs($CreateWhat);
	}

	/*
	 * Private function used to init vars based on the environment the script runs in
	 */
	private function InitVars()
	{
		/*
		 * Determine where we run. Beware that Dev / Text / Live environments **WILL HAVE** to use different paths
		 */
        if(isset($_SERVER['HTTP_HOST']))
        {
            switch($_SERVER['HTTP_HOST'])
			{
				case 'microsurvey.local' :
					$this->DSN = 'mysql:host=127.0.0.1;dbname=assetsms_nexus;charset=utf8';
					$this->DBUser = 'root';
					$this->DBPass = '';
				break;
				case 'nexus.microsurvey.local' :
					$this->DSN = 'mysql:host=127.0.0.1;dbname=assetsms_nexus;charset=utf8';
					$this->DBUser = 'root';
					$this->DBPass = '';
				break;
			default :
					$this->DSN = 'mysql:host=127.0.0.1;dbname=assetsms_nexus;charset=utf8';
					$this->DBUser = 'assetsms_admin';
					$this->DBPass = 'Zzty!#89Lty_';
				break;
			}
        }
        else
        {
            $hostname = gethostname();

            switch($hostname)
            {
                case 'SurfaceJoe' :
                    $this->DSN = 'mysql:host=127.0.0.1;dbname=assetsms_nexus;charset=utf8';
                    $this->DBUser = 'root';
                    $this->DBPass = '';
                    break;

                case 'host.leica-geosystems.us' :
                    $this->DSN = 'mysql:host=127.0.0.1;dbname=assetsms_nexus;charset=utf8';
                    $this->DBUser = 'assetsms_admin';
                    $this->DBPass = 'Zzty!#89Lty_';
                    break;

                default :
                    $this->DSN = 'mysql:host=127.0.0.1;dbname=assetsms_nexus;charset=utf8';
                    $this->DBUser = 'assetsms_admin';
                    $this->DBPass = 'Zzty!#89Lty_';
                    break;
            }
        }

        // check if some project-wide constants used in this class are defined
        defined('NEXUS_INTERNAL_ROOT') or define('NEXUS_INTERNAL_ROOT', realpath(dirname(__FILE__)));
        defined('NEXUS_CLASSES_DIR') or define('NEXUS_CLASSES_DIR', NEXUS_INTERNAL_ROOT .'classes/');
	}

	/*
	 * Private function used to create the various connections requested
 	 * @param Array $CreateWhat an array of objects to create. Currently implemented: MySQL
	 */
	private function CreateAPIs($CreateWhat)
	{
		/*
		 * Creates the MySQL connection. 
		 * Note that it needs to check if already exists, since this is a singleton class and this method can be called multiple times.
		 */		
		if (in_array('MySQL', $CreateWhat) && is_null($this->Db))
		{ 
			try
			{
				$this->Db = new PDO($this->DSN, $this->DBUser, $this->DBPass);
				$this->Db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->Db->query('SET NAMES utf8');
			}
			catch (PDOException $e) // Connection failed, email alert and exit
			{
				var_dump($e); 
				die;
			}
		}
	}

	/*
 	 * Creates a new instance of the class and returns it. If already created, it simply returns the instance, insuring first that all connections requested are created.
	 * This is the main interface of the class with the outside world
 	 * @param Array $CreateWhat an array of objects to create. Currently implemented: MySQL
	 */
	public static function Get($CreateWhat = Array())
	{
		if (!isset(self::$instance))
	    {
			$c = __CLASS__;
			self::$instance = new $c($CreateWhat);
	    }
		else
			self::$instance->CreateAPIs($CreateWhat);

    	return self::$instance;
	}

	/*
	 * Implements autoloading for the classes defined in this project
 	 * @param string $Class - required parameter for this function: the class that has to be created
	 */
	public static function AutoLoad($Class)
	{
		/*
		 * Define an associative array of classes to autocreate. This is the fastest method to reach the correct include path for the class.
		 */
		$Classes = Array(
            'Form' => "plugins/PFBC/Form.php",
            'PHPMailer' => "plugins/PHPMailer/PHPMailer.class.php"
		);

		/*
		 * Set the include path and include the actiual file, if it actually exists. If not try to load the file with the same name as the class
		 */
        if (array_key_exists($Class, $Classes))
        {
            $Path = NEXUS_INTERNAL_ROOT . $Classes[$Class];

            if (file_exists($Path) && !is_dir($Path))
                require_once($Path);
        }
		else
		{			
			$Path = NEXUS_CLASSES_DIR . $Class . '.class.php';
			
			if (file_exists($Path) && !is_dir($Path))
				require_once($Path);
		}
	}

}

/*
 * register with PHP's autoload system
 */
if(function_exists('spl_autoload_register'))
    spl_autoload_register(array('Db', 'AutoLoad'));
else
{
    Db::AutoLoad('');
}
