<?php
/**
 * Created by PhpStorm.
 * Author: Joseph Santos
 * Date: 12/4/14
 * Time: 4:15 AM
 * Description: Required: a controller (c) and an action (a) to be passed into the $argv globals or $_GET parameters.
 *              Optional: array parameter to be passed into the controller constructor is defined in the cp[] variable.
 *                        array parameter to be passed into the action is defined in the ap[] variable.
 *
 *              Example: php cron.php c=myController a=myAction cp[param1]=val1 cp[param2]=val2 ap[param1]=val1
 *                       would run the following code:
 *
 *                      $cp = array( 'param1' => val1, 'param2' => val2 );
 *                      $ap = array( 'param1' => val1 );
 *
 *                      $controller = new myController($cp);
 *                      $controller->$action($ap);
 *
 *
 */
session_start();

// set the max execution time to 20 minutes
set_time_limit(1200);

// so we can pass arguments via command line
if(isset($argv))
    parse_str(implode('&', array_slice($argv, 1)), $_GET);

// at minimum, we need to pass a controller and an action (function) to run
if(isset($_GET['c']) && isset($_GET['a']))
{
    $controller = $_GET['c'];
    $action = $_GET['a'];

    // include everything in the /classes directory
    defined('NEXUS_INTERNAL_ROOT') or define('NEXUS_INTERNAL_ROOT', realpath(dirname(__FILE__)) .'/');
    require_once(NEXUS_INTERNAL_ROOT .'DB.class.php');
    Db::Get(array('MySQL'));

    // we check if the class and method exists
    if(class_exists($controller) && (method_exists($controller, $action) && is_callable($controller, $action)))
    {
        // our parameters to the class constructor
        $cparams = isset($_GET['cp']) ? $_GET['cp'] : array();

        // our parameters to pass to the action (function)
        $aparams = isset($_GET['ap']) ? $_GET['ap'] : array();

        $myController = new $controller($cparams);
        $myController->$action($aparams);
        exit();
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
</body>
</html>
