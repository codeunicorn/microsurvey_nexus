<?php
/**
 * Created by JetBrains PhpStorm.
 * Author: Joseph Santos
 * Date: 5/27/14
 * Time: 11:47 PM
 * Description: 
 */

ini_set('max_execution_time', 300);
date_default_timezone_set('America/Los_Angeles');

session_start();

defined('NEXUS_INTERNAL_ROOT') or define('NEXUS_INTERNAL_ROOT', realpath(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR);
defined('NEXUS_CLASSES_DIR') or define('NEXUS_CLASSES_DIR', NEXUS_INTERNAL_ROOT .'classes/');
$externalRoot = $_SERVER['SERVER_NAME'] == 'assets.microsurvey.com' ? $_SERVER['SERVER_NAME'] . '/apps/nexus' : $_SERVER['SERVER_NAME'];
defined('NEXUS_EXTERNAL_ROOT') or define("NEXUS_EXTERNAL_ROOT", $externalRoot ."/");

include_once(NEXUS_INTERNAL_ROOT .'DB.class.php');
Db::Get(array('MySQL'));

if(!isset($_GET['request']))
    die();

switch($_GET['request'])
{
    case 'get_data' :
        $reports = new Reports();
        $data = $reports->getDatatableData($_POST);
        echo $data;
        break;

    case 'get_kml' :

        $kml = new KML();

        if(isset($_GET['map']) && isset($_GET['preset']))
        {
            $map = urldecode($_GET['map']);
            $preset = urldecode($_GET['preset']);
            $mapping = $kml->getSegmentAndDivFromPresetAndMap($preset, $map);

            $kml->setMap($map);
            $kml->setSegment($mapping['segment']);
            $kml->setDivision($mapping['division']);

            $wasMapped = $kml->mapToJson();
            if($wasMapped)
            {
                echo($kml->getJson());
                exit();
            }
            else
            {
                echo json_encode(array('error' => "error mapping data to kml", 'reason' => $kml->getLastError()));
                exit();
            }
        }

        break;

    case 'auth_user' :
        include_once(NEXUS_INTERNAL_ROOT . 'inc/PasswordHash.php');

        $user = new User();
        $user->fetchUser($_GET['username']);
        $user->authenticate_password($_GET['password']);
        $result = $user->isAuthenticated();
        $userRole = $user->isAuthenticated() ? $user->getRole() : '';
        $numAttempts = ((int) $user::ALLOWED_ATTEMPTS) - ((int) $user->getNumberOfAttempts());
        $message = $user->isAuthenticated() ? 'Successfully logged in' :
            $user->isBanned() ?
                'You have exceeded the maximum number of attempts. Please wait 24 hours before attempting to log in again or contact your system administrator.' :
                'Invalid username and/or password. You have '. $numAttempts .' attempts remaining';

        // store user and authentication in session
        if($user->isAuthenticated())
        {
            $_SESSION['username'] = $user->getUsername();
            $_SESSION['userRole'] = $user->getRole();
        }
        echo json_encode(array("result" => $result, "role" => $userRole, "message" => $message));
        break;

    case 'logout' :

        unset($_SESSION['username']);
        unset($_SESSION['userRole']);
        header("Location: http://". NEXUS_EXTERNAL_ROOT . "reports/");
        die();

        break;

    case 'hash_password' :

        include_once(NEXUS_INTERNAL_ROOT . 'inc/PasswordHash.php');
        echo create_hash($_GET['pw']);

        break;

    case 'get_highcharts_data' :

        $reports = new Reports();
        $data = $reports->getHighchartsData($_GET['type']);
        echo $data;
        break;

    case 'post_data' :

        if(!isset($_SESSION['username']))
            die();

        // our data
        $dataArray = $_POST['data'];

        // get our form data
        $form = new Forms();
        $nexus = new Nexus();

        // our return data
        $ret = array(
            "status" => "",
            "style" => "",
            "message" => "",
            "owner" => array());
        $retStatus = '';
        $retOwner = array();

        foreach($dataArray as $key => $data)
        {
            $form->loadFormFromId($data['id']);
            $nexus->loadProfiles($form);

            // get our old values from the DB
            $oldOwner = $form->x_udf_form_owner148;
            $oldStatusLabel = $form->status_label;
            $oldChannelTactic = $form->x_udf_channel___tactic400__ss_label;
            $postedData = json_decode($form->posted_data, true);
            $oldSegment = $form->x_udf_primary_market_segment122__ss_label;
            $oldDivision = $form->x_udf_division146__ss_label;
            $oldDealer = $form->x_udf_ext_dealer375;

            // get our new values that we passed in
            $newOwner = $data['x_udf_form_owner148'];
            $newStatusLabel = $data['status_label'];

            $newChannelTactic = $data['x_udf_channel___tactic400__ss_label'];
            $newSegment = $data['x_udf_primary_market_segment122__ss_label'];
            $newDivision = $data['x_udf_division146__ss_label'];
            $newDealer = $data['x_udf_ext_dealer375'];

            // update our database if there is a change in data
            if($oldOwner != $newOwner || $oldStatusLabel != $newStatusLabel || $oldChannelTactic != $newChannelTactic || $oldSegment != $newSegment || $oldDivision != $newDivision || $oldDealer != $newDealer)
            {
                // get our owner data
                $owner = new Owner();
                $owner_shortcode = !empty( $newOwner ) ? $newOwner : $nexus->mapTerritoryAndCountryToOwner($nexus->config_territory_map->value, $nexus->x_country->value, $nexus->x_state->value, $nexus->x_city->value);

                // if the new owner_shortcode is blank, fallback to the old owner
                $owner_shortcode = !empty($owner_shortcode) ? $owner_shortcode : $oldOwner;

                // get our owner data
                $own = $owner->getOwnerByShortcode($owner_shortcode);

                // set new status
                $statusArray = array_flip($form->getStatusArray());
                $form->status = $statusArray[$newStatusLabel];
                $form->status_label = $newStatusLabel;

                // set new owner
                $form->owner_id = $own['owner_id'];
                $form->owner_label = $own['owner_shortcode'];
                $form->x_owner = $own['owner_id'];
                $form->x_udf_form_owner148 = $own['owner_shortcode'];
                $form->x_udf_form_owner148_label = $own['owner_label'];

                // set new channel tactic
                $form->x_udf_channel___tactic400__ss_label = $newChannelTactic;
                $newChannelTacticValue = $form->getChannelTacticValueFromLabel($newChannelTactic);
                $form->channel_tactic = $newChannelTacticValue;
                $form->x_udf_channel___tactic400__ss = $newChannelTacticValue;

                // set new post_data
                $newSegmentValue = $form->getSegmentValueFromSegment($newSegment);
                $newDivisionValue = $form->getDivValueFromDiv($newDivision);
                $postedData['x_udf_primary_market_segment122__ss'] = $newSegmentValue;
                $postedData['x_udf_division146__ss'] = $newDivisionValue;
                $postedData['x_udf_ext_dealer375'] = $newDealer;
                $form->posted_data = json_encode($postedData);

                // set new segment
                $form->x_udf_primary_market_segment122__ss = $newSegmentValue;
                $form->x_udf_primary_market_segment122__ss_label = $newSegment;

                // set new division
                $form->x_udf_division146__ss = $newDivisionValue;
                $form->x_udf_division146__ss_label = $newDivision;

                // set new preset
                $form->preset = $form->getPresetFromSegmentAndDivision($newSegment, $newDivision, $form->x_udf_origin_code114__ss_label);

                // set new dealer
                $form->x_udf_ext_dealer375 = $newDealer;

                $dat = $form->update();

                // if successful
                if((int) $dat !== 0)
                {
                    $ret["status"] = "SUCCESS";
                    $ret["style"] = "alert-success";
                    $ret["message"] .= $ret["message"] == "" ? "Successfully updated data for row(s) ". $data["count"] : ", ". $data["count"];
                    $ret["owner"][$data["count"]] = $owner_shortcode;
                }
                else
                {
                    $ret = array("status" => "ERROR", "style" => "alert-danger", "message" => "Unable to update data on server. Please try again.");
                }
            }
            else
            {
                $ret = array("status" => "ALERT", "style" => "alert-info", "message" => "No Changes were made.");
            }
        }

        echo json_encode($ret);
        break;

    case 'notify_owner' :

        if(!isset($_SESSION['username']))
            die();

        // our data
        $dataArray = $_POST['data'];

        // get our form data
        $form = new Forms();

        foreach($dataArray as $key => $data)
        {
            $form->loadFormFromId($data['id']);

            if(! $form->doesAuthKeyExist())
            {
                // create our unique ID for authentication
                include_once(NEXUS_INTERNAL_ROOT. 'inc/PasswordHash.php');
                $doesAuthKeyExist = true;

                while($doesAuthKeyExist)
                {
                    $form->loadAuthKey(create_hash(create_random_password(), true));
                    $doesAuthKeyExist = $form->doesAuthKeyExist();
                }

                $form->update();
            }

            $querystring = json_decode($form->posted_data, true);
            $querystring['owner'] = $data['x_udf_form_owner148'];
            $querystring['sms'] = false;
            $querystring['email_notify'] = true;
            $querystring['auto_responder'] = false;
            $querystring['save_to_db'] = false;
            $querystring['auth_key'] = $form->auth_key;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'http://'. NEXUS_EXTERNAL_ROOT . 'map_owner.php?' . http_build_query($querystring));
            curl_setopt ($ch, CURLOPT_HEADER, 0);
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER,1);
            $result = curl_exec($ch);
            curl_close ($ch);
        }

        $ret = array("status" => "SUCCESS", "style" => "alert-success", "message" => "Notified owner");

        echo json_encode($ret);
        break;

    case 'importcsv' :

        // A list of permitted file extensions
        $allowed = array('csv');

        if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){

            $extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);

            if(!in_array(strtolower($extension), $allowed)){
                echo '{"status":"error", "message":"File is not allowed"}';
                exit;
            }

            // let's parse the csv
            $csv = new File_CSV_DataSource();
            $csv->load($_FILES['upl']['tmp_name']);
            $rows = $csv->connect();
            $headers = $csv->getHeaders();

            // is this an import csv or an opportunities csv?
            $opportunitiesArray = array(
                'Opportunity Name',
                'Form Key',
                'Type',
                'Lead Source',
                'Amount Currency',
                'Amount',
                'Close Date',
                'Next Step',
                'Stage',
                'Probability (%)',
                'Fiscal Period',
                'Age',
                'Created Date',
                'Opportunity Owner',
                'Owner Role',
                'Account Name'
            );

            $returnHeaders = array();
            $returnData = array();

            if(count(array_diff($opportunitiesArray, $headers)) == 0) // opportunities
            {
                foreach($rows as $key => $row)
                {
                    $form = new Forms();
                    $form->loadAuthKey($row['Form Key'])->loadFormFromAuthKey();

                    if($form->id !== null)
                    {
                        $opportunities = array();

                        foreach($opportunitiesArray as $oval)
                        {
                            $opportunities[$oval] = $row[$oval];
                            $col = strtolower(str_replace(' ', '_', str_replace(' (%)', '', $oval)));
                            $form->$col = $row[$oval];
                        }
                        $effectiveValue = $row['Amount'] * $row['Probability (%)'] / 100;
                        $opportunities['Effective Value'] = $effectiveValue;
                        $form->effective_value = $effectiveValue;
                        $form->opportunities = json_encode($opportunities);

                        $form->x_udf_extcustomer = strtolower($row['Type']) == 'existing customer' ? 'Yes' : (strtolower($row['Type']) == 'new customer' ? 'No' : '');


                        $form->update();
                    }
                }

                // CSV is correct, let's return a datatables data
                echo json_encode(array("status" => "success", "type" => "opportunities", "message" => "File accepted", ));
                exit;
            }
            else
            {
                // required, at a bare-minimum for reach record: country and email address
                $hasEmail = $csv->hasColumn('email') || $csv->hasColumn('Email Address');
                $hasCountry = $csv->hasColumn('country');
                if(!($hasCountry && $hasEmail))
                {
                    echo '{"status":"error", "message":"CSV file has invalid format"}';
                    exit;
                }

                // If ANY column header exists that isn't in the array of valid headers/variations, then just error out
                $nexus = new Nexus();
                $mappings = $nexus->getMappings();
                foreach($headers as $header)
                {
                    $lookupHeader = str_replace(' ', '_', strtolower($header));
                   if(!array_key_exists($lookupHeader, $mappings))
                   {
                       echo '{"status":"error", "message":"CSV file has invalid column: '. $header .'"}';
                       exit;
                   }
                }

                // our headers for the datatable
                $isFormSet = false;
                $isOwnerSet = false;
                $isPresetSet = false;
                $isCtSet = false;

                foreach($headers as $header)
                {
                    // let's make everything lowercase and turn spaces into underscores
                    $returnHeaders[] = array('data' => $header, 'title' => $header, 'sClass' => $header . '_import import_header');

                    if($header == 'form')
                        $isFormSet = true;

                    if($header == 'owner')
                        $isOwnerSet = true;

                    if($header == 'preset')
                        $isPresetSet = true;

                    if($header == 'ct')
                        $isCtSet = true;

                }

                if(! $isFormSet)
                    $returnHeaders[] = array('data' => 'form', 'title' => 'Form', 'sClass' => 'form_import import_header');

                if(! $isOwnerSet)
                    $returnHeaders[] = array('data' => 'owner', 'title' => 'Owner', 'sClass' => 'owner_import import_header');

                if(! $isPresetSet)
                    $returnHeaders[] = array('data' => 'preset', 'title' => 'Segment', 'sClass' => 'preset_import import_header');

                if(! $isCtSet)
                    $returnHeaders[] = array('data' => 'ct', 'title' => 'Channel Tactic', 'sClass' => 'ct_import import_header');

                $returnHeaders[] = array('data' => 'commit', 'title' => 'Commit', 'sClass' => 'commit_row_btn_import import_header');


                // add a commit button to the rows
                foreach($rows as $key => $row)
                {
                    $returnData[$key] = $row;

                    $returnData[$key]['commit'] = '<button class="commit_row_btn btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil"></span></button>';

                    $returnData[$key]["DT_RowId"] = 'import_row_'. $key;

                    $returnData[$key]['index'] = $key;

                    if(! isset($rows[$key]['form']))
                        $returnData[$key]['form'] = '';

                    if(! isset($rows[$key]['preset']))
                        $returnData[$key]['preset'] = '';

                    if(! isset($rows[$key]['ct']))
                        $returnData[$key]['ct'] = '';

                    // if owner is not set, get the owner for the given origin, seg, and div
                    if(! isset($rows[$key]['owner']))
                        $returnData[$key]['owner'] = '';
                }

                // CSV is correct, let's return a datatables data
                echo json_encode(array("status" => "success", "type" => "manual", "message" => "File accepted", "columns" => $returnHeaders, "data" => $returnData, "datacount" => count($returnData)));
                exit;
            }
        }

        echo '{"status":"error", "message":"unknown error has occurred"}';
        exit;
        break;

    case 'import_data':

        $nexus = new Nexus();
        $mappings = $nexus->getMappings();
        $isPresetSet = false;
        $isCtSet = false;
        $querystringVars = array();

        foreach($_POST['data'] as $key => $val)
        {
            $lowerKey = str_replace(' ', '_', strtolower($key));

            // for preset
            if($lowerKey == 'preset' && !empty($val))
            {
                // get our origin, segment and division from preset
                $mappedPreset = $nexus->getMappedValuesFromPreset($val);
                $querystringVars['orig'] = $mappedPreset['origin'];
                $querystringVars['x_udf_origin_code114__ss'] = $nexus->getOriginValueFromOrigin($mappedPreset['origin']);

                $querystringVars['seg'] = $mappedPreset['segment'];
                $querystringVars['x_udf_primary_market_segment122__ss'] = $nexus->getSegmentValueFromSegment($mappedPreset['segment']);

                $querystringVars['div'] = $mappedPreset['division'];
                $querystringVars['x_udf_division146__ss'] = $nexus->getDivValueFromDiv($mappedPreset['division']);

                if(!empty($mappedPreset['map']))
                {
                    $querystringVars['map'] = $mappedPreset['map'];
                }

                $isPresetSet = true;
            }

            else if($lowerKey == 'ct' && !empty($val))
            {
                $querystringVars['x_udf_channel___tactic400__ss'] = $val;
                $isCtSet = true;
            }

            // for product
            else if($lowerKey == 'product' && !empty($val))
            {
                $querystringVars['x_udf_form_product149'] = $val;
                $productElement = $nexus->getProductElementFromProductShortcode($val);
                $productValue = $nexus->getProductValueFromProductShortcode($val);
                $querystringVars[$productElement] = $productValue;
            }

            else if($lowerKey == 'form' && !empty($val))
            {
                $querystringVars['form'] = $val;
                $querystringVars['espFormID'] = $val;
            }

            // for everything else
            else if(array_key_exists($lowerKey, $mappings) && !array_key_exists($mappings[$lowerKey], $querystringVars))
            {
                $querystringVars[$mappings[$lowerKey]] = $val;
            }

            else {
                $querystringVars[$lowerKey] = $val;
            }
        }

        // if preset is not set, get the default origin, seg, and div from form ID
        if(!$isPresetSet)
        {
            $formIdRow = $nexus->getFormIdRow($_POST['data']['form']);

            $querystringVars['x_udf_origin_code114__ss'] = $nexus->getOriginValueFromOrigin($formIdRow['Default Origin']);
            $querystringVars['x_udf_primary_market_segment122__ss'] = $nexus->getSegmentValueFromSegment($formIdRow['Default Segment']);
            $querystringVars['x_udf_division146__ss'] = $nexus->getDivValueFromDiv($formIdRow['Default Division']);

            if(!$isCtSet)
            {
                $querystringVars['x_udf_channel___tactic400__ss'] = $nexus->getChannelTacticValueFromLabel($formIdRow['Default Channel Tactic']);
            }
        }

        // additional parameters we want to pass
        $tz = new DateTimeZone('America/Los_Angeles');
        $dateNow = new DateTime('now', $tz);
        $querystringVars['x_udf_privacy_policy_consent301'] = $dateNow->format("Ymd");
        $querystringVars['x_udf_privacy_policy_version143'] = '20121001';
        $querystringVars['import'] = 'true';

        // pass the data to map_owner.php to get the owner
        $myUrl = 'http://'. NEXUS_EXTERNAL_ROOT . 'map_owner.php?' . http_build_query($querystringVars);

        error_log($myUrl);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $myUrl);
        curl_setopt ($ch, CURLOPT_HEADER, 0);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER,1);
        $response = curl_exec($ch);
        curl_close ($ch);

        // we'll grab the submitted results and create our form to pass to our api call for eTrigue
        $result = json_decode($response, true);
        $etrReg = '<form action id="etrReg" method="post" name="etrReg">';

        if(isset($result['submitted']) && !empty($result['submitted']) && is_array($result['submitted']))
        {
            // we don't care about these, we'll exclude these
            $excludes = array('commit', 'dt_rowid', 'index', 'import');

            foreach($result['submitted'] as $key => $val)
            {
                if(!is_array($val)) {
                    // we'll runcate after 1000 characters because eTrigue has issues with over 1000 characters
                    $theVal = (strlen($val) > 1000) ? substr($val, 0, 995) . '...' : $val;
                }

                if(!in_array($key, $excludes))
                    $etrReg .= '<input type="hidden" id="'. $key .'" name="'. $key .'" value="'. $theVal .'">';
            }
        }
        $etrReg .= '</form>';

        // don't submit to eTrigue if it's my local
        $submitEtrigue = NEXUS_EXTERNAL_ROOT !== 'nexus.microsurvey.local/' ? true : false;

        $return = array(
            'status'    =>  'success',
            'message'   =>  'import completed',
            'etrReg'    =>  $etrReg,
            'submitEtrigue' => $submitEtrigue
        );

        echo json_encode($return);
        break;
}