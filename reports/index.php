<?php
/**
 * Created by JetBrains PhpStorm.
 * Author: Joseph Santos
 * Date: 5/24/14
 * Time: 10:32 AM
 * Description: 
 */
ini_set('max_execution_time', 300);
session_start();

/* Set internal character encoding to UTF-8 */
mb_internal_encoding("UTF-8");

if(isset($_GET['nosession']) && session_status() == PHP_SESSION_ACTIVE){session_destroy();}

date_default_timezone_set('America/Los_Angeles');
header('Content-Type: text/html; charset=utf8');

defined('NEXUS_INTERNAL_ROOT') or define('NEXUS_INTERNAL_ROOT', realpath(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR);
defined('NEXUS_CLASSES_DIR') or define('NEXUS_CLASSES_DIR', NEXUS_INTERNAL_ROOT .'classes/');
include_once(NEXUS_INTERNAL_ROOT . 'DB.class.php');
Db::Get(array('MySQL'));

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Nexus Reporting Tool</title>
    <?php
    if(isset($_GET['debug']))
    {
        echo '
            <link href="assets/backbone-forms/distribution/templates/bootstrap.css" rel="stylesheet" />
            <link href="assets/css/smoothness/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
            <link href="assets/css/dataTables.css" rel="stylesheet" />
            <link href="assets/TableTools-2.2.3/css/dataTables.tableTools.min.css" rel="stylesheet" />
            <link href="assets/Responsive-1.0.4/css/dataTables.responsive.css" rel="stylesheet" />
            <link href="assets/css/select2.css" rel="stylesheet" />
            <link href="assets/css/daterangepicker-bs3.css" rel="stylesheet" />
            <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
            <link href="assets/css/reports.css" rel="stylesheet"/>
        ';
    }
    else
    {
        echo '<link href="assets/css/main.min.css" rel="stylesheet" />';
    }
    ?>
</head>

<body>
<div id="modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"></div>
<div id="mainContainer">

    <div id="headerContainer">
        <div id="logo" class="<?php echo str_replace('.' , ' ', $_SERVER['HTTP_HOST']); ?>"></div>
        <h4 id="tag">Lead Generation Nexus Reporting</h4>
        <div id="logged_in"></div>
    </div>

    <div id="notificationContainer"></div>

    <div id="loginContainer"></div>

    <div id="theContentContainer">

        <div id="menuContainer"></div>
        <div id="kmlMenuContainer"></div>
        <div id="importMenuContainer"></div>

        <div id="contentContainer">

            <!-- Datatables container -->
            <div id="dataTablesContainer" class="middle-view">
                <button type="button" class="menu_toggle_btn btn btn-primary">
                    <span class="glyphicon glyphicon-align-justify"></span>
                </button>
            </div>

            <!-- Dashboard container -->
            <div id="dashboardContainer" class="middle-view active-view">
                <div id="dashboardStackedArea"></div>
                <div id="dashboardHeatMap"></div>
                <div id="dashboardDoughnutChart"></div>
                <div id="dashboardHighMap"></div>
            </div>

            <!-- Custom container -->
            <div id="customContainer" class="middle-view"></div>

            <!-- KML Map container -->
            <div id="kmlMapContainer">

                <button type="button" class="kml_menu_toggle_btn btn btn-primary">
                    <span class="glyphicon glyphicon-align-justify"></span>
                </button>

                <div id="nexus_map"></div>
            </div>

            <!-- Import container -->
            <div id="importContainer" class="middle-view">

            </div>

        </div>
    </div>

    <div id="etrigue_form_container" class="hidden"></div>

</div>
<?php
$reports = new Reports();
$kml = new KML();

$reportsVariables = $reports->toJSON();
$dataTablesColumns = $reports->getDatatableColumns();
$username = isset($_SESSION["username"]) ? $_SESSION["username"] : '';
$userRole = isset($_SESSION['userRole']) ? $_SESSION['userRole'] : 'testRole';
$kmlData = json_encode($kml->getDropdowns(), true);

echo '<script>
    var datatablesColumns = '. $dataTablesColumns .',
        reportsVariables = '. $reportsVariables .',
        domain = "'. str_replace('.' , ' ', $_SERVER['HTTP_HOST']) .'",
        isLoggedIn = "'. isset($_SESSION["username"]) .'",
        username = "'.$username .'",
        role = "'. $userRole .'",
        kmldata = '. $kmlData .';
    ;
</script>';
;

// Template files
include_once('assets/templates/menuTemplate.js');
include_once('assets/templates/kmlMenuTemplate.js');
include_once('assets/templates/importMenuTemplate.js');
include_once('assets/templates/datatablesTemplate.js');
include_once('assets/templates/loginTemplate.js');
include_once('assets/templates/headerMenuTemplate.js');
include_once('assets/templates/importUploadTemplate.js');
include_once('assets/templates/importDatatablesTemplate.js');
include_once('assets/templates/tableToolsModalTemplate.js');

if(isset($_GET['debug']))
{
    echo '
    <!-- Required files -->
    <script src="assets/jquery-1.11.2.min.js"></script>
    <script src="assets/jquery-ui.js"></script>
    <script src="assets/bootstrap.min.js"></script>
    <script src="assets/highcharts/highcharts.js"></script>
    <script src="assets/highcharts/modules/heatmap.js"></script>
    <script src="assets/highcharts/modules/map.js"></script>
    <script src="assets/highcharts/modules/data.js"></script>
    <script src="assets/highcharts/modules/drilldown.js"></script>
    <script src="assets/highcharts/modules/mapdata/us-all.js"></script>
    <script src="assets/highcharts/modules/mapdata/north-america.js"></script>
    <script src="assets/highcharts/themes/sand-signika.js"></script>
    <script src="assets/json2.js"></script>
    <script src="assets/moment.js"></script>
    <script src="assets/moment-timezome-with-data.js"></script>
    <script src="assets/select2.js"></script>
    <script src="assets/daterangepicker.js"></script>
    <script src="assets/bootstrap-multiselect.js"></script>
    <script src="assets/underscore.js"></script>
    <script src="assets/backbone-min.js"></script>
    <script src="assets/jquery.dataTables.min.js"></script>
    <script src="assets/TableTools-2.2.3/js/dataTables.tableTools.min.js"></script>
    <script src="assets/Responsive-1.0.4/js/dataTables.responsive.min.js"></script>
    <script src="assets/googlemap.js"></script>
    <script src="assets/kml.js"></script>
    <script src="assets/jquery.knob.js"></script>
    <script src="../scripts/etrigueForm_v2.js"></script>

    <!-- jQuery File Upload Dependencies -->
    <script src="assets/jquery.ui.widget.js"></script>
    <script src="assets/jquery.iframe-transport.js"></script>
    <script src="assets/jquery.fileupload.js"></script>

    <!-- Backbone files -->
    <script src="assets/models/AppModel.js"></script>
    <script src="assets/views/backbone-forms.js"></script>
    <script src="assets/views/LoginFormView.js"></script>
    <script src="assets/views/MenuFormView.js"></script>
    <script src="assets/views/KmlMenuFormView.js"></script>
    <script src="assets/views/ImportMenuFormView.js"></script>
    <script src="assets/views/headerMenuView.js"></script>
    <script src="assets/views/KMLMapView.js"></script>
    <script src="assets/views/importView.js"></script>
    <script src="assets/views/DatePickerView.js"></script>
    <script src="assets/views/DataTablesView.js"></script>
    <script src="assets/views/HighChartsView.js"></script>
    <script src="assets/views/Select2.js"></script>
    <script src="assets/views/DateRangePickerView.js"></script>
    <script src="assets/views/Multiselect.js"></script>
    <script src="assets/views/ModalView.js"></script>
    <script src="assets/models/MenuModel.js"></script>
    <script src="assets/models/KmlMenuModel.js"></script>
    <script src="assets/models/ImportMenuModel.js"></script>
    <script src="assets/models/userModel.js"></script>
    <script src="assets/models/DataTablesModel.js"></script>
    <script src="assets/models/HighChartsModel.js"></script>
    <script src="assets/reports.js"></script>
    ';
}
else
{
    echo '<script src="assets/main.min.js"></script>
        <script src="../scripts/etrigueForm_v2.js"></script>';
}
?>
</body>
</html>