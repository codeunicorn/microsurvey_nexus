/** 
 * Include this template file after backbone-forms.amd.js to override the default templates
 *
 * 'data-*' attributes control where elements are placed
 */
define(['jquery', 'underscore', 'backbone', '../.'], function($, _, Backbone) {
  var Form = Backbone.Form;

  {{body}}

});
