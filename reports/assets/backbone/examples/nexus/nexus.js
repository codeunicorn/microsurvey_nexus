/**
 * Created with JetBrains PhpStorm.
 * User: jsantos317
 * Date: 5/23/14
 * Time: 4:06 PM
 * To change this template use File | Settings | File Templates.
 */

// Load the application once the DOM is ready, using `jQuery.ready`:
$(function(){

    // Models
    window.Wine = Backbone.Model.extend();

    window.WineCollection = Backbone.model.extend(
        {
            model : Wine,
            url : "../api/wines"
        }
    );

    // Views
    window.WineListView = Backbone.View.extend(
        {
            tagname : "ul",

            initialize : function()
            {
                this.model.bind("reset", this.render, this);
            },

            render : function(eventName)
            {
                _.each(this.model.models, function(wine)
                {
                    $(this.el).append(new WineListItemView({model: wine}).render().el);
                }, this);

                return this;
            }
        }
    );

    window.WineListItemView = Bankbone.View.extend(
        {
            tagName : "li",

            template : _.template($('#tpl-wine-list-item').html()),

            render : function(eventName)
            {
                $(this.el).html(this.template(this.omdel.toJSON()));
                return this;
            }
        }
    );

    window.WineView = Backbone.View.extend(
        {
            template : _.template($('tpl-wine-details').html()),

            render : function(eventName)
            {
                $(this.el).html(this.template(this.model.toJSON()));
                return this;
            }
        }
    );

    // Router
    var AppRouter = Backbone.Router.extend(
        {
            routes : {
                "" : "list",
                "wines/:id" : "wineDetails"
            },

            list : function()
            {
                this.wineList = new WineCollection;
                this.wineListView = new WineListView({model : this.wineList});
                this.wineList.fetch();
                $('#sidebar').html(this.wineListView.render().el);
            },

            windDetail : function(id)
            {
                this.wine = this.wineList.get(id);
                this.wineView = new WineView({model : this.wine});
                $('#content').html(this.wineView.render().el);
            }
        }
    );

    var app = new AppRouter();
    Backbone.history.start();

});