/**
 * Created with JetBrains PhpStorm.
 * User: jsantos317
 * Date: 5/24/14
 * Time: 5:10 PM
 * To change this template use File | Settings | File Templates.
 */

// create our menu form MODEL
App.Models.KmlMenuModel = Backbone.Model.extend({

    defaults: {
        map: '',
        preset : ''
    },

    urlRoot: 'api.php?request=get_kml',

    schema: {
        map      :   {type : 'Select', title: 'Origin', placeholder: "Select All", options: "", buttonWidth: '315px', editorClass : 'form-control buttonWidth', fieldClass: 'form-input'},
        preset   :   {type : 'Select', title: 'Segment', placeholder: "Select All", options: "", buttonWidth: '315px', editorClass : 'form-control buttonWidth', fieldClass: 'form-input'}
    },

    initialize: function(options)
    {
        this.options = options;
        var isCleared = false;

        // let's load the maps
        for(var i in window.kmldata)
        {
            var item = window.kmldata[i],
                html = item.name !== '' ? item.name : 'None/Catch-all',
                selected = item.selected ? "selected='selected'" : '',
                option = "<option value='"+ item.name +"' "+ selected +">"+ html +"</option>";

            this.schema[item.id].options += option;

            if(item.hasOwnProperty('children') && item.selected)
            {
                for(var j in item.children)
                {
                    var child = item.children[j],
                        childhtml = child.name !== '' ? child.name : 'None/Catch-all',
                        childselected = child.selected ? "selected='selected'" : '',
                        childoption = "<option value='"+ child.name +"' "+ childselected +">"+ childhtml +"</option>";

                    this.schema[child.id].options += childoption;
                }
            }
        }
    },

    getPresetForMap : function(map)
    {
        var options = '';

        // let's load the maps
        for(var i in window.kmldata)
        {
            var item = window.kmldata[i];

            if(item.name === map)
            {
                if(item.hasOwnProperty('children'))
                {
                    for(var j in item.children)
                    {
                        var child = item.children[j],
                            childhtml = child.name !== '' ? child.name : 'None/Catch-all',
                            childselected = child.selected ? "selected='selected'" : '',
                            childoption = "<option value='"+ child.name +"' "+ childselected +">"+ childhtml +"</option>";

                        options += childoption;
                    }
                }
            }
        }

        return options;
    },

    fetch : function(options)
    {
        // perform the same functions as the parent
        this.constructor.__super__.fetch.apply(this, arguments);
    }

});