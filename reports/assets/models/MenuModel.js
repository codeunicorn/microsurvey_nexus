/**
 * Created with JetBrains PhpStorm.
 * User: jsantos317
 * Date: 5/24/14
 * Time: 5:10 PM
 * To change this template use File | Settings | File Templates.
 */

// create our menu form MODEL
App.Models.MenuModel = Backbone.Model.extend({

    data: {},

    defaults: {
        date : '',
        origin: '',
        formId: '',
        owner : '',
        seg : '',
        div : '',
        map: '',
        region: '',
        country: '',
        state: '',
        preset : '',
        channel : '',
        status : ''
    },

    urlRoot: 'api.php?request=get_data',

    schema: {
        date        :   {type : 'DateRangePicker', title : 'Date', editorClass : 'form-control', fieldClass: 'form-input'},
        origin      :   {type : 'Multiselect', editorAttrs : {'multiple' : 'multiple'}, title: 'Origin', placeholder: "Select All", buttonWidth: '315px', editorClass : 'form-control', fieldClass: 'form-input'},
        formId      :   {type : 'Multiselect', editorAttrs : {'multiple' : 'multiple'}, title: 'Form/ID', placeholder: "Select All", buttonWidth: '315px', editorClass : 'form-control', fieldClass: 'form-input'},
        owner       :   {type : 'Multiselect', editorAttrs : {'multiple' : 'multiple'}, title: 'Owner', placeholder: "Select All", buttonWidth: '315px', editorClass : 'form-control', fieldClass: 'form-input'},
        seg         :   {type : 'Multiselect', editorAttrs : {'multiple' : 'multiple'}, title: 'Primary Market Segment', placeholder: "Select All", buttonWidth: '315px', editorClass : 'form-control', fieldClass: 'form-input'},
        div         :   {type : 'Multiselect', editorAttrs : {'multiple' : 'multiple'}, title: 'Division Override', placeholder: "Select All", buttonWidth: '315px', editorClass : 'form-control', fieldClass: 'form-input'},
        preset      :   {type : 'Multiselect', editorAttrs : {'multiple' : 'multiple'}, title: 'Segment', placeholder: "Select All", buttonWidth: '315px', editorClass : 'form-control', fieldClass: 'form-input'},
        map         :   {type : 'Select', title: 'Map', placeholder: "Select All", buttonWidth: '315px', editorClass : 'form-control', fieldClass: 'form-input'},
        region      :   {type : 'Select', title: 'Region', placeholder: "Select All", buttonWidth: '315px', editorClass : 'form-control', fieldClass: 'form-input'},
        country     :   {type : 'Select', title: 'Country', placeholder: "Select All", buttonWidth: '315px', editorClass : 'form-control', fieldClass: 'form-input'},
        state       :   {type : 'Multiselect', editorAttrs : {'multiple' : 'multiple'}, title: 'State', placeholder: "Select All", buttonWidth: '315px', editorClass : 'form-control', fieldClass: 'form-input'},
        channel     :   {type : 'Multiselect', editorAttrs : {'multiple' : 'multiple'}, title: 'Channel Tactic', placeholder: "Select All", buttonWidth: '315px', editorClass : 'form-control', fieldClass: 'form-input'},
        status      :   {type : 'Multiselect', editorAttrs : {'multiple' : 'multiple'}, title: 'Status', placeholder: "Select All", buttonWidth: '315px', editorClass : 'form-control', fieldClass: 'form-input'}
    },

    initialize: function(options)
    {
        this.initSchemaForms();
        this.initSchemaOrigins();
        this.initSchemaOwners();
        this.initSchemaSeg();
        this.initSchemaDiv();
        this.initSchemaPreset();
        this.initSchemaMap();
        this.initSchemaRegion();
        this.initSchemaCountry();
        this.initSchemaState();
        this.initSchemaChannel();
        this.initSchemaStatus();
        this.options = options;
    },

    initSchemaOrigins : function()
    {
        for(var n in window.reportsVariables.origins)
        {
            var value = window.reportsVariables.origins[n]['eTrigue Value'];
            var text = window.reportsVariables.origins[n]['Element'];

            if(n !== 'defaultValue')
            {
                this.schema.origin.options += '<option value="'+ value +'">'+ text +'</option>';
            }

            else
            {
                this.set('origin', window.reportsVariables.origins.defaultValue);
            }

        }
    },

    initSchemaForms : function()
    {
        for(var n in window.reportsVariables.forms)
        {
            var value = window.reportsVariables.forms[n]['Form ID'];
            var text = window.reportsVariables.forms[n]['Label'] + ' / ' + value;

            this.schema.formId.options += '<option value="'+ value +'">'+ text +'</option>';
        }
    },

    initSchemaOwners : function()
    {
        for(var n in window.reportsVariables.owners)
        {
            var value = window.reportsVariables.owners[n]['Short-Code'];
            var text = window.reportsVariables.owners[n]['Short-Code'];

            this.schema.owner.options += '<option value="'+ value +'">'+ text +'</option>';
        }
    },

    initSchemaSeg : function()
    {
        for(var n in window.reportsVariables.segments)
        {
            var value = window.reportsVariables.segments[n]['eTrigue Value'];
            var text = window.reportsVariables.segments[n]['Element'];

            this.schema.seg.options += '<option value="'+ value +'">'+ text +'</option>';
        }
    },

    initSchemaDiv : function()
    {
        for(var n in window.reportsVariables.divisions)
        {
            var value = window.reportsVariables.divisions[n]['eTrigue Value'];
            var text = window.reportsVariables.divisions[n]['Element'];

            this.schema.div.options += '<option value="'+ value +'">'+ text +'</option>';
        }
    },

    initSchemaPreset : function()
    {
        for(var presetName in window.reportsVariables.presets)
        {
            var value = '';
            var presetArray = window.reportsVariables.presets[presetName];

            for(var type in presetArray)
            {
                value += type + '=' + presetArray[type].join('||') + '&';
            }

            value = value.slice(0, - 1);

            this.schema.preset.options += '<option value="'+ value +'">'+ presetName +'</option>';
        }
    },

    initSchemaMap : function() {

        this.schema.map.options += '<option value="">Select All</option>';

        for(var n in window.reportsVariables.countriesAndStates)
        {
            this.schema.map.options += '<option value="'+ n +'">'+ n +'</option>';
        }
    },

    initSchemaRegion : function() {

        this.schema.region.options += '<option value="">Select All</option>';

        var regionArray = [];
        var regionExists = false;

        for(var n in window.reportsVariables.countriesAndStates)
        {
            var cs = window.reportsVariables.countriesAndStates[n],
                salesRegion = typeof cs['Sales Region'] !== 'undefined' ? cs['Sales Region'] : '';

            for(var i = 0, len = regionArray.length; i < len; i++) {
                if(regionArray[i] === salesRegion) {
                    regionExists = true;
                }
            }

            if(!regionExists && salesRegion !== '') {
                regionArray.push(salesRegion);
                this.schema.region.options += '<option value="'+ salesRegion +'">'+ salesRegion +'</option>';
            }
        }
    },

    initSchemaCountry : function() {

        this.schema.country.options += '<option value="">Select All</option>';

        for(var n in window.reportsVariables.countriesAndStates)
        {
            var cs = window.reportsVariables.countriesAndStates[n];

            if(cs['Level'] == 0) {
                var value = cs['Country/Region/Sub-Region'];
                var text = cs['Country/Region/Sub-Region'];

                this.schema.country.options += '<option value="'+ value +'">'+ text +'</option>';
            }

        }
    },

    initSchemaState : function() {

        this.schema.state.options += '<option value="">Select All</option>';

    },

    initSchemaChannel : function()
    {
        for(var n in window.reportsVariables.channels)
        {
            var value = window.reportsVariables.channels[n]['eTrigue Value'];
            var text = window.reportsVariables.channels[n]['Label'];

            this.schema.channel.options += '<option value="'+ value +'">'+ text +'</option>';
        }
    },

    initSchemaStatus : function()
    {
        this.schema.status.options += '<option value="0">Pending</option>' +
            '<option value="1">Accepted</option>' +
            '<option value="2">Rejected</option>' +
            '<option value="3">Rejected - Duplicate</option>' +
            '<option value="4">Rejected - Reassign</option>' +
            '<option value="5">Rejected - Garbage</option>' +
            '<option value="6">Rejected - Dealer/Partner/Staff</option>' +
            '<option value="7">Utility</option>';
    },

    getDivisionsForSegment : function(segValue)
    {
        var newOptions = [];

        if(segValue == null)
        {
            for(var n in window.reportsVariables.divisions)
            {
                var value = window.reportsVariables.divisions[n]['eTrigue Value'];
                var text = window.reportsVariables.divisions[n]['Element'];

                newOptions.push('<option value="'+ value +'">'+ text +'</option>');
            }
        }
        else
        {
            for(var k in window.reportsVariables.segments)
            {
                for(var m in segValue)
                {
                    var sValue = segValue[m];

                    if(window.reportsVariables.segments[k]['eTrigue Value'] == sValue)
                    {
                        var divArray = window.reportsVariables.segments[k]['div'];

                        for(var m in divArray)
                        {
                            var divText = divArray[m];

                            for(var n in window.reportsVariables.divisions)
                            {
                                var value = window.reportsVariables.divisions[n]['eTrigue Value'];
                                var text = window.reportsVariables.divisions[n]['Element'];

                                if(divText == text)
                                {
                                    var myOption = '<option value="'+ value +'">'+ text +'</option>';
                                    var doesOptionExist = false;
                                    for(var o in newOptions)
                                    {
                                        if(newOptions[o] == myOption)
                                        {
                                            doesOptionExist = true;
                                        }
                                    }

                                    if(! doesOptionExist)
                                    {
                                        newOptions.push(myOption);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        var returnOptions = newOptions.join();

        return returnOptions;
    },

    getRegionForMap : function(map) {

        // array that holds our new region options
        var newOptions = ['<option value="">Select All</option>'],
            regionArray = [];

        for(var m in window.reportsVariables.countriesAndStates) {

            if (m == map) {

                var myMap = window.reportsVariables.countriesAndStates[m],
                    regionExists = false;

                for (var n in myMap) {

                    // our country or state array
                    var cs = myMap[n],

                        // our region
                        salesRegion = typeof cs['Sales Region'] !== 'undefined' ? cs['Sales Region'] : '';

                    for(var i = 0, len = regionArray.length; i < len; i++) {

                        if(salesRegion === regionArray[i]) {
                            regionExists = true;
                        }
                    }


                    if (!regionExists && salesRegion !== '') {
                        regionArray.push(salesRegion);
                        newOptions.push('<option value="' + salesRegion + '">' + salesRegion + '</option>');
                    }
                }
            }
        }

        var returnOptions = newOptions.join();
        return returnOptions;
    },

    getCountriesForMap : function(map) {

        // array that holds our new country options
        var newOptions = ['<option value="">Select All</option>'];

        for(var m in window.reportsVariables.countriesAndStates) {

            if(m == map) {

                var myMap = window.reportsVariables.countriesAndStates[m];

                for(var n in myMap) {

                    // our country or state array
                    var countryState = myMap[n],

                    // our country or state level (0 = country, 1 = state)
                        level = countryState['Level'],

                    // our country or state
                        country = countryState['Country/Region/Sub-Region'];

                    if(level == "0" && country !== null && country.substr(0,3) !== '---') {

                        newOptions.push('<option value="'+ country +'">'+ country +'</option>');
                    }
                }
            }
        }

        var returnOptions = newOptions.join();
        return returnOptions;

    },

    getCountriesForRegion : function(map, region) {

        // array that holds our new country options
        var newOptions = ['<option value="">Select All</option>'];

        for(var m in window.reportsVariables.countriesAndStates) {

            if(m == map) {

                var myMap = window.reportsVariables.countriesAndStates[m];

                for(var n in myMap) {

                    // our country or state array
                    var countryState = myMap[n],

                    // our region
                        salesRegion = typeof countryState['Sales Region'] !== 'undefined' ? countryState['Sales Region'] : '',

                    // our country or state level (0 = country, 1 = state)
                        level = countryState['Level'],

                    // our country or state
                        country = countryState['Country/Region/Sub-Region'];

                    if(level == "0" && region !== null && country !== null && country.substr(0,3) !== '---' && salesRegion === region) {

                        newOptions.push('<option value="'+ country +'">'+ country +'</option>');
                    }
                }
            }
        }

        var returnOptions = newOptions.join();
        return returnOptions;

    },

    getStatesForCountry : function(map, country) {

        // array that holds our new state options
        var newOptions = ['<option value="">Select All</option>'];

        //flag if we've found our country
        var foundCountry = false;

        for(var m in window.reportsVariables.countriesAndStates) {

            if(m == map) {

                var myMap = window.reportsVariables.countriesAndStates[m];

                for (var n in myMap) {

                    // our country or state array
                    var countryState = myMap[n],

                    // our country or state level (0 = country, 1 = state)
                        level = countryState['Level'],

                    // our country or state
                        region = countryState['Country/Region/Sub-Region'],

                    // code
                        code = countryState['Code'];

                    // if we find a country and we've already found our country, exit the loop
                    if (countryState['Level'] == 0 && foundCountry) {
                        break;
                    }

                    // if we find a country and the country matches
                    else if (level == 0 && region == country) {

                        // set our flag
                        foundCountry = true;
                    }

                    // else if we find a state and we've already found our country,
                    // this state must be under our country (or else we would have exited the
                    // loop from the first if-else check
                    else if (level == 1 && foundCountry && region !== null) {

                        newOptions.push('<option value="' + code + '">' + region + '</option>');
                    }

                }
            }
        }

        var returnOptions = newOptions.join();
        return returnOptions;
    },

    fetch : function(options)
    {
        // additional function to remove the data from the attributes
        if(this.get("data"))
        {
            this.unset("data", { silent: true });
            this.unset("columns", { silent: true });
            this.unset("recordsTotal", { silent: true });
            this.unset("opportunities_count", { silent: true });
            this.unset("total_effective_value", { silent: true });
        }

        // perform the same functions as the parent
        this.constructor.__super__.fetch.apply(this, arguments);
    }

});