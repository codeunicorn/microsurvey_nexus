/**
 * Created with JetBrains PhpStorm.
 * User: jsantos317
 * Date: 5/24/14
 * Time: 5:10 PM
 * To change this template use File | Settings | File Templates.
 */

// create our menu form MODEL
App.Models.ImportMenu = Backbone.Model.extend({

    defaults: {
        origin: '',
        formId: '',
        preset : '',
        ct: ''
    },

    urlRoot: 'api.php?request=commit',

    schema: {
        owner       :   {type : 'Multiselect', title: 'Owner', placeholder: "Select All", buttonWidth: '315px', editorClass : 'form-control', fieldClass: 'form-input', editorAttrs : {}},
        formId      :   {type : 'Multiselect', title: 'Form/ID', placeholder: "Select All", buttonWidth: '315px', editorClass : 'form-control', fieldClass: 'form-input', editorAttrs : {}},
        preset      :   {type : 'Multiselect', title: 'Segment', placeholder: "Select All", buttonWidth: '315px', editorClass : 'form-control', fieldClass: 'form-input', editorAttrs : {}},
        ct          :   {type : 'Multiselect', title: 'Channel Tactic', placeholder: "Select All", buttonWidth: '315px', editorClass : 'form-control', fieldClass: 'form-input', editorAttrs : {}}
    },

    initialize: function(options)
    {
        this.initSchemaOwners();
        this.initSchemaForms();
        this.initSchemaPreset();
        this.initSchemaChannel();

        this.options = options;
    },

    initSchemaOwners : function()
    {
        this.schema.owner.options = '<option value="" disabled>Please select an option</option>';

        for(var n in window.reportsVariables.owners)
        {
            var value = window.reportsVariables.owners[n]['eTrigue Value'];
            var text = window.reportsVariables.owners[n]['Short-Code'];

            this.schema.owner.options += '<option value="'+ value +'">'+ text +'</option>';
        }
    },

    initSchemaChannel : function()
    {
        this.schema.ct.options = '<option value="" disabled>Please select an option</option>';

        for(var n in window.reportsVariables.channels)
        {
            var value = window.reportsVariables.channels[n]['eTrigue Value'];
            var text = window.reportsVariables.channels[n]['Label'];

            this.schema.ct.options += '<option value="'+ value +'">'+ text +'</option>';
        }
    },

    initSchemaOrigins : function()
    {
        this.schema.origin.options = '<option value="" disabled>Please select an option</option>';

        for(var n in window.reportsVariables.origins)
        {
            var value = window.reportsVariables.origins[n]['eTrigue Value'];
            var text = window.reportsVariables.origins[n]['Element'];

            if(n !== 'defaultValue')
            {
                this.schema.origin.options += '<option value="'+ value +'">'+ text +'</option>';
            }

            else
            {
                this.set('origin', window.reportsVariables.origins.defaultValue);
            }

        }
    },

    initSchemaForms : function()
    {
        this.schema.formId.options = '<option value="" disabled>Please select an option</option>';

        for(var n in window.reportsVariables.forms)
        {
            var value = window.reportsVariables.forms[n]['Form ID'];
            var text = window.reportsVariables.forms[n]['Label'] + ' / ' + value;

            this.schema.formId.options += '<option value="'+ value +'">'+ text +'</option>';
        }
    },

    initSchemaSeg : function()
    {
        this.schema.seg.options = '<option value="" disabled>Please select an option</option>';

        for(var n in window.reportsVariables.segments)
        {
            var value = window.reportsVariables.segments[n]['eTrigue Value'];
            var text = window.reportsVariables.segments[n]['Element'];

            this.schema.seg.options += '<option value="'+ value +'">'+ text +'</option>';
        }
    },

    initSchemaDiv : function()
    {
        this.schema.div.options = '<option value="" disabled>Please select an option</option>';

        for(var n in window.reportsVariables.divisions)
        {
            var value = window.reportsVariables.divisions[n]['eTrigue Value'];
            var text = window.reportsVariables.divisions[n]['Element'];

            this.schema.div.options += '<option value="'+ value +'">'+ text +'</option>';
        }
    },

    initSchemaPreset : function()
    {
        this.schema.preset.options = '<option value="" disabled>Please select an option</option>';

        for(var presetName in window.reportsVariables.presets)
        {
            this.schema.preset.options += '<option value="'+ presetName +'">'+ presetName +'</option>';
        }
    },

    getDivisionsForSegment : function(segValue)
    {
        var newOptions = [];

        if(segValue === null)
        {
            for(var n in window.reportsVariables.divisions)
            {
                var value = window.reportsVariables.divisions[n]['eTrigue Value'];
                var text = window.reportsVariables.divisions[n]['Element'];

                newOptions.push('<option value="'+ value +'">'+ text +'</option>');
            }
        }
        else
        {
            for(var k in window.reportsVariables.segments)
            {
                for(var m in segValue)
                {
                    var sValue = segValue[m];

                    if(window.reportsVariables.segments[k]['eTrigue Value'] === sValue)
                    {
                        var divArray = window.reportsVariables.segments[k]['div'];

                        for(var m in divArray)
                        {
                            var divText = divArray[m];

                            for(var n in window.reportsVariables.divisions)
                            {
                                var value = window.reportsVariables.divisions[n]['eTrigue Value'];
                                var text = window.reportsVariables.divisions[n]['Element'];

                                if(divText == text)
                                {
                                    var myOption = '<option value="'+ value +'">'+ text +'</option>';
                                    var doesOptionExist = false;
                                    for(var o in newOptions)
                                    {
                                        if(newOptions[o] == myOption)
                                        {
                                            doesOptionExist = true;
                                        }
                                    }

                                    if(! doesOptionExist)
                                    {
                                        newOptions.push(myOption);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        var returnOptions = newOptions.join();

        return returnOptions;
    },

    fetch : function(options)
    {
        // additional function to remove the data from the attributes
        if(this.get("data"))
        {
            this.unset("data", { silent: true });
        }

        // perform the same functions as the parent
        this.constructor.__super__.fetch.apply(this, arguments);
    }

});