/**
 * Created by jsantos317 on 7/2/14.
 */
App.Models.HighChartsModel = Backbone.Model.extend({

    urlRoot: 'api.php?request=get_highcharts_data',

    defaults : {
        type : 'Line'
    },


    initialize : function(options)
    {
        if(typeof this['initConfig' + this.attributes.type] === 'function') {
            this['initConfig' + this.attributes.type](options);
        }

        return this;
    },

    initConfigStackedAreaBySegmentDashboard : function(options) {

        this.config = {

            type: 'area',

            chart: {
                type: 'area',
                width: ($(window).width() / 2) - 60,
                reflow: false
            },
            title: {
                text: 'Sales-ready Leads by Segment'
            },
            subtitle: {
                text: 'Last 30 days'
            },
            xAxis: {
                categories: ['30', '25', '20', '15', '10', '5', 'Today'],
                tickmarkPlacement: 'on',
                title: {
                    text: 'Days Ago'
                }
            },
            yAxis: {
                title: {
                    text: 'Count'
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            legend : {
                itemStyle : {"color": "#333333", "cursor": "pointer", "fontSize": "12px", "fontWeight": "normal"}
            },
            tooltip: {
                shared: true,
                valueSuffix: ''
            },
            plotOptions: {
                area: {
                    stacking: 'normal',
                    lineColor: '#666666',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#666666'
                    }
                }
            },
            series: [{
                name: 'Asia',
                data: [502, 635, 809, 947, 1402, 3634, 5268]
            }, {
                name: 'Africa',
                data: [106, 107, 111, 133, 221, 767, 1766]
            }, {
                name: 'Europe',
                data: [163, 203, 276, 408, 547, 729, 628]
            }, {
                name: 'America',
                data: [18, 31, 54, 156, 339, 818, 1201]
            }, {
                name: 'Oceania',
                data: [2, 2, 2, 6, 13, 30, 46]
            }]
        };
    },

    initConfigLineDashboard : function(options){
        this.config = {
            chart: {
                width: ($(window).width() / 2) - 60,
                reflow: false
            },
            title: {
                text: 'Monthly Average Temperature',
                x: -20 //center
            },
            subtitle: {
                text: 'Source: WorldClimate.com',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Temperature (°C)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: '°C'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0,
                itemStyle : {"color": "#333333", "cursor": "pointer", "fontSize": "12px", "fontWeight": "normal"}
            },
            series: [{
                name: 'Tokyo',
                data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
            }, {
                name: 'New York',
                data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
            }, {
                name: 'Berlin',
                data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
            }, {
                name: 'London',
                data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
            }]
        }
    },

    initConfigDonutChartByCategoryDashboard : function(options) {

        var colors = Highcharts.getOptions().colors,
            categories = ['MSIE', 'Firefox', 'Chrome', 'Safari', 'Opera'],
            name = 'Browser brands',
            data = [{
                y: 55.11,
                color: colors[0],
                drilldown: {
                    name: 'MSIE versions',
                    categories: ['MSIE 6.0', 'MSIE 7.0', 'MSIE 8.0', 'MSIE 9.0'],
                    data: [10.85, 7.35, 33.06, 2.81],
                    color: colors[0]
                }
            }, {
                y: 21.63,
                color: colors[1],
                drilldown: {
                    name: 'Firefox versions',
                    categories: ['Firefox 2.0', 'Firefox 3.0', 'Firefox 3.5', 'Firefox 3.6', 'Firefox 4.0'],
                    data: [0.20, 0.83, 1.58, 13.12, 5.43],
                    color: colors[1]
                }
            }, {
                y: 11.94,
                color: colors[2],
                drilldown: {
                    name: 'Chrome versions',
                    categories: ['Chrome 5.0', 'Chrome 6.0', 'Chrome 7.0', 'Chrome 8.0', 'Chrome 9.0',
                        'Chrome 10.0', 'Chrome 11.0', 'Chrome 12.0'],
                    data: [0.12, 0.19, 0.12, 0.36, 0.32, 9.91, 0.50, 0.22],
                    color: colors[2]
                }
            }, {
                y: 7.15,
                color: colors[3],
                drilldown: {
                    name: 'Safari versions',
                    categories: ['Safari 5.0', 'Safari 4.0', 'Safari Win 5.0', 'Safari 4.1', 'Safari/Maxthon',
                        'Safari 3.1', 'Safari 4.1'],
                    data: [4.55, 1.42, 0.23, 0.21, 0.20, 0.19, 0.14],
                    color: colors[3]
                }
            }, {
                y: 2.14,
                color: colors[4],
                drilldown: {
                    name: 'Opera versions',
                    categories: ['Opera 9.x', 'Opera 10.x', 'Opera 11.x'],
                    data: [ 0.12, 0.37, 1.65],
                    color: colors[4]
                }
            }];


        // Build the data arrays
        var browserData = [];
        var versionsData = [];
        for (var i = 0; i < data.length; i++) {

            // add browser data
            browserData.push({
                name: categories[i],
                y: data[i].y,
                color: data[i].color
            });

            // add version data
            for (var j = 0; j < data[i].drilldown.data.length; j++) {
                var brightness = 0.2 - (j / data[i].drilldown.data.length) / 5 ;
                versionsData.push({
                    name: data[i].drilldown.categories[j],
                    y: data[i].drilldown.data[j],
                    color: Highcharts.Color(data[i].color).brighten(brightness).get()
                });
            }
        }

        // Create the chart
        this.config = {
            chart: {
                type: 'pie',
                width: ($(window).width() / 2) - 60,
                reflow: false
            },
            title: {
                text: 'Submit Count by Category'
            },
            subtitle: {
                text: 'Last 90 days',
                x: -20
            },
            yAxis: {
                title: {
                    text: 'Total form counts'
                }
            },
            plotOptions: {
                pie: {
                    shadow: false,
                    center: ['50%', '50%']
                }
            },
            tooltip: {
                valueSuffix: ''
            },
            series: [{
                name:  'Count',
                data: browserData,
                size: '60%',
                showInLegend: true,
                dataLabels: {
                    formatter: function() {
                        return this.y > 1 ? this.point.name +':</b> '+ this.y : null;
                    },
                    distance: 20
                }
            }/*, {
                name: 'Count',
                data: versionsData,
                size: '80%',
                innerSize: '60%',
                dataLabels: {
                    formatter: function() {
                        // display only if larger than 1
                        return this.y > 1 ? '<b>'+ this.point.name +':</b> '+ this.y : null;
                    }
                }
            }*/
            ]
        };
    },

    initConfigBarChartByCategoryDashboard : function() {

        this.config = {
            chart: {
                type: 'column',
                width: ($(window).width() / 2) - 60,
                reflow: false
            },
            title: {
                text: 'Sales-ready Leads by Channel'
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total Leads Count'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                layout: 'vertical',
                verticalAlign: 'middle',
                itemMarginTop: 10,
                itemMarginBottom: 10,
                margin: 10,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false,
                itemStyle : {"color": "#333333", "cursor": "pointer", "fontSize": "12px", "fontWeight": "normal"}
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.x +'</b><br/>'+
                        this.series.name +': '+ this.y +'<br/>'+
                        'Total: '+ this.point.stackTotal;
                }
            },

            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        formatter: function() {
                            return this.y > 5 ? this.y : null;
                        },
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                        style: {
                            textShadow: '0 0 3px black, 0 0 3px black'
                        }
                    }
                }
            },
            series: [{
                name: 'John',
                data: [5, 3, 4, 7, 2, 5, 3, 4, 7, 2, 7, 2]
            }, {
                name: 'Jane',
                data: [2, 2, 3, 2, 1, 2, 2, 3, 2, 1, 2, 1]
            }, {
                name: 'Joe',
                data: [3, 4, 4, 2, 5, 3, 4, 4, 2, 5, 3, 2]
            }]
        };

    },

    initConfigHeatMapDashboard : function(options) {

        this.config = {

            chart: {
                type: 'heatmap',
                width: ($(window).width() / 2) - 60,
                reflow: false
            },


            title: {
                text: 'Submit Count by Origin'
            },

            xAxis: {
                categories: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
            },

            yAxis: {
                categories: ['07/01', '07/07', '07/14', '07/28'],
                title: null
            },

            colorAxis: {
                min: 0,
                minColor: '#FFFFFF',
                maxColor: Highcharts.getOptions().colors[0]
            },

            legend: {
                align: 'right',
                layout: 'vertical',
                margin: 0,
                verticalAlign: 'top',
                y: 25,
                symbolHeight: 320
            },

            tooltip: {
                formatter: function () {
                    var dateArray = this.series.yAxis.categories[this.point.y].split('/');
                    var month = dateArray[0];
                    var date = parseInt(dateArray[1]) + this.point.x;
                    var newDate = month + '/' + date;
                    return '<b>' + this.point.value + '</b> form fills on <br><b>'+ this.series.xAxis.categories[this.point.x] + ', ' + newDate + '</b>';
                }
            },

            series: [{
                name: 'Sales per employee',
                borderWidth: 1,
                data: [[0,0,10],[0,1,19],[0,2,8],[0,3,24],[0,4,67],[1,0,92],[1,1,58],[1,2,78],[1,3,117],[1,4,48],[2,0,35],[2,1,15],[2,2,123],[2,3,64],[2,4,52],[3,0,72],[3,1,132],[3,2,114],[3,3,19],[3,4,16],[4,0,38],[4,1,5],[4,2,8],[4,3,117],[4,4,115],[5,0,88],[5,1,32],[5,2,12],[5,3,6],[5,4,120],[6,0,13],[6,1,44],[6,2,88],[6,3,98],[6,4,96],[7,0,31],[7,1,1],[7,2,82],[7,3,32],[7,4,30],[8,0,85],[8,1,97],[8,2,123],[8,3,64],[8,4,84],[9,0,47],[9,1,114],[9,2,31],[9,3,48],[9,4,91]],
                dataLabels: {
                    enabled: true,
                    color: 'black',
                    style: {
                        textShadow: 'none',
                        HcTextStroke: null
                    }
                }
            }]
        }
    },

    initConfigHighMapByStateDashboard : function(options)
    {
        var self = this;

        this.config = {

            chart : {
                width: ($(window).width() / 2) - 60,
                reflow: false
            },

            title : {
                text : 'Sales-ready Leads by State'
            },

            subtitle: {
                text: 'Last 90 days',
                x: -20
            },

            legend: {
                align: 'right',
                layout: 'vertical',
                margin: 0,
                verticalAlign: 'top',
                y: 25,
                symbolHeight: 320
            },

            mapNavigation: {
                enabled: true
            },

            colorAxis: {
                min: 0,
                minColor: '#EEEEFF',
                maxColor: '#000022',
                stops: [
                    [0, '#EFEFFF'],
                    [0.67, '#4444FF'],
                    [1, '#000022']
                ]
            },

            series : [{
                animation: {
                    duration: 1000
                },
                data : [],
                mapData: Highcharts.maps['countries/us/us-all'],
                joinBy: ['postal-code', 'code'],
                dataLabels: {
                    enabled: true,
                    color: 'white',
                    format: '{point.code}'
                },
                name: 'Submit Count',
                tooltip: {
                    pointFormat: '{point.code}: {point.value}'
                }
            }]
        }

    },

    initConfigHighMapByRegionDashboard : function(options)
    {
        var self = this;

        this.config = {

            chart : {
                width: ($(window).width() / 2) - 60,
                reflow: false
            },

            title : {
                text : 'Sales-ready Leads by Region'
            },

            subtitle: {
                text: 'Last 90 days',
                x: -20
            },

            legend: {
                align: 'right',
                layout: 'vertical',
                margin: 0,
                verticalAlign: 'top',
                y: 25,
                symbolHeight: 320
            },

            mapNavigation: {
                enabled: true
            },

            colorAxis: {
                min: 0,
                minColor: '#EEEEFF',
                maxColor: '#000022',
                stops: [
                    [0, '#EFEFFF'],
                    [0.67, '#4444FF'],
                    [1, '#000022']
                ]
            },

            series : [{
                animation: {
                    duration: 1000
                },
                data : [],
                mapData: Highcharts.maps['custom/north-america'],
                joinBy: ['name', 'code'],
                dataLabels: {
                    enabled: true,
                    color: 'white',
                    format: '{point.code}'
                },
                name: 'Submit Count',
                tooltip: {
                    pointFormat: '{point.code}: {point.value}'
                }
            }]
        }

    },

    initConfigPieChartByEffectiveValueDashboard : function(options) {

        var self = this;

        var colors = Highcharts.getOptions().colors,
            categories = ['MSIE', 'Firefox', 'Chrome', 'Safari', 'Opera'],
            name = 'Browser brands',
            data = [{
                y: 55.11,
                color: colors[0],
                drilldown: {
                    name: 'MSIE versions',
                    categories: ['MSIE 6.0', 'MSIE 7.0', 'MSIE 8.0', 'MSIE 9.0'],
                    data: [10.85, 7.35, 33.06, 2.81],
                    color: colors[0]
                }
            }, {
                y: 21.63,
                color: colors[1],
                drilldown: {
                    name: 'Firefox versions',
                    categories: ['Firefox 2.0', 'Firefox 3.0', 'Firefox 3.5', 'Firefox 3.6', 'Firefox 4.0'],
                    data: [0.20, 0.83, 1.58, 13.12, 5.43],
                    color: colors[1]
                }
            }, {
                y: 11.94,
                color: colors[2],
                drilldown: {
                    name: 'Chrome versions',
                    categories: ['Chrome 5.0', 'Chrome 6.0', 'Chrome 7.0', 'Chrome 8.0', 'Chrome 9.0',
                        'Chrome 10.0', 'Chrome 11.0', 'Chrome 12.0'],
                    data: [0.12, 0.19, 0.12, 0.36, 0.32, 9.91, 0.50, 0.22],
                    color: colors[2]
                }
            }, {
                y: 7.15,
                color: colors[3],
                drilldown: {
                    name: 'Safari versions',
                    categories: ['Safari 5.0', 'Safari 4.0', 'Safari Win 5.0', 'Safari 4.1', 'Safari/Maxthon',
                        'Safari 3.1', 'Safari 4.1'],
                    data: [4.55, 1.42, 0.23, 0.21, 0.20, 0.19, 0.14],
                    color: colors[3]
                }
            }, {
                y: 2.14,
                color: colors[4],
                drilldown: {
                    name: 'Opera versions',
                    categories: ['Opera 9.x', 'Opera 10.x', 'Opera 11.x'],
                    data: [ 0.12, 0.37, 1.65],
                    color: colors[4]
                }
            }];


        // Build the data arrays
        var browserData = [];
        var versionsData = [];
        for (var i = 0; i < data.length; i++) {

            // add browser data
            browserData.push({
                name: categories[i],
                y: data[i].y,
                color: data[i].color,
                drilldown: categories[i]
            });

            // add version data
            var mydata = [];
            for (var j = 0; j < data[i].drilldown.data.length; j++) {
                mydata.push([data[i].drilldown.categories[j], data[i].drilldown.data[j]]);
            }

            versionsData.push({
                name: categories[i],
                id: categories[i],
                data: mydata
            });
        }

        this.config = {

            chart: {
                type: 'pie',
                width: ($(window).width() / 2) - 60
            },
            title: {
                text: 'Effective Value Shares by Channel Tactic'
            },
            subtitle: {
                text: 'Last 180 Days. (Click the slices to drill down)'
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: {point.y:.2f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name} - Effective Value</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b><br/>'
            },

            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: browserData
            }],
            drilldown: {
                series: versionsData
            }
        };

    },

    fetch : function(options)
    {
        // perform the same functions as the parent
        this.constructor.__super__.fetch.apply(this, arguments);
    },

    setConfig : function(name, value)
    {
        this.config[name] = value;
    },

    setSeriesData : function(dataObj)
    {
        for(var n in dataObj)
        {
            if(! isNaN(n))
            {
                this.config.series[n].data = dataObj[n];
            }
        }
    }


});