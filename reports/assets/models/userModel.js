/**
 * Created with JetBrains PhpStorm.
 * User: jsantos317
 * Date: 5/24/14
 * Time: 5:10 PM
 * To change this template use File | Settings | File Templates.
 */

// create our menu form MODEL
App.Models.UserModel = Backbone.Model.extend({

    defaults: {
        username    : '',
        password    : ''
    },

    urlRoot: 'api.php?request=auth_user',

    schema: {
        username          :   {type : 'Text', title: 'username', fieldClass : 'user1', editorClass : 'form-control'},
        password          :   {type : 'Password', title: 'password', fieldClass : 'pass1', editorClass : 'form-control'}
    },

    initialize: function()
    {

    },

    fetch : function(options)
    {
        // additional function to remove the data from the attributes
        if(this.get("result") || this.attributes.result != undefined)
        {
            this.unset("result", { silent: true });
        }

        // perform the same functions as the parent
        this.constructor.__super__.fetch.apply(this, arguments);
    }

});