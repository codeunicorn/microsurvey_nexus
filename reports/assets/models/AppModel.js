App = {
    Models: {},
    Collections: {},
    Views: {},
    notifications: 0,
    originChanged: false
};
App.start = function()
{
    if(isLoggedIn)
    {
        this.Models.user.set('username', username);
        this.Views.loginForm.container.hide();
        this.preInit();
        this.init();
        this.postInit();
    }
    else
    {
        // else we render our login view
        this.Views.loginForm.render();
    }
};

App.preInit = function()
{
    $('#logged_in').hide();
    $('#dataTablesContainer').hide();
    $('#kmlMapContainer').hide();

    // adjust the height of our containers
    var $content = $('#contentContainer');

    var $window = $(window).on('resize', function(){

        var availableHeight = $(this).height();
        $content.css('min-height', availableHeight - 85);

        App.Views.highchartsView.resize();
        App.Views.highchartsView2.resize();
        App.Views.highchartsView3.resize();
        App.Views.highchargsView4.resize();

    }).trigger('resize'); //on page load
};

App.init = function()
{
    var self = this;

    // render our views
    self.Views.menu.render().onChange().buildModelData();
    self.Views.headerMenu.render();
    self.Views.kmlMenu.render().onChange();
    self.Views.importMenu.render().onChange();

    self.Views.dataTablesView.render();

    if(window.domain !== 'nexus microsurvey local')
    {
        self.Views.dataTablesView.liveUpdate();
        self.Views.highchartsView.render();
        self.Views.highchartsView2.render();
        self.Views.highchartsView3.render();
        self.Views.highchargsView4.render();
    }

    self.Views.importView.render('importUploadTemplate').initForm();

    if($('#welcome_logged_in_user').length > 0)
    {
        $('#welcome_logged_in_user').append("Welcome, "+ self.Models.user.get('username') + "<a class='logout_link' href='/reports/api.php?request=logout'>[logout]</a>");
    }

    // timeout to make sure the multiselect function finishes first
    setTimeout(function () {
        self.Views.menu.hideUtilityOption();
    }, 1);

    // set our click events
    self.setClickEvents();
};

App.postInit = function()
{
    $('#logged_in').fadeIn();
    $('#contentContainer').fadeIn();
    $('#dashboardContainer').fadeIn();
};

App.setClickEvents = function()
{
    var self = this;

    $('#dashboard_btn').on('click', function(event){
        event.preventDefault();
        $('#state_btn').html('Dashboard <span class="caret"></span>');
        $('.active-view').hide().toggleClass('active-view');
        $('#dashboardContainer').show().toggleClass('active-view');
    });

    $('#state_btn').on('click', function(){

        if($('#menuContainer').hasClass('menu_active'))
        {
            $('#contentContainer').animate({'left':'0',  width: '100%', display: 'table'}, function(){
                $('#menuContainer').toggleClass('menu_active');
            }).css({overflow: 'visible'});
        }

        if($('#kmlMenuContainer').hasClass('menu_active'))
        {
            $('#contentContainer').animate({'left':'0',  width: '100%'}, function(){
                $('#kmlMenuContainer').toggleClass('menu_active');
            }).css({overflow: 'visible'});
        }

        if($('#importMenuContainer').hasClass('menu_active'))
        {
            $('#contentContainer').animate({'left':'0',  width: '100%', display: 'table'}, function(){
                $('#importMenuContainer').toggleClass('menu_active');
            }).css({overflow: 'visible'});
        }

        if($('#notificationContainer').hasClass('active-notification'))
        {
            $('#notificationContainer').removeClass('active-notification').slideUp('fast');
        }

    });

    $('#datatable_btn').on('click', function(event){
        event.preventDefault();
        $('#state_btn').html('DataTables <span class="caret"></span>');
        $('.active-view').hide().toggleClass('active-view');
        $('#dataTablesContainer').show().toggleClass('active-view');

        var TT = TableTools.fnGetInstance( App.Views.dataTablesView.elem );
        if(TT === 'undefined' || TT === null)
        {
           App.Views.dataTablesView.renderTableTools();
        }


    });

    $('#kmlmap_btn').on('click', function(event){
        event.preventDefault();
        $('#state_btn').html('KML Map <span class="caret"></span>');
        $('.active-view').hide().toggleClass('active-view');
        $('#kmlMapContainer').show().toggleClass('active-view');

        // we'll render the map here
        self.Views.mykml.render();
    });

    $('#import_btn').on('click', function(event){
        event.preventDefault();
        $('#state_btn').html('Import <span class="caret"></span>');
        $('.active-view').hide().toggleClass('active-view');
        $('#importContainer').show().toggleClass('active-view');
    });

    $('#notification_btn').on('click', function(event){
        event.preventDefault();

        if($('#notificationContainer').hasClass('active-notification'))
        {
            $('#notificationContainer').removeClass('active-notification').slideUp('fast');
        }
        else
        {
            $('#notificationContainer').addClass('active-notification').slideDown('fast');
        }
    });

    $('.menu_toggle_btn').on('click', function(event){
        event.preventDefault();
        var theWidth;

        if($('#menuContainer').hasClass('menu_active'))
        {
            $('#contentContainer').animate({'left':'0', width: '100%'}, function(){
            }).css({overflow: 'visible'});

            $('#menuContainer').animate({'left':'-400'}, function(){
                $('#menuContainer').toggleClass('menu_active');
            }).css({overflow: 'visible'});
        }
        else
        {
            theWidth = $('#contentContainer').width() - 400;
            $('#menuContainer').animate({'left':'0'}).toggleClass('menu_active');
            $('#contentContainer').animate({'left':'400', display: 'table', width: theWidth + 'px'}).css({overflow: 'visible'});

        }
    });

    $('.kml_menu_toggle_btn').on('click', function(event){
        event.preventDefault();
        var theWidth;

        if($('#kmlMenuContainer').hasClass('menu_active'))
        {
            $('#contentContainer').animate({'left':'0', width: '100%'}, function(){
            }).css({overflow: 'visible'});

            $('#kmlMenuContainer').animate({'left':'-400'}, function(){
                $('#kmlMenuContainer').toggleClass('menu_active');
            }).css({overflow: 'visible'});

        }
        else
        {
            theWidth = $('#contentContainer').width() - 400;
            $('#kmlMenuContainer').animate({'left':'0'}).toggleClass('menu_active');
            $('#contentContainer').animate({'left':'400', display: 'table', width: theWidth+ 'px'}).css({overflow: 'visible'});
        }
    });

    $('#importContainer').on('click', '.import_menu_toggle_btn', function(event){
        event.preventDefault();
        var theWidth;

        if($('#importMenuContainer').hasClass('menu_active'))
        {
            $('#contentContainer').animate({'left':'0', width: '100%'}, function(){
            }).css({overflow: 'visible'});

            $('#importMenuContainer').animate({'left':'-400'}, function(){
                $('#importMenuContainer').toggleClass('menu_active');
            }).css({overflow: 'visible'});
        }
        else
        {
            theWidth = $('#contentContainer').width() - 400;
            $('#importMenuContainer').animate({'left':'0'}).toggleClass('menu_active');
            $('#contentContainer').animate({'left':'400', display: 'table', width: theWidth+ 'px'}).css({overflow: 'visible'});
        }
    });

    $('#notificationContainer').on('click', '.alert button', function(){
        App.notifications -= 1;
        App.checkNotifications();

    });

    $('#advanced').on('click', function(){

        // hide fields
        $('.menu_field').addClass('hidden');
        $('.utility_row').removeClass('hidden');
        $('.advanced_field').removeClass('hidden');

        // hide datatable columns
        $('.datatables_column').addClass('hidden');
        $('.advanced_column').removeClass('hidden');

        $('.opportunities_no_amount').removeClass('hidden');

        self.Views.menu.showUtilityOption();

        App.Models.dataTablesModel.set('type', 'advanced');
        $('#dataTables_info').html('Results - Count: '+ App.Models.dataTablesModel.get('recordsTotal'));

    });

    $('#basic').on('click', function(){

        $('.menu_field').addClass('hidden');
        $('.basic_field').removeClass('hidden');

        // hide datatable columns
        $('.datatables_column').addClass('hidden');
        $('.basic_column').removeClass('hidden');
        $('.opportunities_no_amount').removeClass('hidden');
        $('.utility_row').addClass('hidden');

        self.Views.menu.hideUtilityOption();

        App.Models.dataTablesModel.set('type', 'basic');
        $('#dataTables_info').html('Results - Count: '+ App.Models.dataTablesModel.get('basic_count'));

    });

    $('#opportunities').on('click', function(){

        $('.menu_field').addClass('hidden');
        $('.utility_row').removeClass('hidden');
        $('.opportunities_field').removeClass('hidden');

        // hide datatable columns
        $('.datatables_column').addClass('hidden');
        $('.opportunities_column').removeClass('hidden');
        $('.opportunities_no_amount').addClass('hidden');

        self.Views.menu.showUtilityOption();

        App.Models.dataTablesModel.set('type', 'opportunities');
        $('#dataTables_info').html('Results - Count: '+ App.Models.dataTablesModel.get('opportunities_count') + ' <span class="effective_value">Total Effective Value: '+ App.Models.dataTablesModel.get('total_effective_value') + '</span>');
    });

    this.Views.menu.on('origin:change', function(form, originEditor) {
        self.originChanged = true;
    });

};

App.getOwnerSelect = function(currentOwner, insertEmptyOption, id) {
    var selecthtml = document.createElement("select");
        selecthtml.id = typeof id !== 'undefined' ? 'editOwnerSelect' + id : 'editOwnerSelect';
        selecthtml.className = 'form-control';

    if(insertEmptyOption)
    {
        var option = document.createElement("option");
        option.value = "";
        option.innerHTML = "";
        selecthtml.appendChild(option);
    }

    for(var n in window.reportsVariables.owners)
    {
        var ownerShortcode = window.reportsVariables.owners[n]['Short-Code'],
            ownerLabel = window.reportsVariables.owners[n]['Label'],
            ownerElement = window.reportsVariables.owners[n]['Element'],
            ownerValue = window.reportsVariables.owners[n]['Value'];

        var option = document.createElement("option");
        option.value = ownerValue;
        option.innerHTML = ownerShortcode;

        if( ownerShortcode === currentOwner ||
            ownerLabel === currentOwner ||
            ownerElement === currentOwner ||
            ownerValue === currentOwner
        )
        {
            option.setAttribute('selected', 'selected');
        }

        selecthtml.appendChild(option);
    }

    return App.getHTML(selecthtml, true);
};

App.getStatusSelect = function(currentStatus) {
    var selecthtml = document.createElement("select");
    selecthtml.className = 'form-control';

    var options = ['Pending', 'Accepted', 'Rejected', 'Rejected - Duplicate', 'Rejected - Reassign', 'Rejected - Garbage', 'Rejected - Dealer/partner/staff', 'Utility'];

    for(var n in options)
    {
        var option = document.createElement("option");
        option.value = n;
        option.innerHTML = options[n];

        if(options[n] === currentStatus || n === currentStatus)
        {
            option.setAttribute('selected', 'selected');
        }

        selecthtml.appendChild(option);
    }
    return App.getHTML(selecthtml, true);
};

App.getChannelSelect = function(currentChannel, insertEmptyOption, id) {
    var selecthtml = document.createElement("select");
    selecthtml.id = typeof id !== 'undefined' ? 'editChannelSelect' + id : 'editChannelSelect';
    selecthtml.className = 'form-control';

    if(insertEmptyOption)
    {
        var option = document.createElement("option");
        option.value = "";
        option.innerHTML = "";
        selecthtml.appendChild(option);
    }

    for(var n in window.reportsVariables.channels)
    {
        var channel = window.reportsVariables.channels[n]['Label'],
            value = window.reportsVariables.channels[n]['eTrigue Value'];

        var option = document.createElement("option");
        option.value = value;
        option.innerHTML = channel;

        if(channel === currentChannel || value === currentChannel)
        {
            option.setAttribute('selected', 'selected');
        }

        selecthtml.appendChild(option);
    }

    return App.getHTML(selecthtml, true);
};

App.getSegmentSelect = function(currentSegment, insertEmptyOption, id) {
    var selecthtml = document.createElement("select");
    selecthtml.id = typeof id !== 'undefined' ? 'editSegmentSelect' + id : 'editSegmentSelect';
    selecthtml.className = 'form-control';

    if(insertEmptyOption)
    {
        var option = document.createElement("option");
        option.value = "";
        option.innerHTML = "";
        selecthtml.appendChild(option);
    }

    for(var n in window.reportsVariables.segments)
    {
        var segment = window.reportsVariables.segments[n]['Element'],
            value = window.reportsVariables.segments[n]['eTrigue Value'];

        var option = document.createElement("option");
        option.value = value;
        option.innerHTML = segment;

        if(segment === currentSegment || value === currentSegment)
        {
            option.setAttribute('selected', 'selected');
        }

        selecthtml.appendChild(option);
    }

    return App.getHTML(selecthtml, true);

};

App.getDivisionSelect = function(currentDivision, insertEmptyOption, id) {
    var selecthtml = document.createElement("select");
    selecthtml.id = typeof id !== 'undefined' ? 'editDivisionSelect' + id : 'editDivisionSelect';
    selecthtml.className = 'form-control';

    if(insertEmptyOption)
    {
        var option = document.createElement("option");
        option.value = "";
        option.innerHTML = "";
        selecthtml.appendChild(option);
    }

    for(var n in window.reportsVariables.divisions)
    {
        var division = window.reportsVariables.divisions[n]['Element'],
            value = window.reportsVariables.divisions[n]['eTrigue Value'];

        var option = document.createElement("option");
        option.value = value;
        option.innerHTML = division;

        if(division === currentDivision || value === currentDivision)
        {
            option.setAttribute('selected', 'selected');
        }

        selecthtml.appendChild(option);
    }

    return App.getHTML(selecthtml, true);

};

App.getFormSelect = function(currentForm, insertEmptyOption)
{
    var selecthtml = document.createElement("select");
    selecthtml.id = 'editFormSelect';
    selecthtml.className = 'form-control';

    if(insertEmptyOption)
    {
        var option = document.createElement("option");
        option.value = "";
        option.innerHTML = "";
        selecthtml.appendChild(option);
    }

    for(var n in window.reportsVariables.forms)
    {
        var form = window.reportsVariables.forms[n]['Form ID'],
            profile = window.reportsVariables.forms[n]['Label'];

        var option = document.createElement("option");
        option.value = form;
        option.innerHTML = profile + ' / ' + form;

        if(form === currentForm)
        {
            option.setAttribute('selected', 'selected');
        }

        selecthtml.appendChild(option);
    }

    return App.getHTML(selecthtml, true);
};

App.getOriginSelect = function(currentOrigin, insertEmptyOption)
{
    var selecthtml = document.createElement("select");
    selecthtml.id = 'editOriginSelect';
    selecthtml.className = 'form-control';

    if(insertEmptyOption)
    {
        var option = document.createElement("option");
        option.value = "";
        option.innerHTML = "";
        selecthtml.appendChild(option);
    }

    for(var n in window.reportsVariables.origins)
    {
        var orig = window.reportsVariables.origins[n]['eTrigue Value'],
            html = window.reportsVariables.origins[n]['Element'];

        var option = document.createElement("option");
        option.value = orig;
        option.innerHTML = html;

        if(orig === currentOrigin)
        {
            option.setAttribute('selected', 'selected');
        }

        selecthtml.appendChild(option);
    }

    return App.getHTML(selecthtml, true);
};

App.getPresetSelect = function(currentPreset, insertEmptyOption)
{
    var selecthtml = document.createElement("select");
    selecthtml.id = 'editPresetSelect';
    selecthtml.className = 'form-control';

    if(insertEmptyOption)
    {
        var option = document.createElement("option");
        option.value = "";
        option.innerHTML = "";
        selecthtml.appendChild(option);
    }

    for(var n in window.reportsVariables.presets)
    {
        var option = document.createElement("option");
        option.value = n;
        option.innerHTML = n;

        if(n === currentPreset)
        {
            option.setAttribute('selected', 'selected');
        }

        selecthtml.appendChild(option);
    }

    return App.getHTML(selecthtml, true);
};

App.getInputField = function(defaultInput, id)
{
    var inputhtml = document.createElement("input");
    inputhtml.id = id;
    inputhtml.type = 'text';
    inputhtml.className = 'form-control';
    inputhtml.value = defaultInput;
    return App.getHTML(inputhtml, true);

}

App.postData = function(thedata, request, callback)
{
    $.post( "api.php?request="+request, {data: thedata}, function( dat ) {

        var data = $.parseJSON(dat);

        var alert = '<div class="myAlert alert alert-dismissible '+ data.style +'" role="alert">' +
            '<strong id="myAlertStatus">'+ data.status +'! </strong>' + data.message +
            '</div>';

        $(alert).appendTo('body').slideDown("fast", function(){
            var $alert = this;

            var notification = '<div class="alert alert-dismissible '+ data.style +'" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' +
                '<strong>'+ data.status +'! </strong>' + data.message +
                '</div>';

            setTimeout(function(){
                $($alert).slideUp("fast", function(){
                    $(this).remove();
                    $('#notificationContainer').append(notification);

                    App.notifications += 1;
                    App.checkNotifications();
                });
            }, 2000);
        });

        if(typeof callback !== "undefined")
            callback(dat);
    });

};

App.getHTML = function(who, deep){
    if(!who || !who.tagName) return '';
    var txt, ax, el= document.createElement("div");
    el.appendChild(who.cloneNode(false));
    txt= el.innerHTML;
    if(deep){
        ax= txt.indexOf('>')+1;
        txt= txt.substring(0, ax)+who.innerHTML+ txt.substring(ax);
    }
    el= null;
    return txt;
};

App.checkNotifications = function()
{
    if(App.notifications > 0)
    {
        $('#notification_btn').addClass('btn-success');
        $('#nexusBadge').html(App.notifications);

        if(!document.hasFocus())
        {
            App.isOldTitle = true;
            window.interval = null;
            window.interval = setInterval(App.changeTitle, 700);
        }
    }
    else
    {
        $('#notification_btn').removeClass('btn-success');
        $('#nexusBadge').html("");
        document.title = 'Nexus Reporting Tool';
    }

};

App.changeTitle = function() {
    document.title = App.isOldTitle ? App.oldTitle : App.newTitle;
    App.isOldTitle = !App.isOldTitle;
};

App.oldTitle = "Nexus Reporting Tool";
App.newTitle = "New notifications";

$(window).focus(function () {
    clearInterval(window.interval);
    document.title = App.oldTitle;
});

App.resetImportMenu = function() {

    this.Views.importMenu.remove();

    this.Models.importMenuModel = new this.Models.ImportMenu();
    this.Views.importMenu = new this.Views.ImportMenuFormView({
        template: _.template($('#importMenuTemplate').html()),
        model : this.Models.importMenuModel,
        container : $('#importMenuContainer'),
        toggleButton : $('#state_button'),
        toggleSelect : $('.dropdown-menu a')
    });

    this.Views.importMenu.render().onChange();

};