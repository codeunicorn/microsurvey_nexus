/**
 * Created with JetBrains PhpStorm.
 * User: jsantos317
 * Date: 5/28/14
 * Time: 7:56 PM
 * To change this template use File | Settings | File Templates.
 */

App.Models.DataTablesModel = Backbone.Model.extend({

    defaults: {
        data: [],
        columns : [],
        recordsTotal : '0',
        opportunities_count : '0',
        basic_count : '0',
        total_effective_value : '0',
        type: ''
    },

    initialize: function(options)
    {
        this.set(options);
        return this;
    },

    update : function(dat)
    {
        this.set({
            data : dat.data,
            columns : dat.columns,
            recordsTotal : dat.recordsTotal,
            opportunities_count : dat.opportunities_count,
            basic_count : dat.basic_count,
            total_effective_value : dat.total_effective_value,
            type: dat.type
        });

        return this;
    },

    pushData : function(dat)
    {
        this.attributes.data.concat(dat.data);
    }

});