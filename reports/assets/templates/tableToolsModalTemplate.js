<script id="tableToolsModalTemplate" type="text/template">

    <div class="modal-dialog">
        <div class="modal-content">

                <div class="panel panel-primary">
                    <div class="panel-heading modal-header">
                        <h4 class="modal-title">Edit</h4>
                    </div>
                    <div class="panel-body modal-body">

                        <form>
                            <div class="owner_field input-group" data-ltarget="x_udf_form_owner148" data-vtarget="x_owner" data-type="select">
                                <span class="input-group-addon">Owner</span>
                                <%= App.getOwnerSelect(row.x_udf_form_owner148, true) %>

                            </div>

                            <div class="status_field input-group" data-ltarget="status_label" data-vtarget="status" data-type="select">
                                <span class="input-group-addon">Status</span>
                                <%= App.getStatusSelect(row.status) %>
                            </div>

                            <% if( App.Models.dataTablesModel.get("type") == "advanced"){ %>

                                <div class="segment_field input-group" data-ltarget="x_udf_primary_market_segment122__ss_label" data-vtarget="x_udf_primary_market_segment122__ss" data-type="select">
                                    <span class="input-group-addon">Segment</span>
                                    <%= App.getSegmentSelect(row.x_udf_primary_market_segment122__ss, true) %>
                                </div>

                                <div class="div_field input-group" data-ltarget="x_udf_division146__ss_label" data-vtarget="x_udf_division146__ss" data-type="select">
                                    <span class="input-group-addon">Division</span>
                                    <%= App.getDivisionSelect(row.x_udf_division146__ss, true) %>
                                </div>

                            <% } %>

                            <div class="category_field input-group" data-ltarget="x_udf_channel___tactic400__ss_label" data-vtarget="x_udf_channel___tactic400__ss" data-type="select">
                                <span class="input-group-addon">Channel Tactic</span>
                                <%= App.getChannelSelect(row.x_udf_channel___tactic400__ss, true) %>
                            </div>

                            <% if( App.Models.dataTablesModel.get("type") == "advanced"){ %>

                                <div class="referrer_field input-group" data-ltarget="x_udf_ext_dealer375" data-vtarget="x_udf_ext_dealer375" data-type="input">
                                    <span class="input-group-addon">Referrer/Source</span>
                                    <input class="form-control" value="<%= row.x_udf_ext_dealer375 %>" />
                                </div>

                            <% } %>

                        </form>
                    </div>
                </div>

            <div class="modal-footer">
                <button id="mCancelButton" type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Cancel</button>
                <button id="mSaveButton" type="button" class="btn btn-success"><span class="glyphicon glyphicon-ok-circle"></span> Save</button>
                <button id="mSavePushButton" type="button" class="btn btn-warning"><span class="glyphicon glyphicon-upload"></span> Save & Repush</button>
            </div>
        </div>
    </div>

</script>
