<script id="klmMenuTemplate" type="text/template">
    <form>

        <div class="map_field">
            <label>Map:</label>
            <div data-editors="map" class="inputDiv"></div>
        </div>

        <div class="kml_preset_field">
            <label>Segment:</label>
            <div data-editors="preset" class="inputDiv"></div>
        </div>

        <input id="kml_menu_submit" class="btn btn-danger btn-lg" type="submit" value="View Map">

    </form>
</script>