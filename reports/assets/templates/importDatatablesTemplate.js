<script id="importDatatablesTemplate" type="text/template">
    <button type="button" class="import_menu_toggle_btn btn btn-primary">
        <span class="glyphicon glyphicon-align-justify"></span>
    </button>
    <table id="importDatatablesTable" class="cell-border table table-condensed table-bordered" cellspacing="0" width="100%">
    </table>
    </script>