<script id="formTemplate" type="text/template">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#" id="basic" role="tab" data-toggle="tab" data-column="8">SRL</a></li>
        <li><a href="#" id="advanced" role="tab" data-toggle="tab" data-column="">Advanced</a></li>
        <li><a href="#" id="opportunities" role="tab" data-toggle="tab" data-column="">Opportunities</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">

        <form>

            <div class="date_field menu_field basic_field advanced_field opportunities_field">
                <label>Date:</label>
                <div data-editors="date" class="inputDiv"></div>
            </div>

            <div class="origin_field menu_field basic_field advanced_field">
                <label>Origin:</label>
                <div data-editors="origin" class="inputDiv"></div>
            </div>

            <div class="owner_field menu_field advanced_field hidden">
                <label>Form/ID:</label>
                <div data-editors="formId" class="inputDiv"></div>
            </div>

            <div class="owner_field menu_field advanced_field opportunities_field hidden">
                <label>Owner:</label>
                <div data-editors="owner" class="inputDiv"></div>
            </div>

            <div class="map_field menu_field advanced_field hidden">
                <label>Map:</label>
                <div data-editors="map" class="inputDiv"></div>
            </div>

            <div class="region_field menu_field advanced_field hidden">
                <label>Region:</label>
                <div data-editors="region" class="inputDiv"></div>
            </div>

            <div class="country_field menu_field advanced_field hidden">
                <label>Country:</label>
                <div data-editors="country" class="inputDiv"></div>
            </div>

            <div class="state_field menu_field advanced_field hidden">
                <label>State:</label>
                <div data-editors="state" class="inputDiv"></div>
            </div>

            <div class="segment_field menu_field advanced_field hidden">
                <label>Primary Market Segment:</label>
                <div data-editors="seg" class="inputDiv"></div>
            </div>

            <div class="div_field menu_field advanced_field hidden">
                <label>Division Override:</label>
                <div data-editors="div" class="inputDiv"></div>
            </div>

            <div class="preset_field menu_field basic_field opportunities_field">
                <label>Segment:</label>
                <div data-editors="preset" class="inputDiv"></div>
            </div>

            <div class="category_field menu_field basic_field advanced_field opportunities_field">
                <label>Channel & Tactic:</label>
                <div data-editors="channel" class="inputDiv"></div>
            </div>

            <div class="status_field menu_field basic_field advanced_field">
                <label>Status:</label>
                <div data-editors="status" class="inputDiv"></div>
            </div>

            <input id="menu_submit" class="btn btn-danger btn-lg" type="submit" value="FILTER">

        </form>

    </div>

</script>