<script id="DataTablesTemplate" type="text/template">
    <table id="<%= elem %>" class="cell-border table table-condensed table-bordered" cellspacing="0" width="100%">
        <thead>
        </thead>
        <tbody>
        </tbody>
    </table>
</script>