<script id="importMenuTemplate" type="text/template">
    <form>

        <div class="form_field">
            <label>Form:</label>
            <div data-editors="formId" class="inputDiv"></div>
        </div>

        <div class="owner_field">
            <label>Owner:</label>
            <div data-editors="owner" class="inputDiv"></div>
        </div>

        <div class="preset_field">
            <label>Segment:</label>
            <div data-editors="preset" class="inputDiv"></div>
        </div>

        <div class="ct_field">
            <label>Channel Tactic:</label>
            <div data-editors="ct" class="inputDiv"></div>
        </div>

        <input id="import_menu_submit" class="btn btn-danger btn-lg" type="submit" value="Commit All">
        <input id="import_menu_cancel" class="btn btn-warning btn-lg" type="button" value="Reset">

        </form>
    </script>