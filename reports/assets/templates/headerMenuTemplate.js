<script id="headerMenuTemplate" type="text/template">

    <button id="notification_btn" type="button" class="btn btn-default">
        <span class="glyphicon glyphicon-flag"></span>
        <span id="nexusBadge" class="badge pull-right"></span>
    </button>

    <!-- Single button -->
    <div id="state_buttons" class="btn-group">
        <button id="state_btn" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
        Dashboard <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
            <li><a id="dashboard_btn" href="#">Dashboard</a></li>
            <li><a id="datatable_btn" href="#">DataTable</a></li>
            <li><a id="kmlmap_btn" href="#">KML Map</a></li>
            <%= importButton %>
        </ul>
    </div>

    <div id="welcome_logged_in_user"></div>

    <div id="log_out"></div>
</script>
