<script id="loginTemplate" type="text/template">
    <form>

        <div class="form-group">
            <label>username:</label>
            <div data-editors="username"></div>
        </div>

        <div class="form-group">
            <label>password:</label>
            <div data-editors="password"></div>
        </div>

        <div>
            <input id="login_submit" class="btn btn-info" type="submit" value="login">
        </div>

    </form>
</script>