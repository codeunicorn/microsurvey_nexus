/**
 * Created with JetBrains PhpStorm.
 * User: jsantos317
 * Date: 5/26/14
 * Time: 3:26 AM
 * To change this template use File | Settings | File Templates.
 */

/**
 This file just combines all js files into one minified file for release.
 JS files will be compressed and minified in order.

 @depends jquery-1.11.2.min.js
 @depends jquery-ui.js
 @depends bootstrap.min.js
 @depends highcharts/highcharts.js
 @depends highcharts/modules/heatmap.js
 @depends highcharts/modules/map.js
 @depends highcharts/modules/data.js
 @depends highcharts/modules/drilldown.js
 @depends highcharts/modules/mapdata/us-all.js
 @depends highcharts/modules/mapdata/north-america.js
 @depends highcharts/themes/sand-signika.js
 @depends jquery.slimscroll.min.js
 @depends json2.js
 @depends moment.js
 @depends moment-timezome-with-data.js
 @depends select2.js
 @depends daterangepicker.js
 @depends bootstrap-multiselect.js
 @depends underscore.js
 @depends backbone-min.js
 @depends jquery.dataTables.min.js
 @depends TableTools-2.2.3/js/dataTables.tableTools.min.js
 @depends Responsive-1.0.4/js/dataTables.responsive.min.js
 @depends googlemap.js
 @depends kml.js
 @depends jquery.knob.js
 @depends jquery.ui.widget.js
 @depends jquery.iframe-transport.js
 @depends jquery.fileupload.js

 @depends models/AppModel.js
 @depends views/backbone-forms.js
 @depends views/LoginFormView.js
 @depends views/MenuFormView.js
 @depends views/KmlMenuFormView.js
 @depends views/ImportMenuFormView.js
 @depends views/headerMenuView.js
 @depends views/KMLMapView.js
 @depends views/importView.js
 @depends views/DatePickerView.js
 @depends views/DataTablesView.js
 @depends views/HighChartsView.js
 @depends views/Select2.js
 @depends views/DateRangePickerView.js
 @depends views/Multiselect.js
 @depends views/ModalView.js
 @depends models/MenuModel.js
 @depends models/KmlMenuModel.js
 @depends models/ImportMenuModel.js
 @depends models/userModel.js
 @depends models/DataTablesModel.js
 @depends models/HighChartsModel.js
 @depends reports.js
 */