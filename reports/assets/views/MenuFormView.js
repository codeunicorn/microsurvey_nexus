App.Views.MenuForm = Backbone.Form.extend({

    events : {
        "submit" : "submitForm"

    },

    submitForm : function(event) {

        event.preventDefault();
        this.commit();
        App.Views.dataTablesView.showLoading();

        // don't submit hidden fields
        this.buildModelData();

        // reload datatables
        App.Views.dataTablesView.oTable.ajax.reload();
    },

    buildModelData: function()
    {
        // don't submit hidden fields
        var myData = this.model.attributes,
            data = [];

        for(var n in this.fields)
        {
            var field = this.fields[n];
            var prop = field.editor.key;
            var parent = field.editor.$el.closest('.menu_field');

            if(parent.hasClass('hidden'))
            {
                delete myData[prop];
            }
        }

        var activeId = $('#menuContainer .nav .active a').attr('id');
        App.Views.dataTablesView.setType(activeId);
        myData['type'] = activeId;

        for(var m in myData)
        {
            data.push({data: myData[m], name: m});
        }

        this.model.data = data;
        return this;
    },

    onChange : function()
    {
        this.on('seg:change', function(form, segEditor){
            var newOptions = this.model.getDivisionsForSegment(segEditor.getValue());
            form.fields.div.editor.setOptions(newOptions);
        });

        this.on('region:change', function(form, regionEditor) {

            var mapValue = form.fields.map.editor.getValue(),
                regionValue = regionEditor.getValue();

            var newOptions = regionValue === '' ? this.model.getCountriesForMap(mapValue) : this.model.getCountriesForRegion(mapValue, regionValue);
            form.fields.country.editor.setOptions(newOptions);

            // set our state dropdown to Select All only
            form.fields.state.editor.setOptions('<option value="">Select All</option>');

        });

        this.on('country:change', function(form, countryEditor) {

            var mapValue = form.fields.map.editor.getValue();
            var newOptions = this.model.getStatesForCountry(mapValue, countryEditor.getValue());
            form.fields.state.editor.setOptions(newOptions);

        });

        this.on('map:change', function(form, mapEditor) {

            var mapValue = mapEditor.getValue();

            if(mapValue !== '') {

                var regionOptions = this.model.getRegionForMap(mapValue);
                form.fields.region.editor.setOptions(regionOptions);

                var countryOptions = this.model.getCountriesForMap(mapValue);
                form.fields.country.editor.setOptions(countryOptions);

                // show our sales region, country, and state fields
                $('.region_field, .country_field, .state_field').removeClass('hidden');

            }
            else {
                $('.region_field, .country_field, .state_field').addClass('hidden');
            }

        });

        return this;
    },

    hideUtilityOption : function()
    {
        $('#menu-status').next().find('ul.multiselect-container li:last-child').addClass('hidden');
        return this;
    },

    showUtilityOption : function()
    {
        $('#menu-status').next().find('ul.multiselect-container li:last-child').removeClass('hidden');
        return this;
    }

});