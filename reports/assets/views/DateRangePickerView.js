/**
 * Created with JetBrains PhpStorm.
 * User: jsantos317
 * Date: 5/24/14
 * Time: 5:08 PM
 * To change this template use File | Settings | File Templates.
 */

// Extend our form editor to include DatePicker as a schema option
Backbone.Form.editors.DateRangePicker = Backbone.Form.editors.Text.extend({

    render: function() {
        // Call the parent's render method
        Backbone.Form.editors.Text.prototype.render.call(this);
        var that = this.$el;

        this.$el.val(moment().format('MMMM D, YYYY') +"   -   "+ moment().format('MMMM D, YYYY'));

        // Then make the editor's element a datepicker.
        this.$el.daterangepicker({
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'Last 90 Days': [moment().subtract(89, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            format: 'MMMM D, YYYY'
    }, function(start, end) {
            that.html(start.format('MMMM D, YYYY') + '   -   ' + end.format('MMMM D, YYYY'));
});

        return this;
    }
});