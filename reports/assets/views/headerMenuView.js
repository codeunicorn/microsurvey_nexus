App.Views.headerMenuView = Backbone.View.extend({

    el: $('#logged_in'),

    initialize: function(attrs){

        this.options = attrs;
        this.template = attrs.template || this.template || this.constructor.template;
        return this;
    },

    render: function()
    {
        var importButton = role === 'admin' ? '<li><a id="import_btn" href="#">Import</a></li>' : '';

        // Load the compiled HTML into the Backbone "el"
        this.$el.html(this.template({importButton : importButton}));
        return this;
    }

});