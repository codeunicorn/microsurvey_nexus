App.Views.modalView = Backbone.View.extend({

    el: $('#modal'),

    initialize: function(attrs){

        this.options = attrs;
        return this;
    },

    tableToolsEdit: function(tt)
    {
        // our dataTable instance
        var dt = App.Views.dataTablesView.oTable;

        // array of row indexes to edit
        var rowIndexes = tt.fnGetSelectedIndexes();
        var rowData = tt.fnGetSelectedData();

        // show our modal
        if(rowData.length > 0)
        {
            this.$el.html( _.template($('#tableToolsModalTemplate').html(), {row: rowData[0]}) );
            this.$el.modal({backdrop: 'static'});
        }

        var self = this;

        // edit button click event
        $('#mSaveButton, #mSavePushButton').on('click', function(){

            var that = this;

            var arowIndexes = rowIndexes;
            var arowData = rowData;

            // find our inputs/select fields
            //self.$el.find('.panel').each(function(){

                //var rowDataIndex = $(this).data("row");
                //var browData = arowData;

            self.$el.find('.panel .input-group').each(function(){

                var browData = arowData,
                    rowDataLabel = $(this).data("ltarget"),    // label
                    rowDataValue = $(this).data("vtarget");   // value

                var inputLabel = $(this).data("type") == 'select' ? $(this).find("select option:selected").text() : $(this).find("input").val();
                var inputValue = $(this).data("type") == 'select' ? $(this).find("select option:selected").val() : $(this).find("input").val();

                console.log(arowIndexes);
                console.log(browData);

                // now update our rowData
                for(var idx in arowIndexes) {
                    browData[idx][rowDataLabel] = inputLabel;
                    browData[idx][rowDataValue] = inputValue;
                }
            });
            //});

            // update our rows in datatables
            for(var n in rowIndexes)
            {
                var index = rowIndexes[n];
                App.Views.dataTablesView.oTable.row( index ).data( rowData[n] );

            }

            // Update data on server
            App.postData(rowData, "post_data", function(dat){

                var data = $.parseJSON(dat);

                if(data.owner !== "undefined")
                {
                    for( var n in data.owner )
                    {
                        var owner = data.owner[n];

                        for( var m in rowData)
                        {
                            if(n == rowData[m].count)
                            {
                                rowData[m].x_owner = owner;
                                rowData[m].x_udf_form_owner148 = owner;
                                App.Views.dataTablesView.oTable.row( n - 1 ).data( rowData[m] );
                            }
                        }
                    }
                }

                if($(that).attr('id') == 'mSavePushButton')
                    App.postData(rowData, "notify_owner");

            });

            // remove click events and close modal
            $('#mSaveButton').off();
            $('#mCancelButton').off();
            $('#mSavePushButton').off();
            self.$el.modal('hide');

        });

        $('#mCancelButton').on('click', function(){
            $('#mSaveButton').off();
            $('#mSavePushButton').off();
            $('#mCancelButton').off();
            self.$el.modal('hide');

        });
    }

});