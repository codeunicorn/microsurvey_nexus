App.Views.KmlMenuFormView = Backbone.Form.extend({

    events : {
        "submit" : "submitForm"

    },

    submitForm : function(event) {

        event.preventDefault();
        this.commit();
        nexusKml.clearMap( );
        App.Views.mykml.showLoading();

        var theModel = this.model;
        var myData = theModel.attributes;

        theModel.fetch({
            reset: false,
            data: myData,
            success: function (dat) {
                nexusKml.setMarkersAndOverlays(dat.toJSON());
                App.Views.mykml.hideLoading();
                theModel.clear().set(theModel.defaults);
            }
        });
    },

    onChange : function()
    {
        this.on('map:change', function(form, mapEditor){
            var newOptions = this.model.getPresetForMap(mapEditor.getValue());
            form.fields.preset.editor.setOptions(newOptions);
        });

        return this;
    }

});