/**
 * Created by jsantos317 on 6/30/14.
 */
App.Views.HighChartsView = Backbone.View.extend({

    initialize: function(attrs){

        this.options = attrs;
        this.container = attrs.container || this.container || this.constructor.container;
        this.template = attrs.template || this.template || this.constructor.template;
        this.model = attrs.model || this.model || this.constructor.model;

        return this;
    },

    render: function(){

        var self = this;

        this.model.fetch({
            data: {type : self.model.attributes.type},
            success: function (dat) {

                var myData = dat.toJSON();

                if(self.model.attributes.type === 'DonutChartByCategoryDashboard' || self.model.attributes.type === 'HighMapByStateDashboard' || self.model.attributes.type === 'HighMapByRegionDashboard')
                {
                    self.model.setSeriesData(myData);
                }
                else
                {
                    for(var n in myData)
                    {
                        self.model.setConfig(n, myData[n]);
                    }
                }

                self.$el.addClass('highcharts_container img-thumbnail');

                var data = self.model.config;

                if(self.model.attributes.type === 'HighMapByStateDashboard' || self.model.attributes.type === 'HighMapByRegionDashboard')
                {
                    self.container.append(self.$el.highcharts('Map', data));
                }
                else
                {
                    self.container.append(self.$el.highcharts(data));
                }
            }
        });
    },

    resize: function()
    {
        if(typeof this.$el.highcharts() !== 'undefined')
        {
            var chart = this.$el.highcharts;
            var myHeight = chart.height;
            var myWidth = ($(window).width() / 2) - 60;
            this.$el.highcharts().setSize(myWidth, myHeight, doAnimation = true);
        }
    }

});