Backbone.Form.editors.Multiselect = Backbone.Form.editors.Select.extend({
    attributes : {},

    render : function () {
        var self = this;

        this.setOptions(this.schema.options);

        setTimeout(function () {
            self.$el.multiselect({
                nonSelectedText : self.schema.placeholder !== undefined ? self.schema.placeholder : 'Select All',
                buttonWidth: self.schema.buttonWidth !== undefined ? self.schema.buttonWidth : '',
                maxHeight: '500',
                selectedClass: self.schema.fieldClass !== undefined ? self.schema.fieldClass : '',
                numberDisplayed: 2,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true
            });

        }, 0);

        return this;
    },

    setValue : function (values) {
        if (!_.isArray(values)) values = [values];
        this.$el.val(values);
        return this;
    }

});