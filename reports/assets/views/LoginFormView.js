/**
 * Created by jsantos317 on 7/18/14.
 */

App.Views.LoginFormView = Backbone.Form.extend({

    events : {
        'submit' : 'submitForm'
    },

    submitForm : function(event)
    {
        this.container.find('.invalid_login').remove();
        event.preventDefault();
        this.commit();

        var self = this;
        this.model.fetch({
            data: this.model.attributes,
            success: function (dat) {

                var data = dat.toJSON();

                if(data.result)
                {
                    window.role = data.role;

                    self.container.fadeOut(function(){
                        self.container.html("");
                        App.preInit();
                        App.init();
                        App.postInit();
                    });
                }
                else
                {
                    var error = $("<div class='invalid_login'>" + data.message + "</div>");
                    self.container.append(error);
                }

            }
        });
    }

});