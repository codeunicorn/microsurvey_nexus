/**
 * Created with JetBrains PhpStorm.
 * User: jsantos317
 * Date: 5/28/14
 * Time: 6:26 PM
 * To change this template use File | Settings | File Templates.
 */

App.Views.DataTablesView = Backbone.View.extend({

    initialize: function(attrs){

        this.options = attrs;
        this.template = attrs.template || this.template || this.constructor.template;
        this.container = attrs.container || this.container || this.constructor.container;
        this.elem = attrs.elem || this.elem || this.constructor.elem;
        this.type = "";
        this.opportunities_count = 0;
        this.total_effective_value = 0;

        return this;
    },

    render: function(){

        // Load the compiled HTML into the Backbone "el"
        this.$el.html( this.template({elem : this.elem}));
        this.container.append(this.$el);

        var that = this;

        // load DataTables
        this.oTable = $('#' + this.elem).DataTable({

            // tells datatable to use server-side processing
            serverSide: true,

            // the ajax info
            ajax: {

                // ajax url
                url: 'api.php?request=get_data',

                // we'll send data using POST
                "type": "POST",

                // we'll replace the data sent to the server
                data: function(data) {

                    // instead of the columns, we'll send the menu data
                    data.columns = App.Models.myMenu.data;
                }
            },

            // specify the columns. This is done at page load
            columns : window.datatablesColumns,

            // set the width of each column to auto
            autoWidth: true,

            // defer rendering until all the data has been processed
            deferRender: true,

            // sets the default length to 5000
            displayLength: 5000,

            // display parameters
            dom : '<"info"i>t<"paging"p>',   // only display the table

            // We'll edit the html of the info
            language: {
                "info": "Count: _TOTAL_",
                infoEmpty: "Count: _TOTAL_",
                infoFiltered: "Count: _TOTAL_"
            },

            // tells datatable to order the data on the first column, descending
            order: [[ 0, "desc" ]],

            // disable sorting
            ordering:  false,

            // enable responsive
            // responsive: true,
/*
            // scroller options
            scrollY: $('#contentContainer').height(),
            //scrollX: "100%",
            scrollCollapse: false,
            oScroller: {
                loadingIndicator: false,
                displayBuffer: 10,
                serverWait: 100
            },
*/
            // callback on each row
            "fnRowCallback": function(nRow, aaData, iDisplayIndex) {

                var filteredList = {
                    status_label : 20,
                    x_companyname : 20,
                    x_firstname : 15,
                    x_lastname : 15,
                    x_udf_primary_market_segment122__ss_label : 15,
                    x_emailaddress : 30,
                    x_udf_ext_dealer375 : 30
                };

                for( var n in filteredList ) {
                    if( aaData[ n ] != null && aaData[ n ].length > filteredList[n] + 5 )
                    {
                        $(nRow).find( '.'+ n ).html( aaData[ n ].substring( 0, filteredList[n] ) + '(...)' );
                    }
                }

                // hide or show opportunity fields
                if( aaData.opportunity_name == null
                    || aaData.opportunity_name.length === 0
                    || aaData.opportunity_name === aaData.x_companyname
                    || aaData.opportunity_name === aaData.x_firstname + ' ' + aaData.x_lastname )
                {
                    $(nRow).addClass('opportunities_no_amount');

                    if(that.type === 'opportunities')
                    {
                        $(nRow).addClass('hidden');
                    }
                }

                // hide or show Utility rows
                if(aaData.status == 7)
                {
                    $(nRow).addClass('utility_row');

                    if(that.type == 'basic')
                    {
                        $(nRow).addClass('hidden');
                    }

                }

            },

            // draw callback
            "drawCallback": function( settings ) {
                App.Views.dataTablesView.hideLoading();
                that.hideOrShowColumns(that.type);
            },

            infoCallback: function (oSettings, iTotal) {

                var info = that.oTable.ajax.json();
                App.Models.dataTablesModel.set('recordsTotal', info.recordsTotal);
                App.Models.dataTablesModel.set('opportunities_count', info.opportunities_count);
                App.Models.dataTablesModel.set('basic_count', info.basic_count);
                App.Models.dataTablesModel.set('total_effective_value', info.total_effective_value);

                var infoText = '';
                if(info.type === 'basic')
                {
                    infoText = "Results - Count: " + info.basic_count;
                }
                if(info.type === 'advanced')
                {
                    infoText = "Results - Count: " + info.recordsTotal;
                }
                else if(info.type === 'opportunities')
                {
                    infoText = 'Results - Count: '+ info.opportunities_count + ' <span class="effective_value">Total Effective Value: '+ info.total_effective_value + '</span>';
                }

                return infoText;
            }

        });

        this.hideOrShowColumns(this.type);

        return this;
    },

    renderTableTools: function()
    {
        var that = this;
        var tableTools = new $.fn.dataTable.TableTools( this.oTable, {
            sRowSelect: "os",
            sSelectedClass: "selected-row",
            aButtons: [

                // custom Edit button
                {
                    "sExtends":    "text",
                    "sButtonText": "Edit",
                    "fnClick": function ( nButton, oConfig, oFlash ) {

                        var tt = TableTools.fnGetInstance(that.elem);
                        App.Views.myModal.tableToolsEdit(tt);

                    },
                    "fnInit": function ( nButton, oConfig ) {
                        $(nButton)
                            .removeClass("DTTT_button DTTT_button_text")
                            .addClass('btn btn-default btn-sm')
                            .prepend('<span class="glyphicon glyphicon-pencil"></span> ');
                    },
                    "fnMouseover": function ( nButton, oConfig, oFlash ) {
                        $(nButton).removeClass("btn-default").addClass("btn-success");
                    },
                    "fnMouseout": function ( nButton, oConfig, oFlash ) {
                        $(nButton).removeClass("btn-success").addClass("btn-default");
                    }
                },

                // custom Notify button
                {
                    "sExtends":    "text",
                    "sButtonText": "Notify",
                    "fnClick": function ( nButton, oConfig, oFlash ) {

                        var tt = TableTools.fnGetInstance(that.elem);
                        var rowData = tt.fnGetSelectedData();

                        if(rowData.length > 0)
                            App.postData(rowData, "notify_owner");

                    },
                    "fnInit": function ( nButton, oConfig ) {
                        $(nButton)
                            .removeClass("DTTT_button DTTT_button_text")
                            .addClass('btn btn-default btn-sm')
                            .prepend('<span class="glyphicon glyphicon-envelope"></span> ');
                    },
                    "fnMouseover": function ( nButton, oConfig, oFlash ) {
                        $(nButton).removeClass("btn-default").addClass("btn-warning");
                    },
                    "fnMouseout": function ( nButton, oConfig, oFlash ) {
                        $(nButton).removeClass("btn-warning").addClass("btn-default");
                    }
                },

                {
                    "sExtends":    "copy",
                    "fnInit": function ( nButton, oConfig ) {
                        $(nButton)
                            .removeClass("DTTT_button DTTT_button_text")
                            .addClass('btn btn-default btn-sm')
                            .prepend('<span class="glyphicon glyphicon-duplicate"></span> ');
                    },
                    "fnMouseover": function ( nButton, oConfig, oFlash ) {
                        $(nButton).removeClass("btn-default").addClass("btn-info");
                    },
                    "fnMouseout": function ( nButton, oConfig, oFlash ) {
                        $(nButton).removeClass("btn-info").addClass("btn-default");
                    }

                },

                {
                    "sExtends":    "csv",
                    "fnInit": function ( nButton, oConfig ) {
                        $(nButton)
                            .removeClass("DTTT_button DTTT_button_text")
                            .addClass('btn btn-default btn-sm')
                            .prepend('<span class="glyphicon glyphicon-save"></span> ');
                    },
                    "fnMouseover": function ( nButton, oConfig, oFlash ) {
                        $(nButton).removeClass("btn-default").addClass("btn-primary");
                    },
                    "fnMouseout": function ( nButton, oConfig, oFlash ) {
                        $(nButton).removeClass("btn-primary").addClass("btn-default");
                    }
                },

                {
                    "sExtends":    "print",
                    "fnInit": function ( nButton, oConfig ) {
                        $(nButton)
                            .removeClass("DTTT_button DTTT_button_text")
                            .addClass('btn btn-default btn-sm')
                            .prepend('<span class="glyphicon glyphicon-print"></span> ');
                    },
                    "fnMouseover": function ( nButton, oConfig, oFlash ) {
                        $(nButton).removeClass("btn-default").addClass("btn-danger");
                    },
                    "fnMouseout": function ( nButton, oConfig, oFlash ) {
                        $(nButton).removeClass("btn-danger").addClass("btn-default");
                    }
                }
            ],
            sSwfPath: "assets/TableTools-2.2.3/swf/copy_csv_xls_pdf.swf"
        } );

        $( tableTools.fnContainer() ).insertBefore('div.dataTables_wrapper').addClass('btn-group');
    },

    hideOrShowColumns : function(type)
    {
        switch(type)
        {
            case '' :
            case null :
            case 'basic' :

                $('.datatables_column').addClass('hidden');
                $('.basic_column').removeClass('hidden');

                break;

            case 'advanced' :

                $('.datatables_column').addClass('hidden');
                $('.advanced_column').removeClass('hidden');

                break;

            case 'opportunities' :

                $('.datatables_column').addClass('hidden');
                $('.opportunities_column').removeClass('hidden');

                var opportunities_count =  App.Models.dataTablesModel.get('opportunities_count');
                var total_effective_value = App.Models.dataTablesModel.get('total_effective_value');
                $('#dataTables_info').html('Count: '+ opportunities_count + ' <span class="the_effective_value">Total Effective Value: '+ total_effective_value + '</span>');

                break;

        }
    },

    liveUpdate : function()
    {
        if(this.menu === undefined)
        {
            this.menu = new App.Models.MenuModel({
                from : moment().tz("America/Los_Angeles").format("YYYY-MM-DD HH:mm:ss"),
                to : moment().tz("America/Los_Angeles").format("YYYY-MM-DD HH:mm:ss")
            });
        }

        var menu = this.menu;
        var model = this.model;
        var table = this.oTable;
        var $this = this;

        // update the from and to attribute
        menu.attributes.from = menu.attributes.to;
        menu.attributes.to = moment().tz("America/Los_Angeles").format("YYYY-MM-DD HH:mm:ss");

        if(menu.attributes.from !== menu.attributes.to)
        {
            var data = {
                from: menu.attributes.from,
                to: menu.attributes.to
            };

            // fetch new data
            menu.fetch({
                reset: false,
                data: data,
                success: function (dat) {

                    var myData = dat.toJSON();

                    if(myData.data.length > 0)
                    {
                        var theData = myData.data;

                        for(var n in theData)
                        {
                            var liveData = theData[n];

                            var dataString = liveData['date_submitted'] + '    |  ' + liveData['x_firstname'] + ' ' + liveData['x_lastname'];

                            if(liveData['x_companyname'] != null && liveData['x_companyname'].length > 0)
                            {
                                dataString +=  ' @ ' + liveData['x_companyname'];
                            }
                            dataString += ' -> ' + liveData['form_label'];

                            var notification = '<div class="alert alert-dismissible alert-info" role="alert">' +
                                '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' +
                                '<strong>Live Feed: </strong>' + dataString + '</div>';



                            $('#notificationContainer').append(notification);

                            App.notifications += 1;

                            App.checkNotifications();
                        }

                        model.pushData(myData);

                    }
                }
            });
        }

        setTimeout(function(){$this.liveUpdate();}, 60000);

        return this;
    },

    showLoading : function()
    {
        this.loadingHtml = $("<div id='loadingOverlay'><div>Loading</div></div>");
        $('#menuContainer form').append(this.loadingHtml);
        return this;

    },

    hideLoading : function()
    {
        if(this.loadingHtml !== undefined)
            this.loadingHtml.remove();

        return this;
    },

    hide : function()
    {
        this.container.hide();
        return this;
    },

    setType : function(type)
    {
        this.type = type;
    }


});
