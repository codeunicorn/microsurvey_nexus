/**
 * Select2
 *
 * Renders Select2 - jQuery based replacement for select boxes
 *
 * Simply pass a 'config' object on your schema, with any options to pass into Select2.
 * See http://ivaynberg.github.com/select2/#documentation
 */

Backbone.Form.editors.Select2 = Backbone.Form.editors.Select.extend({
    attributes : {multiple : 'multiple'},

    render : function () {
        var self = this;

        this.setOptions(this.schema.options);

        setTimeout(function () {
            self.$el.select2({
                width : 'resolve',
                placeholder: self.schema.placeholder !== undefined ? self.schema.placeholder : 'Select All',
                closeOnSelect: false
            });

            if(self.key === 'origin')
            {
                self.$el.select2('val', self.model.attributes.origin);
            }

            if(self.key === 'status')
            {
                self.$el.select2('val', [0,1]);
            }

        }, 0);

        return this;
    },

    setValue : function (values) {
        if (!_.isArray(values)) values = [values];
        this.$el.val(values);
        return this;
    }

});