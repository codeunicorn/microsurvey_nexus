App.Views.ImportMenuFormView = Backbone.Form.extend({

    events : {
        "submit" : "submitForm"

    },

    submitForm : function(event) {

        event.preventDefault();

        $('#import_menu_submit').prop("disabled", true);
        var rowCount = App.Views.importView.datacount;
        this.postData(0, rowCount);
    },

    postData : function(rowIndex, rowCount)
    {
        var self = this;
        var $this = $('#import_row_'+ rowIndex),
            myImg = $('<img class="commit_loader">'); //Equivalent: $(document.createElement('img'))
            myImg.attr('src', 'assets/images/select2-spinner.gif');

        var myRow = App.Views.importView.oTable.api().row($this),
            rowData = myRow.data();

        // if the rowIndex is not in the submittedData array, submit it
        if(App.Views.importView.submittedData.indexOf(rowIndex) === -1)
        {
            // form html
            var formhtml = $this.children(".form_import").find("select").val();
            rowData.form = formhtml;

            // owner html
            var ownerhtml = $this.children(".owner_import").find("select").val();
            rowData.owner = ownerhtml;

            // preset html
            var presethtml = $this.children(".preset_import").find("select").val();
            rowData.preset = presethtml;

            // channel html
            var cthtml = $this.children(".ct_import").find("select").val();
            rowData.ct = cthtml;

            // commit button
            var commitrowBtn = $this.find(".commit_row_btn");

            commitrowBtn.attr('disabled', 'disabled').append(myImg).find('.glyphicon-pencil').addClass('hidden');

            $.post( "api.php?request=import_data", {data : rowData}, function( dat ) {
                var data = $.parseJSON(dat);
                if(data.status === 'success')
                {
                    commitrowBtn.find('.commit_loader').remove();
                    commitrowBtn.removeClass('btn-default').addClass('btn-success');
                    commitrowBtn.find('.glyphicon-pencil').removeClass('glyphicon-pencil hidden').addClass('glyphicon-ok');
                }
                else
                {
                    commitrowBtn.find('.commit_loader').remove();
                    commitrowBtn.removeClass('btn-default').addClass('btn-danger').attr('disabled', 'disabled');
                    commitrowBtn.find('.glyphicon-pencil').removeClass('glyphicon-pencil hidden').addClass('glyphicon-remove');
                }

                App.Views.importView.submittedData.push(rowIndex);

                $('#etrigue_form_container').html(data.etrReg);


                var etrigueForm = new EtrigueForm(979);
                etrigueForm.submitClassic("etrReg", function(dat) {
                    $('#etrigue_form_container').empty();

                    if(rowIndex + 1 < rowCount)
                    {
                        self.postData(rowIndex + 1, rowCount);
                    }
                    else
                    {
                        $('#import_menu_submit').prop("disabled", false);
                    }
                });
            });
        }

        // else if the rowIndex is in the submittedData array, don't submit it again and go to next row
        else
        {
            if(rowIndex + 1 < rowCount)
            {
                self.postData(rowIndex + 1, rowCount);
            }
            else
            {
                $('#import_menu_submit').prop("disabled", false);
            }
        }
    },

    onChange : function()
    {
        this.on('formId:change', function(model, editor){
            $('.form_import select').each(function(){
                $(this).val(editor.getValue());
            });
        });

        this.on('owner:change', function(model, editor){
            $('.owner_import select').each(function(){
                $(this).val(editor.getValue());
            });
        });

        this.on('preset:change', function(form, editor){
            $('.preset_import select').each(function(){
                $(this).val(editor.getValue());
            });
        });

        this.on('ct:change', function(form, editor){
            $('.ct_import select').each(function(){
                $(this).val(editor.getValue());
            });
        });

        $('#import_menu_cancel').on('click', function(){
            $('#import_menu_submit').prop("disabled", false);
            $('#contentContainer').animate({'left':'0', width: '100%'}).css({overflow: 'visible'});
            $('#importMenuContainer').animate({'left':'-400'}, function(){
                $('#importMenuContainer').toggleClass('menu_active');
                App.resetImportMenu();
            });
            App.Views.importView.reset();
        });

        return this;
    }

});