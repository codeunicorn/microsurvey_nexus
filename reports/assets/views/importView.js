App.Views.ImportView = Backbone.View.extend({

    initialize : function(attrs)
    {
        this.container = $('#importContainer');
        return this;
    },

    render : function(tpl)
    {
        var template = _.template($('#' + tpl).html());
        this.$el.html(template);
        this.container.html(this.$el);
        return this;
    },

    initForm : function()
    {
        var $this = this;

        var ul = $('#upload ul');

        $('#drop a').click(function(){
            // Simulate a click on the file input button
            // to show the file browser dialog
            $(this).parent().find('input').click();
        });

        // Initialize the jQuery File Upload plugin
        $('#upload').fileupload({

            // This element will accept file drag/drop uploading
            dropZone: $('#drop'),

            // This function is called when a file is added to the queue;
            // either via the browse button, or via drag/drop:
            add: function (e, data) {

                var self = $this;

                var tpl = $('<li class="working"><input type="text" value="0" data-width="48" data-height="48"'+
                    ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');

                // Append the file name and file size
                tpl.find('p').text(data.files[0].name)
                    .append('<i>' + self.formatFileSize(data.files[0].size) + '</i>');

                // Add the HTML to the UL element
                data.context = tpl.appendTo(ul);

                // Initialize the knob plugin
                tpl.find('input').knob();

                // Listen for clicks on the cancel icon
                tpl.find('span').click(function(){

                    if(tpl.hasClass('working')){
                        jqXHR.abort();
                    }

                    tpl.fadeOut(function(){
                        tpl.remove();
                    });

                });

                // Automatically upload the file once it is added to the queue
                var jqXHR = data.submit();
            },

            progress: function(e, data){

                // Calculate the completion percentage of the upload
                var progress = parseInt(data.loaded / data.total * 100, 10);

                // Update the hidden input field and trigger a change
                // so that the jQuery knob plugin knows to update the dial
                data.context.find('input').val(progress).change();

                if(progress == 100){
                    data.context.removeClass('working');
                }
            },

            fail:function(e, data){
                // Something has gone wrong!
                data.context.addClass('error');
            },

            done:function(e, data)
            {
                //console.log(data);

                var self = $this,
                    myData = $.parseJSON(data.result);

                if(myData.status === 'success')
                {
                    if(myData.type === 'manual')
                    {
                        var that = self;
                        self.$el.fadeOut('fast', function(){

                            that.render('importDatatablesTemplate').initDatatables(myData);
                            that.$el.fadeIn('fast');

                        });
                    }

                    if(myData.type === 'opportunities')
                    {
                        data.context.addClass('success').append('<div class="message">' + myData.message + '</div>');
                    }
                }
                else
                {
                    data.context.addClass('error').append('<div class="message">' + myData.message + '</div>');
                }
            }

        });

        // Prevent the default action when a file is dropped on the window
        $(document).on('drop dragover', function (e) {
            e.preventDefault();
        });
    },

    initDatatables : function(data) {

        // modify our data for form, origin, seg, and div to select dropdowns
        for(var n in data.data)
        {
            var form = data.data[n].form,
                owner = data.data[n].owner,
                preset = data.data[n].preset,
                ct = data.data[n].ct;

            data.data[n].form = App.getFormSelect(form, false);
            data.data[n].owner = App.getOwnerSelect(owner, true);
            data.data[n].preset = App.getPresetSelect(preset, true);
            data.data[n].ct = App.getChannelSelect(ct, true);
        }

        this.datacount = data.datacount;
        this.submittedData = [];

        this.oTable = $('#importDatatablesTable').dataTable({
            data : data.data,
            columns : data.columns,
            sDom : 't',
            bPaginate: false,
            ordering: false,
            columnDefs: [
                {
                    // The `data` parameter refers to the data for the cell (defined by the
                    // `data` option, which defaults to the column being worked with, in
                    // this case `data: 0`.
                    render: function ( data, type, row ) {
                        return '<div>'+ data +'</div>';
                    },
                    targets: "_all"
                }
            ]
        });

        this.setClickEvents();

    },

    reset : function() {

        var self = this;

        this.$el.fadeOut('fast', function(){

            self.render('importUploadTemplate').initForm();
            self.$el.fadeIn('fast');

        });
    },

    setClickEvents : function() {

        var self = this;

        $('#importDatatablesTable tbody tr').on('click', '.commit_row_btn', function(e){
            e.preventDefault();
            var $this = $(this);
            var theRow = $this.closest('tr');
            var myRow = self.oTable.api().row(theRow),
                rowData = myRow.data(),
                rowIndex = rowData.index;

            var myImg = $('<img class="commit_loader">'); //Equivalent: $(document.createElement('img'))
            myImg.attr('src', 'assets/images/select2-spinner.gif');

            // if the rowIndex is not in the submittedData array, submit it
            if(self.submittedData.indexOf(rowIndex) === -1)
            {

                // form html
                var formhtml = $this.parent().parent().siblings(".form_import").find("select").val();
                rowData.form = formhtml;

                // owner html
                var ownerhtml = $this.parent().parent().siblings(".owner_import").find("select option:selected").text();
                rowData.owner = ownerhtml;

                // preset html
                var presethtml = $this.parent().parent().siblings(".preset_import").find("select").val();
                rowData.preset = presethtml;

                // channel html
                var cthtml = $this.parent().parent().siblings(".ct_import").find("select").val();
                rowData.ct = cthtml;

                $this.attr('disabled', 'disabled').append(myImg).find('.glyphicon-pencil').addClass('hidden');

                var that = self;

                $.post( "api.php?request=import_data", {"data" : rowData}, function( dat ) {
                    var data = $.parseJSON(dat);

                    if(data.status === 'success')
                    {
                        $this.find('.commit_loader').remove();
                        $this.removeClass('btn-default').addClass('btn-success').attr('disabled', 'disabled');
                        $this.find('.glyphicon-pencil').removeClass('glyphicon-pencil hidden').addClass('glyphicon-ok');

                        that.submittedData.push(rowIndex);
                    }
                    else
                    {
                        $this.find('.commit_loader').remove();
                        $this.removeClass('btn-default').addClass('btn-danger').attr('disabled', 'disabled');
                        $this.find('.glyphicon-pencil').removeClass('glyphicon-pencil hidden').addClass('glyphicon-remove');
                    }

                    if(data.submitEtrigue)
                    {
                        $('#etrigue_form_container').html(data.etrReg);

                        var etrigueForm = new EtrigueForm(979);
                        etrigueForm.submitClassic("etrReg", function(dat) {
                            $('#etrigue_form_container').empty();
                        });
                    }


                });
            }



        });

    },

    // Helper function that formats the file sizes
    formatFileSize : function(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }

        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }

        return (bytes / 1000).toFixed(2) + ' KB';
    }

});