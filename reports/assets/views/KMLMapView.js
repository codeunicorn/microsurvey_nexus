App.Views.KMLMap = Backbone.View.extend({

    hasRendered: false,

    el: $('#nexus_map'),

    render: function()
    {
        if(! this.hasRendered)
        {
            this.$el.height($('#contentContainer').height());

            nexusKml.initSelects(nexusKml.dropdowns);

            var mapOptions = {
                center: new google.maps.LatLng( 37.09024, -95.712891 ),
                zoom: 4
            };

            nexusKml.map = new google.maps.Map(document.getElementById("nexus_map"),
                mapOptions);

            google.maps.event.addListener( nexusKml.map, 'zoom_changed', nexusKml.loadMarkersAndOverlays );

            this.hasRendered = true;
        }
    },

    showLoading : function()
    {
        $('#nexus_map').addClass('blurred');
        this.loadingHtml = $("<div id='loadingOverlay'><div>Loading</div></div>");
        $('#kmlMenuContainer form').append(this.loadingHtml);
        return this;

    },

    hideLoading : function()
    {
        $('#nexus_map').removeClass('blurred');
        this.loadingHtml.remove();
        return this;
    }

});