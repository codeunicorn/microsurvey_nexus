/**
 * Created with JetBrains PhpStorm.
 * User: jsantos317
 * Date: 5/24/14
 * Time: 5:08 PM
 * To change this template use File | Settings | File Templates.
 */

// Extend our form editor to include DatePicker as a schema option
Backbone.Form.editors.DatePicker = Backbone.Form.editors.Text.extend({

    render: function() {
        // Call the parent's render method
        Backbone.Form.editors.Text.prototype.render.call(this);
        // Then make the editor's element a datepicker.
        this.$el.datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            weekStart: 1
        });
        this.$el.datepicker('setDate', new Date());

        return this;
    },

    // The set value must correctl
    setValue: function(value) {
        this.$el.val(moment(value).format('YYYY-MM-DD'));
    }
});