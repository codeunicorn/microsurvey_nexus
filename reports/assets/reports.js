/**
 * Created with JetBrains PhpStorm.
 * User: jsantos317
 * Date: 5/24/14
 * Time: 10:34 AM
 */

var models = App.Models;
var views = App.Views;

// create our user model
models.user = new models.UserModel();

// create our login view
views.loginForm = new views.LoginFormView({
    template: _.template($('#loginTemplate').html()),
    model : models.user,
    idPrefix : 'login-',
    container : $('#loginContainer')
});

// Create a new Menu model
models.myMenu = new models.MenuModel({type: 'basic'});

// create a new menu view
views.menu = new views.MenuForm({
    template: _.template($('#formTemplate').html()),
    model : models.myMenu,
    idPrefix : 'menu-',
    container : $('#menuContainer'),
    templateData: {domain: window.domain},
    toggleButton : $('#state_button'),
    toggleSelect : $('.dropdown-menu a')
});

// create our kml menu model and view
models.kmlMenuModel = new models.KmlMenuModel();

views.kmlMenu = new views.KmlMenuFormView({
    template: _.template($('#klmMenuTemplate').html()),
    model : models.kmlMenuModel,
    idPrefix : 'menu-',
    container : $('#kmlMenuContainer'),
    toggleButton : $('#state_button'),
    toggleSelect : $('.dropdown-menu a')
});

// create our importt menu model and view
models.importMenuModel = new models.ImportMenu();

views.importMenu = new views.ImportMenuFormView({
    template: _.template($('#importMenuTemplate').html()),
    model : models.importMenuModel,
    container : $('#importMenuContainer'),
    toggleButton : $('#state_button'),
    toggleSelect : $('.dropdown-menu a')
});

views.headerMenu = new views.headerMenuView({
    template: _.template($('#headerMenuTemplate').html())
});

// create our datatable model and view
models.dataTablesModel = new models.DataTablesModel(window.dataTablesData);
views.dataTablesView = new views.DataTablesView({
    template : _.template( $("#DataTablesTemplate").html()),
    model : models.dataTablesModel,
    elem : 'dataTables',
    container : $('#dataTablesContainer')
});


// create our highchart models
models.highChartsStackedAreaModel = new models.HighChartsModel({type : 'PieChartByEffectiveValueDashboard'});
models.highChartsBarChartModel = new models.HighChartsModel({type: 'BarChartByCategoryDashboard'});
models.highChartsHeatMapModel = new models.HighChartsModel({type: 'HighMapByRegionDashboard'});
models.highChartsHighMapModel = new models.HighChartsModel({type: 'HighMapByStateDashboard'});

// create our highchart views
views.highchartsView = new views.HighChartsView({container : $('#dashboardStackedArea'), model : models.highChartsStackedAreaModel});
views.highchartsView2 = new views.HighChartsView({container : $('#dashboardDoughnutChart'), model : models.highChartsBarChartModel});
views.highchartsView3 = new views.HighChartsView({container : $('#dashboardHeatMap'), model : models.highChartsHeatMapModel});
views.highchargsView4 = new views.HighChartsView({container : $('#dashboardHighMap'), model : models.highChartsHighMapModel});

// create our kml map view
views.mykml = new views.KMLMap();

// create our import view
views.importView = new views.ImportView();

// create our modal view to be used if we need a modal popup
views.myModal = new views.modalView();

// start our app
App.start();
