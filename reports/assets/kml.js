var nexusKml = {

    // this will hold our map object from google maps
    map : null,

    // this array will hold our marker objects that we're going to place into our map object
    markers : [],

    // this will hold our overlay layer for the map
    layers : [],

    dropdowns : window.kmldata,

    initSelects : function(dropdownsJson)
    {
        var isCleared = false;

        // let's load the maps
        for(var i in dropdownsJson)
        {
            var item = dropdownsJson[i];

            // this is the jquery element that this dropdown belongs to
            var element = jQuery('#' + item.id);

            if(!isCleared)
            {
                element.empty();
                isCleared = true;
            }

            var option = jQuery('<option>', {
                value: item.name,
                html: item.name !== '' ? item.name : 'None/Catch-all',
                selected: item.selected
            });

            // if this item has javascript triggers
            if(item.hasOwnProperty('js'))
            {
                for(var attribute in item.js)
                {
                    element.attr(attribute, item.js[attribute]);
                }
            }

            element.append(option);

            // if this item has children, we want to invoke an 'onchange' function
            if(item.hasOwnProperty('children') && !jQuery.isEmptyObject(item.children))
            {
                if(item.selected)
                {
                    nexusKml.initSelects(item.children);
                }
            }
        }
    },

    reloadSelect: function(elem)
    {
        // let's get our dropdown data again
        var dropdownsJson = nexusKml.dropdowns;

        var $elem = jQuery(elem);

        // our element's id and value
        var elemId = $elem.attr('id');
        var elemVal = $elem.val();

        if(elemId == 'controls_map')
        {
        // iterate through our data to get the match for this element
        for(var n in dropdownsJson)
        {
        var item = dropdownsJson[n];

        if(item.id == elemId && item.name == elemVal)
        {
        // if the item has children
        if(item.hasOwnProperty('children') && !jQuery.isEmptyObject(item.children))
        {
        // get our children for this dropdown data
        var childrenData = item.children;

        var childIdsArray = [];

        for(var i in childrenData)
        {
            if(childIdsArray.indexOf(childrenData[i].id) == -1)
            {
                childIdsArray.push(childrenData[i].id);
            }
        }

        for(var i = 0, len = childIdsArray.length; i < len; i++)
                            {
                                // get the element id of the children
                                var childElem = jQuery('#'+ childIdsArray[i]);
                                childElem.empty();
                                }

            nexusKml.initSelects(childrenData);
            }
            }
            }
        }


            else if(elemId == 'controls_seg')
            {
                var mapId = 'controls_map',
                mapVal = jQuery('#controls_map').val();

                // iterate through our data to get the match for this element
                for(var n in dropdownsJson)
                {
                var item = dropdownsJson[n];

                if(item.id == mapId && item.name == mapVal)
                {
                // if the item has children
                if(item.hasOwnProperty('children') && !jQuery.isEmptyObject(item.children))
                {
                // get our children for this dropdown data
                var childrenData = item.children;

                for(var m in childrenData)
                {
                var itemm = childrenData[m];

                if(itemm.id == elemId && itemm.name == elemVal)
                {
                // if the item has children
                if(itemm.hasOwnProperty('children') && !jQuery.isEmptyObject(itemm.children))
                {
                // get our children for this dropdown data
                var childrenDatam = itemm.children;

                var childIdsArray = [];

                for(var i in childrenDatam)
                {
                if(childIdsArray.indexOf(childrenDatam[i].id) == -1)
                {
                childIdsArray.push(childrenDatam[i].id);
                }
            }

            for(var i = 0, len = childIdsArray.length; i < len; i++)
                                        {
                                            // get the element id of the children
                                            var childElem = jQuery('#'+ childIdsArray[i]);
                                            childElem.empty();
                                            }

                nexusKml.initSelects(childrenDatam);
                }
                }
                }
                }
                }
                }

                }
                },

                loadMap : function()
        {
            // get our data
            var data = {'map' : jQuery('#controls_map').val(), 'seg' : jQuery('#controls_seg').val(), 'div' : jQuery('#controls_div').val()};

                // load via ajax
                jQuery.ajax({
                    type: 'GET',
                    url: 'api.php?request=get_kml',
                    cache: false,
                    async: true,
                    global: false,
                    crossDomain: true,
                    dataType: "json",
                    data: data,

                    success: function(dat)
                    {
                    nexusKml.setMarkersAndOverlays(dat);
                    }
                });
                },

        setMarkersAndOverlays : function(dat)
        {
            for(var n in dat)
            {
                var myLatlng = new google.maps.LatLng(dat[n].lat, dat[n].long);

                var marker = new google.maps.Marker({
                    position: myLatlng,
                    title: dat[n].title,
                    level: dat[n].level,
                    region: dat[n].region,
                    icon: dat[n].icon,
                    code: dat[n].code,
                    color: dat[n].color,
                    isParent: dat[n].isParent,
                    owner: dat[n].owner,
                    owner_title: dat[n].owner_title,
                    owner_email: dat[n].owner_email
                });

                nexusKml.setInfoWindow(marker);

                nexusKml.markers.push(marker);

            }

            // load markers
            this.loadMarkers( );

            // load overlays
            this.setOverlays( 'countries', 0);
            this.setOverlays( 'canada_province', 1 );
            this.setStatesOverlay( );

        },

        /**
         *  This function is called after every zoom change event.
         */
        loadMarkersAndOverlays: function()
        {
            nexusKml.loadMarkers();
            nexusKml.loadOverlays();
        },

        /**
         *  This function loads the markers into google maps without having to redownload the data.
         *  This is called the first time google map data is downloaded and after every zoom change event.
         */
        loadMarkers : function( )
        {
            var zoomLevel = nexusKml.map.getZoom( );

            for(var n in nexusKml.markers)
            {
                var marker = nexusKml.markers[n];

                // for zoom level 4 or less, show only level 0 markers
                if( zoomLevel < 5 )
                {
                    if(marker.level == 0)
                    {
                        marker.setMap(nexusKml.map);
                    }
                    else
                    {
                        marker.setMap(null);
                    }
                }

                // for zoom level 5 or 6, show only level 1 markers or level 0 markers with no children (not a parent)
                else if( zoomLevel >= 5 && zoomLevel <= 6 )
                {
                    if(marker.level == 1 || ( marker.level == 0 && marker.isParent == false ) )
                    {
                        marker.setMap(nexusKml.map);
                    }
                    else
                        marker.setMap(null);
                }

                // for zoom 6 or more, show only level 2 markers or level 0 or 1 markers with no children (not a parent)
                else if( zoomLevel > 6 )
                {
                    if(marker.level == 2 || ( ( marker.level == 0 || marker.level == 1 ) && marker.isParent == false ) )
                    {
                        marker.setMap(nexusKml.map);
                    }
                    else
                        marker.setMap(null);
                }

            }

        },

        setInfoWindow: function(marker)
        {
            // add our infowindows
            var owner = marker.owner == null || marker.owner == '' ? "No owner assigned for this territory" : marker.owner;
            var title = marker.owner_title == null || marker.owner_title == '' ? "" : marker.owner_title;
            var email = marker.owner_email == null || marker.owner_email == '' ? "" : marker.owner_email;

            var info = "<div class='content'>" +
                    "<h4 class='title'>" + marker.title + "</h4>" +
                    "<div class='owner_name'>" + owner + "<div>" +
                    "<div class='owner_title'>" + title + "<div>" +
                    "<div class='owner_email'>" + email + "<div>" +
                "</div>";

            var infowindow = new google.maps.InfoWindow({
                content: info
            });

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(nexusKml.map, marker);
            });
        },

        /**
         *  Initializes the overlay layer to be added to the map.
         *  Stores each layer in an array, nexusKml.layers, to
         *  be loaded into the map (called by loadOverlays() ).
         */
        setOverlays: function(type, level)
        {
            var filename    =   '',
                tag         =   '';

            if(type == 'countries')
            {
                filename = 'countries_polygon.kml';
                tag = 'name';
            }
            else if(type == 'canada_province')
            {
                filename = 'canada_province.kml';
                tag = 'value';
            }

            // Let's load our counries overlay first
                // Read countries data from countries_polygon.kml
                jQuery.get("/plugins/GoogleMaps/" + filename, {}, function(data) {

                    // let's find each Placemark nodes that contains each country
                    jQuery(data).find("Placemark").each(function() {

                        // our country string
                        var country = this.getElementsByTagName(tag)[0].textContent;

                        // this flag let's us know if we should add the country in this Placemark
                        var shouldAddCountry = false;

                        var colour = '';
                        var isParent = false;
                        var region = '';
                        var theLevel = '';

                        // we go through each marker and check if our country matches the marker's region
                        for(var n in nexusKml.markers)
                        {
                            var marker = nexusKml.markers[n];

                            if(marker.region == country && marker.level == level)
                            {
                                shouldAddCountry = true;
                                colour = '#'+ marker.color;
                                isParent = marker.isParent;
                                theLevel = marker.level;
                                region = marker.region;
                            }
                        }

                        if(shouldAddCountry)
                        {
                            var Polygons = this.getElementsByTagName("Polygon");
                            for (var n = 0; n < Polygons.length; n++)
                            {
                            var Polygon = Polygons[n];
                            var coordinatesString = Polygon.getElementsByTagName("coordinates")[0].textContent;
                            var coordinatesArray = coordinatesString.split(" ");

                            var pts = [];
                            for (var i = 0; i < coordinatesArray.length; i++) {
                            var coords = coordinatesArray[i].split(',');
                            pts[i] = new google.maps.LatLng( parseFloat(coords[1]), parseFloat(coords[0]) );
                        }

                        var theLayer = new google.maps.Polygon({
                            paths: pts,
                            strokeColor: '#000000',
                            strokeOpacity: 0.8,
                            strokeWeight: 1,
                            fillColor: colour,
                            fillOpacity: 0.5,
                            isParent: isParent,
                            level: theLevel,
                            region: region
                        });

                // store the layers into our layers array
                nexusKml.layers.push(theLayer);
                }
                }
                });

                });
                },

                setStatesOverlay: function()
        {
            // Read the data from states.xml
            jQuery.get("/plugins/GoogleMaps/states_polygon.xml", {}, function(data) {

                    jQuery(data).find("state").each(function() {

                        var state = this.getAttribute('name');
                        var shouldAddState = false;

                        var colour = this.getAttribute('colour');
                        var isParent = false;
                        var level = 0;

                        for(var n in nexusKml.markers)
                        {
                            var marker = nexusKml.markers[n];

                            if(marker.region == state && marker.level == 1)
                            {
                                shouldAddState = true;
                                colour = '#'+ marker.color;
                                isParent = marker.isParent;
                                level = marker.level;
                            }
                        }

                        if( shouldAddState )
                        {
                            var points = this.getElementsByTagName("point");
                            var pts = [];
                            for (var i = 0; i < points.length; i++) {
                                pts[i] = new google.maps.LatLng(parseFloat(points[i].getAttribute("lat")), parseFloat(points[i].getAttribute("lng")));
                            }

                            var theLayer = new google.maps.Polygon({
                                paths: pts,
                                strokeColor: '#000000',
                                strokeOpacity: 0.8,
                                strokeWeight: 1,
                                fillColor: colour,
                                fillOpacity: 0.5,
                                isParent: isParent,
                                level: level
                            });

                            // store the layers into our layers array
                            nexusKml.layers.push(theLayer);

                        }
                    });

                // load our overlays
                nexusKml.loadOverlays();

                });
                },

                loadOverlays: function()
        {
            var zoomLevel = nexusKml.map.getZoom();

            for(var i = 0, layersLength = nexusKml.layers.length; i < layersLength; i++)
            {
                var theLayer = nexusKml.layers[i];

                // for zoom levels less than 5, we only want to show the countries (level 0)
                if(zoomLevel < 5 && theLayer.level == 0)
                {
                    theLayer.setMap(nexusKml.map);
                }

                // for zoom levels greater than 6, we want to show the states (level 1) and countries with no states
                else if(zoomLevel >= 5 && (theLayer.level == 1 || (theLayer.level == 0 && theLayer.isParent == false )))
                {
                    theLayer.setMap(nexusKml.map);
                }

                else
                {
                    theLayer.setMap(null);
                }
            }

                nexusKml.hideLoading();
                },

                loadStyles: function(column ,level)
        {
            var styles = [];
            var multiples = [];

            // foreach of our markers, get the ones that have multiple placemark colors assigned
            var sorter = nexusKml.sortByMarkerColors();

            // we're allowed 5 styles in a layer,
            var styler = {};

                var count = 0;
                while(count < 5 && count < sorter.length)
            {
                var color = sorter[count][0];

                for(var n in nexusKml.markers)
                {
                var marker = nexusKml.markers[n];

                if(marker.level == level && marker.code != '' && marker.color == color)
                {
                if(styler.hasOwnProperty(color))
                {
                styler[color].push(marker.code);
                }
                    else
                        {
                            styler[color] = [];
                            styler[color].push(marker.code);
                            }
                    }
                    }
                    count++;
                    }

                    for(var n in styler)
            {
                var arr = styler[n];
                var where = column +" IN ("+ "'" + arr.join("', '") + "'" +")";
                var fillColor = '#' + n;

                var styleObj = {

                where: where,
                polygonOptions: {
                fillColor: fillColor,
                fillOpacity: 0.2
                }
                    }

                    styles.push(styleObj);

                    }

                    return styles;
                    },

                    removeOverlay: function()
        {
            nexusKml.layer.setMap(null);
            },

                    sortByMarkerColors: function()
        {
            var sorter = {};
                    var region = '';

                    for(var n in nexusKml.markers)
            {
                var marker = nexusKml.markers[n];

                if(marker.level == 0)
                region = marker.region;

                if(marker.level == 1 && region == 'USA')
                {
                // if the color is already a property, increase the count
                if( sorter.hasOwnProperty(marker.color))
                sorter[marker.color] += 1;
                else
                sorter[marker.color] = 1;
                }
                    }

                    var sortable = [];
                    for (var color in sorter)
                    sortable.push([color, sorter[color]])
                    sortable.sort(function(a, b) {return b[1] - a[1]})

                    return sortable;
                    },

                    displayLoading: function(msg)
        {
            jQuery('#loading').html(msg);
            },

                    hideLoading: function()
        {
            jQuery('#loading').html("");
        },

        clearMap: function()
        {
            for (var i = 0; i < nexusKml.markers.length; i++) {
                nexusKml.markers[i].setMap(null);
                nexusKml.markers[i] = null;
            }

            for (var i = 0; i < nexusKml.layers.length; i++) {
                nexusKml.layers[i].setMap(null);
                nexusKml.layers[i] = null;
            }

            nexusKml.markers = [];
            nexusKml.layers = [];
        }

    }

    jQuery( '#controls_submit' ).click( function( e ) {
        nexusKml.clearMap( );
        nexusKml.loadMap( );
        e.preventDefault( );
    });

    function initialize( ) {

        nexusKml.initSelects(nexusKml.dropdowns);

        var mapOptions = {
            center: new google.maps.LatLng( 37.09024, -95.712891 ),
            zoom: 4
        };

        nexusKml.map = new google.maps.Map(document.getElementById("nexus_map"),
            mapOptions);

        google.maps.event.addListener( nexusKml.map, 'zoom_changed', nexusKml.loadMarkersAndOverlays );

    }