<?php
// Demo Nexus v2.0 - Jareb Coupe 2013
// ### Form Builder (core) ###
// This is the core of the demo nexus, it parses the form profile array and builds the form

// Common initialization
// define all of our project specific constants
function siteProtocol()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    return $protocol;
}
defined('PROTOCOL') or define( 'PROTOCOL', siteProtocol() );
defined('NEXUS_INTERNAL_ROOT') or define('NEXUS_INTERNAL_ROOT', realpath(dirname(__FILE__)) .'/');
require_once(NEXUS_INTERNAL_ROOT .'/common_init.php');

/****************************************
 * Error Checker
 * **************************************/
$e = new ErrorChecker();

// Check for browser version cut off (IE8)
if( $e->checkBrowser( )->isError( ) )
{
    // let's override the default error message with the one from our profiles
    if( isset( $nexus->espBrowser ) && $nexus->espBrowser->value)
        $e->set_error_message( "<h3 class='form-error' style='color:red;'>". $nexus->espBrowser->label ." <a href='mailto:". $nexus->espReportErrorsTo->value ."'>". $nexus->espReportErrorsTo->value ."</a></h3>" );

    echo $e->get_error_message( );
}
// If any key elements are missing from the querystring, let's bail gracefully
else if( $e->checkQuerystring( $nexus->form->code, $nexus->prof->code )->isError( ) )
{
    if( isset( $nexus->espFatalError ) && $nexus->espFatalError->value)
        $e->set_error_message( "<h3 class='form-error' style='color:red;'>Error Code: 202. ". $nexus->espFatalError->label ." <a href='mailto:". $nexus->espReportErrorsTo->value ."'>". $nexus->espReportErrorsTo->value ."</a></h3>" );

    echo $e->get_error_message( );
}
// if the profile doesn't exist in profiles.csv
else if(! isset($nexus->form->code) || empty($nexus->form->code))
{
    $e->set_error_message( "<h3 class='form-error' style='color:red;'>Error Code: 203. Missing form profile.</h3>" );
    echo $e->get_error_message( );
}

/****************************************
 * END Error Checker
 * **************************************/

// no errors, display form or confirmation
else {
    echo '<noscript><h3 style="color:red;margin:0 0 20px 0">We\'ve detected that your javascript may be disabled. Javascript is required in order to fill out the form below. Please refer to your browser manual on how to enable javascript.</h3></noscript>';

    $useBootstrap = isset($_GET['bootstrap']) && $_GET['bootstrap'] == 'true';

    // This random number will allow us to add multiple nexus forms on the same page with different IDs
    $multiple = isset($_GET['multiple']) && !empty($_GET['multiple']) ? $_GET['multiple'] : '';

    // This is the ID of the container
    $containerId = isset($_GET['multiple']) && !empty($_GET['multiple']) ? 'nexus'. $_GET['multiple'] : 'nexus';

    if($useBootstrap) {
        echo '<div id="'. $containerId .'">'. $nexus->renderBootstrap($multiple) . '</div>';
    }
    else {
        echo '<div id="'. $containerId .'">'. $nexus->render($multiple) . '</div>';
    }
}
?>

<script src="<?php echo PROTOCOL; ?>www.google-analytics.com/urchin.js" type="text/javascript"></script>
<script type="text/javascript">_uacct = "UA-1552062-1";urchinTracker();</script>
<script type="text/javascript">
    var nexusMultiple = "<?php echo $multiple; ?>";

    window['protocol'] = "<?php echo PROTOCOL; ?>";
    window["nexusVars" + nexusMultiple] = [];
    window["nexusVars" + nexusMultiple]["config_report_errors_to"] = "<?php echo (isset($config_report_errors_to) ? $config_report_errors_to : 'false'); ?>";
    window["nexusVars" + nexusMultiple]["config_report_text_label"] = "<?php echo $config_report_text_label; ?>";
    window["nexusVars" + nexusMultiple]["config_required_list_label"] = "<?php echo (isset($config_required_list_label) ? $config_required_list_label : ''); ?>";
    window["nexusVars" + nexusMultiple]["config_valid_email_label"] = "<?php echo $config_valid_email_label; ?>";
    window["nexusVars" + nexusMultiple]["config_valid_privacy_label"] = "<?php echo $config_valid_privacy_label; ?>";
    window["nexusVars" + nexusMultiple]["config_required_list"] = "<?php echo (isset($config_required_list) ? $config_required_list : ''); ?>";
    window["nexusVars" + nexusMultiple]["useBootstrap"] = "<?php echo $useBootstrap; ?>";

<?php
$required_list = isset($config_required_list) ? explode("||", $config_required_list) : array();
    foreach($required_list as $key => $val)
    {
        $label = ${$val . '_label'};
        echo 'window["nexusVars" + nexusMultiple]["'. $val . $multiple. '_label"] = "'. $label . '";';
    }
?>
    window["nexusVars" + nexusMultiple]["x_udf_privacy_policy_version143"] = "<?php echo $x_udf_privacy_policy_version143; ?>";
    window["nexusVars" + nexusMultiple]["querystring_code"] = "<?php echo $querystring_code; ?>";
    window["nexusVars" + nexusMultiple]["origin_override"] = "<?php echo (isset($origin_override) ? $origin_override : "false"); ?>";
    window["nexusVars" + nexusMultiple]["origin_value"] = "<?php echo $origin_value; ?>";
    window["nexusVars" + nexusMultiple]["origin_code"] = "<?php echo $origin_code; ?>";
    window["nexusVars" + nexusMultiple]["privacy_policy"] = '<?php echo $nexus->getPrivacyPolicy(); ?>';
    window["nexusVars" + nexusMultiple]["adwords_id"] = "<?php echo $adwords_id; ?>";
    window["nexusVars" + nexusMultiple]["adwords_code"] = "<?php echo $adwords_code; ?>";
    window["nexusVars" + nexusMultiple]["adwords_label"] = "<?php echo $adwords_label; ?>";
    window["nexusVars" + nexusMultiple]["internalRoot"] = "<?php echo NEXUS_INTERNAL_ROOT; ?>";
    window["nexusVars" + nexusMultiple]["externalRoot"] = "<?php echo NEXUS_EXTERNAL_ROOT; ?>";
    window["nexusVars" + nexusMultiple]["dropdownData"] = <?php echo $nexus->getDropdowns(); ?>;
    window["nexusVars" + nexusMultiple]["channelTactic"] = <?php echo json_encode($nexus->getChannelTactics());?>;
</script>
<script type="text/javascript" src="<?php echo PROTOCOL . NEXUS_EXTERNAL_ROOT; ?>scripts/nexus_ct.js"></script>
<script type="text/javascript" src="<?php echo PROTOCOL . NEXUS_EXTERNAL_ROOT; ?>scripts/nexus.modal.js"></script>
<script type="text/javascript" src="<?php echo PROTOCOL . NEXUS_EXTERNAL_ROOT; ?>scripts/nexus.js"></script>
