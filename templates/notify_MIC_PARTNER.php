[sender]us.marketing@leicaus.com[/sender]
[subject]Contact Form-Fill[/subject]
[body]
<font face='Verdana, Arial, Helvetica, sans-serif' size=2>
<b><font size=3>New Partner</font></b><br />
<b><font size=1 color=#848484>IP Address: [[ip]]</font></b><br />
<b><font size=1 color=#848484>Form Profile: [[form_profile]] (ID:[[espFormID]])</font></b><br />
<b><font size=1 color=#848484>Assigned To: [[owner_label]]</font></b><br /><br />
<b>Name:</b> [[x_firstname]] [[x_lastname]]<br />
<b>Company:</b> [[x_companyname]]<br />
<b>Title:</b> [[x_title]]<br />
<b>Address1:</b> [[x_address]]<br />
<b>Address2:</b> [[x_address2]]<br />
<b>City:</b> [[x_city]]<br />
<b>Prov/State:</b> [[x_state]]<br />
<b>Country:</b> [[x_country]]<br />
<b>ZIP/Postal Code:</b> [[x_zip]]<br />
<b>Phone:</b> [[x_phone]]<br />
<b>Mobile/Skype:</b> [[x_phone2]]<br />
<b>Email:</b> [[x_emailaddress]]<br />
<b>Comments:</b> [[x_udf_comments126]]<br /><br />
</font>
[/body]