[body]

<a href="http://nexus.microsurvey.com/status.php?form_key=[[auth_key]]&status=1"><img src="http://nexus.leica-geosystems.us/apps/nexus/images/notifications/accept.gif" height="35" width="183" border="0" alt="Accept" style="color: #e71429; font-size: 30px;"></a>

 &nbsp;  &nbsp; 

<a href="http://nexus.microsurvey.com/status.php?form_key=[[auth_key]]&status=2"><img src="http://nexus.leica-geosystems.us/apps/nexus/images/notifications/reject.gif" height="35" width="77" border="0" alt="Reject" style="color: #848484; font-size: 30px;"></a>

<BR><BR>

<ul>
<li>
	<font size=2 color=#848484><em>Press <font color="red"><strong>Accept</strong></font> if this is a new lead or a new engagement of an existing lead. Immediately after accepting, you will be taken to the "Create New Lead" page in Salesforce, with all possible fields auto-populated with the above information.</em></font>
</li>
<li>
	<font size=2 color=#848484><em>Press <font color="red"><strong>Reject</strong></font> if there is a recent duplicate of this lead, or if it was assigned to the wrong person/segment, or if it's not a valid lead (e.g., solicitation). After rejecting, you will be given options to choose the reason for rejection.</em></font>
</li>
</ul>

[/body]