[sender]us.marketing@leicaus.com[/sender]
[subject]Your Registration Info[/subject]
[body]
<table border="0" cellpadding="0" cellspacing="0" width="101%">
    <tbody>
    <tr>
        <td bgcolor="#cccccc">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                <tbody>
                <tr>
                    <td bgcolor="#6e6e6e" height="55">
                        <a href="http://www.leica-geosystems.us/en/index.htm">
                            <img alt="Leica Geosystem" height="55" src="http://assets.leica-geosystems.us/email/Leica/2016-04-12/images/header.gif" style="display:block; height:55px; border: 0;" width="600" />
                        </a>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#fff" height="200">
                        <img alt="I Spy" height="200" src="http://assets.leica-geosystems.us/GNSS_LP/GNSS-PureSurveying-header.png" style="display:block; height:200px; border: 0;" width="600" />
                    </td>
                </tr>
                <tr>
                    <td align="center" bgcolor="#f2f2f2" width="600" height="150"> <p style="font-family:Arial, sans-serif; font-size:31px; color:#939393;font-weight:normal; line-height: 34px;">YOU'RE ALL SET FOR...</p> </td>
                </tr>
                <tr>
                    <td align="center" bgcolor="#f2f2f2">
                        <table border="1" cellpadding="10" cellspacing="0" style="border-color:#ddd;">
                            <tbody>
                            <tr>
                                <td align="center" bgcolor="#f2f2f2" valign="top"><span style="font-family:Arial, sans-serif; font-size:20px; color:#333333;font-weight:400; line-height: 27px;">[[date]]</span></td>
                            </tr>
                            <tr>
                                <td align="center" bgcolor="#f2f2f2" valign="top" colspan="2">
                                    <table>
                                        <tr>
                                            <td align="center"><span style="font-family:Arial, sans-serif; font-size:16px; color:#999;font-weight:400; line-height: 27px;">LOCATION</span></td>
                                        </tr>
                                        <tr>
                                            <td align="center"><span style="font-family:Arial, sans-serif; font-size:20px; color:#333;font-weight:400; line-height: 27px;">[[address]]</span></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" bgcolor="#f2f2f2" valign="top" colspan="2">
                                    <table>
                                        <tr>
                                            <td align="center"><span style="font-family:Arial, sans-serif; font-size:16px; color:#999;font-weight:400; line-height: 27px;">YOUR CONTACT</span></td>
                                        </tr>
                                        <tr>
                                            <td align="center"><span style="font-family:Arial, sans-serif; font-size:20px; color:#333;font-weight:400; line-height: 27px;">[[contact]]<br>[[contact_email]]</span></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" bgcolor="#f2f2f2" width="600" height="150"> <p style="font-family:Arial, sans-serif; font-size:18px; color:#333;font-weight:normal; line-height: 34px;">Thank you for registering.<br>See your event details above.</p> </td>
                </tr>
                <tr>
                    <td bgcolor="#ffffff">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                            <tr>
                                <td bgcolor="#f1f2f2" width="300">
                                    <a href="http://www.hexagon.com/en/index.htm">
                                        <img alt="Leica Geosystems" height="80" src="http://assets.leica-geosystems.us/email/Leica/2015-09-08/images/footer_hexagon.jpg" style="display:block; height:80px; border: 0;" width="300" />
                                    </a>
                                </td>
                                <td bgcolor="#f1f2f2" style="color: #000000;" width="300">
                                    <a href="http://www.leica-geosystems.us/en/index.htm">
                                        <img alt="Leica Geosystems" height="80" src="http://assets.leica-geosystems.us/email/Leica/2015-09-08/images/footer_leica.jpg" style="display:block; height:80px; border: 0;" width="300" />
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<p style='font-family:Arial,Verdana,Helvetica,sans-serif;line-height:16px;color:#999999;font-size:9px;letter-spacing:0px;margin-top:14px;margin-bottom:14px;'>[[emailDisclaimer]]</p>
[/body]
