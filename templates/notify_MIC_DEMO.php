[sender]marketing@microsurvey.com[/sender]
[subject]Demo Request: [[product_label]][/subject]
[body]
<font face='Verdana, Arial, Helvetica, sans-serif' size=2>
<b><font size=3>[[form_label]]</font></b><br />
<b><font size=1 color=#848484><font color=#848484>IP Address:</font> <font color=#A8A8A8>[[ip]]</font></font></b><br />
<b><font size=1 color=#848484><font color=#848484>Form Profile &amp; ID:</font> <font color=#A8A8A8>[[form_profile]] (ID:[[espFormID]])</font></font></b><br />
<b><font size=1 color=#848484><font color=#848484>Channel &amp; Tactic:</font> <font color=#A8A8A8>[[ct_label]]</font></font></b><br />
<b><font size=1 color=#848484><font color=#848484>Primary Market Segment:</font> <font color=#A8A8A8>[[segment]]</font></font></b><br />
<b><font size=1 color=#848484><font color=#848484>Division Override:</font> <font color=#A8A8A8>[[division]]</font></font></b><br />
<b><font size=1 color=#848484>Assigned To: [[owner_label]] ([[owner_email]])</font></b><br />
<b><font size=1 color=#848484>Origin Code: [[origin]]</font></b><br /><br />

<b>Name:</b> [[x_firstname]] [[x_lastname]]<br />
<b>Company:</b> [[x_companyname]]<br />
<b>Title:</b> [[x_title]]<br />
<b>Address1:</b> [[x_address]]<br />
<b>Address2:</b> [[x_address2]]<br />
<b>City:</b> [[x_city]]<br />
<b>Prov/State:</b> [[x_state]]<br />
<b>Country:</b> [[x_country]]<br />
<b>ZIP/Postal Code:</b> [[x_zip]]<br />
<b>Phone:</b> [[x_phone]]<br />
<b>Mobile/Skype:</b> [[x_phone2]]<br />
<b>Email:</b> [[x_emailaddress]]<br /><br />
<b>Work type:</b> [[x_udf_extjobfunction]]<br />
<b>Heard about us from:</b> [[x_udf_extheardof]]<br />
<b>Purchase time:</b> [[x_udf_extpurchasetime]]<br />
<b>Licenses required:</b> [[x_udf_extlicensesreq]]<br /><br />
<b>Comments:</b> [[x_udf_comments126]]<br /><br />
</font>

[/body]