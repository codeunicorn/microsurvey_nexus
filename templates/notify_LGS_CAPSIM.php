<p>[sender]us.marketing@leicaus.com[/sender]
  [subject]Contact: [[x_companyname]] ([[espFormID]])[/subject]
  [body]
  <font face='Verdana, Arial, Helvetica, sans-serif' size=2>
  <b><font size=3>[[form_label]]</font></b><br />
  <b><font size=1 color=#848484><font color=#848484>IP Address:</font> <font color=#A8A8A8>[[ip]]</font></font></b><br />
  <b><font size=1 color=#848484><font color=#848484>Form Profile &amp; ID:</font> <font color=#A8A8A8>[[form_profile]] (ID:[[espFormID]])</font></font></b><br />
  <b><font size=1 color=#848484><font color=#848484>Channel &amp; Tactic:</font> <font color=#A8A8A8>[[ct_label]]</font></font></b><br />
  <b><font size=1 color=#848484><font color=#848484>Primary Market Segment:</font> <font color=#A8A8A8>[[segment]]</font></font></b><br />
  <b><font size=1 color=#848484><font color=#848484>Division Override:</font> <font color=#A8A8A8>[[division]]</font></font></b><br /><br />
    
  <b>Segment/Division:</b> [[preset]]<br />
  <b>Assigned To:</b> [[owner_label]] ([[owner_email]])<br />
  <b>Referrer/Secondary Source:</b> [[x_udf_ext_dealer375]]<br />
  <b>Event:</b> [[x_udf_event443]]<br /><br />

  <b>Name:</b> [[x_firstname]] [[x_lastname]]<br />
  <b>Company:</b> [[x_companyname]]<br />
  <b>Title:</b> [[x_title]]<br />
  <b>Address1:</b> [[x_address]]<br />
  <b>Address2:</b> [[x_address2]]<br />
  <b>City:</b> [[x_city]]<br />
  <b>Prov/State:</b> [[x_state]]<br />
  <b>Country:</b> [[x_country]]<br />
  <b>ZIP/Postal Code:</b> [[x_zip]]<br />
  <b>Phone:</b> [[x_phone]]<br />
  <b>Mobile/Skype:</b> [[x_phone2]]<br />
  <b>Email:</b> [[x_emailaddress]]<br /><br />
    
  <b>Existing customer:</b> [[x_udf_extcustomer]]<br />
  <b>Plans to buy in the next 6 months:</b> [[x_udf_ext_purchase_question405]]<br />
  <b>Plans to buy CAD or BIM in the next 6 months:</b> [[x_udf_ext_purchase_question__2_420]]<br />
  <b>Plans to buy scanner in the next 6 months:</b> [[x_udf_ext_purchase_question__3_421]]<br />
  <b>Plans to buy point cloud software in the next 6 months:</b> [[x_udf_ext_purchase_question__4_422]]<br />
  <b>Plans to buy TS or MS in the next 6 months:</b> [[x_udf_ext_purchase_question__5_423]]<br />
  <b>How are you currently performing layout?</b> [[x_udf_ext_bim_gc_question426]]<br />
  <b>What is your primary responsibility?</b> [[x_udf_ext_bim_gc_question__2_428]]<br />
  <b>Are you interested in buying a total station for layout or as-builting?</b> [[x_udf_ext_bim_gc_question__3_430]]<br />
  <b>Would you like a Leica layout expert to contact you?</b> [[x_udf_ext_bim_gc_question__4_432]]<br />
  <b>Would you like to be contacted?</b> [[x_udf_ext_contact_question441]]<br />
  <b>How are you currently performing layout?</b> [[x_udf_ext_bim_mep_question427]]<br />
  <b>What is your primary responsibility?</b> [[x_udf_ext_bim_mep_question__2_429]]<br />
  <b>Are you interested in buying a total station for layout?</b> [[x_udf_ext_bim_mep_question__3_431]]<br />
  <b>Would you like a Leica layout expert to contact your company about maximizing efficiency in construction layout processes?</b> [[x_udf_ext_bim_mep_question__4_433]]<br />
  <b>Are you planning on buying a laser scanning solution (software or hardware) in the next 6 months?</b> [[x_udf_ext_hds_question434]]<br />
  <b>Has a budget been allocated or is it in the process of being allocated for this purchase?</b> [[x_udf_ext_hds_question__2_435]]<br />
  <b>What office platform are you running?</b> [[x_udf_ext_surv_ground_question442]]<br /><br />
    
  <b>Topic:</b> [[x_udf_ext_last_topic364]]<br />
  <b>Area of interest:</b> [[x_udf_ext_interest401]]<br />
  <b>Area of interest:</b> [[x_udf_ext_interest__2_415]]<br />
  <b>Main Application:</b> [[app_label]]<br />
  <b>Currently using:</b> [[x_udf_ext_product_question413]]<br />
  <b>How they use scanning currently:</b> [[x_udf_ext_scanning_question414]]<br />
  <b>Product:</b> [[product_label]]<br /><br />
    
  <b>Comments:</b> [[x_udf_comments126]]<br /><br />
  
<font color="red">IMPORTANT INSTRUCTIONS:</font> Prospect has already received the download link and video resources; however they will need a key code to unlock the simulator so they can enter raw data values to generate points.<BR><BR>

<strong>Step 1</strong>: Order the software simulator key code through SAP.<BR>
<blockquote>&bull; Order part number 832206 from customer service. There is no charge for it, but each code does need to be ordered separately. Please note that this is a demo key for the simulator and will expire after one year.<BR>
&bull; This key code goes in the CLM, not in the application itself.<BR>
&bull; The CLM has to be installed and working ON THE INTERNET to register the key. If the customer is not connected to the Internet, they can't use the key.</blockquote>
<strong>Step 2</strong>: Email the prospect the information below: <BR><BR>
<blockquote><em>Thank you for your interest in test driving our new Captivate field software. Here is your key code to unlock the software so that you can enter raw data values: [enter key code]<BR><BR>
To begin using the simulator immediately, please follow these steps:<BR><BR>
1) Install the simulator<BR>
2) Verify your Internet connection<BR>
3) Open the CLM<BR>
4) Enter the key code<BR>
5) Verify functionality<BR><BR>
I will call you within the next week to set up an appointment to walk you through the simulator. In the meantime, please don't hesitate to reach out to me with any questions. Thanks again for your interest in Leica Geosystems Surveying Solutions.</em></blockquote>
<strong>Step 3</strong>: Call the prospect within seven business days of receiving this simulator download notification to follow up.<BR><BR>
<strong>Resources: </strong><BR><BR>
Simulator download:<BR>
<a href="http://leica.gs/capsim-jobs">http://leica.gs/capsim-jobs</a><BR><BR>
Videos:<BR>
<a href="http://bcove.me/nfrxpeo8">Captivate Getting Started</a><BR>
<a href="http://bcove.me/8czyke8h">Captivate 3D Viewer</a><BR>
<a href="http://bcove.me/vu6mwf10">Coding & Linework</a><BR>
<a href="http://bcove.me/ko6cejjw">Advanced Coding & Multi-Codes</a><BR>
<a href="http://bcove.me/lbk17r5o">Captivate Stake-Out</a><BR>
<a href="http://bcove.me/3b6dgh3h">Staking with a DTM</a><BR><BR><BR>

  </font>
  [/body]</p>
