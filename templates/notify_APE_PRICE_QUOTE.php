[sender]us.marketing@leicaus.com[/sender]
[subject]Price Quote Request: [[x_firstname]] [[x_lastname]] ([[x_emailaddress]]) - [[x_companyname]][/subject]
[body]
<font face='Verdana, Arial, Helvetica, sans-serif' size=2>
    <b><font size=3>Quote Request: [[x_companyname]]</font></b><br />
    <b><font size=1 color=#848484>IP Address: [[ip]]</font></b><br />
    <b><font size=1 color=#848484>Form Profile: [[form_profile]] (ID:[[espFormID]])</font></b><br />
    <b><font size=1 color=#848484>Assigned To: [[owner_label]] ([[owner_email]])</font></b><br /><br />
    <b>Name:</b> [[x_firstname]] [[x_lastname]]<br />
    <b>Company:</b> [[x_companyname]]<br />
    <b>City:</b> [[x_city]]<br />
    <b>Prov/State:</b> [[x_state]]<br />
    <b>Country:</b> [[x_country]]<br />
    <b>Phone:</b> [[x_phone]]<br />
    <b>Email:</b> [[x_emailaddress]]<br />
    <b>Product Page:</b> [[x_url]]<br />
    <b>Segment:</b> [[segment_label]]<br />
    <b>Comments:</b> [[x_udf_comments126]]<br /><br />
</font>
[/body]