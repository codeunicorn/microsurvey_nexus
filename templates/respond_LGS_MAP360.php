[sender][[owner_email]][/sender] [subject][[productSubject]][/subject]
[body]
<html>
<head>
<title>[[product_label]]</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
	body {
	width:100% !important;
	color: #ffffff;
	background:#f0f0f0; 
	font-family:arial; 
	font-size:13px; 
	-webkit-text-size-adjust:none; 
	-ms-text-size-adjust:none; 
	mso-line-height-rule:exactly; 
	margin:0; 
	padding:0;
	}
	table {
	border-collapse:collapse; 
	mso-table-lspace:0pt; 
	mso-table-rspace:0pt;
	}
	table td {border-collapse:collapse;}
	img {display:block;}
	.ExternalClass{display:inline-block; line-height: 100%;}
</style>
</head>
<body bgcolor="#f0f0f0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
		<td bgcolor="#f0f0f0">
<table width="600" border="0" cellpadding="0" cellspacing="0" align="center">

	
     <tr>
		<td colspan="2" bgcolor="#f1f2f2" align="top">
        	<table style="margin: 20px 0 0 0;"  cellspacing="0" cellpadding="0" border="0">
            	<tr>
                <td width="230"><div style="height:60px;"><a href='http://nexus.leica-geosystems.us/redir.php?orig=[[origin]]&pathvar=c2a_logo'><img src='http://nexus.leica-geosystems.us/images/origin/[[origin]]_c2a_logo.jpg' width='230' height='60' border='0'></a>
        
        </div>
       
        </td>
                <td valign="bottom" width="370" align="left" bgcolor="#f0f0f0" style="color: #4d4d4d;"><p style='font-family:Arial,Helvetica,sans-serif;font-size:14px; margin:20px 20px 0 20px;'><i>[[productTagLine]]</i></p>
			
                </td>
         
                </tr>
              
        	</table>
        </td>
    </tr>
    <tr>
		<td bgcolor="#ffffff" height="20">
			<img src="http://assets.leica-geosystems.us/email/Spacers/spacer_grey_600x20.jpg" width="600" height="20" alt="Leica Geosystems" style="display:block; height:20px; border: 0;"></td>
	</tr>
    <tr>
		<td colspan="3" bgcolor="#f1f2f2" align="top">
        	<table cellspacing="0" cellpadding="0" border="0">
            	<tr>
                <td valign="top" width="35" bgcolor="#ffffff"><img src="http://assets.leica-geosystems.us/email/Spacers/spacer-40x1.gif" width="40" height="1" alt="Spacer" style="display:block; height:1px; width:40px; border: 0;">
                
                </td>
                <td valign="top" align="left" width="520" bgcolor="#ffffff" style="color: #000000;"><p style='line-height:22px; font-family:Arial,Helvetica,sans-serif;font-size:14px; margin:40px 20px 0 20px;'>[[emailBody0]]</p>
                
			<p style='line-height:22px; font-family:Arial,Helvetica,sans-serif;font-size:14px; margin:20px 20px 40px 20px;'>[[emailBody1]]</p>
			
			

			
                </td>
        <td valign="top" width="35" bgcolor="#ffffff"><img src="http://assets.leica-geosystems.us/email/Spacers/spacer-40x1.gif" width="40" height="1" alt="Spacer" style="display:block; height:1px; width:40px; border: 0;">
                
                </td>     
                </tr>
              
        	</table>
        </td>
    </tr>
  
  
    <tr>
		<td bgcolor="#ffffff" align="center" width="600">
        	<table cellspacing="0" cellpadding="0" border="0">
            	<tr>
               
                    <td align="right" height="51" width="3" bgcolor="#ffffff"><img src="http://assets.leica-geosystems.us/email/Spacers/red_btn_left.jpg" width="3" height="51" alt="Spacer" style="display:block; height:51px; width:1px; border: 0;">
                
                </td>
                <td valign="middle" height="51" align="center" bgcolor="#ff0033" style="color: #000000; ">
<p style=' margin:0px 50px 0 50px;'><strong><a style="color: #ffffff; font-family:Arial,Helvetica,sans-serif;font-size:16px; text-decoration: none;" href='http://nexus.leica-geosystems.us/redir.php?orig=[[origin]]&pathvar=productdownloadpath_[[product]]'>[[emailBody2]]</a></strong></p>

                </td>

                   <td align="left" height="51" width="3" bgcolor="#ffffff"><img src="http://assets.leica-geosystems.us/email/Spacers/red_btn_right.jpg" width="3" height="51" alt="Spacer" style="display:block; height:51px; width:1px; border: 0;">
                
                </td>
       
                </tr>
              
        	</table>
        </td>
    </tr>
  
<tr>
		<td bgcolor="#ffffff" height="40">
			<a href="http://leica.gs/capsim-jobs"><img src="http://assets.leica-geosystems.us/email/Spacers/spacer600x20.jpg" width="600" height="40" alt="Leica Geosystems" style="display:block; height:40px; border: 0;"></a></td>
	</tr>
   
    <tr>
		<td bgcolor="#e6e6e6" align="top">
        	<table cellspacing="0" cellpadding="0" border="0">
            	<tr>
                <td valign="top" width="35" bgcolor="#e6e6e6"><img src="http://assets.leica-geosystems.us/email/Spacers/spacer40x1.jpg" width="40" height="1" alt="Spacer" style="display:block; height:1px; width:40px; border: 0;">
                
                </td>
                <td valign="top" align="left" width="520" bgcolor="#e6e6e6" style="color: #000000;">
<p style=' color: #ff0033; line-height:22px; font-family:Arial,Helvetica,sans-serif;font-size:14px; margin:40px 20px 0 20px;'><strong>[[emailBody3]]</strong></p>
			<p style='color: #4d4d4d; line-height:22px; font-family:Arial,Helvetica,sans-serif;font-size:14px; margin:20px 20px 0 20px;'>[[emailBody4]]</strong></p>
			<p style='color: #4d4d4d; line-height:22px; font-family:Arial,Helvetica,sans-serif;font-size:14px; margin:20px 20px 40px 20px;'>[[emailBody5]]</p>
	
                </td>
        <td valign="top" width="35" bgcolor="#e6e6e6"><img src="http://assets.leica-geosystems.us/email/Spacers/spacer40x1.jpg" width="40" height="1" alt="Spacer" style="display:block; height:1px; width:40px; border: 0;">
                
                </td>     
                </tr>
              
        	</table>
        </td>
    </tr>
    <tr>
		<td bgcolor="#e6e6e6" align="top">
        	<table cellspacing="0" cellpadding="0" border="0">
            	<tr>
                <td valign="top" width="35" bgcolor="#ffffff"><img src="http://assets.leica-geosystems.us/email/Spacers/spacer-40x1.gif" width="40" height="1" alt="Spacer" style="display:block; height:1px; width:40px; border: 0;">
                
                </td>
                <td valign="top" align="left" width="520" bgcolor="#ffffff" style="color: #000000;">
<p style='line-height:22px; font-family:Arial,Helvetica,sans-serif;font-size:14px; margin:40px 20px 0px 20px;'><strong>[[emailBody6]]<BR />[[owner_label]]<BR /><span style="color: #ff0033;">(</span><a href='mailto:[[owner_email]]'><span style='color:#ff0033;border-bottom:none;text-decoration:underline;'>[[owner_email]]</span></a><span style="color: #ff0033;">)</span><BR />[[owner_phone]]<BR /></strong></p>
	<p style='line-height:22px; font-family:Arial,Helvetica,sans-serif;font-size:14px; margin:20px 20px 40px 20px;'>[[emailBody7]]</p>
                </td>
        <td valign="top" width="35" bgcolor="#ffffff"><img src="http://assets.leica-geosystems.us/email/Spacers/spacer-40x1.gif" width="40" height="1" alt="Spacer" style="display:block; height:1px; width:40px; border: 0;">
                
                </td>     
                </tr>
              
        	</table>
        </td>
    </tr>
    
    <tr><td height="19" bgcolor="#ffffff">
    <img src="http://assets.leica-geosystems.us/email/Spacers/ln_spacer600x19.jpg" width="600" height="19" alt="Spacer"></td></tr>
     <tr>
		<td bgcolor="#ffffff" height="20">
			<a href="http://leica.gs/capsim-jobs"><img src="http://assets.leica-geosystems.us/email/Spacers/spacer600x20.jpg" width="600" height="20" alt="Spacer" style="display:block; height:20px; border: 0;"></a></td>
	</tr>
    <tr>
		<td bgcolor="#e6e6e6" align="top">
        	<table cellspacing="0" cellpadding="0" border="0">
            	<tr>
                <td valign="top" width="35" bgcolor="#ffffff"><img src="http://assets.leica-geosystems.us/email/Spacers/spacer-40x1.gif" width="40" height="1" alt="Spacer" style="display:block; height:1px; width:40px; border: 0;">
                
                </td>
                <td valign="middle" align="left" width="520" height="60" bgcolor="#ffffff" style="color: #000000;">
<p style='font-family:Arial,Helvetica,sans-serif;font-size:12px; margin:0px 20px 0 20px;'><a style="color: #000000; text-decoration: none;" href='http://nexus.leica-geosystems.us/redir.php?orig=[[origin]]&pathvar=producthomepath_[[product]]'><strong>[[product_label]]</strong></a><span style="color: #4d4d4d;"> - [[productDesc]]</span></p>
	
                </td>
        <td valign="middle" width="35" bgcolor="#ffffff"><a href='http://nexus.leica-geosystems.us/redir.php?orig=[[origin]]&pathvar=producthomepath_[[product]]'><img border="0" src="http://assets.microsurvey.com/email/[[product]]/product-logo.gif" width='200' height='60' /></a>
                </td>
                 <td valign="top" width="35" bgcolor="#ffffff"><img src="http://assets.leica-geosystems.us/email/Spacers/spacer-40x1.gif" width="40" height="1" alt="Spacer" style="display:block; height:1px; width:40px; border: 0;">
                
                </td>     
                </tr>
              
        	</table>
        </td>
    </tr>
     <tr>
		<td bgcolor="#ffffff" height="20">
			<a href="http://leica.gs/capsim-jobs"><img src="http://assets.leica-geosystems.us/email/Spacers/spacer600x20.jpg" width="600" height="20" alt="Spacer" style="display:block; height:20px; border: 0;"></a></td>
	</tr>
    <tr><td height="19" bgcolor="#ffffff">
    <img src="http://assets.leica-geosystems.us/email/Spacers/ln_spacer600x19.jpg" width="600" height="19" alt="Spacer"></td></tr>
    <tr>
		<td bgcolor="#ffffff" height="20">
			<a href="http://leica.gs/capsim-jobs"><img src="http://assets.leica-geosystems.us/email/Spacers/spacer600x20.jpg" width="600" height="20" alt="Spacer" style="display:block; height:20px; border: 0;"></a></td>
	</tr>
    
<tr>
        <td><a href='http://nexus.leica-geosystems.us/redir.php?orig=[[origin]]&pathvar=c2a_footer'><img src='http://nexus.leica-geosystems.us/images/origin/[[origin]]_c2a_footer.jpg' width='600' height='120' border='0'></a></td>
         
    </tr>
    <tr>
        <td align="center"> <p style='font-family:Arial,Verdana,Helvetica,sans-serif;line-height:16px;color:#999999;font-size:9px;letter-spacing:0px;margin-top:14px;margin-bottom:14px;'>[[emailDisclaimer]]</p> </td>
         
    </tr>
 
  
</table>
		</td>
        
	</tr>
</table>

</body>
</html>
[/body]