[body]

<a href="http://nexus.microsurvey.com/status.php?form_key=[[auth_key]]&status=1"><img src="http://nexus.leica-geosystems.us/apps/nexus/images/notifications/accept.gif" height="35" width="183" border="0" alt="Accept" style="color: #e71429; font-size: 30px;"></a>
<BR><BR>

<ul>
    <li>
        <font size=2 color=#848484><em>Press <font color="red"><strong>Accept</strong></font> This lead has already been vetted and accepted in the Leica portal. Press Accept, where you will be taken to the "Create New Lead" page in Salesforce, with all possible fields auto-populated with the above information.</em></font>
    </li>
</ul>

[/body]