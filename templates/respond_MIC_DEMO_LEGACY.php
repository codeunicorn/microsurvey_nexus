[sender]marketing@microsurvey.com[/sender]
[subject]Your [[product_label]] Download[/subject]
[body]

<table border='0' cellpadding='0' cellspacing='0' width='548' align=center>
    <tr>
		<td width=225><a href='http://assets.microsurvey.com/apps/nexus/redir.php?orig=[[origin]]&var=c2a_logo'><img src='http://assets.microsurvey.com/apps/nexus/images/origin/[[origin]]_c2a_logo.jpg' width='230' height='50' border='0'></a></td>
		<td width='318' align='right' valign='bottom'>&nbsp;</td>
    </tr>
    <tr>
        <td colspan='3' style='line-height:6px;'><img src='http://assets.microsurvey.com/email/spacers/spacer-548x6.gif' style='display:block;' width='548' height='6' /></td>
    </tr>
</table>

<table border='0' cellpadding='0' cellspacing='0' width='548' align='center'>
	<tr>
		<td colspan='10'>
			<table border='0' cellpadding='0' cellspacing='0' width='548'>
				<tr>
					<td colspan='3' style='line-height:19px;'><img src='http://assets.microsurvey.com/email/[[product]]/header-top.jpg' style='display:block;' width='548' height='19' /></td>
				</tr>
				<tr>
					<td>
						<img src='http://assets.microsurvey.com/email/[[product]]/header-left.jpg' style='display:block;' width='18' height='81' />
					</td>
					<td bgcolor='[[colorLight]]' valign=center>
						<table border='0' cellpadding='0' cellspacing='0' width='196'>
							<tr>
								<td valign='top' align='center' width='196'>
									<p style='font-family:Arial,Verdana,Helvetica,sans-serif;color:'[[colorMain]]';font-size:14px;font-weight:bold;letter-spacing:0px;line-height:18px;margin:0;'>[[productTagLine]]</p>
								</td>
							</tr>
							<tr height='1' style='line-height:1px;'>
								<td><img src='http://assets.microsurvey.com/email/spacers/spacer-tag.gif' style='display:block;' width='196' height='1' /></td>
							</tr>
						</table>
					</td>
					<td>
						<img src='http://assets.microsurvey.com/email/[[product]]/header-right.jpg' style='display:block;' width='334' height='81' />
					</td>
				</tr>
				<tr>
					<td colspan='3'>
						<img src='http://assets.microsurvey.com/email/[[product]]/header-bottom.jpg' style='display:block;' width='548' height='241' />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr>
		<td width='1' bgcolor='#98855a'><img src='http://assets.microsurvey.com/email/spacers/spacer.gif' width='1' height='1' /></td>
		<td width='1' bgcolor='#ffffff'><img src='http://assets.microsurvey.com/email/spacers/spacer.gif' width='1' height='1' /></td>
		<td width='12' bgcolor='[[colorMain]]'><img src='http://assets.microsurvey.com/email/spacers/spacer-12x1.gif' width='12' height='1' /></td>
		<td width='24' bgcolor='[[colorLight]]'><img src='http://assets.microsurvey.com/email/spacers/spacer-24x1.gif' width='24' height='1' /></td>
		<td width='309' bgcolor='[[colorLight]]' valign='top'>
			<p style='font-family:Arial,Verdana,Helvetica,sans-serif;color:#000;font-size:12px;letter-spacing:0px;margin:0;margin-bottom:18px;'>[[emailBody0]]</p>
			<p style='font-family:Arial,Verdana,Helvetica,sans-serif;color:#000;font-size:12px;letter-spacing:0px;margin:0;margin-bottom:18px;'>[[emailBody1]]</p>
			<p style='font-family:Arial,Verdana,Helvetica,sans-serif;color:#000;font-size:20px;letter-spacing:0px;margin:0;margin-bottom:18px;'><strong><a href='http://assets.microsurvey.com/apps/nexus/redir.php?orig=[[origin]]&var=[[######]]'>[[emailBody2]]</a></strong></p>
			<p style='font-family:Arial,Verdana,Helvetica,sans-serif;color:#000;font-size:16px;letter-spacing:0px;margin:0;margin-bottom:0;'><strong>[[emailBody3]]</strong></p>
			<p style='font-family:Arial,Verdana,Helvetica,sans-serif;color:#000;font-size:12px;letter-spacing:0px;margin:0;margin-bottom:18px;'>[[emailBody4]]</strong></p>
			<p style='font-family:Arial,Verdana,Helvetica,sans-serif;color:#000;font-size:12px;letter-spacing:0px;margin:0;margin-bottom:18px;'>[[emailBody5]]</p>
			<p style='font-family:Arial,Verdana,Helvetica,sans-serif;color:#000;font-size:12px;letter-spacing:0px;margin:0;margin-bottom:18px;'><strong>[[emailBody6]]<BR />[[owner_label]]<BR />([[owner_email]])</strong></p>
			<p style='font-family:Arial,Verdana,Helvetica,sans-serif;color:#000;font-size:12px;letter-spacing:0px;margin:0;margin-bottom:18px;'>[[emailBody7]]</p>
		</td>
		<td width='12' bgcolor='[[colorLight]]'><img src='http://assets.microsurvey.com/email/spacers/spacer-12x1.gif' width='12' height='1' /></td>
		<td width='175' bgcolor='[[colorDark]]' valign='top' align='center'>
			<p style='margin-left:0px;margin-bottom:0px;margin-top:0px;'><a href='http://assets.microsurvey.com/apps/nexus/redir.php?orig=[[origin]]&var=c2a_panel_short'><img src='http://assets.microsurvey.com/apps/nexus/images/origin/[[origin]]_c2a_panel_med.gif' width='175' height='519' border='0'></a></p>
		<td width='12' bgcolor='[[colorMain]]'><img src='http://www.microsurvey.com/email/spacers/spacer-12x1.gif' width='12' height='1' /></td>
		<td width='1' bgcolor='#ffffff'><img src='http://assets.microsurvey.com/email/spacers/spacer.gif' width='1' height='1' /></td>
		<td width='1' bgcolor='#98855a'><img src='http://assets.microsurvey.com/email/spacers/spacer.gif' width='1' height='1' /></td>
	</tr>
	<tr>
		<td colspan='10' style='line-height:14px;'><img src='http://assets.microsurvey.com/email/[[product]]/cap.gif' style='display:block;' width='548' height='14' /></td>
	</tr>
</table>

<table width='548' border='0' cellpadding='0' cellspacing='0' align='center'>

    <tr>
        <td colspan='3' style='line-height:14px;'><img src='http://assets.microsurvey.com/email/spacers/spacer.gif' width='548' height='14' /></td>
    </tr>

    <tr>
        <td valign='top' width='175'><a href='http://assets.microsurvey.com/apps/nexus/redir.php?orig=[[origin]]&var=[[######]]'><img src='http://assets.microsurvey.com/email/[[product]]/logo.gif' width=175 height=53 border=0 /></a></td>
        <td width='14'><img src='http://assets.microsurvey.com/email/spacers/spacer-14x14.gif' width='14' height='14' /></td>
        <td valign='top' width='359' rowspan='3'>
			<p style='font-family:Arial,Verdana,Helvetica,sans-serif;line-height:16px;color:#000;font-size:10px;letter-spacing:0px;margin-top:0;margin-bottom:14px;'><a href='http://assets.microsurvey.com/apps/nexus/redir.php?orig=[[origin]]&var=[[######]]'><strong style='color:[[colorMain]];border-bottom:none;text-decoration:underline;'>[[product_label]]</strong></a> - [[productDesc]]</p>
			<p style='font-family:Arial,Verdana,Helvetica,sans-serif;line-height:18px;color:#000;font-size:11pt;letter-spacing:0px;Margin-top:0;margin-bottom:14px;'><strong><span>[[owner_label]]</span></strong></p>
			<p style='font-family:Arial,Verdana,Helvetica,sans-serif;line-height:18px;color:#000;font-size:10pt;letter-spacing:0px;margin-top:0;margin-bottom:14px;'><strong>T:</strong> [[owner_phone]]<br><strong>E:</strong> <a href='mailto:[[owner_email]]'><span style='color:#0097ba;border-bottom:none;text-decoration:underline;'>[[owner_email]] </span></a></p>
		</td>
    </tr>

    <tr>
        <td style='line-height:14px;'><img src='http://assets.microsurvey.com/email/spacers/spacer-175x14.gif' width='175' height='14' /></td>
        <td style='line-height:14px;'><img src='http://assets.microsurvey.com/email/spacers/spacer-14x14.gif' width='14' height='14' /></td>
    </tr>

    <tr>
        <td align='center' valign='top'><a href='http://assets.microsurvey.com/apps/nexus/redir.php?orig=[[origin]]&var=c2a_shop'><img src='http://assets.microsurvey.com/apps/nexus/images/origin/[[origin]]_c2a_shop.gif' width='150' height='150' border='0'></a></td>
        <td width='14'><img src='http://assets.microsurvey.com/email/spacers/spacer-14x50.gif' width='14' height='50' /></td>
    </tr>

    <tr>
        <td colspan='3'><a href='http://assets.microsurvey.com/apps/nexus/redir.php?orig=[[origin]]&var=c2a_footer'><img src='http://assets.microsurvey.com/apps/nexus/images/origin/[[origin]]_c2a_footer.jpg' width='548' height='110' border='0'></a></td>
    </tr>

</table>

<p style='font-family:Arial,Verdana,Helvetica,sans-serif;line-height:16px;color:#999999;font-size:9px;letter-spacing:0px;margin-top:14px;margin-bottom:14px;'>[[emailDisclaimer]]</p>

<BR>
[/body]