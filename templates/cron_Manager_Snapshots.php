[sender]us.marketing@leicaus.com[/sender]
[subject]Sales-Ready Lead Summary Report[/subject]
[body]
<table style='border:1px solid #ddd;border-spacing:0px;line-height: 1.42857143;color: black;font-family: arial;font-size: 10px;'>
    <thead style='background: #333;color: white;border-spacing:0px;font-size: 12px;'>
    <tr style='border-bottom-width: 2px;border-spacing:0px;'>
        <th style='border:1px solid #ddd;padding:7px;'>Date</th>
        [[owner_header]]
        [[manager_header]]
        <th style='border:1px solid #ddd;padding:7px;'>Company</th>
        <th style='border:1px solid #ddd;padding:7px;'>Name</th>
        <th style='border:1px solid #ddd;padding:7px;'>Email</th>
        <th style='border:1px solid #ddd;padding:7px;'>Segment</th>
        <th style='border:1px solid #ddd;padding:7px;'>Channel & Tactic</th>
        <th style='border:1px solid #ddd;padding:7px;'>Status</th>
        [[acceptReject_header]]
    </tr>
    </thead>
    [[rows]]
[/body]