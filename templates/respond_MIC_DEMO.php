[sender][[owner_email]][/sender] [subject][[productSubject]][/subject]
[body]
<html><head><title>[[product_label]]</title></head>
<body style="margin:0;padding:0;" bgcolor="#f0f0f0">

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin:0;padding:0;"><tr><td bgcolor="#f0f0f0">

<table border="0" cellpadding="0" cellspacing="0" width="600" align="center">
    <tr>
		<td width="230"><div style="height:60px;"><a href='http://assets.microsurvey.com/apps/nexus/redir.php?orig=[[origin]]&pathvar=c2a_logo'><img src='http://assets.microsurvey.com/apps/nexus/images/origin/[[origin]]_c2a_logo.jpg' width='230' height='60' border='0'></a></div></td>
		<td width="370" align="right" valign="bottom"><div style='font-family:Arial,Helvetica,sans-serif;color:#aaaaaa;font-size:9px;letter-spacing:0px;margin:0px;margin-bottom:4px;margin-top:0px;'>&nbsp;</div></td>
    </tr>
</table>

<table border='0' cellpadding='0' cellspacing='0' width="600" align='center'>
	<tr>
		<td colspan="4"><div style="height:50px;"><img src='http://assets.microsurvey.com/email/cap-top.jpg' style="display:block;" width="600" height="50" /></div></td>
	</tr>
	<tr>
		<td colspan="4">
			<table border='0' cellpadding='0' cellspacing='0' width="600" align="center">
				<tr>
					<td width="94" bgcolor="[[colorDark]]" style="line-height:1px;"><div style="height:1px;"><img src='http://assets.microsurvey.com/email/spacers/spacer-94x1.gif' style="display:block;" width='94' height='1' /></div></td>
					<td width="218" bgcolor="[[colorMain]]" style="line-height:1px;"><div style="height:1px;"><img src='http://assets.microsurvey.com/email/spacers/spacer-218x1.gif' style="display:block;" width='218' height='1' /></div></td>
					<td width="288" bgcolor="#a2a2a2" style="line-height:1px;"><div style="height:1px;"><img src='http://assets.microsurvey.com/email/spacers/spacer-288x1.gif' style="display:block;" width='288' height='1' /></div></td>
				</tr>
				<tr>
					<td width="94" bgcolor="[[colorDark]]" align="center"><div style="height:48px;"><img src='http://assets.microsurvey.com/email/[[product]]/icon.gif' width="48" height="48" /></div></td>
					<td width="218" bgcolor="[[colorMain]]" align="center"><p style='font-family:Arial,Helvetica,sans-serif;color:#ffffff;font-size:15px;font-weight:bold;letter-spacing:0px;line-height:18px;margin:10px 10px 10px 10px;'>[[productTagLine]]</p></td>
					<td width="288" bgcolor="#a2a2a2" valign="center"><div style="height:56px;"><a href="http://nexus.microsurvey.com/wrapper_MIC_DEMO.php?form=[[form]]&orig=[[origin]]&lang=[[lang]]"><img src='http://assets.microsurvey.com/email/demo-button.jpg' style="display:block;" align="center" width="288" height="56" border="0" /></a></div></td>
				</tr>
				<tr>
					<td width="94" bgcolor="[[colorDark]]" style="line-height:1px;"><div style="height:1px;"><img src='http://assets.microsurvey.com/email/spacers/spacer-94x1.gif' style="display:block;" width='94' height='1' /></div></td>
					<td width="218" bgcolor="[[colorMain]]" style="line-height:1px;"><div style="height:1px;"><img src='http://assets.microsurvey.com/email/spacers/spacer-218x1.gif' style="display:block;" width='218' height='1' /></div></td>
					<td width="288" bgcolor="#a2a2a2" style="line-height:1px;"><div style="height:1px;"><img src='http://assets.microsurvey.com/email/spacers/spacer-288x1.gif' style="display:block;" width='288' height='1' /></div></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="4"><div style="height:420px;"><img src='http://assets.microsurvey.com/email/[[product]]/main.jpg' style="display:block;" width="600" height="420" /></div></td>
	</tr>
	<tr>
		<td width="10" bgcolor="[[colorMain]]"><img src='http://assets.microsurvey.com/email/spacers/spacer-10x3.gif' width='10' height='1' /></td>
		<td width="390" valign="top" bgcolor="[[colorLight]]">
			<p style='font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:18px;letter-spacing:0px;margin:10px 20px 0 20px;'>[[emailBody0]]</p>
			<p style='font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:18px;letter-spacing:0px;margin:10px 20px 0 20px;'>[[emailBody1]]</p>
			<p style='font-family:Arial,Helvetica,sans-serif;font-size:16px;line-height:18px;letter-spacing:0px;margin:10px 20px 0 20px;'><a href='http://assets.microsurvey.com/apps/nexus/redir.php?orig=[[origin]]&pathvar=productdownloadpath_[[product]]'><strong style='color:#bf9a2e;border-bottom:none;text-decoration:underline;'>[[emailBody2]]</strong></a></p>
			<p style='font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:18px;letter-spacing:0px;margin:10px 20px 0 20px;'><strong>[[emailBody3]]</strong></p>
			<p style='font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:18px;letter-spacing:0px;margin:0 20px 0 20px;'>[[emailBody4]]</strong></p>
			<p style='font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:18px;letter-spacing:0px;margin:10px 20px 0 20px;'>[[emailBody5]]</p>
			<p style='font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:18px;letter-spacing:0px;margin:10px 20px 0 20px;'><strong>[[emailBody6]]<BR />[[owner_label]]<BR />(<a href='mailto:[[owner_email]]'><span style='color:#bf9a2e;border-bottom:none;text-decoration:underline;'>[[owner_email]]</span></a>)</strong></p>
			<p style='font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:18px;letter-spacing:0px;margin:10px 20px 0 20px;'>[[emailBody7]]</p>
			&nbsp;
		</td>
		<td width="190" bgcolor="[[colorDark]]" valign="top">
			<a href='http://assets.microsurvey.com/apps/nexus/redir.php?orig=[[origin]]&var=c2a_panel_med'><img src='http://assets.microsurvey.com/apps/nexus/images/origin/[[origin]]_c2a_panel_med.gif' width='190' height='519' border='0'></a>
		</td>
		<td width="10" bgcolor="[[colorMain]]"><img src='http://assets.microsurvey.com/email/spacers/spacer-10x3.gif' width='10' height='1' /></td>
	</tr>
	<tr>
		<td colspan="4"><div style="height:30px;"><img src='http://assets.microsurvey.com/email/[[product]]/cap-bottom.jpg' style="display:block;" width="600" height="30" /></div></td>
	</tr>
	<tr>
		<td colspan="4" bgcolor="#ffffff">
			<table width="600" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td width='400' valign="center"><p style='font-family:Arial,Verdana,Helvetica,sans-serif;line-height:15px;color:#000000;font-size:10px;letter-spacing:0px;margin:0 20px 0 30px;'><a href='http://assets.microsurvey.com/apps/nexus/redir.php?orig=[[origin]]&pathvar=producthomepath_[[product]]'><strong style='color:#bf9a2e;border-bottom:none;text-decoration:underline;'>[[product_label]]</strong></a> - [[productDesc]]</p></td>
                    <td width='200' valign="center" align="center" bgcolor="#f8f8f8"><div style="height:60px;"><a href='http://assets.microsurvey.com/apps/nexus/redir.php?orig=[[origin]]&pathvar=producthomepath_[[product]]'><img border="0" src="http://assets.microsurvey.com/email/[[product]]/product-logo.gif" width='200' height='60' /></a></div></td>
                </tr>
                <tr>
                    <td width="400" bgcolor="#ffffff" colspan="2" style="line-height:14px;"><div style="height:28px;"><img src='http://assets.microsurvey.com/email/spacers/sep-600x28.gif' style="display:block;" width='600' height='28' /></div></td>
                </tr>
                <tr>
                    <td width='400' valign='bottom'>
                        <p style='font-family:Arial,Verdana,Helvetica,sans-serif;line-height:18px;color:#777777;font-size:11pt;letter-spacing:0px;margin:0 0 2px 30px;'>
                            <strong><span id='sig_name'>[[owner_label]]</span></strong>
                        </p>
                        <p style='font-family:Arial,Verdana,Helvetica,sans-serif;line-height:18px;color:#777777;font-size:10pt;letter-spacing:0px;margin-top:0;margin:0 0 14px 30px;'>
                            <span id='sig_title'>[[owner_title]]</span><br>
                            <strong>T:</strong> [[owner_phone]]<br>
                            <strong>E:</strong> <a href='mailto:[[owner_email]]'><span style='color:#bf9a2e;border-bottom:none;text-decoration:underline;'>[[owner_email]]</span></a></p>
                        <p style='font-family:Arial,Verdana,Helvetica,sans-serif;line-height:18px;color:#777777;font-size:10pt;letter-spacing:0px;margin-top:0;margin:0 0 0 30px;'><strong><span id='sig_company'>[[owner_company]]</span></strong><br>
                            <span id='sig_address1'>[[owner_address]]</span><br>
                            <span id='sig_address2'>[[owner_address2]]</span></p>
                    </td>
                    <td valign='bottom' width='200' bgcolor="#f8f8f8" align="center"><a href='http://assets.microsurvey.com/apps/nexus/redir.php?orig=[[origin]]&pathvar=c2a_shop'><img src='http://assets.microsurvey.com/apps/nexus/images/origin/[[origin]]_c2a_shop.gif' width='200' height='150' border='0'></a></td>
                </tr>
                <tr>
                    <td width="400" bgcolor="#ffffff" style="line-height:14px;"><div style="height:14px;"><img src='http://assets.microsurvey.com/email/spacers/spacer-400x14.gif' width='400' height='14' /></div></td>
                    <td width="200" bgcolor="#f8f8f8" style="line-height:14px;"><div style="height:14px;"><img src='http://assets.microsurvey.com/email/spacers/spacer-200x14.gif' width='200' height='14' /></div></td>
                </tr>
			</table>
		</td>
	</tr>
    <tr>
        <td colspan='4'><a href='http://assets.microsurvey.com/apps/nexus/redir.php?orig=[[origin]]&pathvar=c2a_footer'><img src='http://assets.microsurvey.com/apps/nexus/images/origin/[[origin]]_c2a_footer.jpg' width='600' height='120' border='0'></a></td>
    </tr>
	<tr>
		<td style="line-height:3px;"><img src='http://assets.microsurvey.com/email/spacers/spacer-10x3.gif' style="display:block;" width="10" height="3" /></td>
		<td style="line-height:3px;"><img src='http://assets.microsurvey.com/email/spacers/spacer-390x1.gif' style="display:block;" width="390" height="1" /></td>
		<td style="line-height:3px;"><img src='http://assets.microsurvey.com/email/spacers/spacer-190x1.gif' style="display:block;" width="190" height="1" /></td>
		<td style="line-height:3px;"><img src='http://assets.microsurvey.com/email/spacers/spacer-10x3.gif' style="display:block;" width="10" height="3" /></td>
	</tr>
</table>

<p style='font-family:Arial,Verdana,Helvetica,sans-serif;line-height:16px;color:#999999;font-size:9px;letter-spacing:0px;margin-top:14px;margin-bottom:14px;'>[[emailDisclaimer]]</p>

&nbsp;

</td></tr></table>

</body>
</html>
[/body]