<?php
// define all of our project specific constants
defined('NEXUS_INTERNAL_ROOT') or define('NEXUS_INTERNAL_ROOT', realpath(dirname(__FILE__)) .'/');
defined('NEXUS_CLASSES_DIR') or define('NEXUS_CLASSES_DIR', NEXUS_INTERNAL_ROOT .'classes/');

// include our main database abstraction class, which also loads all other classes
include_once(NEXUS_INTERNAL_ROOT .'DB.class.php');

$origin = isset($_GET['orig']) ? urldecode($_GET['orig']) : '';
$redir = isset($_GET['var']) ? urldecode($_GET['var']) : ( isset($_GET['pathvar']) ? urldecode($_GET['pathvar']) : '' );

$nexus = new Nexus();
$redirectUrl = $nexus->getRedirectLink($origin, $redir);

header(sprintf("Location: %s", $redirectUrl));
?>