<?php

// Demo Nexus v2.0 - Jareb Coupe 2013
// ### Form Builder (core) ###
// Wrapper for leica-geosystems.us forms

// Common initialization

require_once('common_init.php');
$page = $_SERVER['PHP_SELF']."?".$querystring;
?>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Leica Geosystems</title>
<link rel="icon" href="http://gis.leica-geosystems.us/media/favicon/websites/6/IDR_MAINFRAME.ico" type="image/x-icon" />
<link rel="shortcut icon" href="http://gis.leica-geosystems.us/media/favicon/websites/6/IDR_MAINFRAME.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/fortis/default/css/styles.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/fortis/gis/css/skin.css" media="all" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript">jQuery.noConflict();
function reloadparent() {
	parent.location.reload();
}
</script>

<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/base/default/css/styles-ie.css" media="all" />
<![endif]-->
<!--[if lt IE 7]>
<script type="text/javascript" src="http://gis.leica-geosystems.us/js/lib/ds-sleight.js"></script>
<script type="text/javascript" src="http://gis.leica-geosystems.us/skin/frontend/base/default/js/ie6.js"></script>
<![endif]-->
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/fortis/default/css/styles-ie-all.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/fortis/default/css/skin-ie-all.css" media="all" />
<![endif]-->
<!--[if lte IE 7]>
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/fortis/default/css/styles-ie7.css" media="all" />
<![endif]-->

<style>
a {
color: #000 !important;
text-decoration: none;
}
a:hover {
color: #f00 !important;
text-decoration: underline;
}
.col-main {
	width:90% !important;
	margin: auto;
	float: center;
}
label {
	width: 200px;
	text-align: right;
	padding-right: 15px;
}
input, select, textarea {
	margin-left: 12px;
}
.fields {
	text-align: left;
}
.form-results {
	float: right;
	margin: 9px 8px 0 0;
}
body {
	margin: 0;
	padding: 0;
	text-align: left;
}

.form-preamble, .form-postamble {
	margin: 17px 0 17px 0;
}
.legend {
	font-family: Arial, Helvetica, sans-serif !important;
	font-weight: bold !important;
	font-size: 18px !important;
	color: #000 !important;
}
.form-list textarea {
	height: 8em !important;
}
</style>
</head>

<body class="customer-account-create">

<div class="col-main">
    <div class="account-create">
      <?php require_once('core.php'); ?>
    </div>
</div>

</body>
</html>