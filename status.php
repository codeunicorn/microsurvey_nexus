<?php
// define all of our project specific constants
defined('NEXUS_INTERNAL_ROOT') or define('NEXUS_INTERNAL_ROOT', realpath(dirname(__FILE__)) .'/');
defined('NEXUS_CLASSES_DIR') or define('NEXUS_CLASSES_DIR', NEXUS_INTERNAL_ROOT .'classes/');

// include our main database abstraction class, which also loads all other classes
include_once(NEXUS_INTERNAL_ROOT .'DB.class.php');
DB::Get(array('MySQL'));

// exit if form_key and status is not passed as an argument
if(!isset($_GET['form_key']) || !isset($_GET['status']))
    exit();

$form_key = $_GET['form_key'];
$status = (int) $_GET['status'];
$reassignType = isset($_GET['reassignType']) ? (int) $_GET['reassignType'] : null;
$reassignValue = isset($_GET['reassignValue']) ? $_GET['reassignValue'] : null;

// load our classes
$form = new Forms();
$report = new Reports();
$nexus = new Nexus();

$form->loadAuthKey($form_key);
if($form->doesAuthKeyExist())
{
    $form->loadFormFromAuthKey();
    $form->setStatus($status);

    // if status = 1, it's accepted and we redirect to salesforce
    if($status == 1)
    {
        $salesforceUrl = $form->getSalesForceRedirectLink();
        header(sprintf("Location: %s", $salesforceUrl));
    }

    /*
        If status = 4, we need to reassign notify all ‘Editor’ account types.
        Have a dropdown (Presets. aka LGS Segments) become available in that
        line giving the user the option to change the segment? It should
        pre-populate with the best match for the currently assigned segment.
        When the user chooses a different segment and presses “Submit”, the
        status actually sets back to Pending, the Seg/Div are updated, and the
        lead is re-pushed to whoever would be the new owner under that segment.
    */
    if($status == 4)
    {
        // set our status back to pending
        $form->status = 0;
        $form->status_label = 'Pending';

        $presets = $report->getPresets();

        // the data we're passing into map_owner.php
        // at minimum, we need the form id, auth_key, x_country, x_state, x_city, seg, div, orig
        $querystring = array();

        switch($reassignType) {

            case 1 :

                // our old segment and div values
                $segment = $form->x_udf_primary_market_segment122__ss;
                $division = $form->x_udf_division146__ss;
                $origin = $form->origin_label;
                $map = $form->map;

                // update seg/div
                foreach($presets as $key => $val)
                {
                    if($key == $reassignValue)
                    {
                        $segment = $val['seg'][0];
                        $division = $val['div'][0];
                        $origin = $val['origin'][0];
                        $map = $val['map'][0];
                    }
                }

                break;

            case 2 :

                $o = new Owner();
                $owner = $o->getOwnerByShortcode($reassignValue);

                // our new segment and division
                $segment_label = !empty($owner['owner_default_segment']) ? $owner['owner_default_segment'] : $form->x_udf_primary_market_segment122__ss_label;
                $division_label = !empty($owner['owner_default_segment']) ? $owner['owner_default_division'] : $form->x_udf_division146__ss_label;
                $origin_label = $form->x_udf_origin_code114__ss_label;
                $map = $form->map;

                $segment = $form->getSegmentValueFromSegment($segment_label);
                $division = $form->getDivValueFromDiv($division_label);

                // get our new preset
                $preset = $form->getPresetFromSegmentAndDivision($segment_label, $division_label, $origin_label);
                $form->preset = $preset;
                $querystring['owner'] = $reassignValue;

                break;

            case 3 :

                // our new segment and division
                $segment =  $form->x_udf_primary_market_segment122__ss;
                $division = $form->x_udf_division146__ss;
                $map = $form->map;

                $querystring['owner'] = $reassignValue;

                break;
        }

        $form->x_udf_primary_market_segment122__ss = $segment;
        $form->x_udf_primary_market_segment122__ss_label = $form->getSegmentFromSegmentId($segment);
        $form->x_udf_division146__ss = $division;
        $form->x_udf_division146__ss_label = $form->getDivFromDivValue($division);
        $form->map = $map;

        $form->update();

        // the data we're passing into map_owner.php
        // at minimum, we need the form id, auth_key, x_country, x_state, x_city, seg, div, orig
        $querystring['form'] = $form->form_id;
        $querystring['map'] = $map;
        $querystring['espFormID'] = $form->form_id;
        $querystring['auth_key'] = $form->auth_key;
        $querystring['seg'] = $segment;
        $querystring['div'] = $division;
        $querystring['x_country'] = $form->x_country;
        $querystring['x_state'] = $form->x_state;
        $querystring['x_city'] = $form->x_city;
        $querystring['x_udf_ext_purchase_question405'] = 'Yes'; // hack to force email notification to be sent out ( this is so bad!! )
        $querystring['email_notify'] = 'true';
        $querystring['auto_responder'] = 'false';
        $querystring['save_to_db'] = 'true';
        $querystring['notify_type'] = 'editor';

        $ch = curl_init();
        $url = 'http://'. NEXUS_EXTERNAL_ROOT . 'map_owner.php?' . http_build_query($querystring);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_HEADER, 0);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        $result = curl_exec($ch);
        curl_close ($ch);
    }

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="reports/assets/css/bootstrap.min.css">

    <style>
        body {
            background: #333;
            font-family: Arial;
            font-size: 20px;
        }
        .container {
            margin-top: 50px;
        }
        .main .btn-group {
            display: block;
        }
        .segment_panel {
            display: none;
        }
        .dropdown-menu{
            font-size: 20px;
        }
        form .dropdown-menu>li>a {
            padding: 3px 10px;
            white-space: pre-wrap;
        }
        .input-group-addon {
            color: #fff;
            font-size: 20px;
        }
        .addon-info {
            background-color: #5bc0de;
        }
        .addon-warning {
            background-color: #f0ad4e;
        }
        .addon-primary {
            background-color: #337ab7;
        }
        .addon-success {
            background-color: #5cb85c;
        }
        .addon-danger {
            background-color: #d9534f;
        }
        .btn-submit {
            padding: 30px;
            margin-top: 50px;
            font-size: 25px;
            width: 100%;
        }
    </style>

</head>
<body>
<div class='main container'>
<?php

if($status == 2) // rejected
{
    echo '<form method="get" action="/status.php" role="form">

            <input type="hidden" name="form_key" value="'.$form_key.'"/>
            <input id="status" type="hidden" name="status" value="2">
            <input id="reassignType" type="hidden" name="reassignType" value="">
            <input id="reassignValue" type="hidden" name="reassignValue" value="">


            <label class="sr-only" for="reject_reason_input">Reject Reason</label>
            <div id="reject_reason_group" class="form-group input-group">
              <div class="input-group-btn">
                <button type="button" id="reject-btn" class="btn btn-info btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Reject reason <span class="caret"></span>
                    <span class="sr-only">Reject reason</span>
                </button>
                <ul id="reject_dropdown" class="dropdown-menu">
                            <li><a href="#" data-status="3" data-class="primary">This is a duplicate lead that was already recently submitted</a></li>
                            <li><a href="#" data-status="4" data-class="warning">This is not my territory and/or segment, please reassign</a></li>
                            <li><a href="#" data-status="6" data-class="success">This is a dealer, partner, or staff member</a></li>
                            <li><a href="#" data-status="5" data-class="danger">This is an invalid lead (fake/solicitous/bot/spam)</a></li>
                          </ul>
              </div>
              <input type="text" id="reject_reason_input" readonly class="form-control input-lg" aria-label="Please select the reason for rejection" value="Please select the reason for rejection">
              <span id="reject_reason_addon" class="input-group-addon addon-info">-</span>
            </div><!-- /.input-group -->


            <div id="reassign_reason_group" class="form-group input-group hidden">
              <div class="input-group-btn">
                <button type="button" id="reassign-btn" class="btn btn-info btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Reassign reason <span class="caret"></span>
                    <span class="sr-only">Reassign reason</span>
                </button>
                <ul id="reassign_dropdown" class="dropdown-menu">
                            <li><a href="#" data-status="1" data-class="primary">Reassign this SRL to another segment</a></li>
                            <li><a href="#" data-status="2" data-class="warning">Reassign this SRL to another Owner, and automatically re-assign Segment</a></li>
                            <li><a href="#" data-status="3" data-class="success">Reassign this SRL to another Owner, but retain original Segment</a></li>
                          </ul>
              </div>
              <input type="text" id="reassign_reason_input" readonly class="form-control input-lg" aria-label="Please select the reason for reassignment" value="Please select the reason for rejection">
              <span id="reassign_reason_addon" class="input-group-addon addon-info">-</span>
            </div><!-- /.input-group -->



            <div id="preset_dropdown_group" class="form-group input-group hidden">
              <div class="input-group-btn">
                <button type="button" id="preset_dropdown-btn" class="btn btn-info btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Select Segment <span class="caret"></span>
                    <span class="sr-only">Select Segment</span>
                </button>
                <ul id="preset_dropdown" class="dropdown-menu">';

                    $presets = $report->getPresets();
                    $segment = $form->x_udf_primary_market_segment122__ss_label;
                    $division = $form->x_udf_division146__ss_label;
                    $origin = $form->origin_label;

                    $preset = $nexus->getPresetFromSegmentAndDivision($segment, $division, $origin);

                    $o = new Owner();
                    $owner = $o->getOwnerByShortcode($form->x_udf_form_owner148);
                    $map = 'map-'. $owner['owner_map'];

                    foreach($presets as $key => $pre)
                    {
                        echo in_array($map, $pre["map"]) || empty($map) ?
                            '<li class="col-xs-12 col-sm-12 col-md-6"><a href="#" data-status="'. $key .'" data-class="success">'. $key .'</a></li>' : '';
                    }

              echo ' </ul>
              </div>
              <input type="text" id="preset_dropdown_input" readonly class="form-control input-lg" aria-label="Please select the Segment to reassign to" value="Please select the Segment to reassign to">
              <span id="preset_dropdown_addon" class="input-group-addon addon-info">-</span>
            </div><!-- /.input-group -->



            <div id="owner_dropdown_group" class="form-group input-group hidden">
              <div class="input-group-btn">
                <button type="button" id="owner_dropdown-btn" class="btn btn-info btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Select Owner <span class="caret"></span>
                    <span class="sr-only">Select Owner</span>
                </button>
                <ul id="owner_dropdown" class="dropdown-menu">';

                $owners = $report->getOwners();

                $o = new Owner();
                $owner = $o->getOwnerByShortcode($form->x_udf_form_owner148);
                $map = 'map-'. $owner['owner_map'];

                foreach($owners as $key => $own)
                {
                    echo $own['Map'] == $map || empty($map) ?
                        '<li class="col-xs-12 col-sm-6 col-md-4"><a href="#" data-status="'. $own["Short-Code"] .'" data-class="success">'. $own["Short-Code"] .'</a></li>' : '';
                }

    echo ' </ul>
              </div>
              <input type="text" id="owner_dropdown_input" readonly class="form-control input-lg" aria-label="Please select the Segment to reassign to" value="Please select the Owner to reassign to">
              <span id="owner_dropdown_addon" class="input-group-addon addon-info">-</span>
            </div><!-- /.input-group -->


            <button type="submit" class="btn-submit btn btn-success">Submit</button>
    </form>';
}
else
{
    $statusText = $status == 4 ? 'The lead has been reassigned.' : 'The status has been updated.';

    echo '<div class="panel panel-info">
            <div class="panel-heading">Thank you for the feedback.</div>
            <div class="panel-body">'. $statusText .'</div>
        </div>';
}

?>
</div>
<script type="text/javascript" src="scripts/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="reports/assets/bootstrap.min.js"></script>
<script>

    var statusManager = function(){

        // current reject variables
        this.currentRejectStatus = "2";
        this.currentRejectClass = "info";
        this.currentRejectText = "Please select the reason for rejection";
        
        // new reject variables
        this.newRejectStatus = "2";
        this.newRejectClass = "info";
        this.newRejectText = "Please select the reason for rejection";

        // current reassign variables
        this.currentReassignStatus = "";
        this.currentReassignClass = "info";
        this.currentReassignText = "Please select the reason for reassignment";
        
        // new reassign variables
        this.newReassignStatus = ""
        this.newReassignClass = "info";
        this.newReassignText = "Please select the reason for reassignment";

        // current preset variables
        this.currentPresetStatus = "";
        this.currentPresetClass = "info";
        this.currentPresetText = "Please select the Segment to reassign to";

        // new preset variables
        this.newPresetStatus = ""
        this.newPresetClass = "success";
        this.newPresetText = "Please select the Segment to reassign to";

        // current owner variables
        this.currentOwnerStatus = "";
        this.currentOwnerClass = "info";
        this.currentOwnerText = "Please select the Owner to reassign to";

        // new owner variables
        this.newOwnerStatus = ""
        this.newOwnerClass = "success";
        this.newOwnerText = "Please select the Owner to reassign to";
        
        
        this.rejectaddonMap = {
            primary: "D",
            warning: "R",
            success: "S",
            danger:  "I"
        }
        this.reassignaddonMap = {
            primary: "SS",
            warning: "OR",
            success: "OO"
        }

        this.init = function() {

            var self = this;

            $("#reject_dropdown li a").on('click', function(e){
                e.preventDefault();

                var newRejectStatus = $(this).data("status");
                var newRejectClass = $(this).data("class");
                var newRejectText = $(this).html();

                self.newRejectStatus = newRejectStatus;
                self.newRejectClass = newRejectClass;
                self.newRejectText = newRejectText;

                self.updateRejectFields();
                self.showOrHideRejectChildren();

            });

            $("#reassign_dropdown li a").on('click', function(e){
                e.preventDefault();

                var newReassignStatus = $(this).data("status");
                var newReassignClass = $(this).data("class");
                var newReassignText = $(this).html();

                self.newReassignStatus = newReassignStatus;
                self.newReassignClass = newReassignClass;
                self.newReassignText = newReassignText;
                
                self.updateReassignFields();

                self.showOrHideReassignChildren();

            });

            $("#preset_dropdown li a").on('click', function(e){
                e.preventDefault();

                var newPresetStatus = $(this).data("status");
                var newPresetClass = $(this).data("class");
                var newPresetText = $(this).html();

                self.newPresetStatus = newPresetStatus;
                self.newPresetClass = newPresetClass;
                self.newPresetText = newPresetText;

                self.updatePresetFields();
            });

            $("#owner_dropdown li a").on('click', function(e){
                e.preventDefault();

                var newOwnerStatus = $(this).data("status");
                var newOwnerClass = $(this).data("class");
                var newOwnerText = $(this).html();

                self.newOwnerStatus = newOwnerStatus;
                self.newOwnerClass = newOwnerClass;
                self.newOwnerText = newOwnerText;

                self.updateOwnerFields();
            });

        };

        this.updateRejectFields = function() {

            $("#reject-btn.btn-" + this.currentRejectClass).removeClass("btn-" + this.currentRejectClass).addClass("btn-" + this.newRejectClass );
            $("#reject_reason_addon.addon-" + this.currentRejectClass).removeClass("addon-" + this.currentRejectClass).addClass("addon-" + this.newRejectClass).html(this.rejectaddonMap[this.newRejectClass]);
            $("#reject_reason_input").val(this.newRejectText);

            // update our actual input field with the new status
            $("#status").val(this.newRejectStatus);

            this.currentRejectStatus = this.newRejectStatus;
            this.currentRejectClass = this.newRejectClass;
            this.currentRejectText = this.newRejectText;
        };

        this.updateReassignFields = function() {

            $("#reassign-btn.btn-" + this.currentReassignClass).removeClass("btn-" + this.currentReassignClass).addClass("btn-" + this.newReassignClass );
            $("#reassign_reason_addon.addon-" + this.currentReassignClass).removeClass("addon-" + this.currentReassignClass).addClass("addon-" + this.newReassignClass).html(this.reassignaddonMap[this.newReassignClass]);
            $("#reassign_reason_input").val(this.newReassignText);

            // update our actual input field with the new status
            $("#reassignType").val(this.newReassignStatus);

            this.currentReassignStatus = this.newReassignStatus;
            this.currentReassignClass = this.newReassignClass;
            this.currentReassignText = this.newReassignText;

        };

        this.updatePresetFields = function() {

            var newPresetStatusArray = this.newPresetStatus.split(' - ');
            var addonChars = newPresetStatusArray[0].charAt(0) + newPresetStatusArray[1].charAt(0);

            $("#preset_dropdown-btn.btn-" + this.currentPresetClass).removeClass("btn-" + this.currentPresetClass).addClass("btn-" + this.newPresetClass );
            $("#preset_dropdown_addon.addon-" + this.currentPresetClass).removeClass("addon-" + this.currentPresetClass).addClass("addon-" + this.newPresetClass).html(addonChars);
            $("#preset_dropdown_input").val(this.newPresetText);

            // update our actual input field with the new status
            $("#reassignValue").val(this.newPresetStatus);

            this.currentPresetStatus = this.newPresetStatus;
            this.currentPresetClass = this.newPresetClass;
            this.currentPresetText = this.newPresetText;
        };

        this.updateOwnerFields = function() {

            var newOwnerStatusArray = this.newOwnerStatus.split('.');
            var addonChars = newOwnerStatusArray[0].charAt(0).toUpperCase() + newOwnerStatusArray[1].charAt(0).toUpperCase();

            $("#owner_dropdown-btn.btn-" + this.currentOwnerClass).removeClass("btn-" + this.currentOwnerClass).addClass("btn-" + this.newOwnerClass );
            $("#owner_dropdown_addon.addon-" + this.currentOwnerClass).removeClass("addon-" + this.currentOwnerClass).addClass("addon-" + this.newOwnerClass).html(addonChars);
            $("#owner_dropdown_input").val(this.newOwnerText);

            // update our actual input field with the new status
            $("#reassignValue").val(this.newOwnerStatus);

            this.currentOwnerStatus = this.newOwnerStatus;
            this.currentOwnerClass = this.newOwnerClass;
            this.currentOwnerText = this.newOwnerText;
        };

        this.showOrHideRejectChildren = function() {

            // show or hide other field?
            if(this.newRejectStatus == 4)
            {
                $('#reassign_reason_group').removeClass('hidden');

                var reassignformwidth = $('#reassign_reason_group').width() - 20;
                $('#reassign_dropdown').width(reassignformwidth);

                this.showOrHideReassignChildren();

            }
            else
            {
                if($('#reassign_reason_group').is(":visible"))
                {
                    $('#reassign_reason_group').addClass('hidden');
                }

                if($('#preset_dropdown_group').is(":visible"))
                {
                    $('#preset_dropdown_group').addClass('hidden');
                }

                if($('#owner_dropdown_group').is(":visible"))
                {
                    $('#owner_dropdown_group').addClass('hidden');
                }
            }

        };

        this.showOrHideReassignChildren = function() {

            // show or hide other field?
            if(this.newReassignStatus == 1)
            {
                $('#preset_dropdown_group').removeClass('hidden');

                if($('#owner_dropdown_group').is(":visible"))
                {
                    $('#owner_dropdown_group').addClass('hidden');
                }

                var preset_dropdown_formwidth = $('#preset_dropdown_group').width() - 20;
                $('#preset_dropdown').width(preset_dropdown_formwidth);
            }
            else if(this.newReassignStatus == 2 || this.newReassignStatus == 3)
            {
                $('#owner_dropdown_group').removeClass('hidden');

                if($('#preset_dropdown_group').is(":visible"))
                {
                    $('#preset_dropdown_group').addClass('hidden');
                }

                var owner_dropdown_formwidth = $('#owner_dropdown_group').width() - 20;
                $('#owner_dropdown').width(owner_dropdown_formwidth);
            }
            else
            {
                if($('#preset_dropdown_group').is(":visible"))
                {
                    $('#preset_dropdown_group').addClass('hidden');
                }

                if($('#owner_dropdown_group').is(":visible"))
                {
                    $('#owner_dropdown_group').addClass('hidden');
                }
            }

        };

        // run init function
        this.init();

    };

    $(document).ready(function(e) {

        function customwidth()
        {
            var rejectformwidth = $('#reject_reason_group').width() - 20;
            $('#reject_dropdown').width(rejectformwidth);
        };

        customwidth();
        $(window).resize(function(e) {
            customwidth();
        });

        var myStatus = new statusManager();

    });
</script>
</body>
</html>