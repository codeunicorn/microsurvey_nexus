
function TrimString(sInString) {
  if ( sInString ) {
    sInString = sInString.replace( /^\s+/g, "" );// strip leading
    return sInString.replace( /\s+$/g, "" );// strip trailing
  }
}

// Populates the segment select
function populateSegment(defaultSegment) {
  if (postSegment != '') {
    defaultSegment = postSegment;
  }
  var segmentLineArray = segmentData.split('|');  // Split into lines
  var selObj = document.getElementById('x_udf_primary_market_segment122__ss');
  selObj.options[0] = new Option(window.OptionLabel,'');
  selObj.selectedIndex = 0;
  for (var loop = 0; loop < segmentLineArray.length; loop++) {
    lineArray = segmentLineArray[loop].split(':');
    segmentCode  = TrimString(lineArray[0]);
    segmentLabel  = TrimString(lineArray[1]);
    if (segmentCode != '') {
      selObj.options[loop + 1] = new Option(segmentLabel, segmentCode);
    }
    if (defaultSegment == segmentCode) {
      selObj.selectedIndex = loop + 1;
    }
  }
}

function populateSegmentChild() {
  var foundDivision = false;
  var selObj = document.getElementById('x_udf_division146__ss');
  // Empty options just in case new drop down is shorter
  if (selObj.type == 'select-one') {
	for (var i = 0; i < selObj.options.length; i++) {
	  selObj.options[i] = null;
	}
	selObj.options.length=null;
	selObj.options[0] = new Option(window.OptionLabel,'');
	selObj.selectedIndex = 0;
  }
  // Populate the drop down with divisions associated with the selected segment
  var divisionLineArray = divisionData.split("|");  // Split into lines
  var optionCntr = 1;
  for (var loop = 0; loop < divisionLineArray.length; loop++) {
    lineArray = divisionLineArray[loop].split(":");
    segmentCode = TrimString(lineArray[0]);
    divisionCode = TrimString(lineArray[1]);
    divisionLabel = TrimString(lineArray[2]);
  if (document.getElementById('x_udf_primary_market_segment122__ss').value == segmentCode && segmentCode != '' ) {
    // If it's a hidden element, change it to a select
      if (selObj.type == 'hidden') {
        parentObj = document.getElementById('x_udf_division146__ss').parentNode;
        parentObj.removeChild(selObj);
        var inputSel = document.createElement("SELECT");
        inputSel.setAttribute("name","x_udf_division146__ss");
        inputSel.setAttribute("id","x_udf_division146__ss");
        parentObj.appendChild(inputSel);
        selObj = document.getElementById('x_udf_division146__ss');
        selObj.options[0] = new Option(window.OptionLabel,'');
        selObj.selectedIndex = 0;
		document.getElementById('x_udf_division146__ss_fieldpair').style.display = 'block';
		selectDefaultChild()
      }
      if (divisionCode != '') {
        selObj.options[optionCntr] = new Option(divisionLabel, divisionCode);
      }
      // See if it's selected from a previous post
      if (divisionCode == postDivision && segmentCode == postSegment) {
        selObj.selectedIndex = optionCntr;
      }
      foundDivision = true;
      optionCntr++
    }
  }
  // If the Segment has no Divisions, remove the select
  if (!foundDivision) {
    parentObj = document.getElementById('x_udf_division146__ss').parentNode;
    parentObj.removeChild(selObj);
  // Create a hidden field as a placeholder
     var inputEl = document.createElement("INPUT");
     inputEl.setAttribute("id", "x_udf_division146__ss");
     inputEl.setAttribute("type", "hidden");
     inputEl.setAttribute("name", "x_udf_division146__ss");
     inputEl.setAttribute("class", "form-text");
     inputEl.setAttribute("value", "");
     parentObj.appendChild(inputEl);
	 document.getElementById('x_udf_division146__ss_fieldpair').style.display = 'none';
  }
}

function selectDefaultChild() {
	var selObj = document.getElementById('x_udf_division146__ss');
	var defaultChild = '<?php echo $division_value; ?>';
	for (var i = 0; i < selObj.options.length; i++) {
	  if (selObj.options[i].value == defaultChild) {selObj.selectedIndex = i;}
	}
}

function initSegment(segmentData,OptionLabel) {
  window.OptionLabel = OptionLabel;
  populateSegment(segmentData);
  populateSegmentChild(OptionLabel);
  selectDefaultChild();
}