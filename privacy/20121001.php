<?php
// Common initialization
require_once('../common_init.php');
$config_privacy_details = explode("||",$config_privacy_details);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Privacy Policy</title>

<style>

h1 {
color: #ed1941;
font-size: 22px;
font-weight: 600;
line-height: 1.25;
}

body {
margin: 40px;
color: #333;
}

</style>
</head>

<body>

<div>
  <h1>Privacy Policy</h1>
</div>
<div>
  <p>Please read this privacy policy carefully. Your use of any <?php echo $config_brand_name; ?> Web site constitutes your agreement to this privacy policy (the &quot;<?php echo $config_brand_name; ?> Privacy Policy&quot;) and consent to the terms herein. Please see the section labeled &quot;Information We Collect and How We Use It&quot; to learn more about our collection and use of user information.</p>
  <ul>
    <li><a id="en" name="en"></a><a href="#1">Introduction </a></li>
    <li><a href="#2">Important Terms Used Throughout This Privacy Policy</a></li>
  </ul>
  <p> </p>
  <p><strong>INFORMATION WE COLLECT AND HOW WE USE IT</strong></p>
  <ul>
    <li><a href="#3">If You Visit the Site to Browse</a></li>
    <li><a href="#4">If You Register, Send Us a Communication or Engage in a Transaction While at Our Site.</a></li>
    <li><a href="#5">If You Do Not Wish to Disclose User Information</a></li>
    <li><a href="#6">Cookies</a></li>
  </ul>
  <p> </p>
  <p><strong>ACCESS TO YOUR INFORMATION AND COMPLAINTS</strong></p>
  <ul>
    <li><a href="#7">Accessing, Correcting, Updating and Deleting Your Personal Information</a></li>
    <li><a href="#8">Complaints</a></li>
    <li><a href="#9">Removal or Correction of Personally Identifiable Information</a></li>
    <li><a href="#10">Third Parties and Links to Other Web Sites</a></li>
    <li><a href="#11">Security</a></li>
    <li><a href="#12">Site and Service Updates</a></li>
    <li><a href="#13">Children</a></li>
    <li><a href="#14">Merger, Assets, Sales, etc.</a></li>
    <li><a href="#15">How do I Contact <?php echo $config_brand_name; ?></a></li>
    <li><a href="#16">Miscellaneous</a></li>
  </ul>
  <p> </p>
  <div align="center">
    <hr align="center" size="1" width="100%" noshade="noshade" />
  </div>
  <p> </p>
  <p><a id="1" name="1"></a></p>
  <h3>Introduction</h3>
  <p><?php echo $config_brand_name; ?> (&quot;<?php echo $config_brand_name; ?>,&quot; or &quot;we,&quot; as appropriate) is committed to safeguarding the privacy of visitors to our Web sites and the pages therein (collectively, the &quot;Site&quot;) and users of the other services available through our Site (the &quot;Service(s)&quot;). We will use the efforts described under the &ldquo;Security&rdquo; heading below to ensure that the information you provide us remains private and is used only for the purposes set forth herein. <br />
    The <?php echo $config_brand_name; ?> Privacy Policy details our commitment to protect your information and the limited manner in which we will use that information to provide and improve our provision of the Services. We've developed our Privacy Policy from industry guidelines and standards, and local, national, and international laws and requirements. All privacy practices and methods described herein only apply insofar as permitted by the applicable standards, laws and requirements and as limited by the website or interactive banner you are visiting.</p>
  <p><a id="2" name="2"></a></p>
  <h3>Important Terms Used Throughout This Privacy Policy</h3>
  <p>Throughout the <?php echo $config_brand_name; ?> Privacy Policy we use several specialized terms. &quot;Personally Identifiable Information&quot; is information that tells us specifically who you are, like your name, street address, email address, billing address, credit card number and expiration date. &quot;User Information&quot; means all Personally Identifiable Information and any other forms of information discussed in the <?php echo $config_brand_name; ?> Privacy Policy, including the Internet Protocol (IP) address of a user's computer. We use the term &quot;aggregate&quot; when we combine information from various persons or users. Information is said to be &quot;anonymized&quot; if it does not identify individual persons or entities or associate a particular person or entity with such information.</p>
  <p> </p>
  <h2>INFORMATION WE COLLECT AND HOW WE USE IT</h2>
  <p> </p>
  <p><a id="#3" name="#3"></a></p>
  <h3>If you visit the Site to Browse</h3>
  <p>We will only collect and store the domain name and host from which a user accessed the Internet (e.g., aol.com), the IP address of the user's computer, the user's browser software and operating system, the date and time the user accesses the site and the Internet address of any Web site from which the user linked directly to our site or to which the user links after visiting our site. We use this information to measure the number of visitors to sections of our sites, to determine from where our visitors linked and to where our visitors link from the sites and to help us make our sites more useful. For example, we organize and analyze IP addresses so that we can provide efficient service, enhance security, monitor appropriate usage and produce traffic volume statistics. This type of information is sometimes shared with affiliates of <?php echo $config_brand_name; ?> as well as third parties, such as when we aggregate and disclose site traffic information for marketing and commercial purposes. When we share such information with third parties we will require them to abide by the standards of the <?php echo $config_brand_name; ?> Privacy Policy. <?php echo $config_brand_name; ?> is the sole owner of the information collected on this Site. We will not sell, share or rent this information to others in ways different from that disclosed in the <?php echo $config_brand_name; ?> Privacy Policy.</p>
  <p>This website uses Google Analytics, a web analytics service provided by Google Inc. (&quot;Google&quot;). Google Analytics uses &quot;cookies&quot;, text files that are stored on your computer to help analyze the use of the website. The information generated by the cookie about your use of this website (including your IP address) is sent to a Google server in the U.S. and stored there. Google will use this information to evaluate your use of the website, compiling reports on website activity for website operators and providing other website activity and internet related services. In addition, Google may also transfer this information to third parties if required by law or if third parties process the information on Google's behalf. Google will not associate your IP address with any other data held by Google. You may refuse the use of cookies by changing the settings on your browser, we would point out, however, that you can use in this case not all the features of this website. By using this site, you consent to the processing of data about you by Google in the manner described and for the aforementioned purpose. You can withdraw your consent to the data collection and storage at any time with effect for the future. Alternatively, you can opt Add-on use of Google Analytics (<a href="http://tools.google.com/dlpage/gaoptout?hl=de">http://tools.google.com/dlpage/gaoptout?hl=de</a>), if available for your browser. Given the debate about the use of analytical tools with full IP addresses, we would like to point out that this website uses Google Analytics with the &quot;_anonymizeIp ()&quot; is used and therefore IP addresses will be processed only shortened to exclude a direct personal link.</p>
  <p><a id="4" name="4"></a></p>
  <h3>If You Register, Send Us a Communication or Engage in a Transaction While at Our Site.</h3>
  <p>If you communicate with us, we collect and store the Personally Identifiable Information and User Information you provide through this process, including if applicable) your user ID and password, which we may elect to provide for you. If you engage in a transaction while logged on to our Site, then we collect and store information about the transaction. If you send us communications, then we may collect such communications in a file specific to you.</p>
  <p>We use the information we collect from you as a registered user to build features that we hope will make the Services more attractive and easier for you to use. This may include better customer support and timely notice of new products and services. We may share User Information with our partners, affiliates and joint venturers that are committed to serving your need for the Services and improving your user experience, but will require such third parties to abide by the standards of the <?php echo $config_brand_name; ?> Privacy Policy. We may also share aggregate anonymized data relating to activity on the Site and use of the Services, such as demographical and statistical information, for marketing, research and other purposes. Furthermore, we may need to disclose User Information to law enforcement or other government officials if appropriate for your protection or in connection with an investigation of fraud, intellectual property infringement or other activity that is illegal or may expose us or our users to legal liability. From time to time, our Site may request information from users via surveys or contests. Participation in these surveys or contests is completely voluntary and the user therefore has a choice whether or not to disclose this information. Information requested may include contact information (such as name and address) and demographic information (such as zip code, age level). Contact information will be used to notify the winners and award prizes. Survey information will be used for purposes of monitoring or improving the use and satisfaction of this Site.</p>
  <p>We send responses to all communications from users. Established members will occasionally receive information on products, services or special deals. Out of respect for the privacy of our users we present the option to not receive these types of communications. Please see our choice and opt-out below.</p>
  <p><a id="5" name="5"></a></p>
  <h3>If You Do Not Wish to Disclose User Information</h3>
  <p>If you do not want to provide us with certain User Information then you may opt out by not using the Service that provides us with such User Information. For example, if you do not want us to retain your name, then you may choose not to become a registered user, not to send us a communication, or not to purchase products through the Site. You will not be entitled to the benefits of registration or a response to the communication, but you are still free to browse the Site.  You may opt out from other uses of User Information by sending an opt-out notice to <?php echo $config_privacy_email; ?></p>
  <p><a id="6" name="6"></a></p>
  <h3>Cookies</h3>
  <p>Cookies are small text files that our Site can send to your browser for storage on your hard drive. Cookies can make your use of our Site easier by saving your status and preferences upon visits to our Site. The cookies are refreshed every time you enter the Site. Most browsers are initially set to accept cookies, but you may be able to change the settings to refuse cookies or to be alerted when cookies are being sent. We may use cookies for two purposes: (1) to enable <?php echo $config_brand_name; ?>' website to customize some of its information for your convenience, such as &quot;remembering&quot; the language you selected; and (2) in some instances, to associate you with your User Information. For example, through the use of cookies we may provide a service so that you can login automatically and thereby save you time. Or, we may associate User Information collected merely through your browsing of the Sites to Personal Information collected by our other systems, such as our accounting or customer account systems. Although rejection of cookies will not interfere with your ability to interact with most of our Site, you may need to accept cookies in order to login and use certain interactive Services on the Site.</p>
  <p><strong>List of cookies we collect</strong><br />
    The table below lists the cookies we collect and what information they store.</p>
  <table border="0" cellspacing="0" cellpadding="0">
    <tbody>
      <tr>
        <td nowrap="nowrap"><strong>COOKIE name</strong></td>
        <td nowrap="nowrap"><strong>COOKIE Description</strong></td>
      </tr>
      <tr>
        <td valign="top"><p>CART</p></td>
        <td valign="top"><p>The association with your shopping cart.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>CATEGORY_INFO</p></td>
        <td valign="top"><p>Stores the category info on the page, that allows to display pages more quickly.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>COMPARE</p></td>
        <td valign="top"><p>The items that you have in the Compare Products list.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>CURRENCY</p></td>
        <td valign="top"><p>Your preferred currency</p></td>
      </tr>
      <tr>
        <td valign="top"><p>CUSTOMER</p></td>
        <td valign="top"><p>An encrypted version of your customer id with the store.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>CUSTOMER_AUTH</p></td>
        <td valign="top"><p>An indicator if you are currently logged into the store.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>CUSTOMER_INFO</p></td>
        <td valign="top"><p>An encrypted version of the customer group you belong to.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>CUSTOMER_SEGMENT_IDS</p></td>
        <td valign="top"><p>Stores the Customer Segment ID</p></td>
      </tr>
      <tr>
        <td valign="top"><p>EXTERNAL_NO_CACHE</p></td>
        <td valign="top"><p>A flag, which indicates whether caching is disabled or not.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>FRONTEND</p></td>
        <td valign="top"><p>You sesssion ID on the server.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>GUEST-VIEW</p></td>
        <td valign="top"><p>Allows guests to edit their orders.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>LAST_CATEGORY</p></td>
        <td valign="top"><p>The last category you visited.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>LAST_PRODUCT</p></td>
        <td valign="top"><p>The most recent product you have viewed.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>NEWMESSAGE</p></td>
        <td valign="top"><p>Indicates whether a new message has been received.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>NO_CACHE</p></td>
        <td valign="top"><p>Indicates whether it is allowed to use cache.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>PERSISTENT_SHOPPING_CART</p></td>
        <td valign="top"><p>A link to information about your cart and viewing history if you have asked the site.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>POLL</p></td>
        <td valign="top"><p>The ID of any polls you have recently voted in.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>POLLN</p></td>
        <td valign="top"><p>Information on what polls you have voted on.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>RECENTLYCOMPARED</p></td>
        <td valign="top"><p>The items that you have recently compared.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>STF</p></td>
        <td valign="top"><p>Information on products you have emailed to friends.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>STORE</p></td>
        <td valign="top"><p>The store view or language you have selected.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>USER_ALLOWED_SAVE_COOKIE</p></td>
        <td valign="top"><p>Indicates whether a customer allowed to use cookies.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>VIEWED_PRODUCT_IDS</p></td>
        <td valign="top"><p>The products that you have recently viewed.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>WISHLIST</p></td>
        <td valign="top"><p>An encrypted list of products added to your Wishlist.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>WISHLIST_CNT</p></td>
        <td valign="top"><p>The number of items in your Wishlist.</p></td>
      </tr>
      <tr>
        <td valign="top"><p>TRK.ETRIGUE.COM</p></td>
        <td valign="top"><p>Additional cookies related to User Information collected on our Site.</p></td>
      </tr>
    </tbody>
  </table>
  <p> </p>
  <h2>ACCESS TO YOUR INFORMATION AND COMPLAINTS</h2>
  <p> </p>
  <p><a name="#7" id="#7"></a></p>
  <h3>Accessing, Correcting, Updating and Deleting Your Personal Information</h3>
  <p>If you have submitted personal information to <?php echo $config_brand_name; ?> through the Site, or if someone else has submitted your personal information through the Site you can: <br />
    Access, Correct, Update Your Personal Information by sending updates <a href="mailto:<?php echo $config_privacy_email; ?>">here</a>, or sending a letter to:<br />
    <?php
    echo $config_brand_name."<br />";
    foreach ($config_privacy_details as $val) {
		echo $val."<br />";
	}
    ?>
    </p>
    <?php echo $config_brand_name; ?> will use reasonable efforts to supply you with the information you requested to access and to correct any factual inaccuracies in this information. <br />
    Delete Your Personal Information by sending the request to this <a href="mailto:<?php echo $config_privacy_email; ?>">email address</a>, specifying your request and giving your email address (important: must be the one you previously submitted). We will then use reasonable efforts to remove your personal information from our files.</p>
  <p><a name="8" id="8"></a></p>
  <h3>Complaints</h3>
  <p><?php echo $config_brand_name; ?> is committed to working with consumers to obtain a fair and rapid resolution of any complaints or disputes about privacy. Please send us your questions or comments regarding our privacy practices either via <a href="mailto:<?php echo $config_privacy_email; ?>">email</a> or sending a letter to: <br />
    <?php
    echo $config_brand_name."<br />";
    foreach ($config_privacy_details as $val) {
		echo $val."<br />";
	}
    ?>
  </p>
  <p><?php echo $config_brand_name; ?> will be happy to respond to your questions and comments.</p>
  <p><a name="9" id="9"></a></p>
  <h3>Removal or Correction of Personally Identifiable Information</h3>
  <p>Upon your request, we may remove your Personally Identifiable Information from our database and correct any errors you identify in such information. If Personally Identifiable Information is removed, you will lose access to the parts of the Site for which you must provide such Personally Identifiable Information to gain access. After removal, we will only retain copies of such information as are necessary for us to comply with governmental orders, resolve disputes, troubleshoot problems, enforce any agreement you have entered into through our Site and as otherwise reasonably necessary. Any Personally Identifiable Information provided as a part of a transaction on the Site represents a record of that business transaction that cannot be altered after the transaction is complete.</p>
  <p><a name="10" id="10"></a></p>
  <h3>Third Parties and Links to Other Web Sites</h3>
  <p>The <?php echo $config_brand_name; ?> Privacy Policy only addresses the use and disclosure of User Information collected by <?php echo $config_brand_name; ?>. If you disclose information to other parties, different rules may apply to their use or disclosure of such information regardless of their affiliation or relationship with <?php echo $config_brand_name; ?>. Because <?php echo $config_brand_name; ?> does not control the privacy practices of third parties you are subject to the privacy customs and policies of those third parties. When you link to another Web site, you are subject to the privacy policy of that Web site. We encourage you to ask questions and review the applicable privacy policy before you disclose your personal information to third parties.</p>
  <p>We do not try to control, and disclaim responsibility for, information provided by other users or third parties that is made available through our Site. Such information may contain errors, intentional or otherwise, or may be offensive, inappropriate or inaccurate, and in some cases will be mislabeled or deceptively labeled.</p>
  <p><a name="11" id="11"></a></p>
  <h3>Security</h3>
  <p>This Site takes every precaution to protect our users' information. <?php echo $config_brand_name; ?> has implemented technology and security features and strict policy guidelines to safeguard the privacy of your Personally Identifiable Information from unauthorized access or improper use, and we will continue to enhance our security procedures as new technology becomes available.</p>
  <p><a name="12" id="12"></a></p>
  <h3>Site and Service Updates</h3>
  <p>We may send Site and Service announcement updates to users. We communicate with the user to provide requested Services via email or phone.</p>
  <p><strong>Choice/Opt-out</strong><br />
    Users who no longer wish to receive our notices or promotional materials may opt-out of receiving these communications by sending and email to replying to <a href="mailto:<?php echo $config_privacy_email; ?>"><?php echo $config_privacy_email; ?></a> with &quot;Unsubscribe / Manage Subscriptions&quot; written in the subject line of the email, or sending a letter to:<br />
    <?php
    echo $config_brand_name."<br />";
    foreach ($config_privacy_details as $val) {
		echo $val."<br />";
	}
    ?>
  </p>
  <p><a name="13" id="13"></a></p>
  <h3>Children</h3>
  <p><?php echo $config_brand_name; ?> joins the industry in recognizing that children may not be able to make informed choices about personal information requested online. Accordingly, <?php echo $config_brand_name; ?> does not target children under the age of 13 for collection of information online. <?php echo $config_brand_name; ?> does not solicit or collect customer identifiable information from children under the age of 13. If a child has provided us with personal information, a parent or guardian of that child may contact us at the addresses listed elsewhere in this Privacy Policy if they would like this information deleted from our records. We will use reasonable efforts to delete the child's information from our databases.</p>
  <p><a name="14" id="14"></a></p>
  <h3>Merger, Assets, Sales, etc.</h3>
  <p>If <?php echo $config_brand_name; ?> or any of its lines of business is sold, pledged or disposed of as a going concern whether by merger, sale of assets, bankruptcy or otherwise, then the user database of <?php echo $config_brand_name; ?> could be sold as part of that transaction and all User Information accessed by such successor or purchaser.</p>
  <p><a name="15" id="15"></a></p>
  <h3>How do I Contact <?php echo $config_brand_name; ?></h3>
  <p>If you have any questions regarding the <?php echo $config_brand_name; ?> Privacy Policy, you can contact <?php echo $config_brand_name; ?> via <a href="mailto:<?php echo $config_privacy_email; ?>">email</a>.</p>
  <p><a name="16" id="16"></a></p>
  <h3>Miscellaneous</h3>
  <p><?php echo $config_brand_name; ?> may from time to time change the <?php echo $config_brand_name; ?> Privacy Policy with or without notice, so please check it regularly for any changes or updates. For future reference, <?php echo $config_brand_name; ?> will make a link to the current <?php echo $config_brand_name; ?> Privacy Policy available to you on your screen when you are on our Site. Your use of the Site constitutes your agreement to the <?php echo $config_brand_name; ?> Privacy Policy and your assent to any modification thereto. If you do not agree with any of these terms in, please do not use this site or submit any personal information. This English version shall always prevail in case of conflicts between it and any translations of the <?php echo $config_brand_name; ?> Privacy Policy.</p>
</div>
</body>
</html>