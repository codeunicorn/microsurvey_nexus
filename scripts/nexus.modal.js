window["nexusVars" + nexusMultiple]["modal"] = function()
{

    var method = {},
        $overlay,
        $modal,
        $content,
        $close,
        $accept,
        $close2,
        $accept2,
        $style;

    // Append the HTML
    $height = nQuery(window).height() - 100;
    $modalStyle = 'position:fixed;background:#fff;z-index:9999;top:5%;left:15%;height:90%;width:70%;overflow-y: scroll;';


    $overlay = nQuery('<div id="nexus_overlay" style="position: fixed;top: 0;left: 0;width: 100%;height: 100%;background: #000;opacity: 0.7;filter: alpha(opacity=0.7);z-index:9999;"></div>');
    $modal = nQuery('<div id="nexus_modal" style="'+ $modalStyle +'"></div>');
    $content = nQuery('<div id="nexus_content" style="border-radius:4px;background:#fff;padding:40px 20px 60px 20px;text-align:left;"></div>');

    $close = nQuery('<a id="nexus_close" href="javascript:void(0)" style="text-align: center;text-decoration: none;font-size: 20px;color: white !important;width: 100px;padding: 10px 10px;border-radius: 4px;background: #999;display: block;margin: 0px auto 20px;">Back</a>');
    $accept = nQuery('<a id="nexus_accept" href="javascript:void(0)" style="text-align: center;text-decoration: none;font-size: 20px;color: white !important;width: 100px;padding: 10px 10px;border-radius: 4px;background: #333;display: block;margin: 0px auto 20px;">Accept</a>');
/*
    $close2 = nQuery('<div style="width:50%;margin:20px auto;text-align: center;"><a id="nexus_close2" href="javascript:void(0)" style="display:inline-block;text-align: center;text-decoration: none;font-size: 20px;color: white !important;width: 100px;padding: 10px 10px;border-radius: 4px;background: #999;left: 50%;">Back</a>');
    $accept2 = nQuery('<a id="nexus_accept2" href="javascript:void(0)" style="display:inline-block;text-align: center;text-decoration: none;font-size: 20px;color: white !important;width: 100px;margin: 10px 0px 0px 50px;padding: 10px 10px;border-radius: 4px;background: #333;left: 50%;">Accept</a>');
*/
    $stylesheet = nQuery('<style>#nexus_content p{padding-left:10px;}</style>');

    $modal.hide();
    $overlay.hide();
    $modal.append($stylesheet, $content, $close, $accept);

    nQuery(document).ready(function(){
        nQuery('body').append($overlay, $modal);
    });

    // Center the modal in the viewport
    /*
    method.center = function () {
        var top, left;

        top = Math.max(nQuery(window).height() - $modal.outerHeight(), 0) / 2;
        left = Math.max(nQuery(window).width() - $modal.outerWidth(), 0) / 2;


        $modal.css({
            top:top + nQuery(window).scrollTop(),
            left:left + nQuery(window).scrollLeft()
        });

    };
    */

    // Open the modal
    method.open = function (settings) {
        $content.empty().append(settings.content);

        //$modal.css({
        //    width: settings.width || 'auto',
        //    height: settings.height || 'auto'
        //})

        // if there's a vertical scroll bar
        //if (nQuery("body").height() >= nQuery(window).height())
        //{
        //    method.center();
        //}

        //nQuery(window).bind('resize.modal', method.center);

        $modal.show();
        $overlay.show();
    };

    // Close the modal
    method.close = function () {
        $modal.hide();
        $overlay.hide();
        $content.empty();
        //nQuery(window).unbind('resize.modal');
    };

    $close.click(function(e){
        e.preventDefault();
        method.close();
    });
    /*
    $close2.click(function(e){
        e.preventDefault();
        method.close();
    });
*/
    $accept.click(function(e){
        e.preventDefault();
        method.close();
        nQuery('#x_udf_privacy_policy_version143').prop('checked', true);
    });
    /*
    $accept2.click(function(e){
        e.preventDefault();
        method.close();
        nQuery('#x_udf_privacy_policy_version143').prop('checked', true);
    });
*/
    return method;
};
