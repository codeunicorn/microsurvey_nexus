
window['nexus' + nexusMultiple] = {

    nexusVars : window["nexusVars" + nexusMultiple],

    nexusMultiple: nexusMultiple,

    loadingImage : function(){
        var self = this;
        return nQuery('<img class="form-results-img" src="'+ window['protocol'] + self.nexusVars.externalRoot +'images/loading.gif">');
    },

    errorImage : function() {
        var self = this;
        return nQuery('<img class="form-results-img" src="'+ window['protocol'] +  self.nexusVars.externalRoot +'images/x.gif"> <a class="form-results-msg" href="mailto:'+ self.nexusVars.config_report_errors_to +'">'+ self.nexusVars.config_report_text_label +'</a>.');
    },

    successImage : function() {
        var self = this;
        return nQuery('<img class="form-results-img" src="'+ window['protocol'] +  self.nexusVars.externalRoot + 'images/checkmark.gif">');
    },

    etrLoadScript: function(location)
    {
        this.script = document.createElement("script");
        this.script.src = location;
        this.script.type="text/javascript";
        document.getElementsByTagName("head")[0].appendChild(this.script);
    },

    checkData : function()
    {
        // clear previous errors
        nQuery('.has-error').removeClass('has-error');

        var ready = true,
            nexusVars = this.nexusVars,
            eMsg = nexusVars.config_required_list_label +':\n',
            errorArray = [],
            nexusId = 'etrReg' + this.nexusMultiple,
            nexusForm = document.getElementById(nexusId);

        if(window['nexus' + this.nexusMultiple].isEmpty(nexusForm.x_emailaddress.value) || ! window['nexus' + this.nexusMultiple].isEmailValid(nexusForm.x_emailaddress.value))
        {
            ready = false;
            eMsg = eMsg +'\xb7 '+ nexusVars.config_valid_email_label +'\n';
            errorArray.push(nQuery('#x_emailaddress'+ this.nexusMultiple +'_fieldpair'));
        }

        var configRequiredList = nexusVars.config_required_list.length > 0 ? nexusVars.config_required_list.split('||') : [];

        for(var n in configRequiredList)
        {
            if(configRequiredList.hasOwnProperty(n))
            {
                var requiredVar = configRequiredList[n].replace("[]", "") + this.nexusMultiple,
                    requiredLabel = nexusVars[requiredVar + '_label'];

                // hack for Cyclone form
                var isCyclone = requiredVar == 'x_udf_ext_hds_question_cyclone451__ms';

                if(isCyclone) {
                    requiredLabel = 'What module(s) do you need? (select all that apply)';
                    var atLeastOneIsChecked = nQuery('input[name="x_udf_ext_hds_question_cyclone451__ms[]"]:checked').length > 0;

                    if(! atLeastOneIsChecked) {
                        ready = false;
                        eMsg = eMsg +"\xb7"+ requiredLabel + "\n";
                    }
                }

                // for everything else
                else {
                    var isEmpty = window['nexus' + this.nexusMultiple].isEmpty(nexusForm[requiredVar].value);

                    if (isEmpty) {
                        ready = false;
                        eMsg = eMsg + "\xb7" + requiredLabel + "\n";

                        errorArray.push(nQuery('#' + requiredVar + '_fieldpair'));
                    }
                }
            }
        }

        if(nexusVars.x_udf_privacy_policy_version143 && nexusVars.x_udf_privacy_policy_version143 != '')
        {
            if(! nQuery('#' + nexusId + ' [name="x_udf_privacy_policy_version143"]').is(":checked"))
            {
                ready = false;
                eMsg = eMsg+"\n"+ nexusVars.config_valid_privacy_label.replace(/\\r\\n|\\r|\\n/gm, "\n") +"\n";

                errorArray.push(nQuery('#x_udf_privacy_policy_version143'+ this.nexusMultiple +'_fieldpair'));
            }
        }

        if(!ready) {

            if(!nexusVars.useBootstrap) {
                alert(eMsg);
            }
            else {

                for(var i = 0, j = errorArray.length; i < j; i++)
                {
                    errorArray[i].addClass('has-error');
                }

            }
        }
        else {
            window['nexus' + this.nexusMultiple].showLoading("#"+ nexusId +' #form-results' + this.nexusMultiple);
            window['nexus' + this.nexusMultiple].submitAjax();
        }
    },

    submitAjax : function()
    {
        var nexusId = 'etrReg' + this.nexusMultiple,
            nexusForm = document.getElementById(nexusId),
            nexusVars = this.nexusVars,
            self = this;

        // disable the button
        nQuery('#etrSubmit' + this.nexusMultiple).prop("disabled",true);

        nQuery.ajax({
            type: 'POST',
            url: window['protocol'] + nexusVars.externalRoot + 'map_owner.php?' + nexusVars.querystring_code,
            cache: false,
            async: true,
            global: false,
            crossDomain: true,
            dataType: "json",
            data: nQuery("#" + nexusId).serialize(),

            success: function(dat) {
                this.ownerID = dat.owner.owner_id;
                this.ownerLabel = dat.owner.owner_shortcode;

                if (self.nexusVars.origin_override) {
                    nexusForm.x_udf_origin_code114__ss.value = self.nexusVars.origin_value;
                }

                if (this.ownerID.match(/^-?\d+$/)) {
                    nexusForm.espUsers.value = this.ownerID;
                    nexusForm.x_udf_form_owner148.value = this.ownerLabel;
                }

                if(dat.hasOwnProperty('coupon')){
                    nexusForm.x_udf_coupon_code472.value = dat.coupon.coupon_code;
                }

                var inputs = nexusForm.getElementsByTagName("input");
                for(var i = 0; i < inputs.length; i++) {
                    if(inputs[i].type == "checkbox") {
                        var newName = inputs[i].getAttribute("name").replace("[]", "");
                        inputs[i].name = newName;
                    }
                }

                // debug mode
                if(dat.debug)
                {
                    alert(
                        'You submittted the following data:\n' + JSON.stringify(dat.submitted, null, "\t")
                            + '\n\nReturned Data (Owner info):\n' + JSON.stringify(dat.owner, null, "\t")
                            + '\n\nAdditional info:\n' + JSON.stringify(dat.additional, null, "\t")
                    );
                }

                if(self.nexusVars.externalRoot !== 'nexus.microsurvey.local/' || dat.debug)
                {
                    // track conversion
                    self.trackConversion();

                    // submit etrigue data
                    self.submitEtrigue(979);
                }
                else
                {
                    window.location = 'core.php?status=confirmation&'+ self.nexusVars.querystring_code;
                }
            },

            error:function(xhr, status, errorThrown) {
                window['nexus' + self.nexusMultiple].showError("#" + nexusId +' #form-results' + self.nexusMultiple);
            },

            complete: function() {
            }

        });
    },

    trackConversion : function()
    {
        var nexusVars = this.nexusVars;

        // set google variables as globals
        window.google_conversion_id = nexusVars.adwords_id ? nexusVars.adwords_id : (nexusVars.adwords_code ? nexusVars.adwords_code : null);
        window.google_conversion_language = "en";
        window.google_conversion_format = "3";
        window.google_conversion_color = "666666";
        window.google_conversion_label = nexusVars.adwords_label ? nexusVars.adwords_label : "YEc6CNHPZRCj3fb-Aw";
        window.google_conversion_value = 0;


        var img = document.createElement("img");
        var goalId = window.google_conversion_id;
        var language = "en";
        var format = "3";
        var randomNum = new Date().getMilliseconds();
        var value = 0;
        var label = "YEc6CNHPZRCj3fb-Aw";
        var conversion_color = "666666";
        var url = encodeURI(location.href);

        var trackUrl = window['protocol'] + "www.googleadservices.com/pagead/conversion/"+goalId+"/?random="+randomNum+"&value="+value+"&language="+language+"&format="+format+"&label="+label+"&guid=ON&script=0&url="+url;
        img.src = trackUrl;
        document.body.appendChild(img);
    },

    submitEtrigue : function(id)
    {
        var nexusId = 'etrReg' + this.nexusMultiple,
            nexusForm = document.getElementById(nexusId),
            nexusVars = this.nexusVars,
            self = this;

        var etrigueForm = new EtrigueForm(id);
        etrigueForm.submitClassic(nexusId,
        function(dat) {
            var that = self,
                thatNexusId = nexusId,
                thatNexusVars = nexusVars,
                thatNexusForm = nexusForm;

            if(dat.err) {
                that.showError("#" + thatNexusId +' #form-results'+ that.nexusMultiple);
                return;
            }

            etrigueFormSuccess = true;
            that.showSuccess("#" + thatNexusId +' #form-results'+ that.nexusMultiple);

            if(dat.thankYouPage) {
                if(dat.repost) {
                    thatNexusForm.action = dat.thankYouPage+'?status=confirmation&'+ thatNexusVars.querystring_code;
                    thatNexusForm.submit();
                } else {
                window.location = dat.thankYouPage+'?status=confirmation&'+ thatNexusVars.querystring_code;
                }
            }
        });
    },

    showLoading : function(element)
    {
        nQuery(element).html(this.loadingImage());
        nQuery("#etrSubmit" + this.nexusMultiple).prop('disabled', true);
        etrigueFormSuccess = false;
        nQuery.support.cors = true;
    },

    showError : function(element)
    {
        nQuery(element).html(this.errorImage());
    },

    showSuccess : function(element)
    {
        nQuery(element).html(this.successImage());
    },

    init: function()
    {
        var thisMultiple = this.nexusMultiple;
        var self = this;

        nQuery('.required-entry').on('blur', function(){
            var multiple = thisMultiple;
            if( ! window['nexus' + multiple].isEmpty(nQuery(this).val())) {
                var id = nQuery(this).attr('id');
                nQuery('#' + id + '_fieldpair').removeClass('has-error');
            }
        });

        // click event for consent checkbox
        if(nQuery('#x_udf_privacy_policy_version143' + thisMultiple).length > 0 && nQuery('#x_emailconsent' + thisMultiple).length > 0 && nQuery('#x_emailconsentcontent' + thisMultiple).length > 0){
            nQuery("#x_udf_privacy_policy_version143" + thisMultiple).change(function() {
                var consent_yes = 535,
                    consent_no = 536,
                    consent_value = nQuery('#x_udf_privacy_policy_version143_label' + thisMultiple).text();

                if(this.checked) {
                    nQuery('#x_emailconsent' + thisMultiple).val(consent_yes);
                    nQuery('#x_emailconsentcontent' + thisMultiple).val(consent_value);
                } else {
                    nQuery('#x_emailconsent' + thisMultiple).val(consent_no);
                    nQuery('#x_emailconsentcontent' + thisMultiple).val('');
                }
            });
        }

        // set click event for form submit button
        nQuery('#etrSubmit' + thisMultiple).on('click', function(){
            self.checkData();
        });

        window['nexus' + this.nexusMultiple].initSelects(this.nexusVars.dropdownData);
        window['nexus' + this.nexusMultiple].loadPrivacyPolicy();
    },

    initSelects: function(dropdownsJson)
    {
        var has_x_show_products = false,
            productId = 'x_show_products' + this.nexusMultiple;

        for(var n = 0; n < dropdownsJson.length; n++)
        {
            var item = dropdownsJson[n];
            var itemId = item.id;
            var myMethod, params;

            if(itemId === 'x_show_products' + this.nexusMultiple)
            {
                has_x_show_products = true;
            }

            // this is the nQuery element that this dropdown belongs to
            var myElement = document.getElementById(itemId);

            if((item.type === 'select' || item.type === 'form-input-select' || item.type === 'form-input-special') && myElement !== null && myElement.tagName === 'SELECT')
            {
                var value =  item.value,
                    selected = item.selected ? 'selected' : '',
                    html = item.label,
                    disabled = item.hasOwnProperty('disabled') && item.disabled === true ? 'disabled' : '';

                var myOption = document.createElement("option");
                myOption.value = value;
                myOption.innerHTML = html;

                if(item.selected)
                {
                    myOption.selected = true;

                    if(item.id === 'x_show_products' + this.nexusMultiple)
                    {
                        productId = item.code;
                    }
                }

                if(item.hasOwnProperty('disabled') && item.disabled === true)
                {
                    myOption.disabled = true;
                }

                // data attribute
                if(item.hasOwnProperty('data'))
                {
                    for(var mydata in item.data)
                    {
                        if(item.data.hasOwnProperty(mydata))
                        {
                            var thedata = item.data[mydata];
                            myOption.setAttribute('data-'+ thedata.name, thedata.value);
                        }
                    }
                }

                if(item.hasOwnProperty('class'))
                {
                    myElement.className += ' ' + item.class;
                }

                // if this item has javascript triggers
                if(item.hasOwnProperty('js')) {

                    for(var myattribute in item.js)
                    {
                        if(item.js.hasOwnProperty(myattribute))
                        {
                            myMethod = item.js[myattribute]['function'];
                            params = item.js[myattribute].hasOwnProperty('params') ? item.js[myattribute].params : '';

                            myElement.onchange = function(myMethod, myParams){
                                var nexusMultiplier = this.nexusMultiple;
                                return function(){
                                    window['nexus' + nexusMultiplier][myMethod](this, myParams);
                                };
                            }(myMethod,params);
                        }
                    }
                }

                myElement.appendChild(myOption);
            }


            else if((item.type === 'checkbox' || item.type === 'form-input-checkbox' ||  item.type === 'form-input-checkbox-multi') && myElement !== null)
            {
                // create our input container
                var checkboxContainer = document.createElement('div');
                checkboxContainer.className = 'control-group checkbox checkbox-success';

                // create our checkbox input
                var checkboxInput = document.createElement('input');
                checkboxInput.id = 'product'+  + this.nexusMultiple +'-' + n;
                checkboxInput.type = 'checkbox';
                checkboxInput.name = item.type == 'form-input-checkbox-multi' ? item.name + '[]' : item.name;
                checkboxInput.className = item.type;
                checkboxInput.value = item.value;
                checkboxInput.selected = item.selected;

                // if this item has javascript triggers
                if(item.hasOwnProperty('js'))
                {
                    for(var theattribute in item.js)
                    {
                        myMethod = item.js[theattribute]['function'];
                        params = item.js[theattribute].hasOwnProperty('params') ? item.js[theattribute].params : '';

                        checkboxInput.onclick = function(myMethod, myParams){
                            return function(){
                                window['nexus' + this.nexusMultiple][myMethod](this, myParams);
                            };
                        }(myMethod,params);
                    }
                }

                // add our input to the checkbox container
                checkboxContainer.appendChild(checkboxInput);

                // create a label element
                var checkboxLabel = document.createElement("label");
                checkboxLabel.className = 'control-label';
                checkboxLabel.style = 'float:none';
                checkboxLabel.setAttribute('for', 'product'+  + this.nexusMultiple +'-' + n);
                checkboxLabel.innerHTML = item.label;

                // add our label to the checkbox container
                checkboxContainer.appendChild(checkboxLabel);

                // finally, append our entire container to the element
                myElement.appendChild(checkboxContainer);
            }

            // if this item has children, we want to initialize the children dropdown
            if(item.hasOwnProperty('children') && item.children.length > 0)
            {
                if(item.selected)
                {
                    window['nexus' + this.nexusMultiple].initSelects(item.children);
                }
            }

            if(itemId === 'x_udf_primary_market_segment122__ss' + this.nexusMultiple && item.selected === true && item.code === 'select_first_dropdown_option')
            {
                nQuery('#x_udf_division146__ss' + this.nexusMultiple +'_fieldpair').hide();
                nQuery('#x_udf_main_application121__ss' + this.nexusMultiple +'_fieldpair').hide();
            }

            if(itemId === 'x_country' + this.nexusMultiple && item.selected === true) {
                nQuery('#x_udf_country_iso_2481').val(item.iso2);
                nQuery('#x_udf_country_iso_3482').val(item.iso3);
            }

        }

        if(has_x_show_products)
        {
            nQuery('#x_show_products' + this.nexusMultiple).attr('name', productId);
        }

        // if jQuery chosen plugin is loaded from 3rd party
        if(window.jQuery && window.jQuery().chosen && nQuery('#x_state' + this.nexusMultiple).is("select") ) {
                window.jQuery( '#x_state' + this.nexusMultiple ).chosen();
        }

    },

    reloadSelect: function(elem) {

        var $elem = nQuery(elem);

        // our element's id and value
        var elemId = $elem.attr('id');
        var elemVal = $elem.val();
        var nexusVars = this.nexusVars;

        // iterate through our data to get the match for this element
        for(var n = 0, len = nexusVars.dropdownData.length; n < len; n++)
        {
            var item = nexusVars.dropdownData[n],
                itemId = item.id;

            if(itemId === elemId && item.value === elemVal)
            {
                // if the item has no children
                if(!item.hasOwnProperty('children') || (item.hasOwnProperty('children') && item.children.length === 0))
                {
                    // case for segment
                    if(elemId === 'x_udf_primary_market_segment122__ss' + this.nexusMultiple)
                    {
                        nQuery('#x_udf_division146__ss' + this.nexusMultiple +'_fieldpair').hide();
                        nQuery('#x_udf_main_application121__ss' + this.nexusMultiple +'_fieldpair').hide();
                    }

                    // case for country
                    if(elemId === 'x_country' + this.nexusMultiple)
                    {
                        // change the state select to a text input
                        var parent = nQuery('#x_state' + this.nexusMultiple).parent();
                        nQuery('#x_state' + this.nexusMultiple).remove();

                        var inputState = document.createElement("input");
                        inputState.id = 'x_state' + this.nexusMultiple;
                        inputState.name = 'x_state';
                        inputState.selected = item.seleted;
                        inputState.className = nexusVars.useBootstrap ? ' input-text form-control' : ' input-text';
                        inputState.type = 'text';

                        parent.append(inputState);

                        // if jQuery chosen plugin is loaded from 3rd party
                        if(window.jQuery && window.jQuery().chosen && nQuery('#x_state'+ this.nexusMultiple +'_chosen').length > 0 ) {
                            nQuery('#x_state'+ this.nexusMultiple +'_chosen').remove();
                        }
                    }
                }

                // if the item has children
                else
                {

                    // get our children for this dropdown data
                    var childrenData = item.children;

                    var childIdsArray = [];

                    for(var i = 0, leng = childrenData.length; i < leng; i++)
                    {
                        if(childIdsArray.indexOf(childrenData[i].id) === -1)
                        {
                            childIdsArray.push(childrenData[i].id);
                        }
                    }

                    for(var j = 0, lenj = childIdsArray.length; j < lenj; j++)
                    {
                        // get the element id of the children
                        var childElem = nQuery('#'+ childIdsArray[j] +  + this.nexusMultiple);
                        childElem.empty();

                        // case for segment
                        if(elemId === 'x_udf_primary_market_segment122__ss' + this.nexusMultiple)
                        {
                            childIdsArray.indexOf('x_udf_division146__ss'+ this.nexusMultiple) !== -1 ? nQuery('#x_udf_division146__ss'+ this.nexusMultiple +'_fieldpair').show() : nQuery('#x_udf_division146__ss' + this.nexusMultiple +'_fieldpair').hide();
                            childIdsArray.indexOf('x_udf_main_application121__ss'+ this.nexusMultiple) !== -1 ? nQuery('#x_udf_main_application121__ss'+ this.nexusMultiple +'_fieldpair').show() : nQuery('#x_udf_main_application121__ss' + this.nexusMultiple +'_fieldpair').hide();
                        }
                    }

                    // case for country
                    if(elemId === 'x_country' + this.nexusMultiple)
                    {
                        // change the state input to a select
                        var parent = nQuery('#x_state' + this.nexusMultiple).parent();
                        nQuery('#x_state' + this.nexusMultiple).remove();

                        var selectState = document.createElement("select");
                        selectState.id = 'x_state' + this.nexusMultiple;
                        selectState.name = 'x_state';
                        selectState.className = nexusVars.useBootstrap ? ' form-control' : ' input-text';
                        selectState.selected = item.selected;

                        parent.append(selectState);

                        // if jQuery chosen plugin is loaded from 3rd party
                        if(window.jQuery && window.jQuery().chosen && nQuery('#x_state'+ this.nexusMultiple +'_chosen').length > 0 ) {
                            nQuery('#x_state'+ this.nexusMultiple +'_chosen').remove();
                        }
                    }

                    window['nexus' + this.nexusMultiple].initSelects(childrenData);
                }

                // case for country
                if(elemId === 'x_country' + this.nexusMultiple) {

                    // update ISO-2 and ISO-3 fields
                    if( nQuery( '#x_udf_country_iso_2481' ).length > 0 && item.hasOwnProperty('iso2') ) {
                        nQuery( '#x_udf_country_iso_2481' ).val(item.iso2);
                    }

                    // update ISO-2 and ISO-3 fields
                    if( nQuery( '#x_udf_country_iso_3482' ).length > 0 && item.hasOwnProperty('iso3') ) {
                        nQuery( '#x_udf_country_iso_3482' ).val(item.iso3);
                    }

                }
            }
        }
    },

    loadSolution : function(elem) {
        var $elem = nQuery(elem),
            elemVal = $elem.val(),
            data = null;

        for(var n = 0, len = nexusVars.dropdownData.length; n < len; n++)
        {
            data = nexusVars.dropdownData[n];

            if(data.type === 'sol' && data.code === elemVal)
            {
                nQuery('#x_udf_primary_market_segment122__ss' + this.nexusMultiple).val(data.segment);
                nQuery('#x_udf_division146__ss' + this.nexusMultiple).val(data.div);
            }
        }
    },

    checkStatus : function() {
        if(nQuery("#nstatus" + this.nexusMultiple).length > 0)
        {
            nQuery("#nstatus" + this.nexusMultiple).remove();
            nQuery("#nstatus" + this.nexusMultiple +"_label").remove();
        }

        var shouldAddHiddenStatus = true,
            nexusId = 'etrReg' + this.nexusMultiple;

        nQuery('.status_type').each(function(){
            var opt = nQuery(this).find(":selected");

            if(opt.data('utility') !== undefined)
            {
                var status = opt.data('utility');

                if(status == 'override')
                {
                    var statusInput = document.createElement("input");
                    statusInput.id = 'nstatus'  + this.nexusMultiple;
                    statusInput.name = 'nstatus';
                    statusInput.type = 'hidden';
                    statusInput.value = '0';
                    nQuery("#" + nexusId).append(statusInput);

                    var statusInput = document.createElement("input");
                    statusInput.id = 'nstatus' + this.nexusMultiple +'_label';
                    statusInput.name = 'nstatus_label';
                    statusInput.type = 'hidden';
                    statusInput.value = 'Pending';
                    nQuery("#" + nexusId).append(statusInput);

                    shouldAddHiddenStatus = false;
                    return false;
                }

                if(status !== 'inclusive')
                {
                    shouldAddHiddenStatus = false;
                }
            }
            else
            {
                shouldAddHiddenStatus = false;
            }
        });

        if( shouldAddHiddenStatus )
        {
            var statusInput = document.createElement("input");
            statusInput.id = 'nstatus' + this.nexusMultiple;
            statusInput.name = 'nstatus';
            statusInput.type = 'hidden';
            statusInput.value = '0';
            nQuery("#" + nexusId).append(statusInput);

            var statusInput = document.createElement("input");
            statusInput.id = 'nstatus'+ this.nexusMultiple +'_label';
            statusInput.name = 'nstatus_label';
            statusInput.type = 'hidden';
            statusInput.value = 'Pending';
            nQuery("#"+ nexusId).append(statusInput);

            return;
        }

    },

    addProductToEtrigueList: function(elem, type) {

        // this is the element we just selected
        var $elem = nQuery(elem),

        // this is the value of the element we just selected
        $elemValue = $elem.val(),

        // this is the id of the element we just selected
        $elemId = $elem.attr('id'),

        // this will hold the product we just selected
        $product = null;

        // go through our dropdownData array and get the matching product shortcode
        for(var n = 0, len = nexusVars.dropdownData.length; n < len; n++)
        {
            if(type === 'select' && nexusVars.dropdownData[n].attribute === 'prod' && nexusVars.dropdownData[n].value === $elemValue && nexusVars.dropdownData[n].id === $elemId)
            {
                $product = nexusVars.dropdownData[n];
            }

            if(type === 'checkbox' && nexusVars.dropdownData[n].attribute === 'prod' && nexusVars.dropdownData[n].value == $elemValue)
            {
                $product = nexusVars.dropdownData[n];
            }
        }

        // our product shortcode
        var $productId = $product.id,
        $productShortcode = $product.shortcode,
        $productName = $product.name;

        // get our stored chosenProducts object
        var chosenProductsObject = window['nexus' + this.nexusMultiple].chosenProductsObject || {};

        // if it's not already in our chosen products object and it's a checkbox and it's checked, add it
        if (!chosenProductsObject.hasOwnProperty($productName) && type === 'checkbox' && $elem.is(':checked')) {
            chosenProductsObject[$productName] = $productShortcode;
        }

        // if it's already in our chosen products object and it's a checkbox and it's unchecked, remove it
        else if (chosenProductsObject.hasOwnProperty($productName) && type === 'checkbox' && !$elem.is(':checked')) {
            delete chosenProductsObject[$productName];
        }

        // if it's not in our chosen products object and it's a select field, add it
        else if (!chosenProductsObject.hasOwnProperty($productId) && type === 'select') {
            chosenProductsObject[$productId] = $productShortcode;

            if ($elem.attr('name') !== $product.name) {
                $elem.attr('name', $product.name);
            }
        }

        // if it's already in our chosen products object and it's a select field, remove it
        else if (chosenProductsObject.hasOwnProperty($productId) && type === 'select') {
            if ($productShortcode !== '')
                chosenProductsObject[$productId] = $productShortcode;
            else
                delete chosenProductsObject[$productId];

            if ($elem.attr('name') !== $product.name) {
                $elem.attr('name', $product.name);
            }
        }

        // save our chosenProductsObject
        window['nexus' + this.nexusMultiple].chosenProductsObject = chosenProductsObject;

        var chosenProductsArray = [];
        for (var n in chosenProductsObject) {
            if (chosenProductsObject[n] !== '') {
                chosenProductsArray.push(chosenProductsObject[n]);
            }
        }

        var x_udf_form_product149_value = chosenProductsArray.join(',');
        nQuery('#x_udf_form_product149' + this.nexusMultiple).val(x_udf_form_product149_value);
    },

    loadPrivacyPolicy : function() {

        var myNexusVars = this.nexusVars;
        nQuery('#x_udf_privacy_policy_version143'+ this.nexusMultiple +'_label a').on('click', function(e) {

            var theNexusVars = myNexusVars;
            e.preventDefault();

            theNexusVars.modal().open({content: theNexusVars.privacy_policy, height: '90%', width: '90%'});

        });
    },

    inArray: function(needle, haystack) {
        var length = haystack.length;
        for(var i = 0; i < length; i++) {
            if(haystack[i] === needle){return i;}
        }
        return -1;
    },

    etrim : function(str){str=str.replace(/^\s+/, '');for(var i = str.length - 1; i >= 0; i--){if(/\S/.test(str.charAt(i))){str=str.substring(0, i + 1);break;}}return str;},
    isEmpty : function(val){if(!val){return true;}val=window['nexus' + this.nexusMultiple].etrim(val);return val.length===0;},
    isEmailValid : function(check_email){var email=check_email;var filter = /^([a-zA-Z0-9]+[a-zA-Z0-9_\-\.]*\@([a-zA-Z0-9]+[a-zA-Z0-9\_-]*\.)+[a-zA-Z0-9]+)$/;if(filter.test(email)){return true;}return false;},
    is_int : function(value){if((parseFloat(value) === parseInt(value)) && !isNaN(value)){return true;} else {return false;}},

    isIE : function() {
        var myNav = navigator.userAgent.toLowerCase();
        return (myNav.indexOf('msie') !== -1) ? parseInt(myNav.split('msie')[1]) : false;
    },

    loadCookies : function() {
        // let's load from the cookie if 'ct' is not in the querystring, otherwise we'll let the server
        // set the value of ct if it's in the querystring

        //If ct is present in the query string
        if (QueryString.ct) {

            //A method to clear the cookie for testing
            if(QueryString.ct == "clear"){
                nexusSetCookie("nexus_ct","");
                //Else just set the cookie and input value to the ct value in the query string
            }else{
                nexusSetCookie("nexus_ct",QueryString.ct,30);
            }
        }

        var channel_tactic_cookie = nexusGetCookie('nexus_ct'),
            channel_tactic_element = document.getElementById('x_udf_channel___tactic400__ss' + this.nexusMultiple);

        if(channel_tactic_cookie.length > 0 && channel_tactic_element !== null)
        {
            for(var ct in nexusVars.channelTactic)
            {
                if(channel_tactic_cookie === ct)
                {
                    nQuery('#x_udf_channel___tactic400__ss' + this.nexusMultiple).val(nexusVars.channelTactic[ct]);
                    return;
                }

                if(channel_tactic_cookie === nexusVars.channelTactic[ct])
                {
                    nQuery('#x_udf_channel___tactic400__ss' + this.nexusMultiple).val(nexusVars.channelTactic[ct]);
                    return;
                }

            }
        }
    }

}; // end nexus object

// load scripts
window['nexus' + this.nexusMultiple].etrLoadScript(window['protocol'] + window["nexusVars" + nexusMultiple].externalRoot + 'scripts/etrigueForm_v2.js'); // js file for eTrigue
if (window['nexus' + this.nexusMultiple].isIE()) {window['nexus' + this.nexusMultiple].etrLoadScript(window['protocol'] + window["nexusVars" + nexusMultiple].externalRoot + 'scripts/xdr.js'); }   // fix for form submit for IE
window['nexus' + this.nexusMultiple].init();  // load our select dropdowns
window['nexus' + this.nexusMultiple].loadCookies(); // load our cookies
