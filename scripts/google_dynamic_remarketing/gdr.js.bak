/**
 * Created with JetBrains PhpStorm.
 * Author: Joseph Santos
 * Date: 8/27/13
 * Time: 2:55 AM
 */
var gdr = {

    // These are the allowed sections
    allowedSections : ['home', 'searchresults', 'category', 'product', 'cart', 'purchase'],

    // the main json array to hold products
    products : [],

    // the google parameters for dynamic remarketing
    google_tag_params : {},

    /**
     * Main function GET
     *
     * @param section - type of page. Valid values are
     * 'home', 'searchresults', 'category', 'product', 'cart', 'purchase', 'other'
     */
    get : function()
    {
        var data = [];
        var pageType = document.getElementById("ecomm_pagetype");
        var ecomm_pagetype = pageType == undefined ? '' : this._trim(pageType.innerHTML);

        for(var section in this.allowedSections)
        {
            if(this.allowedSections[section] == ecomm_pagetype)
            {
                var dom = document.body;

                // return an empty object if the passed parameter is not a dom element
                if(! this._isElement(dom))
                    return data;

                switch(ecomm_pagetype) {

                    case 'home' :
                        data = this._getHomeProducts(dom, ecomm_pagetype);
                        break;

                    case 'searchresults' :
                        data = this._getSearchResultProducts(dom, ecomm_pagetype);
                        break;

                    case 'category' :
                        data = this._getCategoryProducts(dom, ecomm_pagetype);
                        break;

                    case 'product' :
                        data = this._getProduct(dom, ecomm_pagetype);
                        break;

                    case 'cart' :
                        data = this._getCartProducts(dom, ecomm_pagetype);
                        break;
                }
                break;
            }
        }

        // let's store the data into an object variable
        this.products = data;

        // return the data
        return this.products;
    },

    /**
     * Scrapes the 'home' url for the products on this page
     *
     * @param elem
     * @returns {Array}
     * @private
     */
    _getHomeProducts : function(elem, ecomm_pagetype)
    {
        var data = [];
        var itemContainers = elem.getElementsByClassName("itemslider");
        for(var n in itemContainers)
        {
            if(this._isElement(itemContainers[n]))
            {
                var items = itemContainers[n].getElementsByClassName("item");
                for(var i in items)
                {
                    if(this._isElement(items[i]))
                    {
                        // get product name
                        var product_name = this._trim(items[i].getElementsByClassName("product-name")[0].firstElementChild.innerHTML);

                        // get product sku
                        var product_sku = this._trim(items[i].getElementsByClassName("sku-box")[0].innerHTML.replace(/ID: /, ""));

                        // get price
                        var product_price = "";
                        var prices = items[i].getElementsByClassName("price");
                        for(var j in prices)
                        {
                            if(this._isElement(prices[j]))
                            {
                                product_price = this._trim(prices[j].innerHTML.replace(/\$/g, ''));
                            }
                        }

                        // get ecomm_prodid
                        var prodId = document.getElementsByClassName("ecomm_prodid");
                        var ecomm_prodid = prodId[0] == undefined ? '' : this._trim(prodId[0].innerHTML);

                        // construct our object
                        data.push(
                            {
                                name : product_name,
                                sku : product_sku,
                                price : product_price
                            }
                        );
                    }
                }
            }
        }

        // fill our google remarketing object
        this.google_tag_params.ecomm_pagetype = ecomm_pagetype;
        this.google_tag_params.ecomm_prodid = '';
        this.google_tag_params.ecomm_totalvalue = '';

        return data;
    },

    /**
     * Scrapes the 'category' url for the products on this page
     *
     * @param elem
     * @returns {Array}
     * @private
     */
    _getCategoryProducts : function(elem, ecomm_pagetype)
    {
        var data = [];

        // get items in this page
        var items = elem.getElementsByClassName("item");
        for(var i in items)
        {
            if(this._isElement(items[i]))
            {
                // get product name
                var product_name = this._trim(items[i].getElementsByClassName("product-name")[0].firstElementChild.innerHTML);

                // get product sku
                var product_sku = this._trim(items[i].getElementsByClassName("sku-box")[0].innerHTML.replace(/ID: /, ""));

                // get price
                var product_price = "";
                var prices = items[i].getElementsByClassName("price");
                for(var j in prices)
                {
                    if(this._isElement(prices[j]))
                    {
                        product_price = this._trim(prices[j].innerHTML.replace(/\$/g, ''));
                    }
                }

                // get ecomm_prodid
                var prodId = document.getElementsByClassName("ecomm_prodid");
                var ecomm_prodid = prodId[0] == undefined ? '' : this._trim(prodId[0].innerHTML);

                // construct our object
                data.push(
                    {
                        name : product_name,
                        sku : product_sku,
                        price : product_price
                    }
                );
            }
        }

        // fill our google remarketing object
        this.google_tag_params.ecomm_pagetype = ecomm_pagetype;
        this.google_tag_params.ecomm_prodid = '';
        this.google_tag_params.ecomm_totalvalue = '';

        return data;
    },

    /**
     * Scrapes the 'search result' url for the products on this page
     *
     * @param elem
     * @returns {Array}
     * @private
     */
    _getSearchResultProducts : function(elem, ecomm_pagetype)
    {
        var data = [];


        return data;
    },

    /**
     * Scrapes the 'product' url for the products on this page
     *
     * @param elem
     * @returns {Array}
     * @private
     */
    _getProduct : function(elem, ecomm_pagetype)
    {
        var data = [];

        // get product name
        var pn = elem.getElementsByClassName("product-name")[0];
        var product_name = this._trim(pn.textContent) || this._trim(pn.innerText);

        // get price
        var product_price = "";
        var prices = elem.getElementsByClassName("price");
        for(var j in prices)
        {
            if(this._isElement(prices[j]))
            {
                product_price = this._trim(prices[j].innerHTML.replace(/\$/g, ''));
            }
        }

        // get SKU and ecomm_prodid
        var ecomm_prodid = document.getElementById("product-attribute-specs-table").getElementsByTagName("tr")[0].getElementsByTagName("td")[0].innerHTML;
        var product_sku = ecomm_prodid;

        // construct our product object
        data.push(
            {
                name : product_name,
                sku : product_sku,
                price : product_price
            }
        );

        // fill our google remarketing object
        this.google_tag_params.ecomm_pagetype = ecomm_pagetype;
        this.google_tag_params.ecomm_prodid = ecomm_prodid;
        this.google_tag_params.ecomm_totalvalue = product_price;

        return data;
    },

    /**
     * Scrapes the 'cart' url for the products on this page
     *
     * @param elem
     * @returns {Array}
     * @private
     */
    _getCartProducts : function(elem, ecomm_pagetype)
    {
        var data = [];

        var shopping_cart = elem.getElementById("shopping-cart-table").getElementsByTagName("tbody")[0].getElementsByTagName("tr");
        for(var row in shopping_cart)
        {
            // get product name
            var pn = shopping_cart[row].getElementsByClassName("product-name")[0];
            var product_name = this._trim(pn.textContent) || this._trim(pn.innerText);

            // get price
            var product_price = "";
            var prices = shopping_cart[row].getElementsByClassName("price");
            for(var j in prices)
            {
                if(this._isElement(prices[j]))
                {
                    product_price = this._trim(prices[j].innerHTML.replace(/\$/g, ''));
                }
            }

            //ToDo: get SKU and ecomm_prodid

        }

        // get total value
        var totalValue = elem.getElementById("shopping-cart-totals-table")
                            .getElementsByTagName("tfoot")[0]
                            .getElementsByTagName("tr")[0]
                            .getElementsByClassName("price")[0];
        var ecomm_totalvalue = this._trim(totalValue.textContent) || this._trim(totalValue.innerText);

        // fill our google remarketing object
        this.google_tag_params.ecomm_pagetype = ecomm_pagetype;
        this.google_tag_params.ecomm_prodid = '';
        this.google_tag_params.ecomm_totalvalue = ecomm_totalvalue;

    },

    /**
     * removes remove querystring, hash and trailing slash from a url
     *
     * @param string
     *
     * @return string
     */
    _stripSlash : function(str) {
        if(str.substr(-1) == '/') {
            str = str.substr(0, str.length - 1);
        }

        return str;
    },

    /**
     * Checks if object is a dom element
     *
     * @param obj
     * @returns {boolean}
     */
    _isElement : function(obj) {
        try {
            //Using W3 DOM2 (works for FF, Opera and Chrom)
            return obj instanceof HTMLElement || Object.prototype.toString.call(obj) == "[object HTMLDocument]";
        }
        catch(e){
            //Browsers not supporting W3 DOM2 don't have HTMLElement and
            //an exception is thrown and we end up here. Testing some
            //properties that all elements have. (works on IE7)
            return (typeof obj==="object") &&
                (obj.nodeType===1) && (typeof obj.style === "object") &&
                (typeof obj.ownerDocument ==="object");
        }
    },

    _trim : function(str) {
        return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    }
}


function listen(evnt, elem, func) {
    if (elem.addEventListener)  // W3C DOM
        elem.addEventListener(evnt,func,false);
    else if (elem.attachEvent) { // IE DOM
        var r = elem.attachEvent("on"+evnt, func);
        return r;
    }
}
