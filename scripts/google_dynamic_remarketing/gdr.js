/**
 * Created with JetBrains PhpStorm.
 * Author: Joseph Santos
 * Date: 8/27/13
 * Time: 2:55 AM
 */
var gdr = {

    // These are the allowed sections
    allowedSections : ['home', 'searchresults', 'category', 'product', 'cart', 'purchase'],

    // the main json array to hold products
    products : [],

    // the google parameters for dynamic remarketing
    google_tag_params : {},

    /**
     * Main function GET
     *
     * @param section - type of page. Valid values are
     * "home", "searchresults", "category", "product", "cart", "purchase", "other"
     */
    get : function()
    {
        var data = [];
        var pageType = document.getElementById("ecomm_pagetype");
        var ecomm_pagetype = pageType == undefined ? "" : this._trim(pageType.innerHTML);

        for(var section in this.allowedSections)
        {
            if(this.allowedSections[section] == ecomm_pagetype)
            {
                var dom = document.body;

                // return an empty object if the passed parameter is not a dom element
                if(! this._isElement(dom))
                    return data;

                switch(ecomm_pagetype) {

                    case "home" :
                        data = this._getHomeProducts(dom, ecomm_pagetype);
                        break;

                    case "searchresults" :
                        data = this._getSearchResultProducts(dom, ecomm_pagetype);
                        break;

                    case "category" :
                        data = this._getCategoryProducts(dom, ecomm_pagetype);
                        break;

                    case "product" :
                        data = this._getProduct(dom, ecomm_pagetype);
                        break;

                    case "cart" :
                        data = this._getCartProducts(dom, ecomm_pagetype);
                        break;
                }
                break;
            }
        }

        // let's store the data into an object variable
        this.products = data;

        // let's parse the data into our google_tag_params code
        var google_params = this.parseData(data, ecomm_pagetype);

        // return the data
        return google_params;
    },

    /**
     * Scrapes the 'home' url for the products on this page
     *
     * @param elem
     * @returns {Array}
     * @private
     */
    _getHomeProducts : function(elem, ecomm_pagetype)
    {
        var data = [];
        var itemContainers = elem.getElementsByClassName( "itemslider" );

        for( var n = 0, itemContainersLen = itemContainers.length; n < itemContainersLen; n++ )
        {
            var itemContainer = itemContainers.item(n);

            if( this._isElement(itemContainer) )
            {
                var items = itemContainer.getElementsByClassName("item");

                for(var i = 0, iLen = items.length; i < iLen; i++)
                {
                    var item = items.item(i);

                    if(this._isElement(item))
                    {
                        // get product sku
                        var product_sku = this._trim(item.getElementsByClassName("sku-box")[0].innerHTML.replace(/ID: /, ""));

                        // get price
                        var product_price = "";
                        var prices = item.getElementsByClassName("price");
                        for(var j = 0, pricesLen = prices.length; j < pricesLen; j++ )
                        {
                            var price = prices.item(j);
                            if(this._isElement(price))
                            {
                                product_price = this._trim(price.innerHTML.replace(/\$/g, "").replace(/,/g, ""));
                            }
                        }

                        // get ecomm_prodid
                        var prodId = document.getElementsByClassName("ecomm_prodid");
                        var ecomm_prodid = prodId[0] == undefined ? "" : this._trim(prodId[0].innerHTML);

                        // construct our object
                        var obj = {
                            id : product_sku,
                            price : product_price
                        };

                        var hasMatch = false;
                        for(var k in data)
                        {
                            if(JSON.stringify(data[k]) === JSON.stringify(obj))
                                hasMatch = true;
                        }

                        if( ! hasMatch)
                            data.push(obj);
                    }
                }
            }
        }

        return data;
    },

    /**
     * Scrapes the 'category' url for the products on this page.
     * The category page doesn't have products and price. Only has
     * pagetype and additional parameter 'ecomm_category'
     *
     * @param elem
     * @returns {Array}
     * @private
     */
    _getCategoryProducts : function(elem, ecomm_pagetype)
    {
        var data = [];

        // get the category title
        var titles = elem.getElementsByClassName("page-title");
        var category = "";

        if(titles.length > 0)
        {
            category = this._trim( titles[0].firstElementChild.innerHTML );
        }

        // get items in this page
        var items = elem.getElementsByClassName("item");
        for(var i = 0, itemsLen = items.length; i < itemsLen; i++)
        {
            if(this._isElement(items[i]))
            {
                var product_sku = "",
                    product_price = "";

                // get product id
                var productSku = items[i].getElementsByClassName("sku-box");
                if(productSku.length > 0)
                {
                    product_sku = this._trim(productSku[0].innerHTML.replace(/ID: /, ""));
                }


                // get price
                var prices = items[i].getElementsByClassName("price");
                for(var j = 0, pricesLen = prices.length; j < pricesLen; j++)
                {
                    if(this._isElement(prices[j]))
                    {
                        product_price = this._trim(prices[j].innerHTML.replace(/\$/g, "").replace(/,/g, ""));
                    }
                }

                // get ecomm_prodid
                var prodId = elem.getElementsByClassName("ecomm_prodid");
                var ecomm_prodid = prodId[0] == undefined ? '' : this._trim(prodId[0].innerHTML);


                // construct our object
                var obj = {
                    id : product_sku,
                    price : product_price,
                    category: category
                };

                var hasMatch = false;
                for(var n in data)
                {
                    if(JSON.stringify(data[n]) === JSON.stringify(obj))
                        hasMatch = true;
                }

                if( ! hasMatch)
                    data.push(obj);
            }
        }

        return data;
    },

    /**
     * Scrapes the 'search result' url for the products on this page
     *
     * @param elem
     * @returns {Array}
     * @private
     */
    _getSearchResultProducts : function(elem, ecomm_pagetype)
    {
        var data = [];

        // get items in this page
        var items = elem.getElementsByClassName("item");
        for(var i = 0, itemsLen = items.length; i < itemsLen; i++)
        {
            if(this._isElement(items[i]))
            {
                // get product name
                var product_name = this._trim(items[i].getElementsByClassName("product-name")[0].firstElementChild.innerHTML);

                // get product sku
                var product_sku = this._trim(items[i].getElementsByClassName("sku-box")[0].innerHTML.replace(/ID: /, ""));

                // get price
                var product_price = "";
                var prices = items[i].getElementsByClassName("price");
                for(var j = 0, pricesLen = prices.length; j < pricesLen; j++)
                {
                    if(this._isElement(prices[j]))
                    {
                        product_price = this._trim(prices[j].innerHTML.replace(/\$/g, "").replace(/,/g, ""));
                    }
                }

                // get ecomm_prodid
                var prodId = elem.getElementsByClassName("ecomm_prodid");
                var ecomm_prodid = prodId[0] == undefined ? "" : this._trim(prodId[0].innerHTML);

                // construct our object
                var obj = {
                    name : product_name,
                    id : product_sku,
                    price : product_price
                };

                var hasMatch = false;
                for(var n in data)
                {
                    if(JSON.stringify(data[n]) === JSON.stringify(obj))
                        hasMatch = true;
                }

                if( ! hasMatch)
                    data.push(obj);
            }
        }

        return data;
    },

    /**
     * Scrapes the 'product' url for the products on this page
     *
     * @param elem
     * @returns {Array}
     * @private
     */
    _getProduct : function(elem, ecomm_pagetype)
    {
        var data = [];

        // get product name
        var pn = elem.getElementsByClassName("product-name")[0];
        var product_name = this._trim(pn.textContent) || this._trim(pn.innerText);

        // get price
        var product_price = "";
        var prices = elem.getElementsByClassName("price");
        for(var j = 0, pricesLen = prices.length; j < pricesLen; j++)
        {
            if(this._isElement(prices[j]))
            {
                product_price = this._trim(prices[j].innerHTML.replace(/\$/g, "").replace(/,/g, ""));
            }
        }

        // get SKU and ecomm_prodid
        var ecomm_prodid = document.getElementById("product-attribute-specs-table").getElementsByTagName("tr")[0].getElementsByTagName("td")[0].innerHTML;

        // construct our product object
        // construct our object
        var obj = {
            name : product_name,
            id : ecomm_prodid,
            price : product_price
        };

        var hasMatch = false;
        for(var n in data)
        {
            if(JSON.stringify(data[n]) === JSON.stringify(obj))
                hasMatch = true;
        }

        if( ! hasMatch)
            data.push(obj);

        return data;
    },

    /**
     * Scrapes the 'cart' url for the products on this page
     *
     * @param elem
     * @returns {Array}
     * @private
     */
    _getCartProducts : function(elem, ecomm_pagetype)
    {
        var data = [];

        var shopping_cart = document.getElementById("shopping-cart-table").getElementsByTagName("tbody")[0].getElementsByTagName("tr");

        for(var row = 0, shoppingCartLen = shopping_cart.length; row < shoppingCartLen; row++)
        {
            // get product name
            var pn = shopping_cart[row].getElementsByClassName("product-name")[0];
            var product_name = this._trim(pn.textContent) || this._trim(pn.innerText);

            // get price
            var product_price = "";
            var prices = shopping_cart[row].getElementsByClassName("price");
            for(var j = 0, pricesLen = prices.length; j < pricesLen; j++)
            {
                if(this._isElement(prices[j]))
                {
                    product_price = this._trim(prices[j].innerHTML.replace(/\$/g, "").replace(/,/g, ""));
                }
            }

            // ToDo: get ecomm_prodid


        }

        // get total value
        var totalValue = document.getElementById("shopping-cart-totals-table")
                            .getElementsByTagName("tfoot")[0]
                            .getElementsByTagName("tr")[0]
                            .getElementsByClassName("price")[0];
        var ecomm_totalvalue = this._trim(totalValue.textContent.replace(/\$/g, "").replace(/,/g, "")) || this._trim(totalValue.innerText.replace(/\$/g, "").replace(/,/g, ""));

        // construct our object
        var obj = {
            name : product_name,
            id : "",
            price : product_price,
            totalValue : ecomm_totalvalue
        };

        var hasMatch = false;
        for(var n in data)
        {
            if(JSON.stringify(data[n]) === JSON.stringify(obj))
                hasMatch = true;
        }

        if( ! hasMatch)
            data.push(obj);

        return data;

    },

    parseData : function(data, pageType)
    {
        var myObj = {};
        myObj.ecomm_pagetype = pageType;

        // case if we only have 1 product
        if(data.length == 1)
        {
            myObj.ecomm_prodid = data[0].id;
            myObj.ecomm_totalvalue = data[0].price != "" ? parseFloat(data[0].price) : "";
        }

        // case if we have multiple products
        if(data.length > 1)
        {
            var ecomm_prodid = [];
            var ecomm_totalvalue = [];

            for(var n = 0, dataLen = data.length; n < dataLen; n++)
            {
                ecomm_prodid.push(data[n].id);
                ecomm_totalvalue.push( data[n].price != "" ? parseFloat(data[n].price) : "" );
            }

            myObj.ecomm_prodid = ecomm_prodid;
            myObj.ecomm_totalvalue = ecomm_totalvalue;
        }

        // special case for category page
        if(data[0].hasOwnProperty("category"))
            myObj.ecomm_category = data[0].category;

        // special case for cart
        if(data[0].hasOwnProperty("totalValue"))
            myObj.ecomm_totalvalue = data[0].totalValue;

        this.google_tag_params = myObj;

        return this.google_tag_params;
    },

    /**
     * removes remove querystring, hash and trailing slash from a url
     *
     * @param string
     *
     * @return string
     */
    _stripSlash : function(str) {
        if(str.substr(-1) == "/") {
            str = str.substr(0, str.length - 1);
        }

        return str;
    },

    /**
     * Checks if object is a dom element
     *
     * @param obj
     * @returns {boolean}
     */
    _isElement : function(obj) {
        try {
            var isElement = obj instanceof HTMLElement || Object.prototype.toString.call(obj) == "[object HTMLDocument]";
            //Using W3 DOM2 (works for FF, Opera and Chrom)
            return obj instanceof HTMLElement || Object.prototype.toString.call(obj) == "[object HTMLDocument]";
        }
        catch(e){
            //Browsers not supporting W3 DOM2 don't have HTMLElement and
            //an exception is thrown and we end up here. Testing some
            //properties that all elements have. (works on IE7)

            var isElement = (typeof obj==="object") &&
                (obj.nodeType===1) && (typeof obj.style === "object") &&
                (typeof obj.ownerDocument ==="object");

            return (typeof obj==="object") &&
                (obj.nodeType===1) && (typeof obj.style === "object") &&
                (typeof obj.ownerDocument ==="object");
        }
    },

    _trim : function(str) {
        return str.replace(/^\s\s*/, "").replace(/\s\s*$/, "");
    }
}
