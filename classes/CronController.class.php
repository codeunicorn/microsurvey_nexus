<?php
/**
 * Created by PhpStorm.
 * Author: Joseph Santos
 * Date: 12/4/14
 * Time: 4:55 AM
 * Description: 
 */

class CronController extends NexusBase {

    private $access_key = 'XksVFd1uQE2Co5rNbGuOmx0nbRx99YVa';

    private $ownersCsv = null;

    public function __construct($params = array())
    {
        parent::__construct();

        // our cron controller requires an access token to be passed in
        if(!isset($params['key']) || (isset($params['key']) && $params['key'] !== $this->access_key))
            die();

        $this->ownersCsv = $this->loadCSV('Owners');
    }

    public function emailManagerSnapshots($params = array('output' => 'html'))
    {
        $owners = ! is_null($this->ownersCsv) ? $this->ownersCsv : $this->loadCSV('Owners');

        foreach($owners as $owner)
        {
            if($this->_evaluateLeadSummary($owner, $params))
            {
                $html = $this->_getEmailHTML($owner);

                // let's send mail only if there's actually something to send out
                if($html !== '' && $params['output'] == 'email')
                {
                    $msgTemplate = $this->load_template(NEXUS_INTERNAL_ROOT . 'templates/cron_Manager_Snapshots.php', array());
                    $sender = $msgTemplate['sender'];
                    $subject = $msgTemplate['subject'];

                    //Create a new PHPMailer instance
                    $mail = new PHPMailer();

                    // Set PHPMailer to use UTF-8 charset
                    $mail->CharSet = 'UTF-8';

                    // Set PHPMailer to use base64 encoding
                    $mail->Encoding = 'base64';

                    // Set PHPMailer to use the sendmail transport
                    $mail->IsSendmail();

                    //Set who the message is to be sent from
                    $mail->SetFrom($sender, $sender);

                    //Set who the message is to be sent to
                    $mail->AddAddress($owner['Element'], $owner['Short-Code']);

                    // hack for Jorge Paramo report to CC Monica Placecia
                    if($owner['Element'] == 'jorge.paramo@leicaus.com') {
                        $mail->AddCC('Monica.Plasencia@leica-geosystems.com', 'Monica Plasencia');
                        $mail->AddCC('laura.parriRoyo@leica-geosystems.com', 'Laura Parri Royo');
                    }

                    // add Admins as BCC
                    $u = new User();
                    $users = $u->getAllUsers('admin');
                    foreach($users as $user)
                    {
                        $o = new Owner();
                        $own = $o->getOwnerByShortcode($user['username']);

                        if(!empty($own) && $own['owner_email'] !== $owner['Element'])
                        {
                            $mail->AddBCC($own['owner_email'], $own['owner_label']);
                        }
                    }

                    //Set the subject line
                    $mail->Subject = $subject;

                    //Read an HTML message body from an external file, convert referenced images to embedded, convert HTML into a basic plain-text alternative body
                    $mail->MsgHTML($html);

                    //Replace the plain text body with one created manually
                    $mail->AltBody = $mail->html2text($html, true);

                    //Send the message, check for errors. Log if there's error
                    if(!$mail->Send())
                    {
                        Utility::logMessage("Mailer Error: " . $mail->ErrorInfo, 'cron_error');
                    }
                }
                else if($params['output'] == 'html')
                    echo $html;
            }
        }
    }

    /*
     * This function gets run everyday by a cronjob. Calculates the highcharts data and saves it
     * to the database for easy retrieval from the reports page.
     */
    public function calculateHighchartsData()
    {
        $highchartsArray = array(
            'PieChartByEffectiveValueDashboard' => '',
            'BarChartByCategoryDashboard' => '',
            'HighMapByRegionDashboard' => '',
            'HighMapByStateDashboard' => ''
        );

        $reports = new Reports();

        foreach($highchartsArray as $key => $data)
        {
            $method = 'get'. $key;
            if(method_exists('Reports', $method))
            {
                $data = json_encode($reports->$method());

                $sql = "INSERT INTO highcharts (name, date, data)
                VALUES(:name, :date, :data)
                ON DUPLICATE KEY UPDATE date = :date, data = :data";
                $params = array(
                    ':name' => $key,
                    ':date' => date('Y-m-d'),
                    ':data' => $data
                );
                $this->Execute($sql, $params);
            }
        }
    }

    private function _getEmailHTML($owner = array()) {

        $html = '';

        // Section 1: The current owner's leads
        $data = $this->_getTableHTML(array('owner' => $owner['Short-Code'], 'reps' => array($owner['Short-Code']), 'showOwner' => false, 'showAcceptReject' => true));

        if($data['total'] > 0)
        {
            $header = "<h4 style='font-family:arial'>". "Your Sales-Ready Nexus Leads (". $data['firstDate'] ." to ". $data['lastDate'] .") - Pending: ". $data['totalPending'] ." (". round($data['pending']/$data['total'] * 100) ."%), Accepted: ". $data['accepted'] ." (". round($data['accepted']/$data['total'] * 100) ."%), Rejected: ". $data['rejected'] ." (". round($data['rejected']/$data['total'] * 100) ."%)</h4>";
            $table = $data['table'] . '</table>';
            $html .= $header . $table;
        }

        // Section 2: The leads for owners UNDER this manager (2nd level)
        $table = '';
        $firstDate = '';
        $lastDate = '';
        $total = 0;
        $pending = 0;
        $accepted = 0;
        $rejected = 0;

        $repsSecond = $this->_getOwnerReps($owner);
        $data = $this->_getTableHTML(array('owner' => $owner['Short-Code'], 'reps' => array(), 'showOwner' => true, 'showAcceptReject' => false, 'header' => 'Your Reps\' Sales-Ready Nexus Leads'));

        if( ! is_null( $data ) ) {
            $firstDate = empty( $firstDate ) || ( $firstDate !== '' && !empty( $data[ 'firstDate' ] ) && $firstDate > $data['firstDate'] ) ? $data['firstDate'] : $firstDate;
            $lastDate = $data['lastDate'];
            $total += $data['total'];
            $pending += $data['pending'];
            $accepted += $data['accepted'];
            $rejected += $data['rejected'];
            $table .= $data['table'];
        }

        // get leads for owners under the reps (3rd level)
        foreach($repsSecond as $rep) {
            $data = $this->_getTableHTML(array('owner' => $owner['Short-Code'], 'reps' => array($rep), 'showOwner' => true, 'showAcceptReject' => false, 'append' => true, 'rowcolor' => '#9cceeb'));

            if( ! is_null( $data ) ) {
                $firstDate = empty( $firstDate ) || ( $firstDate !== '' && !empty( $data[ 'firstDate' ] ) && $firstDate > $data['firstDate'] ) ? $data['firstDate'] : $firstDate;
                $lastDate = $data['lastDate'];
                $total += $data['total'];
                $pending += $data['pending'];
                $accepted += $data['accepted'];
                $rejected += $data['rejected'];
                $table .= $data['table'];
            }
            // get leads for owners under this rep
            $owner2 = $rep;
            $repsThird = $this->_getownerReps(array('Short-Code' => $owner2));


            // get leads for owners under these reps (4th level)
            foreach($repsThird as $rep) {
                $data = $this->_getTableHTML(array('owner' => $owner2, 'reps' => array($rep), 'showOwner' => true, 'showAcceptReject' => false, 'append' => true, 'rowcolor' => '#ebb5f4'));

                if( ! is_null( $data ) ) {
                    $firstDate = empty( $firstDate ) || ( $firstDate !== '' && !empty( $data[ 'firstDate' ] ) && $firstDate > $data['firstDate'] ) ? $data['firstDate'] : $firstDate;
                    $lastDate = $data['lastDate'];
                    $total += $data['total'];
                    $pending += $data['pending'];
                    $accepted += $data['accepted'];
                    $rejected += $data['rejected'];
                    $table .= $data['table'];
                }
                // get leads for owners under this rep
                $owner3 = $rep;
                $repsFourth = $this->_getownerReps(array('Short-Code' => $owner3));

                // get leads for owners under these reps (5th level)
                foreach($repsFourth as $rep) {
                    $data = $this->_getTableHTML(array('owner' => $owner3, 'reps' => array($rep), 'showOwner' => true, 'showAcceptReject' => false, 'append' => true, 'rowcolor' => '#94dda2'));

                    if( ! is_null( $data ) ) {
                        $firstDate = empty( $firstDate ) || ( $firstDate !== '' && !empty( $data[ 'firstDate' ] ) && $firstDate > $data['firstDate'] ) ? $data['firstDate'] : $firstDate;
                        $lastDate = $data['lastDate'];
                        $total += $data['total'];
                        $pending += $data['pending'];
                        $accepted += $data['accepted'];
                        $rejected += $data['rejected'];
                        $table .= $data['table'];
                    }
                    // get leads for owners under this rep
                    $owner4 = $rep;
                    $repsFifth = $this->_getownerReps(array('Short-Code' => $owner4));

                    if(count($repsFifth) > 0) {
                        $data = $this->_getTableHTML(array('owner' => $owner4, 'reps' => $repsFifth, 'showOwner' => true, 'showAcceptReject' => false, 'append' => true, 'rowcolor' => '#d4d2c8'));

                        if( ! is_null( $data ) ) {
                            $firstDate = empty( $firstDate ) || ( $firstDate !== '' && !empty( $data[ 'firstDate' ] ) && $firstDate > $data['firstDate'] ) ? $data['firstDate'] : $firstDate;
                            $lastDate = $data['lastDate'];
                            $total += $data['total'];
                            $pending += $data['pending'];
                            $accepted += $data['accepted'];
                            $rejected += $data['rejected'];
                            $table .= $data['table'];
                        }
                    }
                }
            }
        }

        if($total > 0)
        {
            $header = "<h4 style='font-family:arial'>Your Reps' Sales-Ready Nexus Leads (". $firstDate ." to ". $lastDate .") - Pending: ". $pending ."</h4>";
            $table .= '</table>';
            $html .= $header . $table;
        }

        return $html;
    }

    private function _getTableHTML($param = array())
    {
        $results = $this->queryPendingStatus(array('owner' => $param['owner'], 'reps' => $param['reps'], 'showOwner' => $param['showOwner']));
        // If there are ZERO records, then do not show this section or header

        if (empty($results) && isset($param['append'])) {
            return null;
        }

        $placeholders = array();
        $firstDate = '';
        $total = count($results);
        $totalPending = 0;
        $totalAccepted = 0;
        $totalRejected = 0;
        $bCounter = 0;
        $tableRow = "";

        $date = new DateTime();
        $date->add(DateInterval::createFromDateString('yesterday'));
        $lastDate = $date->format('Y-m-d');

        foreach($results as $key => $result)
        {
            if(($param['showOwner'] && $result['status'] == 0) || !$param['showOwner'])
            {
                $result['date_submitted'] = substr($result['date_submitted'], 0, strpos($result['date_submitted'], ' '));
                $firstDate = $result['date_submitted'];

                if($result['status'] == 0)
                {
                    $totalPending++;
                }
                else if($result['status'] == 1)
                {
                    $totalAccepted++;
                }
                else if($result['status'] >= 2 && $result['status'] <= 6)
                {
                    $totalRejected++;
                }

                $origin = $result['x_udf_origin_code114__ss_label'];
                $segment = $result['x_udf_primary_market_segment122__ss_label'];
                $division = $result['x_udf_division146__ss_label'];
                $acceptReject = $result['status'] == 0 ? '<a href="http://nexus.microsurvey.com/status.php?form_key='. $result["auth_key"] .'&status=1" style="padding:5px;background-color:#E6162C;text-decoration:none;color:white;border-radius: 5px;"><span style="font-size:13px;">Accept</span></a>
                                &nbsp;&nbsp;
                                <a href="http://nexus.microsurvey.com/status.php?form_key='. $result["auth_key"] .'&status=2" style="padding:5px;background-color:#929292;text-decoration:none;color:white;border-radius: 5px;"><span style="font-size:13px;">Reject</span></a>'
                    : '';
                $placeholders['status'] = strpos($result['status_label'], ' ') !== FALSE ? substr($result['status_label'], 0, strpos($result['status_label'], ' ')) : $result['status_label'];
                $placeholders['x_firstname'] = $result['x_firstname'];
                $placeholders['x_lastname'] = $result['x_lastname'];
                $placeholders['x_companyname'] = strlen($result['x_companyname']) > 30 ? substr($result['x_companyname'], 0, 27) . '...' : $result['x_companyname'];
                $placeholders['x_emailaddress'] = strlen($result['x_emailaddress']) > 30 ? substr($result['x_emailaddress'], 0, 27) . '...' : $result['x_emailaddress'];
                $placeholders['owner_label'] = isset($param['showOwner']) && $param['showOwner'] ? "<td style='border:1px solid #ddd;padding:5px;'>{$result['owner_label']}</td>" : "";
                $placeholders['manager'] = isset($param['showOwner']) && $param['showOwner'] ? "<td style='border:1px solid #ddd;padding:5px;'>{$param['owner']}</td>" : "";
                $placeholders['preset'] = $this->getPresetFromSegmentAndDivision($segment, $division, $origin);
                $placeholders['channel_tactic'] = $result['x_udf_channel___tactic400__ss_label'];
                $placeholders['acceptReject'] = isset($param['showAcceptReject']) && $param['showAcceptReject'] ? "<td style='border:1px solid #ddd;padding:5px;'>$acceptReject</td>" : "";
                $placeholders['backgroundColor'] = isset($param['rowcolor']) ? $param['rowcolor'] : ( $bCounter % 2 == 0 ? '#EEEEEE' : '#FFFFFF' );
                $bCounter++;

                $tableRow .= "<tr style='border-spacing:0px;background-color:". $placeholders['backgroundColor'].";'>
                                <td style='border:1px solid #ddd;padding:5px;'>{$result['date_submitted']}</td>
                                {$placeholders['owner_label']}
                                {$placeholders['manager']}
                                <td style='border:1px solid #ddd;padding:5px;'>{$placeholders['x_companyname']}</td>
                                <td style='border:1px solid #ddd;padding:5px;'><a href='http://". NEXUS_EXTERNAL_ROOT ."?c=Forms&a=detailedView&ap[form_key]={$result["auth_key"]}' target='_blank' style='text-decoration:none;'>{$placeholders['x_firstname']} {$placeholders['x_lastname']}</a></td>
                                <td style='border:1px solid #ddd;padding:5px;'>{$placeholders['x_emailaddress']}</td>
                                <td style='border:1px solid #ddd;padding:5px;'>{$placeholders['preset']}</td>
                                <td style='border:1px solid #ddd;padding:5px;'>{$placeholders['channel_tactic']}</td>
                                <td style='border:1px solid #ddd;padding:5px;'>{$placeholders['status']}</td>
                                {$placeholders['acceptReject']}
                            </tr>
                ";
            }
        }

        $placeholders['owner_header'] = isset($param['showOwner']) && $param['showOwner'] ? '<th style="border:1px solid #ddd;padding:7px;">Owner</th>' : '';
        $placeholders['manager_header'] = isset($param['showOwner']) && $param['showOwner'] ? '<th style="border:1px solid #ddd;padding:7px;">Manager</th>' : '';
        $placeholders['acceptReject_header'] = isset($param['showAcceptReject']) && $param['showAcceptReject'] ? '<th style="border:1px solid #ddd;padding:7px;">Owner</th>' : '';

        $return = array();

        if(isset($param['append']) && $param['append']) {
            $html = $tableRow;
        }
        else {
            $placeholders['rows'] = $tableRow;
            $output = $this->load_template(NEXUS_INTERNAL_ROOT . "templates/cron_Manager_Snapshots.php", $placeholders);
            $html = $output['body'];
        }

        $return = array(
            'table' => $html,
            'total' => $total,
            'pending' => $totalPending,
            'accepted' => $totalAccepted,
            'rejected' => $totalRejected,
            'firstDate' => $firstDate,
            'lastDate' => $lastDate
        );

        return $return;
    }

    private function _getownerReps($owner = array())
    {
        $reps = array();

        // get this owner's reps
        $owners = ! is_null($this->ownersCsv) ? $this->ownersCsv : $this->loadCSV('Owners');

        foreach($owners as $o)
        {
            $managers = explode("||", $o['Manager']);
            if(in_array($owner['Short-Code'], $managers) && !in_array($o['Short-Code'], $reps))
            {
                $reps[] = $o['Short-Code'];
            }
        }
        return $reps;
    }

    private function queryPendingStatus($param)
    {
        $results = array();

        if(! empty($param["reps"])) {

            $sql = 'SELECT * FROM form_submissions
                WHERE owner_label IN("' . implode('","', $param["reps"]) . '")
                   AND auth_key != :auth_key
                   AND x_emailaddress NOT LIKE "JarebTest%"
                   AND x_emailaddress NOT LIKE "JosephTest%"
                   AND x_emailaddress NOT LIKE "test%"
                ';

            // start date= 1 month OLDER than the last record with status=pending
            // So, if, for this owner, the absolute oldest record that has status=pending is June 20,
            // then pull ALL records for this owner starting at May 20.
            // If the table is for 'Your Rep's Leads', only show the pending.  But show ALL pending - no time limit.
            if (!$param['showOwner']) {
                $sql .= ' AND date_submitted BETWEEN (
                          DATE_ADD(
                          (
                            SELECT date_submitted FROM form_submissions
                            WHERE owner_label = :owner
                               AND status = :status
                               AND auth_key != :auth_key
                            ORDER BY date_submitted ASC
                            LIMIT 1
                         ), INTERVAL -31 DAY)
                       ) AND (
                          DATE_ADD(NOW(), INTERVAL -0 DAY)
                       ) ';
            }

            $sql .= 'ORDER BY date_submitted DESC';
            $params = !$param['showOwner'] ? array(':owner' => $param['owner'], ':status' => 0, ':auth_key' => '') : array(':auth_key' => '');
            $results = $this->ExecAndFetchAll($sql, $params);
        }

        return $results;
    }

    /**
     * @param $owner
     * @param array $params
     * @return bool
     *
     * Determines whether the owner (from Owners.csv) should receive a summary report
     */
    private function _evaluateLeadSummary($owner, $params = array())
    {
        $shouldSendReport = false;

        if(isset($params['owner']) && $params['owner'] == $owner['Short-Code'])
            $shouldSendReport = true;

        if(!isset($params['owner']))
        {
            $day = strtolower(date('D'));
            $date = date('j');
            $leadSummaryArray = explode('||', strtolower($owner['Lead Summary']));

            // “daily” – Every day, 8am PST
            if(in_array( 'daily', $leadSummaryArray))
            {
                $shouldSendReport = true;
            }

            // “semi-monthly” – The 1st and 15th of every month, 8am PST
            else if( in_array('semi-monthly', $leadSummaryArray)  && ($date == 1 || $date == 15) )
            {
                $shouldSendReport = true;
            }

            // “monthly” – The 1st of every month, 8am PST
            else if( in_array('monthly', $leadSummaryArray) && $date == 1 )
            {
                $shouldSendReport = true;
            }

            // “weekly” – Every Mon||Tue||Wed||Thu||Fri||Sat||Sun, 8am PST
            else
            {
                if(in_array($day, $leadSummaryArray))
                {
                    $shouldSendReport = true;
                }
            }
        }

        return $shouldSendReport;
    }
} 