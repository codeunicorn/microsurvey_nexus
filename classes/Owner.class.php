<?php
/**
 * Created by JetBrains PhpStorm.
 * Author: Joseph Santos
 * Date: 4/2/14
 * Time: 1:27 AM
 * Description: 
 */

class Owner extends NexusBase{

    // $data will hold the owner data
    private $data;


    // constructor
    public function __construct()
    {
        parent::__construct();

        $this->data = array();
    }

    public function getOwnerByElement($element)
    {
        $this->resetOwner();
        $ownersArray = $this->loadCSV('Owners');

        foreach ($ownersArray as $row) {
            if ($element == $row['Element']) {

                foreach($row as $key => $val)
                {
                    // special case for Element -> add value as 'owner_email'
                    if($key == 'Element')
                    {
                        $this->data['owner_email'] = $val;
                    }

                    $newKey = 'owner_' . str_replace(array(" ", "-"), array("_", ""), strtolower($key));
                    $this->data[$newKey] = $val;
                }

                break;
            }
        }

        $this->_loadOwnerId();
        return $this->get();
    }

    public function getOwnerByEmail($email)
    {
        return $this->getOwnerByElement($email);
    }

    public function getOwnerByShortcode($shortcode)
    {
        $this->resetOwner();
        $ownersArray = $this->loadCSV('Owners');

        foreach ($ownersArray as $row) {
            if ($shortcode == $row['Short-Code']) {

                foreach($row as $key => $val)
                {
                    // special case for Element -> add value as 'owner_email'
                    if($key == 'Element')
                    {
                        $this->data['owner_email'] = $val;
                    }

                    $newKey = 'owner_' . str_replace(array(" ", "-"), array("_", ""), strtolower($key));
                    $this->data[$newKey] = $val;
                }

                break;
            }
        }

        $this->_loadOwnerId();
        return $this->get();
    }

    public function getOwnerByLabel($label)
    {
        $this->resetOwner();
        $ownersArray = $this->loadCSV('Owners');

        foreach ($ownersArray as $row) {
            if ($label == $row['Label']) {

                foreach($row as $key => $val)
                {
                    // special case for Element -> add value as 'owner_email'
                    if($key == 'Element')
                    {
                        $this->data['owner_email'] = $val;
                    }

                    $newKey = 'owner_' . str_replace(array(" ", "-"), array("_", ""), strtolower($key));
                    $this->data[$newKey] = $val;
                }

                break;
            }
        }

        $this->_loadOwnerId();
        return $this->get();
    }

    /**
     * Gets the first owner for the given origin
     *
     * @param $origin
     * @return array
     */
    public function getOwnerByOrigin($origin)
    {
        $this->resetOwner();
        $ownersArray = $this->loadCSV('Owners');

        foreach ($ownersArray as $row) {
            if ($origin == $row['Origin']) {

                foreach($row as $key => $val)
                {
                    // special case for Element -> add value as 'owner_email'
                    if($key == 'Element')
                    {
                        $this->data['owner_email'] = $val;
                    }

                    $newKey = 'owner_' . str_replace(array(" ", "-"), array("_", ""), strtolower($key));
                    $this->data[$newKey] = $val;
                }

                break;
            }
        }

        $this->_loadOwnerId();
        return $this->get();
    }


    /**
     * Returns the owner data array
     *
     * @return array
     */
    public function get()
    {
        return $this->data;
    }

    public function resetOwner()
    {
        $this->data = array();
    }

    private function _loadOwnerId()
    {
        if(file_exists(NEXUS_DATA_DIR . 'Elements.csv'))
        {
            $this->csv->load(NEXUS_DATA_DIR . 'Elements.csv');
            $elementsArray = $this->csv->connect();

            $this->data['owner_id'] = -1;

            foreach($elementsArray as $element)
            {
                if(isset($this->data['owner_shortcode']) && $element['Short-Code'] == $this->data['owner_shortcode'])
                {
                    $this->data['owner_id'] = $element['eTrigue Value'];
                    break;
                }
            }
        }
    }

}