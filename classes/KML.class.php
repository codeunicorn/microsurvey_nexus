<?php
/**
 * Created by JetBrains PhpStorm.
 * Author: Joseph Santos
 * Date: 4/24/14
 * Time: 11:56 PM
 * Description: 
 */

class KML {

    private $kmlData;
    private $kmlInternalFile;
    private $kmlExternalFile;

    private $jsonMap;

    // this will hold the map instance we're gonna use
    private $map;

    // our seg input
    private $seg;

    // our div input
    private $div;

    private $skipHeaders;

    // instance of File_CSV_DataSource class
    private $csv;

    private $error;

    // constructor
    public function __construct($map = '', $seg = '', $div = '')
    {
        // we'll define our internal root and external root if it hasn't already been defined
        defined('NEXUS_INTERNAL_ROOT') or define('NEXUS_INTERNAL_ROOT', realpath(dirname(dirname(__FILE__))) .'/');
        defined('NEXUS_EXTERNAL_ROOT') or define("NEXUS_EXTERNAL_ROOT", "http://".$_SERVER['SERVER_NAME']."/");
        defined('NEXUS_DATA_DIR') or define('NEXUS_DATA_DIR', realpath(dirname(dirname(__FILE__))) .'/data/');

        $this->kmlData = '';
        $time = time();
        $this->kmlInternalFile = NEXUS_INTERNAL_ROOT . 'data/kml/kmlData_'. $time .'.kml';
        $this->kmlExternalFile = NEXUS_EXTERNAL_ROOT . 'data/kml/kmlData_'. $time .'.kml';

        $this->filterPreset = NEXUS_DATA_DIR . 'Filter-Presets.csv';
        $this->map = NEXUS_DATA_DIR . 'map-'. strtoupper($map) .'.csv';
        $this->seg = $seg;
        $this->div = $div;

        $this->error = '';

        $this->skipHeaders = array(
            'Country/Region/Sub-Region',
            'Code',
            'Level',
            'Label',
            'Lat',
            'Long'
        );

        // we'll store our instance of File_CSV_DataSource which is used a lot to load csv files
        $this->csv = new File_CSV_DataSource();
    }

    public function mapToKML()
    {
        $isMapped = false;

        if($this->validate())
        {
            // Creates the Document.
            $dom = new DOMDocument('1.0', 'UTF-8');

            // Creates the root KML element and appends it to the root document.
            $node = $dom->createElementNS('http://earth.google.com/kml/2.1', 'kml');
            $parNode = $dom->appendChild($node);

            // Creates a KML Document element and append it to the KML element.
            $dnode = $dom->createElement('Document');
            $docNode = $parNode->appendChild($dnode);

            // specify our marker styles
            $markerUrl = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|";
            $markerColors = array("ff8b7c", "7aa9fd", "a1e98b", "fdeb6b", "d1aefe", "c5e7fe", "fd9c00", "969696", "ffca7c", "8c7cff", "ffff00", "d7d7d7", "00de15", "c9424f", "0bcec7", "cd73d0", "35404d", "f3f3f3", "3c21de", "b28501");
            $ownerColorMap = array();

            foreach($markerColors as $color)
            {
                $style = $dom->createElement('Style');
                $style->setAttribute('id', $color);
                $docNode->appendChild($style);

                $link = $dom->createElement('Link');
                $viewRefreshMode = $dom->createElement('viewRefreshMode', 'onRegion');
                $link->appendChild($viewRefreshMode);
                $style->appendChild($link);

                $iconStyle = $dom->createElement('IconStyle');
                $style->appendChild($iconStyle);

                $icon = $dom->createElement('Icon');
                $iconStyle->appendChild($icon);

                $iconLocation = $dom->createElement('href', htmlentities($markerUrl . $color));
                $icon->appendChild($iconLocation);
            }

            // Iterates through the map data, creating one Placemark for each row.
            $this->csv->load($this->map);
            $column = !empty($this->div) ? trim($this->seg) . ' (Div:'. $this->div . ')' : trim($this->seg);
            $results = $this->csv->connect(array('Country/Region/Sub-Region', 'Code', 'Level', 'Label', 'Lat', 'Long', $column));

            $parentNode = null;
            $parentValue = array();
            $style = null;
            $styleColor = '';
            $childrenNodes = array();
            $childNode = null;
            $savedRow = null;
            $useChildrenMarkers = false;

            foreach($results as $currentKey => $currentRow)
            {

                // is this a parent row? check the next row if it's a child row
                // if the next row's level is equal to the current row's level plus 1,
                // the current row is a parent row
                $isParent = isset($results[$currentKey + 1]) && $results[$currentKey + 1]['Level'] == $currentRow['Level'] + 1 ? true : false;

                // if this is a parent, we save the parent value in case it's children has
                // blank values, in which case we'll use this value
                if($isParent)
                    $parentValue[$currentRow['Level']] = $currentRow[$column];
                /*
                // is this a parent row?
                if($isParent)
                {
                    if($useChildrenMarkers)
                    {
                        foreach($childrenNodes as $cNode)
                        {
                            $docNode->appendChild($cNode);
                        }
                    }
                    else
                    {
                        if(!is_null($parentNode))
                        {
                            $docNode->appendChild($parentNode);
                        }
                    }


                    $savedRow = $currentRow;
                    $parentNode = null;
                    $childrenNodes = array();
                    $useChildrenMarkers = false;

                    // create a placemark for this parent
                    $parentNode = $dom->createElement('Placemark');

                    // Creates an id attribute and assign it the value of id column.
                    $parentNode->setAttribute('id', 'placemark' . $currentKey);

                    // Create name, and description elements and assigns them the values of the name and address columns from the results.
                    $nameNode = $dom->createElement('name', htmlentities($currentRow['Label']));
                    $parentNode->appendChild($nameNode);
                    $descNode = $dom->createElement('description', $currentRow[$column]);
                    $parentNode->appendChild($descNode);

                    // Create a styleUrl element
                    if(array_key_exists($currentRow[$column], $ownerColorMap))
                    {
                        $styleColor = $ownerColorMap[$currentRow[$column]];
                    }
                    else
                    {
                        $styleColor = array_shift($markerColors);
                        $ownerColorMap[$currentRow[$column]] = $styleColor;

                    }
                    $styleUrlNode = $dom->createElement('styleUrl', '#'. $styleColor);
                    $parentNode->appendChild($styleUrlNode);

                    // Creates a Point element.
                    $pointNode = $dom->createElement('Point');
                    $parentNode->appendChild($pointNode);

                    // Creates a coordinates element and gives it the value of the lng and lat columns from the results.
                    $coorStr = $currentRow['Long'] . ','  . $currentRow['Lat'];
                    $coorNode = $dom->createElement('coordinates', $coorStr);
                    $pointNode->appendChild($coorNode);
                }

                // else this is a child row
                else
                {

                    // create a placemark for this child
                    $childNode = $dom->createElement('Placemark');

                    // Creates an id attribute and assign it the value of id column.
                    $childNode->setAttribute('id', 'placemark' . $currentKey);

                    // Create name, and description elements and assigns them the values of the name and address columns from the results.
                    $nameNode = $dom->createElement('name', htmlentities($currentRow['Label']));
                    $childNode->appendChild($nameNode);

                    // if the current child row value is empty, use the saved parent value
                    $description = !empty($currentRow[$column]) ? $currentRow[$column] : $savedRow[$column];

                    if(!empty($currentRow[$column]))
                        $useChildrenMarkers = true;

                    $descNode = $dom->createElement('description', $description);
                    $childNode->appendChild($descNode);

                    // Create a styleUrl element
                    if(array_key_exists($currentRow[$column], $ownerColorMap))
                    {
                        $styleColor = $ownerColorMap[$currentRow[$column]];
                    }
                    else
                    {
                        $styleColor = array_shift($markerColors);
                        $ownerColorMap[$currentRow[$column]] = $styleColor;

                    }
                    $styleUrlNode = $dom->createElement('styleUrl', '#'. $styleColor);
                    $childNode->appendChild($styleUrlNode);

                    // Creates a Point element.
                    $pointNode = $dom->createElement('Point');
                    $childNode->appendChild($pointNode);

                    // Creates a coordinates element and gives it the value of the lng and lat columns from the results.
                    $coorStr = $currentRow['Long'] . ','  . $currentRow['Lat'];
                    $coorNode = $dom->createElement('coordinates', $coorStr);
                    $pointNode->appendChild($coorNode);

                    // store in our childrenNode array for now
                    $childrenNodes[] = $childNode;
                }
                */

                // create a placemark for this parent
                $placemark = $dom->createElement('Placemark');

                // Creates an id attribute and assign it the value of id column.
                $placemark->setAttribute('id', 'placemark' . $currentKey);

                // Create name, and description elements and assigns them the values of the name and address columns from the results.
                $nameNode = $dom->createElement('name', htmlentities($currentRow['Label']));
                $placemark->appendChild($nameNode);

                $owner = !empty($currentRow[$column]) ? $currentRow[$column] : $parentValue[$currentRow['Level'] - 1];
                $descNode = $dom->createElement('description', $owner);
                $placemark->appendChild($descNode);

                // Create a styleUrl element
                if(array_key_exists($owner, $ownerColorMap))
                {
                    $styleColor = $ownerColorMap[$owner];
                }
                else
                {
                    $styleColor = array_shift($markerColors);
                    $ownerColorMap[$owner] = $styleColor;

                }
                $styleUrlNode = $dom->createElement('styleUrl', '#'. $styleColor);
                $placemark->appendChild($styleUrlNode);

                // Creates a Point element.
                $pointNode = $dom->createElement('Point');
                $placemark->appendChild($pointNode);

                // Creates a coordinates element and gives it the value of the lng and lat columns from the results.
                $coorStr = $currentRow['Long'] . ','  . $currentRow['Lat'];
                $coorNode = $dom->createElement('coordinates', $coorStr);
                $pointNode->appendChild($coorNode);

                if($currentRow['Code'] == 'US')
                {
                    // create a region where we can change the visibility of this placemark
                    $region = $dom->createElement('Region');

                    $latLonAltBox = $dom->createElement('LatLonAltBox');
                    $north = $dom->createElement('north', $currentRow['Lat'] + 3);
                    $south = $dom->createElement('south', $currentRow['Lat'] - 3);
                    $east = $dom->createElement('east', $currentRow['Long'] + 3);
                    $west = $dom->createElement('west', $currentRow['Long'] - 3);

                    $latLonAltBox->appendChild($north);
                    $latLonAltBox->appendChild($south);
                    $latLonAltBox->appendChild($east);
                    $latLonAltBox->appendChild($west);
                    $region->appendChild($latLonAltBox);

                    $lod = $dom->createElement('Lod');
                    $minLodPixels = $dom->createElement('minLodPixels', 128);
                    $maxLodPixels = $dom->createElement('maxLodPixels', 256);

                    $lod->appendChild($minLodPixels);
                    $lod->appendChild($maxLodPixels);
                    $region->appendChild($lod);

                    $placemark->appendChild($region);
                }

                $docNode->appendChild($placemark);

            }

            $kmlOutput = $dom->saveXML();

            $this->kmlData = $kmlOutput;

            // delete our old kml data
            $this->_delete();

            file_put_contents($this->kmlInternalFile, $this->kmlData);

            $isMapped = true;
        }

        return $isMapped;
    }

    /**
     *  Sends the KML data to google maps
     */
    public function html()
    {
        $url = 'https://maps.google.com/?q='. $this->kmlExternalFile. '&amp;z=9&amp;t=p&amp;output=embed&amp;nocache='. time();

        $iframe = '<iframe id="nexus_kml_google_map" width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="'. $url .'"></iframe>';

        return $iframe;
    }

    public function mapToJson()
    {
        $isMapped = false;
        $markers = array();

        if($this->validate())
        {
            // Iterates through the map data, creating one Placemark for each row.
            $this->csv->load($this->map);
            $column = !empty($this->div) ? trim($this->seg) . ' (Div:'. $this->div . ')' : trim($this->seg);
            $results = $this->csv->connect(array('Country/Region/Sub-Region', 'Code', 'Level', 'Label', 'Lat', 'Long', $column));

            $markerUrl = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|";
            $markerColors = array("ff8b7c", "7aa9fd", "a1e98b", "fdeb6b", "d1aefe", "c5e7fe", "fd9c00", "969696", "ffca7c", "8c7cff", "ffff00", "d7d7d7", "00de15", "c9424f", "0bcec7", "cd73d0", "35404d", "f3f3f3", "3c21de", "b28501");
            $ownerColorMap = array();
            $parentValue = array();

            foreach($results as $currentKey => $currentRow)
            {
                /*  Is this a parent row? check the next row if it's a child row:
                 *  if the next row's level is equal to the current row's level plus 1,
                 *  then the current row is a parent row
                 */
                $isParent = isset($results[$currentKey + 1]) && $results[$currentKey + 1]['Level'] == $currentRow['Level'] + 1 ? true : false;

                // if this is a parent, we save the parent value in case it's children has
                // blank values, in which case we'll use this value
                if($isParent)
                    $parentValue[$currentRow['Level']] = $currentRow[$column];

                // let's get the proper ownership assignment
                $owner = '';

                // if this row has an owner, set that as the owner
                if(!empty($currentRow[$column]))
                {
                    $owner = $currentRow[$column];
                }

                // if it's empty, check it's parent values
                else
                {
                    foreach($parentValue as $key => $value)
                    {
                        if($key <= $currentRow['Level'] - 1 && !empty($value))
                        {
                            $owner = $value;
                        }
                    }
                }

                $icon = '';
                $color = '';

                // Create a styleUrl element
                if(array_key_exists($owner, $ownerColorMap))
                {
                    $icon = $markerUrl . $ownerColorMap[$owner];
                    $color = $ownerColorMap[$owner];
                }
                else
                {
                    $color = array_shift($markerColors);
                    $icon = $markerUrl . $color;

                    $ownerColorMap[$owner] = $color;

                }

                $ownerObj = new Owner();
                $own = $ownerObj->getOwnerByShortcode($owner);

                // we add each row to our markers array
                $markers[] = array(
                    'level'         =>  ( int ) $currentRow['Level'],
                    'lat'           =>  ( float ) $currentRow['Lat'],
                    'long'          =>  ( float ) $currentRow['Long'],
                    'title'         =>  $currentRow['Label'],
                    'code'          =>  $currentRow['Code'],
                    'region'        =>  $currentRow['Country/Region/Sub-Region'],
                    'owner'         =>  !empty($own['owner_label']) ? $own['owner_label'] : $owner,
                    'owner_title'   =>  !empty($own['owner_title']) ? $own['owner_title'] : null,
                    'owner_email'   =>  !empty($own['owner_email']) ? $own['owner_email'] : null,
                    'icon'          =>  $icon,
                    'color'         =>  $color,
                    'isParent'      =>  $isParent
                );
            }

            // now that we have all of our markers in an array, let's weed out the children we don't need
            // i.e. if a parent's children all has the same values as the parent, remove all the children for that parent
            $keepGoing = true;
            $parentLevel = 0;
            $childrenLevel = 1;
            $parentKey = 0;
            $totalKeys = count($markers);
            $childrenKeys = array();
            $deleteChildren = true;
            $keysToDelete = array();

            while( $keepGoing )
            {
                // assume that we don't find a parent and will stop the while loop after the foreach loop below runs
                $keepGoing = false;

                foreach( $markers as $markerKey => $marker )
                {
                    // this is a new parent, check the children of the previous parent if they should be deleted
                    if( $marker[ 'level' ] == $parentLevel )
                    {
                        // let's assume we're going to delete it's children
                        $deleteChildren = true;

                        // if each child's value is the same as it's parent, delete all of the children
                        foreach( $childrenKeys as $childKey )
                        {
                            if($markers[ $childKey ][ 'owner' ] != $markers[ $parentKey ][ 'owner' ] )
                                $deleteChildren = false;
                        }

                        if( $deleteChildren && ! empty( $childrenKeys ) )
                        {
                            $keysToDelete = array_merge($keysToDelete, $childrenKeys);
                            $markers[$parentKey]['isParent'] = false;
                        }

                        // now store this new parent's key
                        $parentKey = $markerKey;

                        // clear the children array
                        $childrenKeys = array();

                        // we found a parent, so we'll keep going
                        $keepGoing = true;
                    }

                    // if this is a child, store its key into our childrenKeys array
                    else if( $marker[ 'level' ] == $childrenLevel )
                    {
                        $childrenKeys[] = $markerKey;
                    }

                    // if this is the last key, increase our counter
                    if( $markerKey == $totalKeys - 1 )
                    {
                        $parentLevel += 1;
                        $childrenLevel += 1;
                    }

                }
            }

            // now delete everything in our deleteChildren array from our markers array
            foreach( $keysToDelete as $key )
            {
                if( isset( $markers[ $key ] ) )
                    unset( $markers[ $key ] );
            }


            $this->jsonMap = json_encode($markers);
            $isMapped = true;

        }

        return $isMapped;
    }

    public function getJson()
    {
        return $this->jsonMap;
    }


    /**
     * Validates if the map and segment/div is valid
     *
     * @return bool
     */
    public function validate()
    {
        $isValid = false;

        // first validate that our map csv file exists
        if(file_exists($this->map))
        {
            $this->csv->load($this->map);

            $headers = $this->csv->getHeaders();

            $theHeader = !empty($this->div) ? trim($this->seg). ' (Div:'. $this->div . ')' : trim($this->seg);

            foreach($headers as $key => $currentRow)
            {
                if($currentRow == $theHeader)
                {
                    $isValid = true;

                    break(1);
                }
            }

            if(!$isValid)
                $this->error = 'Header does not exist: '. $theHeader;
        }
        else
        {
            $this->error = 'The file '. $this->map . ' does not exist';
        }

        return $isValid;
    }

    /**
     * sets our csv map
     *
     * @param string $map
     */
    public function setMap($map = '')
    {
        $this->map = NEXUS_DATA_DIR . 'map-'. strtoupper($map) .'.csv';
    }

    /**
     * sets our csv map
     *
     * @param string $map
     */
    public function setSegment($seg = '')
    {
        $this->seg = $seg;
    }

    /**
     * sets our csv map
     *
     * @param string $map
     */
    public function setDivision($div = '')
    {
        $this->div = $div;
    }

    // gets our klm data
    public function get()
    {
        return $this->kmlData;
    }

    public function getLastError()
    {
        return $this->error;
    }

    // delete all the kml data in the data/kml/ directory
    private function _delete()
    {
        $kmlDirectory = NEXUS_INTERNAL_ROOT . 'data/kml/*';
        $files = glob($kmlDirectory); // get all file names
        foreach($files as $file){ // iterate files
            if(is_file($file))
                unlink($file); // delete file
        }
    }

    public function getDropdowns()
    {
        $this->csv->load($this->filterPreset);
        $filterPresetsArray = $this->csv->connect();

        $mapArray = array();
        $firstSelected = true;

        foreach($filterPresetsArray as $row)
        {
            $map = str_replace("map-", "", $row['Map']);

            if(!empty($map))
            {
                if(!array_key_exists($map, $mapArray))
                {
                    $mapArray[$map] = array(
                        'name'  => $map,
                        'id'    => 'map',
                        'selected' => $firstSelected,
                        'children' => array()
                    );
                }

                $mapArray[$map]['children'][] = array(
                    'name'      =>  $row['Preset'],
                    'id'        =>  'preset',
                    'selected'  => $firstSelected
                );

                $firstSelected = false;
            }
        }
        return $mapArray;
    }

    public function getSegmentAndDivFromPresetAndMap($preset, $map)
    {
        $this->csv->load($this->filterPreset);
        $filterPresetsArray = $this->csv->connect();

        $segment = '';
        $div = '';

        foreach($filterPresetsArray as $row)
        {
            if($row['Preset'] == $preset && $row['Map'] == 'map-'. $map)
            {
                $segment = $row['Filter Segment'];
                $div = $row['Filter Division'];
                break(1);
            }
        }
        return array('segment' => $segment, 'division' => $div);
    }

    public function getDropdownsOld()
    {
        $mapArray = array();
        $firstSelected = true;

        $files = scandir(NEXUS_DATA_DIR);
        foreach ($files as $file) {
            if ($file != '.' && $file != '..') {
                // Do stuff here
                $mapPos = strpos($file, 'map-');

                if ($file != "." && $file != ".." && $mapPos !== FALSE) {

                    $mapName = basename(substr_replace($file, '', $mapPos, 4), '.csv');

                    $mapArray[$mapName] = array(
                        'name'      => $mapName,
                        'id'        => 'map',
                        'selected'  =>  $firstSelected,
                        'children'  => $this->_getSegmentsForMap($mapName)
                    );

                    $firstSelected = false;
                }

            }
        }

        return $mapArray;
    }

    private function _getSegmentsForMap($map= '')
    {
        $segArrays = array();

        $this->setMap($map);

        // first validate that our map csv file exists
        if(file_exists($this->map))
        {
            $this->csv->load($this->map);

            $headers = $this->csv->getHeaders();

            $firstSelected = true;

            foreach($headers as $key => $currentRow)
            {
                if(!in_array($currentRow, $this->skipHeaders))
                {
                    $seg = trim(preg_replace('/\(Div:[^)]*\)/', '', $currentRow));

                    if(!array_key_exists($seg, $segArrays))
                    {
                        $segArrays[$seg] = array(
                            'name'      => $seg,
                            'id'        => 'controls_seg',
                            'selected'  =>  $firstSelected,
                            'js'        => array('onchange' => 'nexusKml.reloadSelect(this);'),
                            'children'  => $this->_getDivisionsForSegment($seg)
                        );

                        $firstSelected = false;
                    }
                }
            }
        }

        return $segArrays;
    }

    private function _getDivisionsForSegment($seg = '')
    {
        $divArrays = array();

        $this->setSegment($seg);

        // first validate that our map csv file exists
        if(file_exists($this->map))
        {
            $this->csv->load($this->map);

            $headers = $this->csv->getHeaders();

            $firstSelected = true;

            foreach($headers as $key => $currentRow)
            {
                if(!in_array($currentRow, $this->skipHeaders))
                {
                    $seg = $this->seg;

                    if(strpos($currentRow, $seg) !== false)
                    {
                        $tempDiv = trim(str_replace($seg, "" , $currentRow));

                        // look for Div: string inside the parenthesis
                        preg_match('#\(Div:(.*?)\)#', $tempDiv, $match);

                        $div = isset($match[1]) ? str_replace( "Div:", "" , $match[1]) : "";

                        if(!in_array($div, $divArrays))
                        {
                            $divArrays[$div] = array(
                                'name'  =>  $div,
                                'id'    => 'controls_div',
                                'selected'  =>  $firstSelected,
                            );

                            $firstSelected = false;
                        }
                    }
                }
            }
        }

        return $divArrays;

    }

    /**
     * Check for CURL PHP module
     * @access private
     */
    private function _chk_curl() {
        if (!extension_loaded('curl')) {
            die ("This SMS API class can not work without CURL PHP module! Try using fopen sending method.");
        }
    }

    /**
     * CURL sending method
     * @access private
     */
    private function _curl($command) {
        $this->_chk_curl();
        $ch = curl_init ($command);
        curl_setopt ($ch, CURLOPT_HEADER, 0);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER,1);
        $result = curl_exec ($ch);
        curl_close ($ch);
        return $result;
    }
}