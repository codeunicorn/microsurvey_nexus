<?php
/**
 * Created by PhpStorm.
 * Author: Joseph Santos
 * Date: 12/22/14
 * Time: 8:21 PM
 * Description: 
 */

class TestController extends NexusBase{

    private $access_key = 'lEvIQTj3knv2RX48AaVG6lMNt7ncmVs9';

    public function __construct($params = array())
    {
        parent::__construct();

        // our cron controller requires an access token to be passed in
        if(!isset($params['key']) || (isset($params['key']) && $params['key'] !== $this->access_key))
            die();
    }


    public function getHighchartsData($params = array())
    {
        $sql = 'SELECT * FROM highcharts WHERE name = :name';
        $params = array(':name' => $params['name']);
        $result = $this->ExecAndFetch($sql, $params);
        echo $result['data'];
    }

    public function testTemplate($params = array())
    {
        $file = NEXUS_INTERNAL_ROOT . 'templates/'. $params['template'] . '.php';

        if(file_exists($file))
        {
            $messageBeforeReplace = file_get_contents($file);

            // get the sender
            preg_match('/\[sender\](.*?)\[\\/sender\]/', $messageBeforeReplace, $sender);
            $data['sender'] = isset($sender[1]) ? $this->insertStrings($sender[1]) : '';

            // get the subject
            preg_match('/\[subject\](.*?)\[\\/subject\]/', $messageBeforeReplace, $subject);
            $data['subject'] = isset($subject[1]) ? $this->insertStrings($subject[1]) : '';

            // get the body
            preg_match('/\[body\]\s(.*?)\s\[\\/body\]/is', $messageBeforeReplace, $body);
            $body = isset($body[1]) ? explode("<br />", $body[1]) : '';

            foreach($body as $key => $line)
            {
                $body[$key] = $this->insertStrings($line, array(), true);
            }

            $data['body'] = implode("<br />", array_filter($body));
        }

        var_dump($data);
    }

    public function listOwnersByLastname($owners = array()) 
    {
        // get this owner's reps
        $owners = $this->loadCSV('Owners');
        $num =  count($owners);
        $x = 0;
      
        $ownersArray = array();
        while($x <= $num-1){
           
        $nameArray = explode(" ", $owners[$x]['Label']); #explode the names.
        
        // flip the array           
        rsort($nameArray);
        
        // combine the names again
        $name = implode(" ", $nameArray);
        
        // insert into array
        $ownersArray[] = $name;
            
        $x++;
    }
    // now we should have an array filled with [Last First]
    sort($ownersArray);
    var_dump($ownersArray);
}



    public function listUsersByLastname($params = array())
    {
        $sql = 'SELECT email,date_submitted FROM form_submissions 
        WHERE date_submitted BETWEEN (CURDATE() - INTERVAL 90 DAY) AND CURDATE() ORDER BY email, CHAR_LENGTH(email) DESC';
    
        $params = array();
        $result = $this->ExecAndFetchAll($sql, $params);
        var_dump($result);

    }
/*
    public function importTerritories(){

        $territories = $this->loadCSV('map-LGS');

        $parentId = '8';

        foreach($territories as $ter){

            $sql1 = '';
            $sql2 = '';
            $sql3 = '';

            if($ter['Country/Region/Sub-Region'] !== 'USA' && $ter['Country/Region/Sub-Region'] !== 'Alabama' && $ter['Country/Region/Sub-Region'] !== 'Alaska' && $ter['Country/Region/Sub-Region'] !== 'Arkansas' && $ter['Country/Region/Sub-Region'] !== 'American Samoa' && $ter['Country/Region/Sub-Region'] !== 'Arizona'
                && $ter['Country/Region/Sub-Region'] !== 'California' && $ter['Country/Region/Sub-Region'] !== 'Colorado'
                && ($ter['Level'] == 0 || $ter['Level'] == 1)
            ) {

                $sql1 = "INSERT INTO ug11z8_terms (`name`, slug, term_group) VALUES ('".$ter['Country/Region/Sub-Region']."', '". str_replace(" ", "-",strtolower($ter['Country/Region/Sub-Region']))."', '0')";
                $params = array();

                $this->Execute($sql1, $params);
                $term_id = $this->Db->Db->lastInsertId();

                if($ter['Level'] == 0){
                    $parentId = $term_id;
                }
                $theParent = $ter['Level'] == 0 ? 0 : $parentId;

                $sql2 = "INSERT INTO ug11z8_termmeta (term_id, meta_key, meta_value) VALUES ('". $term_id ."','wpcf-country-code','". $ter['Code'] ."')";
                $this->Execute($sql2, $params);

                $sql3 = "INSERT INTO ug11z8_term_taxonomy (term_id, taxonomy, description, parent, count) VALUES ('". $term_id ."','territory','','".$theParent."','0')";
                $this->Execute($sql3, $params);
            }
        }


    }

    public function importAgents(){

        $agents = $this->loadCSV('Owners');
        $elements = $this->loadCSV('Elements');

        foreach($agents as $agent){

            $agentEmail = $agent['Element'];
            $agentShortcode = strtolower(str_replace(array(".","_", "'"), "-", $agent['Short-Code']));
            $agentName = $agent['Label'];

            foreach($elements as $elem){

                if($elem['Type'] == 'owner' && $elem['Element'] == $agentEmail){
                    $agentEtrigueId = $elem['eTrigue Value'];
                }
            }

            $sql0 = "SELECT * FROM ug11z8_terms WHERE slug = '". $agentShortcode ."'";
            $params = array();
            $res = $this->ExecAndFetch($sql0, $params);

            if(! $res) {

                $sql1 = "INSERT INTO ug11z8_terms (`name`, slug, term_group) VALUES (\"" . $agentName . "\", '" . $agentShortcode . "', '0')";

                $this->Execute($sql1, $params);
                $term_id = $this->Db->Db->lastInsertId();

                $sql2 = "INSERT INTO ug11z8_termmeta (term_id, meta_key, meta_value) VALUES ('" . $term_id . "','wpcf-email','" . $agentEmail . "')";
                $this->Execute($sql2, $params);

                $sql3 = "INSERT INTO ug11z8_termmeta (term_id, meta_key, meta_value) VALUES ('" . $term_id . "','wpcf-etrigue-id','" . $agentEtrigueId . "')";
                $this->Execute($sql3, $params);

                $sql4 = "INSERT INTO ug11z8_term_taxonomy (term_id, taxonomy, description, parent, count) VALUES ('" . $term_id . "','agent','','0','0')";
                $this->Execute($sql4, $params);
            }
        }
    }

    public function updateAgentManager(){

        $agents = $this->loadCSV('Owners');

        foreach($agents as $agent){

            $agentShortcode = strtolower(str_replace(array(".","_", "'"), "-", $agent['Short-Code']));
            $managerShortcode = strtolower(str_replace(array(".","_", "'"), "-", $agent['Manager']));

            $sql0 = "SELECT * FROM ug11z8_terms WHERE slug = '". $agentShortcode ."'";
            $params = array();
            $agent = $this->ExecAndFetch($sql0, $params);

            $sql1 = "SELECT * FROM ug11z8_terms WHERE slug = '". $managerShortcode ."'";
            $params = array();
            $manager = $this->ExecAndFetch($sql1, $params);

            if($agent && $manager){
                $agentId = $agent['term_id'];
                $mnagerSlug = $manager['slug'];

                $sql2 = "INSERT INTO ug11z8_termmeta (term_id, meta_key, meta_value) VALUES ('" . $agentId . "','wpcf-manager','" . $mnagerSlug . "')";
                $this->Execute($sql2, $params);
            }
        }
    }

    public function importOrigins(){

        $elements = $this->loadCSV('Elements');

        foreach($elements as $elem) {

            if($elem['Type'] == 'orig') {

                $originShortcode = $elem['Element'];
                $etrigueId = $elem['eTrigue Value'];

                $sql0 = "SELECT * FROM ug11z8_terms WHERE slug = '". $originShortcode ."'";
                $params = array();
                $origin = $this->ExecAndFetch($sql0, $params);

                if($origin) {

                    $originId = $origin['term_id'];
                    $sql1 = "INSERT INTO ug11z8_termmeta (term_id, meta_key, meta_value) VALUES ('" . $originId . "','wpcf-origin-etrigue-id','" . $etrigueId . "')";
                    $this->Execute($sql1, $params);
                }
                else {

                    $sql1 = "INSERT INTO ug11z8_terms (`name`, slug, term_group) VALUES (\"" . $originShortcode . "\", '" . $originShortcode . "', '0')";

                    $this->Execute($sql1, $params);
                    $term_id = $this->Db->Db->lastInsertId();

                    $sql2 = "INSERT INTO ug11z8_termmeta (term_id, meta_key, meta_value) VALUES ('" . $term_id . "','wpcf-origin-etrigue-id','" . $etrigueId . "')";
                    $this->Execute($sql2, $params);

                    $sql4 = "INSERT INTO ug11z8_term_taxonomy (term_id, taxonomy, description, parent, count) VALUES ('" . $term_id . "','origin','','0','0')";
                    $this->Execute($sql4, $params);
                }

            }

        }

    }
*/

    public function getStateFromCompany() {

        $apiUrl = 'http://maps.google.com/maps/api/geocode/json?address=';
        $companies = $this->loadCSV('companies');

        foreach($companies as $key => $cRow) {

            $company = str_replace(" ", "+", $cRow['Company']);


            $ch = curl_init ($apiUrl . $company);
            curl_setopt ($ch, CURLOPT_HEADER, 0);
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER,1);
            $result = curl_exec ($ch);
            curl_close ($ch);

            $companyInfo = json_decode($result);

            if(isset($companyInfo->results) && !empty($companyInfo->results)) {

                $address_components = $companyInfo->results[0]->address_components;
                $state = '';

                foreach ($address_components as $ac) {

                    if ($ac->types[0] == 'administrative_area_level_1') {
                        $state = $ac->short_name;
                        $companies[$key]['State'] = $state;
                    }
                }
            }
        }

        // write to csv file
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=companies_formatted.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        $outputBuffer = fopen("php://output", 'w') or die("Can't open php://output");
        foreach($companies as $val) {
            fputcsv($outputBuffer, $val);
        }
        fclose($outputBuffer) or die("Can't close php://output");



    }

public function getMyLeads() {
    $sql = 'SELECT * FROM form_submissions 
            WHERE x_udf_form_owner148 = :owner 
                AND status = :status';
    $params = array(
        ':owner' => 'joseph.santos', 
        ':status' => 0 
    );
    $result = $this->ExecAndFetchAll($sql, $params);
    echo json_encode($result);
}

}
 
