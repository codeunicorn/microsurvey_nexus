<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jsantos317
 * Date: 8/21/13
 * Time: 6:35 AM
 * To change this template use File | Settings | File Templates.
 */

class Forms extends NexusBase{

    /*
     * Table columns
     */
    private $id;
    private $form_id;
    private $form_label;
    private $date_submitted;
    private $user_agent;
    private $referer;
    private $ip;
    private $categories;
    private $first_name;
    private $last_name;
    private $email;
    private $posted_data;
    private $owner_id;
    private $owner_label;
    private $owner_data;
    private $origin_id;
    private $origin_label;
    private $channel_tactic;
    private $status;
    private $status_label;
    private $auth_key;
    private $opportunities;
    private $x_owner;
    private $x_udf_form_language302;
    private $x_udf_form_owner148;
    private $x_udf_form_product149;
    private $x_udf_origin_code114__ss;
    private $x_udf_privacy_policy_consent301;
    private $x_udf_channel___tactic400__ss;
    private $x_udf_privacy_policy_version143;
    private $x_udf_customer_group125__ss;
    private $x_udf_ext_last_topic364;
    private $x_udf_ext_hds_question_cyclone451__ms;
    private $x_udf_ext_hds_question_cyclone_2452__ms;
    private $x_udf_hds_cyclone_question_3459;
    private $x_udf_subscription_start_date460;
    private $x_udf_ext_bim_hds_question462__ss;
    private $x_udf_extindustry;
    private $x_udf_extlicensesreq;
    private $x_udf_extpurchasetime;
    private $x_udf_ext_purchase_question405;
    private $x_udf_extcustomer;
    private $x_udf_ext_interest401;
    private $x_udf_event443;
    private $x_udf_main_application121__ss;
    private $x_country;
    private $x_state;
    private $x_udf_division146__ss;
    private $x_udf_extheardof;
    private $x_udf_primary_market_segment122__ss;
    private $x_solution;
    private $x_address;
    private $x_address2;
    private $x_city;
    private $x_companyname;
    private $x_emailaddress;
    private $x_fax;
    private $x_firstname;
    private $x_lastname;
    private $x_phone;
    private $x_phone2;
    private $x_title;
    private $x_udf_employees130;
    private $x_udf_ext_dealer375;
    private $x_udf_dealer_contact465;
    private $x_udf_dealer_phone466;
    private $x_udf_ext_serial_number377;
    private $x_udf_extjobfunction;
    private $x_url;
    private $x_zip;
    private $x_udf_comments126;
    private $x_udf_form_language302_label;
    private $x_udf_form_owner148_label;
    private $x_udf_form_product149_label;
    private $x_udf_origin_code114__ss_label;
    private $x_udf_channel___tactic400__ss_label;
    private $x_udf_main_application121__ss_label;
    private $x_udf_division146__ss_label;
    private $x_udf_primary_market_segment122__ss_label;
    private $x_show_products;
    private $map;
    private $preset;
    private $opportunity_name;
    private $lead_source;
    private $amount_currency;
    private $amount;
    private $close_date;
    private $next_step;
    private $stage;
    private $fiscal_period;
    private $age;
    private $created_date;
    private $opportunity_owner;
    private $owner_role;
    private $account_name;
    private $probability;
    private $effective_value;
    private $notif;
    private $notif_html;
    private $respond;
    private $respond_html;
    private $sms;
    private $sms_html;
    private $notif_sms_html;
    private $DT_RowId;
    private $edit_button;
    private $fields;

        /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->id = null;
        $this->form_id = '';
        $this->form_label = '';
        $this->date_submitted = '';
        $this->user_agent = '';
        $this->ip = '';
        $this->categories = '';
        $this->referer = '';
        $this->first_name = '';
        $this->last_name = '';
        $this->email;
        $this->posted_data = '';
        $this->owner_id = '';
        $this->owner_label = '';
        $this->owner_data = '';
        $this->origin_id = '';
        $this->origin_label = '';
        $this->channel_tactic = '';
        $this->status = '0';
        $this->status_label = 'Pending';
        $this->auth_key = '';
        $this->opportunities = '';
        $this->x_owner = '';
        $this->x_udf_form_language302 = '';
        $this->x_udf_form_owner148 = '';
        $this->x_udf_form_product149 = '';
        $this->x_udf_origin_code114__ss = '';
        $this->x_udf_privacy_policy_consent301 = '';
        $this->x_udf_channel___tactic400__ss = '';
        $this->x_udf_privacy_policy_version143 = '';
        $this->x_udf_customer_group125__ss = '';
        $this->x_udf_ext_last_topic364 = '';
        $this->x_udf_ext_hds_question_cyclone451__ms = '';
        $this->x_udf_ext_hds_question_cyclone_2452__ms = '';
        $this->x_udf_hds_cyclone_question_3459 = '';
        $this->x_udf_subscription_start_date460 = '';
        $this->x_udf_ext_bim_hds_question462__ss = '';
        $this->x_udf_extindustry = '';
        $this->x_udf_extlicensesreq = '';
        $this->x_udf_extpurchasetime = '';
        $this->x_udf_ext_purchase_question405 = '';
        $this->x_udf_extcustomer = '';
        $this->x_udf_ext_interest401 = '';
        $this->x_udf_event443 = '';
        $this->x_udf_main_application121__ss = '';
        $this->x_country = '';
        $this->x_state = '';
        $this->x_udf_division146__ss = '';
        $this->x_udf_extheardof = '';
        $this->x_udf_primary_market_segment122__ss = '';
        $this->x_solution = '';
        $this->x_address = '';
        $this->x_address2 = '';
        $this->x_city = '';
        $this->x_companyname = '';
        $this->x_emailaddress = '';
        $this->x_fax = '';
        $this->x_firstname = '';
        $this->x_lastname = '';
        $this->x_phone = '';
        $this->x_phone2 = '';
        $this->x_title = '';
        $this->x_udf_employees130 = '';
        $this->x_udf_ext_dealer375 = '';
        $this->x_udf_dealer_contact465 = '';
        $this->x_udf_dealer_phone466 = '';
        $this->x_udf_ext_serial_number377 = '';
        $this->x_udf_extjobfunction = '';
        $this->x_url = '';
        $this->x_zip = '';
        $this->x_udf_comments126 = '';
        $this->x_udf_form_language302_label = '';
        $this->x_udf_form_owner148_label = '';
        $this->x_udf_form_product149_label = '';
        $this->x_udf_origin_code114__ss_label = '';
        $this->x_udf_channel___tactic400__ss_label = '';
        $this->x_udf_main_application121__ss_label = '';
        $this->x_udf_division146__ss_label = '';
        $this->x_udf_primary_market_segment122__ss_label = '';
        $this->x_show_products = '';
        $this->map = '';
        $this->preset = '';
        $this->opportunity_name = '';
        $this->lead_source = '';
        $this->amount_currency = '';
        $this->amount = '';
        $this->close_date = '';
        $this->next_step = '';
        $this->stage = '';
        $this->fiscal_period = '';
        $this->age = '';
        $this->created_date = '';
        $this->opportunity_owner = '';
        $this->owner_role = '';
        $this->account_name = '';
        $this->probability = '';
        $this->effective_value = '';
        $this->notif = '';
        $this->notif_html = '';
        $this->respond = '';
        $this->respond_html = '';
        $this->sms = '';
        $this->sms_html = '';
        $this->notif_sms_html = '';
        $this->DT_RowId = '';
        $this->edit_button = '<button class="edit_row_btn btn btn-default btn-xs" data-op="0"><span class="glyphicon glyphicon-pencil"></span></button><button class="notify_row_btn btn btn-warning btn-xs"><span class="glyphicon glyphicon-envelope"></span></button>';

        $this->fields = array(
            'date_submitted',
            'user_agent',
            'referer',
            'ip',
            'categories',
            'form_id',
            'form_label',
            'first_name',
            'last_name',
            'email',
            'posted_data',
            'owner_id',
            'owner_label',
            'origin_id',
            'origin_label',
            'channel_tactic',
            'status',
            'status_label',
            'auth_key',
            'opportunities',
            'x_owner',
            'x_udf_form_language302',
            'x_udf_form_owner148',
            'x_udf_form_product149',
            'x_udf_origin_code114__ss',
            'x_udf_privacy_policy_consent301',
            'x_udf_channel___tactic400__ss',
            'x_udf_privacy_policy_version143',
            'x_udf_customer_group125__ss',
            'x_udf_ext_last_topic364',
            'x_udf_ext_hds_question_cyclone451__ms',
            'x_udf_ext_hds_question_cyclone_2452__ms',
            'x_udf_hds_cyclone_question_3459',
            'x_udf_subscription_start_date460 ',
            'x_udf_ext_bim_hds_question462__ss',
            'x_udf_extindustry',
            'x_udf_extlicensesreq',
            'x_udf_extpurchasetime',
            'x_udf_ext_purchase_question405',
            'x_udf_extcustomer',
            'x_udf_ext_interest401',
            'x_udf_event443',
            'x_udf_main_application121__ss',
            'x_country',
            'x_state',
            'x_udf_division146__ss',
            'x_udf_extheardof',
            'x_udf_primary_market_segment122__ss',
            'x_solution',
            'x_address',
            'x_address2',
            'x_city',
            'x_companyname',
            'x_emailaddress',
            'x_fax',
            'x_firstname',
            'x_lastname',
            'x_phone',
            'x_phone2',
            'x_title',
            'x_udf_employees130',
            'x_udf_ext_dealer375',
            'x_udf_dealer_contact465',
            'x_udf_dealer_phone466',
            'x_udf_ext_serial_number377',
            'x_udf_extjobfunction',
            'x_url',
            'x_zip',
            'x_udf_comments126',
            'x_udf_form_language302_label',
            'x_udf_form_owner148_label',
            'x_udf_form_product149_label',
            'x_udf_origin_code114__ss_label',
            'x_udf_channel___tactic400__ss_label',
            'x_udf_main_application121__ss_label',
            'x_udf_division146__ss_label',
            'x_udf_primary_market_segment122__ss_label',
            'x_show_products',
            'map',
            'preset',
            'opportunity_name',
            'lead_source',
            'amount_currency',
            'amount',
            'close_date',
            'next_step',
            'stage',
            'fiscal_period',
            'age',
            'created_date',
            'opportunity_owner',
            'owner_role',
            'account_name',
            'probability',
            'effective_value',
            'notif',
            'notif_html',
            'respond',
            'respond_html',
            'sms',
            'sms_html',
            'notif_sms_html',
            'DT_RowId',
            'edit_button'
        );

    }

    /**
     * processes the $_POST data
     *
     * @param $post
     */
    public function loadPostData($post)
    {
        $this->form_id = isset($post['espFormID']) ? $post['espFormID'] : (!empty($this->form_id) ? $this->form_id : '');
        $this->first_name = isset($post['x_firstname']) ? $post['x_firstname'] : '';
        $this->last_name = isset($post['x_lastname']) ? $post['x_lastname'] : '';
        $this->email = isset($post['x_emailaddress']) ? $post['x_emailaddress'] : '';
        $this->origin_id = isset($post['origin_id']) ? $post['origin_id'] : '';
        $this->origin_label = isset($post['originz_label']) ? $post['origin_label'] : '';
        $this->channel_tactic = isset($post['x_udf_channel___tactic400__ss']) ? $post['x_udf_channel___tactic400__ss'] : '';
        $this->posted_data = json_encode($post);
        $this->categories = isset($post['categories']) ? $post['categories'] : '';
        $this->x_owner = isset($post['x_owner']) ? $post['x_owner'] : '';
        $this->x_udf_form_language302 = isset($post['x_udf_form_language302']) ? $post['x_udf_form_language302'] : '';
        $this->x_udf_form_owner148 = isset($post['x_udf_form_owner148']) ? $post['x_udf_form_owner148'] : '';
        $this->x_udf_form_product149 = isset($post['x_udf_form_product149']) ? $post['x_udf_form_product149'] : '';
        $this->x_udf_origin_code114__ss = isset($post['x_udf_origin_code114__ss']) ? $post['x_udf_origin_code114__ss'] : '';
        $this->x_udf_privacy_policy_consent301 = isset($post['x_udf_privacy_policy_consent301']) ? $post['x_udf_privacy_policy_consent301'] : '';
        $this->x_udf_channel___tactic400__ss = isset($post['x_udf_channel___tactic400__ss']) ? $post['x_udf_channel___tactic400__ss'] : '';
        $this->x_udf_privacy_policy_version143 = isset($post['x_udf_privacy_policy_version143']) ? $post['x_udf_privacy_policy_version143'] : '';
        $this->x_udf_customer_group125__ss = isset($post['x_udf_customer_group125__ss']) ? $post['x_udf_customer_group125__ss'] : '';
        $this->x_udf_ext_last_topic364 = isset($post['x_udf_ext_last_topic364']) ? $post['x_udf_ext_last_topic364'] : '';
        $this->x_udf_ext_hds_question_cyclone451__ms = isset($post['x_udf_ext_hds_question_cyclone451__ms']) ? implode(',', $post['x_udf_ext_hds_question_cyclone451__ms']) : '';
        $this->x_udf_ext_hds_question_cyclone_2452__ms = isset($post['x_udf_ext_hds_question_cyclone_2452__ms']) ? implode(',', $post['x_udf_ext_hds_question_cyclone_2452__ms']) : '';
        $this->x_udf_hds_cyclone_question_3459 = isset($post['x_udf_hds_cyclone_question_3459']) ? $post['x_udf_hds_cyclone_question_3459'] : '';
        $this->x_udf_subscription_start_date460 = isset($post['x_udf_subscription_start_date460']) ? $post['x_udf_subscription_start_date460'] : '';
        $this->x_udf_ext_bim_hds_question462__ss = isset($post['x_udf_ext_bim_hds_question462__ss']) ? $post['x_udf_ext_bim_hds_question462__ss'] : '';
        $this->x_udf_extindustry = isset($post['x_udf_extindustry']) ? $post['x_udf_extindustry'] : '';
        $this->x_udf_extlicensesreq = isset($post['x_udf_extlicensesreq']) ? $post['x_udf_extlicensesreq'] : '';
        $this->x_udf_extpurchasetime = isset($post['x_udf_extpurchasetime']) ? $post['x_udf_extpurchasetime'] : '';
        $this->x_udf_ext_purchase_question405 = isset($post['x_udf_ext_purchase_question405']) ? $post['x_udf_ext_purchase_question405'] : '';
        $this->x_udf_extcustomer = isset($post['x_udf_extcustomer']) ? $post['x_udf_extcustomer'] : '';
        $this->x_udf_ext_interest401 = isset($post['x_udf_ext_interest401']) ? $post['x_udf_ext_interest401'] : '';
        $this->x_udf_event443 = isset($post['x_udf_event443']) ? $post['x_udf_event443'] : '';
        $this->x_udf_main_application121__ss = isset($post['x_udf_main_application121__ss']) ? $post['x_udf_main_application121__ss'] : '';
        $this->x_country = isset($post['x_country']) ? $post['x_country'] : '';
        $this->x_state = isset($post['x_state']) ? $post['x_state'] : '';
        $this->x_udf_division146__ss = isset($post['x_udf_division146__ss']) ? $post['x_udf_division146__ss'] : '';
        $this->x_udf_extheardof = isset($post['x_udf_extheardof']) ? $post['x_udf_extheardof'] : '';
        $this->x_udf_primary_market_segment122__ss = isset($post['x_udf_primary_market_segment122__ss']) ? $post['x_udf_primary_market_segment122__ss'] : '';
        $this->x_solution = isset($post['x_solution']) ? $post['x_solution'] : '';
        $this->x_address = isset($post['x_address']) ? $post['x_address'] : '';
        $this->x_address2 = isset($post['x_address2']) ? $post['x_address2'] : '';
        $this->x_city = isset($post['x_city']) ? $post['x_city'] : '';
        $this->x_companyname = isset($post['x_companyname']) ? $post['x_companyname'] : '';
        $this->x_emailaddress = isset($post['x_emailaddress']) ? $post['x_emailaddress'] : '';
        $this->x_fax = isset($post['x_fax']) ? $post['x_fax'] : '';
        $this->x_firstname = isset($post['x_firstname']) ? $post['x_firstname'] : '';
        $this->x_lastname = isset($post['x_lastname']) ? $post['x_lastname'] : '';
        $this->x_phone = isset($post['x_phone']) ? $post['x_phone'] : '';
        $this->x_phone2 = isset($post['x_phone2']) ? $post['x_phone2'] : '';
        $this->x_title = isset($post['x_title']) ? $post['x_title'] : '';
        $this->x_udf_employees130 = isset($post['x_udf_employees130']) ? $post['x_udf_employees130'] : '';
        $this->x_udf_ext_dealer375 = isset($post['x_udf_ext_dealer375']) ? $post['x_udf_ext_dealer375'] : '';
        $this->x_udf_dealer_contact465 = isset($post['x_udf_dealer_contact465']) ? $post['x_udf_dealer_contact465'] : '';
        $this->x_udf_dealer_phone466 = isset($post['x_udf_dealer_phone466']) ? $post['x_udf_dealer_phone466'] : '';
        $this->x_udf_ext_serial_number377 = isset($post['x_udf_ext_serial_number377']) ? $post['x_udf_ext_serial_number377'] : '';
        $this->x_udf_extjobfunction = isset($post['x_udf_extjobfunction']) ? $post['x_udf_extjobfunction'] : '';
        $this->x_url = isset($post['x_url']) ? $post['x_url'] : '';
        $this->x_zip = isset($post['x_zip']) ? $post['x_zip'] : '';
        $this->x_udf_comments126 = isset($post['x_udf_comments126']) ? $post['x_udf_comments126'] : '';
        $this->x_show_products = isset($post['x_show_products']) ? $post['x_show_products'] : '';
        $this->map = isset($post['map']) ? $post['map'] : '';
        $this->notif = isset($post['notif']) ? $post['notif'] : 0;
        $this->respond = isset($post['respond']) ? $post['respond'] : 0;
        $this->sms = isset($post['sms']) ? $post['sms'] : 0;
        $this->notif_html = $this->notif == 1 ? '<span class="notif_yes">Email</span>' : '<span class="notif_no">Email</span>';
        $this->respond_html = $this->respond == 1 ? '<span class="notif_yes">Email</span>' : '<span class="notif_no">Email</span>';
        $this->sms_html = $this->sms == 1 ? '<span class="notif_yes">Text</span>' : '<span class="notif_no">Text</span>';
        $this->notif_sms_html = $this->notif_html . $this->sms_html;
        $this->DT_RowId = 'row-';
        $this->edit_button = '<button class="edit_row_btn btn btn-default btn-xs" data-op="0"><span class="glyphicon glyphicon-pencil"></span></button><button class="notify_row_btn btn btn-warning btn-xs"><span class="glyphicon glyphicon-envelope"></span></button>';

        $this->status = isset($post['nstatus']) ? $post['nstatus'] : '0';
        $this->status_label = isset($post['nstatus_label']) ? $post['nstatus_label'] : 'Pending';

        /*
         * Labels
         */
        $this->form_label = isset($post['form_label']) ? $post['form_label'] : !empty($this->form_id) ? $this->_getFormLabelFromFormId($this->form_id) . ' / '. $this->form_id : '';
        $this->x_udf_form_language302_label = isset($post['x_udf_form_language302_label']) ? $post['x_udf_form_language302_label'] : (!empty($this->x_udf_form_language302 ) ? $this->_getLanguageLabelFromLanguageValue($this->x_udf_form_language302) : '');
        $this->x_udf_form_owner148_label = isset($post['x_udf_form_owner148_label']) ? $post['x_udf_form_owner148_label'] : (!empty($this->x_udf_form_owner148) ? $this->_getOwnerLabelFromOwnerShortcode($this->x_udf_form_owner148) : '');
        $this->x_udf_form_product149_label = isset($post['x_udf_form_product149_label']) ? $post['x_udf_form_product149_label'] : (!empty($this->x_udf_form_product149) ? $this->_getProductLabelFromProductShortcode($this->x_udf_form_product149): '');
        $this->x_udf_origin_code114__ss_label = isset($post['x_udf_origin_code114__ss_label']) ? $post['x_udf_origin_code114__ss_label'] : (!empty($this->x_udf_origin_code114__ss) ? $this->_getOriginFromOriginId($this->x_udf_origin_code114__ss) : '');
        $this->x_udf_channel___tactic400__ss_label = isset($post['x_udf_channel___tactic400__ss_label']) ? $post['x_udf_channel___tactic400__ss_label'] : (!empty($this->x_udf_channel___tactic400__ss) ? $this->_getChannelTacticFromValue($this->x_udf_channel___tactic400__ss) : '');
        $this->x_udf_main_application121__ss_label = isset($post['x_udf_main_application121__ss_label']) ? $post['x_udf_main_application121__ss_label'] : (!empty($this->x_udf_main_application121__ss) ? $this->_getApplicationLabelFromApplicationValue($this->x_udf_main_application121__ss) : '');
        $this->x_udf_division146__ss_label = isset($post['x_udf_division146__ss_label']) ? $post['x_udf_division146__ss_label'] : (!empty($this->x_udf_division146__ss) ? $this->getDivFromDivValue($this->x_udf_division146__ss) : '');
        $this->x_udf_primary_market_segment122__ss_label = isset($post['x_udf_primary_market_segment122__ss_label']) ? $post['x_udf_primary_market_segment122__ss_label'] : (!empty($this->x_udf_primary_market_segment122__ss) ? $this->getSegmentFromSegmentId($this->x_udf_primary_market_segment122__ss) : '');

        // get our preest from origin, segment, and div
        $this->preset = isset($post['preset']) ? $post['preset'] : $this->getPresetFromSegmentAndDivision($this->x_udf_primary_market_segment122__ss_label, $this->x_udf_division146__ss_label, $this->x_udf_origin_code114__ss_label);


        /*
         * Opportunities
         */
        $this->opportunities = isset($post['opportunities']) ? $post['opportunities'] : '';

        // OPPORTUNITY NAME

        // if the opportunity name is present from importing opportunities leads
        if(isset($post['opportunity_name']) && !empty($post['opportunity_name']))
            $this->opportunity_name =  $post['opportunity_name'];

        // or if the customer answers the customer field and the company name is present
        else if(isset($post['x_udf_extcustomer']) && isset($post['x_companyname']) && !empty($post['x_companyname']))
            $this->opportunity_name = $post['x_companyname'];

        // or if the customer answers the customer field, and the company name is NOT present, use the customer's name
        else if(isset($post['x_udf_extcustomer']) && isset($post['x_firstname']) && $post['x_lastname'])
            $this->opportunity_name = $post['x_firstname'] . ' '. $post['x_lastname'];

        $this->lead_source = isset($post['lead_source']) ? $post['lead_source'] : '';
        $this->amount_currency = isset($post['amount_currency']) ? $post['amount_currency'] : '';
        $this->amount = isset($post['amount']) ? $post['amount'] : '';
        $this->close_date = isset($post['close_date']) ? $post['close_date'] : '';
        $this->next_step = isset($post['next_step']) ? $post['next_step'] : '';
        $this->stage = isset($post['stage']) ? $post['stage'] : '';
        $this->fiscal_period = isset($post['fiscal_period']) ? $post['fiscal_period'] : '';
        $this->age = isset($post['age']) ? $post['age'] : '';
        $this->created_date = isset($post['created_date']) ? $post['created_date'] : '';
        $this->opportunity_owner = isset($post['opportunity_owner']) ? $post['opportunity_owner'] : '';
        $this->owner_role = isset($post['owner_role']) ? $post['owner_role'] : '';
        $this->account_name = isset($post['account_name']) ? $post['account_name'] : '';
        $this->probability = isset($post['probability']) ? $post['probability'] : '';
        $this->effective_value = isset($post['effective_value']) ? $post['effective_value'] : '';
    }

    /**
     * processes the owner data for form submission
     *
     * @param $post
     */
    public function loadOwnerData($owner)
    {
        $this->owner_id = isset($owner['owner_id']) ? $owner['owner_id'] : '';
        $this->owner_label = isset($owner['owner_shortcode']) ? $owner['owner_shortcode'] : '';
        $this->x_owner = isset($owner['owner_id']) ? $owner['owner_id'] : '';
        $this->x_udf_form_owner148 = isset($owner['owner_shortcode']) ? $owner['owner_shortcode'] : '';
        $this->x_udf_form_owner148_label = isset($owner['owner_label']) ? $owner['owner_label'] : '';
    }

    public function loadAuthKey($key)
    {
        $this->auth_key = $key;
        return $this;
    }

    public function saveSubmission()
    {
        $sql = 'INSERT INTO form_submissions ('. implode(', ', $this->fields) . ') VALUES (:'. implode(', :', $this->fields) .')';
        $params = $this->_getParams();
        $params[':date_submitted'] = date('Y-m-d H:i:s');

        if(!empty($this->opportunity_name))
            $params[':created_date'] = date('Y-m-d H:i:s');

        try {
            $this->Execute($sql, $params);
            $ret = $this->Db->Db->lastInsertId();
            $this->updateDT_RowId($ret);
        }
        catch(Exception $e)
        {
            $ret = $e->getMessage();
            error_log($ret);
        }

        return $ret;
    }

    public function update()
    {
        $sql = 'UPDATE form_submissions SET '. implode(', ', array_map(function($value){return "{$value} = :{$value}";}, $this->fields)) . ' WHERE id = :id';
        $params = $this->_getParams();
        $params[':id'] = (int) $this->id;
        $params[':date_submitted'] = $this->date_submitted;
        try {
            $this->Execute($sql, $params);
            $ret = $this->id;
        }
        catch(Exception $e)
        {
            $ret = $e->getMessage();
        }

        return $ret;
    }

    private function updateDT_RowId($id)
    {
        $sql = 'UPDATE form_submissions SET DT_RowId = :DT_RowId WHERE id = :id';
        $params = array(':DT_RowId' => 'row-'. $id, ':id' =>  (int) $id);

        try {
            $this->Execute($sql, $params);
            $ret = $id;
        }
        catch(Exception $e)
        {
            $ret = $e->getMessage();
        }

        return $ret;
    }

    private function _getParams()
    {
        return array(
            ':user_agent' => isset($this->user_agent) && !empty($this->user_agent) ? $this->user_agent : (isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : ''),
            ':referer'  => isset($this->referer) && !empty($this->referer) ? $this->referer : (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ''),
            ':ip'      => isset($this->ip) && !empty($this->ip) ? $this->ip : Utility::GrabIP(),
            ':categories' => $this->categories,
            ':form_id' => $this->form_id,
            ':form_label' => $this->form_label,
            ':first_name' => $this->first_name,
            ':last_name' => $this->last_name,
            ':email' => $this->email,
            ':posted_data' => $this->posted_data,
            ':owner_id' => $this->owner_id,
            ':owner_label' => $this->owner_label,
            ':origin_id' => $this->origin_id,
            ':origin_label' => $this->origin_label,
            ':channel_tactic' => $this->channel_tactic,
            ':status' => $this->status,
            ':status_label' => $this->status_label,
            ':auth_key' => $this->auth_key,
            ':opportunities' => $this->opportunities,
            ':x_owner' => $this->x_owner,
            ':x_udf_form_language302' => $this->x_udf_form_language302,
            ':x_udf_form_owner148' => $this->x_udf_form_owner148,
            ':x_udf_form_product149' => $this->x_udf_form_product149,
            ':x_udf_origin_code114__ss' => $this->x_udf_origin_code114__ss,
            ':x_udf_privacy_policy_consent301' => $this->x_udf_privacy_policy_consent301,
            ':x_udf_channel___tactic400__ss' => $this->x_udf_channel___tactic400__ss,
            ':x_udf_privacy_policy_version143' => $this->x_udf_privacy_policy_version143,
            ':x_udf_customer_group125__ss' => $this->x_udf_customer_group125__ss,
            ':x_udf_ext_last_topic364' => $this->x_udf_ext_last_topic364,
            ':x_udf_ext_hds_question_cyclone451__ms' => $this->x_udf_ext_hds_question_cyclone451__ms,
            ':x_udf_ext_hds_question_cyclone_2452__ms' => $this->x_udf_ext_hds_question_cyclone_2452__ms,
            ':x_udf_hds_cyclone_question_3459' => $this->x_udf_hds_cyclone_question_3459,
            ':x_udf_subscription_start_date460' => $this->x_udf_subscription_start_date460,
            ':x_udf_ext_bim_hds_question462__ss' => $this->x_udf_ext_bim_hds_question462__ss,
            ':x_udf_extindustry' => $this->x_udf_extindustry,
            ':x_udf_extlicensesreq' => $this->x_udf_extlicensesreq,
            ':x_udf_extpurchasetime' => $this->x_udf_extpurchasetime,
            ':x_udf_ext_purchase_question405' => $this->x_udf_ext_purchase_question405,
            ':x_udf_extcustomer' => $this->x_udf_extcustomer,
            ':x_udf_ext_interest401' => $this->x_udf_ext_interest401,
            ':x_udf_event443' => $this->x_udf_event443,
            ':x_udf_main_application121__ss' => $this->x_udf_main_application121__ss,
            ':x_country' => $this->x_country,
            ':x_state' => $this->x_state,
            ':x_udf_division146__ss' => $this->x_udf_division146__ss,
            ':x_udf_extheardof' => $this->x_udf_extheardof,
            ':x_udf_primary_market_segment122__ss' => $this->x_udf_primary_market_segment122__ss,
            ':x_solution' => $this->x_solution,
            ':x_address' => $this->x_address,
            ':x_address2' => $this->x_address2,
            ':x_city' => $this->x_city,
            ':x_companyname' => $this->x_companyname,
            ':x_emailaddress' => $this->x_emailaddress,
            ':x_fax' => $this->x_fax,
            ':x_firstname' => $this->x_firstname,
            ':x_lastname' => $this->x_lastname,
            ':x_phone' => $this->x_phone,
            ':x_phone2' => $this->x_phone2,
            ':x_title' => $this->x_title,
            ':x_udf_employees130' => $this->x_udf_employees130,
            ':x_udf_ext_dealer375' => $this->x_udf_ext_dealer375,
            ':x_udf_dealer_contact465' => $this->x_udf_dealer_contact465,
            ':x_udf_dealer_phone466' => $this->x_udf_dealer_phone466,
            ':x_udf_ext_serial_number377' => $this->x_udf_ext_serial_number377,
            ':x_udf_extjobfunction' => $this->x_udf_extjobfunction,
            ':x_url' => $this->x_url,
            ':x_zip' => $this->x_zip,
            ':x_udf_comments126' => $this->x_udf_comments126,
            ':x_udf_form_language302_label' => $this->x_udf_form_language302_label,
            ':x_udf_form_owner148_label' => $this->x_udf_form_owner148_label,
            ':x_udf_form_product149_label' => $this->x_udf_form_product149_label,
            ':x_udf_origin_code114__ss_label' => $this->x_udf_origin_code114__ss_label,
            ':x_udf_channel___tactic400__ss_label' => $this->x_udf_channel___tactic400__ss_label,
            ':x_udf_main_application121__ss_label' => $this->x_udf_main_application121__ss_label,
            ':x_udf_division146__ss_label' => $this->x_udf_division146__ss_label,
            ':x_udf_primary_market_segment122__ss_label' => $this->x_udf_primary_market_segment122__ss_label,
            ':x_show_products' => $this->x_show_products,
            ':map' => $this->map,
            ':preset' => $this->preset,
            ':opportunity_name' => $this->opportunity_name,
            ':lead_source' => $this->lead_source,
            ':amount_currency' => $this->amount_currency,
            ':amount' => $this->amount,
            ':close_date' => $this->close_date,
            ':next_step' => $this->next_step,
            ':stage' => $this->stage,
            ':fiscal_period' => $this->fiscal_period,
            ':age' => $this->age,
            ':created_date' => $this->created_date,
            ':opportunity_owner' => $this->opportunity_owner,
            ':owner_role' => $this->owner_role,
            ':account_name' => $this->account_name,
            ':probability' => $this->probability,
            ':effective_value' => $this->effective_value,
            ':notif' => $this->notif,
            ':notif_html' => $this->notif_html,
            ':respond' => $this->respond,
            ':respond_html' => $this->respond_html,
            ':sms' => $this->sms,
            ':sms_html' => $this->sms_html,
            ':notif_sms_html' => $this->notif_sms_html,
            ':DT_RowId' => $this->DT_RowId,
            ':edit_button' => $this->edit_button
        );
    }

    public function getFormData()
    {
        return array(
            'user_agent' => $this->user_agent,
            'referer'  => $this->referer,
            'ip'      => $this->ip,
            'categories' => $this->categories,
            'form_id' => $this->form_id,
            'form_label' => $this->form_label,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'posted_data' => $this->posted_data,
            'owner_id' => $this->owner_id,
            'owner_label' => $this->x_udf_form_owner148_label,  // hack
            'origin_id' => $this->origin_id,
            'origin_label' => $this->origin_label,
            'channel_tactic' => $this->channel_tactic,
            'status' => $this->status,
            'status_label' => $this->status_label,
            'auth_key' => $this->auth_key,
            'opportunities' => $this->opportunities,
            'x_owner' => $this->x_owner,
            'x_udf_form_language302' => $this->x_udf_form_language302,
            'x_udf_form_owner148' => $this->x_udf_form_owner148,
            'x_udf_form_product149' => $this->x_udf_form_product149,
            'x_udf_origin_code114__ss' => $this->x_udf_origin_code114__ss,
            'x_udf_privacy_policy_consent301' => $this->x_udf_privacy_policy_consent301,
            'x_udf_channel___tactic400__ss' => $this->x_udf_channel___tactic400__ss,
            'x_udf_privacy_policy_version143' => $this->x_udf_privacy_policy_version143,
            'x_udf_customer_group125__ss' => $this->x_udf_customer_group125__ss,
            'x_udf_ext_last_topic364' => $this->x_udf_ext_last_topic364,
            'x_udf_ext_hds_question_cyclone451__ms' => $this->x_udf_ext_hds_question_cyclone451__ms,
            'x_udf_ext_hds_question_cyclone_2452__ms' => $this->x_udf_ext_hds_question_cyclone_2452__ms,
            'x_udf_hds_cyclone_question_3459' => $this->x_udf_hds_cyclone_question_3459,
            'x_udf_subscription_start_date460' => $this->x_udf_subscription_start_date460,
            'x_udf_ext_bim_hds_question462__ss' => $this->x_udf_ext_bim_hds_question462__ss,
            'x_udf_extindustry' => $this->x_udf_extindustry,
            'x_udf_extlicensesreq' => $this->x_udf_extlicensesreq,
            'x_udf_extpurchasetime' => $this->x_udf_extpurchasetime,
            'x_udf_ext_purchase_question405' => $this->x_udf_ext_purchase_question405,
            'x_udf_extcustomer' => $this->x_udf_extcustomer,
            'x_udf_ext_interest401' => $this->x_udf_ext_interest401,
            'x_udf_event443' => $this->x_udf_event443,
            'x_udf_main_application121__ss' => $this->x_udf_main_application121__ss,
            'x_country' => $this->x_country,
            'x_state' => $this->x_state,
            'x_udf_division146__ss' => $this->x_udf_division146__ss,
            'x_udf_extheardof' => $this->x_udf_extheardof,
            'x_udf_primary_market_segment122__ss' => $this->x_udf_primary_market_segment122__ss,
            'x_solution' => $this->x_solution,
            'x_address' => $this->x_address,
            'x_address2' => $this->x_address2,
            'x_city' => $this->x_city,
            'x_companyname' => $this->x_companyname,
            'x_emailaddress' => $this->x_emailaddress,
            'x_fax' => $this->x_fax,
            'x_firstname' => $this->x_firstname,
            'x_lastname' => $this->x_lastname,
            'x_phone' => $this->x_phone,
            'x_phone2' => $this->x_phone2,
            'x_title' => $this->x_title,
            'x_udf_employees130' => $this->x_udf_employees130,
            'x_udf_ext_dealer375' => $this->x_udf_ext_dealer375,
            'x_udf_dealer_contact465' => $this->x_udf_dealer_contact465,
            'x_udf_dealer_phone466' => $this->x_udf_dealer_phone466,
            'x_udf_ext_serial_number377' => $this->x_udf_ext_serial_number377,
            'x_udf_extjobfunction' => $this->x_udf_extjobfunction,
            'x_url' => $this->x_url,
            'x_zip' => $this->x_zip,
            'x_udf_comments126' => $this->x_udf_comments126,
            'x_udf_form_language302_label' => $this->x_udf_form_language302_label,
            'x_udf_form_owner148_label' => $this->x_udf_form_owner148_label,
            'x_udf_form_product149_label' => $this->x_udf_form_product149_label,
            'x_udf_origin_code114__ss_label' => $this->x_udf_origin_code114__ss_label,
            'x_udf_channel___tactic400__ss_label' => $this->x_udf_channel___tactic400__ss_label,
            'x_udf_main_application121__ss_label' => $this->x_udf_main_application121__ss_label,
            'x_udf_division146__ss_label' => $this->x_udf_division146__ss_label,
            'x_udf_primary_market_segment122__ss_label' => $this->x_udf_primary_market_segment122__ss_label,
            'x_show_products' => $this->x_show_products,
            'map' => $this->map,
            'preset' => $this->preset,
            'opportunity_name' => $this->opportunity_name,
            'lead_source' => $this->lead_source,
            'amount_currency' => $this->amount_currency,
            'amount' => $this->amount,
            'close_date' => $this->close_date,
            'next_step' => $this->next_step,
            'stage' => $this->stage,
            'fiscal_period' => $this->fiscal_period,
            'age' => $this->age,
            'created_date' => $this->created_date,
            'opportunity_owner' => $this->opportunity_owner,
            'owner_role' => $this->owner_role,
            'account_name' => $this->account_name,
            'probability' => $this->probability,
            'effective_value' => $this->effective_value,
            'notif' => $this->notif,
            'notif_html' => $this->notif_html,
            'respond' => $this->respond,
            'respond_html' => $this->respond_html,
            'sms' => $this->sms,
            'sms_html' => $this->sms_html,
            'notif_sms_html' => $this->notif_sms_html,
            'DT_RowId' => $this->DT_RowId,
            'edit_button' => $this->edit_button
        );
    }

    public function loadFormFromId($id)
    {
        $theId = (int) $id;
        $sql = "SELECT * FROM form_submissions WHERE id = :theId";
        $params = array(
            ':theId' => $theId
        );

        try {
            $data = $this->ExecAndFetch($sql, $params);

            foreach($data as $key => $val)
            {
                $this->$key = $val;
            }
        }
        catch(Exception $e)
        {
            $ret = $e->getMessage();
        }
    }

    public function loadFormFromAuthKey()
    {
        $sql = "SELECT * FROM form_submissions WHERE auth_key = :auth_key";
        $params = array(
            ':auth_key' => $this->auth_key
        );

        try {
            $data = $this->ExecAndFetch($sql, $params);

            if(is_array($data))
            {
                foreach($data as $key => $val)
                {
                    $this->$key = $val;
                }
            }
        }
        catch(Exception $e)
        {
            $ret = $e->getMessage();
        }

        return $this;
    }

    public function doesAuthKeyExist()
    {
        if(empty($this->auth_key))
            return false;

        $sql = "SELECT COUNT(*) as count FROM form_submissions WHERE auth_key = :auth_key";
        $params = array(
            ':auth_key' => $this->auth_key
        );

        try {
            $data = $this->ExecAndFetch($sql, $params);
            $ret = $data['count'] > 0 ? true : false;
        }
        catch(Exception $e)
        {
            $ret = $e->getMessage();
        }

        return $ret;
    }

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    public function getStatusArray()
    {
        return array(
            0 => 'Pending',
            1 => 'Accepted',
            2 => 'Rejected',
            3 => 'Rejected - Duplicate',
            4 => 'Rejected = Reassign',
            5 => 'Rejected - Garbage',
            6 => 'Rejected - Dealer/partner/staff',
            7 => 'Utility'
        );
    }

    public function getApplicationLabelFromApplicationValue($application)
    {
        $this->_getApplicationLabelFromApplicationValue($application);
    }

    public function getApplication2LabelFromValues($value) {

        $returnLabel = '';

        $ElementsArray = $this->loadCSV("Elements");
        $formLabelsArray = $this->loadCSV("Form-Labels");

        foreach ($ElementsArray as $row) {

            if ($row['Type'] == 'form-select-option' && $row['eTrigue Value'] == $value) {
                $applicationElement = $row['Element'];

                foreach($formLabelsArray as $frow)
                {
                    if($frow['Element'] == $applicationElement)
                        $returnLabel = $frow['en_US'];
                }


            }
        }

        return $returnLabel;
    }

    public function getISO2Countrycode($map='map-LGS', $country = 'USA'){

        $returnCountry = '';
        $mapArray = $this->loadCSV($map);

        foreach($mapArray as $map) {

            if($map['Country/Region/Sub-Region'] == $country && $map['Level'] == 0) {

                $returnCountry = $map['Code'];
            }
        }

        return $returnCountry;

    }

    public function getCycloneLabelsFromValues($theCyclone, $delimiter = ',')
    {
        $ElementsArray = $this->loadCSV("Elements");
        $formLabelsArray = $this->loadCSV("Form-Labels");

        $cycloneElements = array();

        $cyclone = is_array($theCyclone) ? $theCyclone : explode(',', $theCyclone);

        foreach($cyclone as $cycloneValue) {

            foreach ($ElementsArray as $row) {

                if ($row['Type'] == 'form-input-checkbox' && $row['eTrigue Value'] == $cycloneValue) {
                    $cycloneElement = $row['Element'];

                    foreach($formLabelsArray as $frow)
                    {
                        if($frow['Element'] == $cycloneElement)
                            $cycloneElements[] = $frow['en_US'];
                    }


                }
            }
        }

        $returnLabels = implode($delimiter, $cycloneElements);

        return $returnLabels;
    }

    public function setStatus($status)
    {
        $theStatus = (int) $status;
        $statusArray = $this->getStatusArray();
        $statusLabel = $statusArray[$theStatus];

        $sql = "UPDATE form_submissions SET status = :status, status_label = :statusLabel WHERE id = :id";
        $params = array(
            ':status' => $theStatus,
            ':statusLabel' => $statusLabel,
            ':id' => (int) $this->id
        );

        try {
            $this->Execute($sql, $params);

            $this->status = $theStatus;
            $this->status_label = $statusLabel;
        }
        catch(Exception $e)
        {
            $ret = $e->getMessage();
        }

    }

    public function detailedView($params = array())
    {
        if(isset($params['form_key']))
        {
            $this->auth_key = $params['form_key'];

            $this->loadFormFromAuthKey();

            if(!empty($this->form_id))
            {
                $_GET = $this->getFormData();
                $_GET['form'] = $this->form_id;
                $_GET['owner'] = $this->x_udf_form_owner148;

                $nexus = new Nexus();

                $nexus->loadProfiles($this);
                $nexusVars = $nexus->createVariablesFromProfile();

                foreach($nexusVars as $key => $val)
                {
                    $nexusVars[$key] = $nexus->insertStrings($val);
                }
                $placeholdersTemp = array_merge($nexusVars, $_GET);

                $owner = new Owner();
                $ownerData = $owner->getOwnerByShortcode($placeholdersTemp['owner']);

                $placeholders = array_merge($placeholdersTemp, $ownerData);

                // additional placeholders we can pass to the templates
                $placeholders['product_label'] = isset($_GET['x_udf_form_product149']) ? $nexus->getProductLabelFromProductShortcode($_GET['x_udf_form_product149']) : '';
                $placeholders['preset'] = $nexus->getPresetFromSegmentAndDivision(
                    $this->getSegmentFromSegmentId($_GET['x_udf_primary_market_segment122__ss']),
                    $this->getDivFromDivValue($_GET['x_udf_division146__ss']),
                    $this->_getOriginFromOriginId($_GET['x_udf_origin_code114__ss'])
                );

                // get html template
                $htmlTemplate = NEXUS_INTERNAL_ROOT . 'templates/'. $nexus->notification_email->code .'.php';
                if(file_exists($htmlTemplate))
                {
                    $msgTemplate = $nexus->load_template($htmlTemplate, $placeholders);
                    $body = $nexus->replaceAtSignWithHTML($msgTemplate['body']);

                    $placeholders['nexus_external_root'] = NEXUS_EXTERNAL_ROOT;

                    if((isset($nexus->crm->code) && $nexus->crm->code == true) || $ownerData['owner_shortcode'] == 'joseph.santos')
                    {
                        foreach($placeholders as $key => $val)
                        {
                            if($key !== 'debug' && $key !== 'debugto')
                                $placeholders[$key] = urlencode($val);
                        }

                        $placeholders['nexus_external_root'] = NEXUS_EXTERNAL_ROOT;

                        $buttonTemplate = NEXUS_INTERNAL_ROOT . 'templates/notify_STATUS.php';
                        $btnTemplate = $nexus->load_template($buttonTemplate, $placeholders);
                        $body .= $btnTemplate['body'];
                    }

                    echo $body;
                }

            }

        }
    }

    public function getSalesForceRedirectLink()
    {
        $placeholders = $this->getFormData();

        $posted_data = json_decode($placeholders['posted_data'], true);
        $placeholders['leadsource'] = isset($posted_data['leadsource']) && !empty($posted_data['leadsource']) ? $posted_data['leadsource'] : 'etrigue';
        $placeholders['campaign'] = isset($posted_data['campaign']) && !empty($posted_data['campaign']) ? $posted_data['campaign'] : '';
        $placeholders['referrer'] = isset($posted_data['referrer']) && !empty($posted_data['referrer']) ? $posted_data['referrer'] : '';

        // add form label and url
        $placeholders['memo'] = 'Form: '. $placeholders['form_label'] .'%0A%0D'. 'URL: '. $placeholders['referrer'];

        // July 25, 2016 - change in SalesForce country field to ISO-2 country code
        $placeholders['country2'] = $this->getISO2Countrycode('map-LGS', $placeholders['x_country']);

        $urlString = 'https://hgs.my.salesforce.com/00Q/e?name_firstlea2=$x_firstname$&name_lastlea2=$x_lastname$&lea3=$x_companyname$&lea4=$x_title$&lea8=$x_phone$&lea11=$x_emailaddress$&lea16street=$x_address$&lea16city=$x_city$&lea16state=$x_state$&lea16zip=$x_zip$&lea16country=$country2$&lea12=$x_url$&00ND0000004D6IN=$x_udf_primary_market_segment122__ss_label$&00ND00000060oZI=$auth_key$&CF00ND0000004zZoD=$x_udf_form_product149$&00ND0000005asJQ=$owner$&00ND00000060XfO=$x_udf_ext_dealer375$&00ND000000536jk=$memo$&00ND000000536jb=$leadsource$&lea20=$campaign$&00ND00000060XfJ=$x_udf_comments126$';

        $salesforceUrl = $this->vksprintf($urlString, $placeholders);
        $salesforceUrl = str_replace("%250A%250D", "%0A%0D", $salesforceUrl);

        return $salesforceUrl;
    }

    private function vksprintf($str, $args)
    {
        if (is_object($args)) {
            $args = get_object_vars($args);
        }

        $map = array_flip(array_keys($args));
        $new_str = preg_replace_callback('/(^|[^\$])\$([a-zA-Z0-9_-]+)\$/',
            function($m) use ($map) {return isset($map[$m[2]]) ? $m[1].'%'.($map[$m[2]] + 1).'$s' : '='; },
            $str);

        foreach($args as $key => $val)
        {
            $args[$key] = urlencode($val);
        }

        return vsprintf($new_str, $args);
    }

}
