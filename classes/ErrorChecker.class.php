<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Joseph
 * Date: 5/15/13
 * Time: 7:51 AM
 * To change this template use File | Settings | File Templates.
 */

class ErrorChecker extends NexusBase{

    private $isError, $error_msg;

    public function __construct()
    {
        $this->isError = false;
        $this->error_msg = '';
    }

    public function checkBrowser()
    {
        if(isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/(?i)msie [2-6]/',$_SERVER['HTTP_USER_AGENT']))
            $this->isError = true;
        $this->error_msg = "<h3 class='form-error' style='color:red;'>We've detected that you're using an older browser, which may cause this page to not display properly. Please update your browser to the latest version.</h3>";
        return $this; // allow method chaining
    }

    public function checkQuerystring($form, $form_profile)
    {
        $this->isError = (empty($form) || empty($form_profile)) ? true : false;
        $this->error_msg = "<h3 class='form-error' style='color:red;'>Error loading form. Invalid form profile passed.</h3>";
        return $this; // allow method chaining
    }

    public function checkCountry()
    {
        $country = isset($_REQUEST['x_country']) ? $_REQUEST['x_country'] : '';
        $this->isError = empty($country) ? true : false;
        $this->error_msg = "<h3 class='form-error' style='color:red;'>Error Code: 300</h3>";
        return $this; // allow method chaining
    }

    public function isError()
    {
        return $this->isError;
    }

    public function get_error_message()
    {
        return $this->error_msg;
    }

    public function set_error_message($e)
    {
        $this->error_msg = $e;
        return $this;
    }

}