<?php
/**
 * Created by PhpStorm.
 * Author: Joseph Santos
 * Date: 12/9/14
 * Time: 8:10 PM
 * Description: Base Nexus class. All other nexus classes should extend this class.
 */

class NexusBase extends DatabaseClass{

    /**
     *  This will hold our instance of File_CSV_DataSource that the Nexus uses
     */
    protected $csv;

    /*
     * $data holds a key-value paring to our csv data.
     * Add/modify the data in the constructor below
     */
    protected $files;

    /**
     * Public constructor for the class. Simply creates the database connection
     */
    public function __construct()
    {
        parent::__construct();

        $this->Db = Db::Get(Array('MySQL'));

        // we'll define our internal root and external root if it hasn't already been defined
        defined('NEXUS_INTERNAL_ROOT') or define('NEXUS_INTERNAL_ROOT', realpath(dirname(dirname(__FILE__))) .'/');
        defined('NEXUS_DATA_DIR') or define('NEXUS_DATA_DIR', realpath(dirname(dirname(__FILE__))) .'/data/');

        $externalRoot = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] == 'assets.microsurvey.com' ? $_SERVER['SERVER_NAME'] . '/apps/nexus' : $_SERVER['SERVER_NAME'] : 'nexus.microsurvey.com';
        defined('NEXUS_EXTERNAL_ROOT') or define("NEXUS_EXTERNAL_ROOT", $externalRoot ."/");

        // define our csv data and their locations here so when we want to load it,
        // all we have to do is call loadCSV - ie. $this->loadCSV('Ban-List');
        $this->files = array(
            'Elements'          => NEXUS_DATA_DIR . 'Elements.csv',
            'Filter-Presets'    => NEXUS_DATA_DIR . 'Filter-Presets.csv',
            'Form-ID'           => NEXUS_DATA_DIR . 'Form-ID.csv',
            'Form-Profiles'     => NEXUS_DATA_DIR . 'Form-Profiles.csv',
            'Form-Labels'       => NEXUS_DATA_DIR . 'Form-Labels.csv',
            'Seg-Div'           => NEXUS_DATA_DIR . 'Seg-Div.csv',
            'Seg-App'           => NEXUS_DATA_DIR . 'Seg-App.csv',
            'Select-Group'      => NEXUS_DATA_DIR . 'Select-Group.csv',
            'Product-Group'     => NEXUS_DATA_DIR . 'Product-Group.csv',
            'Email-Labels'      => NEXUS_DATA_DIR . 'Email-Labels.csv',
            'Owners'            => NEXUS_DATA_DIR . 'Owners.csv',
            'map-LGS'           => NEXUS_DATA_DIR . 'map-LGS.csv',
            'map-APE'           => NEXUS_DATA_DIR . 'map-APE.csv',
            'map-MIC'           => NEXUS_DATA_DIR . 'map-MIC.csv',
            'map-GMP'           => NEXUS_DATA_DIR . 'map-GMP.csv',
            'map-CAP'           => NEXUS_DATA_DIR . 'map-CAP.csv',
            'map-LGS-pipeline'  => NEXUS_DATA_DIR . 'map-LGS-pipeline.csv',
            'Languages'         => NEXUS_DATA_DIR . 'Languages.csv',
            'Sub-Regions'       => NEXUS_DATA_DIR . 'Sub-Regions.csv',
            'Origin-Redir'      => NEXUS_DATA_DIR . 'Origin-Redir.csv',
            'Sol-Map'           => NEXUS_DATA_DIR . 'Sol-Map.csv',
            'Ban-List'          => NEXUS_DATA_DIR . 'Ban-List.csv'
        );

        // we'll store our instance of File_CSV_DataSource which is used a lot to load csv files
        $this->csv = new File_CSV_DataSource();
    }

    /**
     * Loads file into our File_CSV_DataSource object.
     * Checks if the passed filename exists in our $data array.
     * Throws an exception if it doesn't exist in our $data array (not allowed)
     *
     * @param $fileName
     * @throws Exception
     */
    public function loadCSV($fileName, $columns = array())
    {
        if(array_key_exists($fileName, $this->files))
        {
            $this->csv->load($this->files[$fileName]);
            return $this->csv->connect($columns);
        }
        else
        {
            $this->csv->load($fileName);
            return $this->csv->connect($columns);
        }
    }

    /**
     *  Gets the headers from the most recently loaded csv file
     *
     *  @return array
     */
    public function getHeaders()
    {
        return $this->csv->getHeaders();
    }

    /**
     * Loads template file
     *
     * @param $templateFile
     * @param array $placeholders
     *
     * @return array containing 'sender', 'subject' and 'body'
     */
    public function load_template($templateFile, $placeholders = array(), $lineEndingTag = "<br />")
    {
        if(file_exists($templateFile))
        {
            $messageBeforeReplace = file_get_contents($templateFile);

            // get the sender
            preg_match('/\[sender\](.*?)\[\\/sender\]/', $messageBeforeReplace, $sender);
            $data['sender'] = isset($sender[1]) ? $this->insertStrings($sender[1], $placeholders) : '';

            // get the subject
            preg_match('/\[subject\](.*?)\[\\/subject\]/', $messageBeforeReplace, $subject);
            $data['subject'] = isset($subject[1]) ? $this->insertStrings($subject[1], $placeholders) : '';

            // get the body
            preg_match('/\[body\]\s(.*?)\s\[\\/body\]/is', $messageBeforeReplace, $body);
            $body = isset($body[1]) ? explode("<br />", $body[1]) : '';

            foreach($body as $key => $line)
            {
                if(empty($line))
                    $body[$key] = " ";
                else
                    $body[$key] = $this->insertStrings(trim($line), $placeholders, true);
            }
            $data['body'] = implode($lineEndingTag, array_filter($body));

        }
        else
        {
            $data['sender'] = '';
            $data['subject'] = '';
            $data['body'] = '';
        }

        return $data;
    }

    /**
     * Loads template file
     *
     * @param $templateFile
     * @param array $placeholders
     *
     * @return array containing 'sender', 'subject' and 'body'
     */
    public function load_template_legacy($templateFile, $placeholders = array())
    {
        if(file_exists($templateFile))
        {
            $messageBeforeReplace = file_get_contents($templateFile);

            $messageAfterReplace = $this->insertStrings($messageBeforeReplace, $placeholders);

            // get the sender
            preg_match('/\[sender\](.*?)\[\\/sender\]/', $messageAfterReplace, $sender);
            $data['sender'] = isset($sender[1]) ? $sender[1] : '';

            // get the subject
            preg_match('/\[subject\](.*?)\[\\/subject\]/', $messageAfterReplace, $subject);
            $data['subject'] = isset($subject[1]) ? $subject[1] : '';

            // get the body
            preg_match('/\[body\]\s(.*?)\s\[\\/body\]/is', $messageAfterReplace, $body);
            $data['body'] = isset($body[1]) ? $body[1] : '';
        }
        else
        {
            $data['sender'] = '';
            $data['subject'] = '';
            $data['body'] = '';
        }

        return $data;
    }

    // This is just a function that will find instances of "[[variables]]"
    // and replace them with the value assigned to that named variable.
    // If named variable does not exist, replaces it with an empty space
    //
    // returns modified string
    public function insertStrings($full_string, $placeholders = array(), $clearIfNoReplacement = false, $delimiterStart = "[[", $delimiterEnd = "]]") {
        $count = substr_count($full_string, $delimiterStart);

        $hasPlaceholder = $count > 0 ? true : false;
        $hasReplacement = false;

        while ($count>0){
            $start_pos = strpos($full_string, $delimiterStart);
            $end_pos = strpos($full_string, $delimiterEnd);
            $var_name = substr($full_string,($start_pos+2),($end_pos-$start_pos-2));
            $replacement = isset($placeholders[$var_name]) && !is_bool($placeholders[$var_name]) ? $placeholders[$var_name] : ( isset($this->globals[$var_name]) && !is_bool($this->globals[$var_name]) ? $this->globals[$var_name] : "" );

            if(!empty($replacement))
            {
                $hasReplacement = true;
            }
            $full_string = substr_replace($full_string, $replacement, $start_pos, $end_pos - $start_pos + 2);
            $count--;
        }
        if($clearIfNoReplacement && $hasPlaceholder && !$hasReplacement)
        {
            $full_string = '';
        }
        return $full_string;
    }

    public function getPresetFromSegmentAndDivision($segment, $division, $origin)
    {
        $presets = $this->loadCSV('Filter-Presets');
        $ret = '';

        foreach($presets as $pre)
        {
            if(substr($segment, 0, 2) == substr($pre['Filter Segment'], 0, 2) && $division == $pre['Filter Division'] && $origin == $pre['Filter Origin'])
            {
                $ret = $pre['Preset'];
                break(1);
            }
        }

        return $ret;
    }

    public function getSegmentFromSegmentId($segmentId)
    {
        $ElementsArray = $this->loadCSV("Elements");

        $segment = '';
        foreach($ElementsArray as $row)
        {
            if($row['Type'] == 'seg' && $segmentId == $row['eTrigue Value'])
            {
                $segment = $row['Element'];
                break(1);
            }
        }

        return $segment;
    }

    public function getSegmentValueFromSegment($segment)
    {
        $ElementsArray = $this->loadCSV("Elements");

        $segmentVal = '';
        foreach($ElementsArray as $row)
        {
            if($row['Type'] == 'seg' && $segment == $row['Element'])
            {
                $segmentVal = $row['eTrigue Value'];
                break(1);
            }
        }

        return $segmentVal;
    }

    public function getOriginValueFromOrigin($origin)
    {
        $ElementsArray = $this->loadCSV("Elements");

        $originVal = '';
        foreach($ElementsArray as $row)
        {
            if($row['Type'] == 'orig' && $origin == $row['Element'])
            {
                $originVal = $row['eTrigue Value'];
                break(1);
            }
        }

        return $originVal;
    }

    public function getDivValueFromDiv($div)
    {
        $ElementsArray = $this->loadCSV("Elements");

        $divVal = '';
        foreach($ElementsArray as $row)
        {
            if($row['Type'] == 'div' && $div == $row['Element'])
            {
                $divVal = $row['eTrigue Value'];
                break(1);
            }
        }

        return $divVal;
    }

    public function getDivFromDivValue($divValue)
    {
        $ElementsArray = $this->loadCSV("Elements");

        $division = '';
        foreach($ElementsArray as $row)
        {
            if($row['Type'] == 'div' && $divValue == $row['eTrigue Value'])
            {
                $division = $row['Element'];
                break(1);
            }
        }

        return $division;
    }

    public function getProductLabelFromProductShortcode($productIds)
    {
        $ElementsArray = $this->loadCSV("Elements");
        $LabelsArray = $this->loadCSV("Form-Labels");

        $productElement = '';
        $productLabel = array();
        $products = explode(",", $productIds);

        foreach($products as $productId)
        {
            foreach($ElementsArray as $row)
            {
                if($row['Type'] == 'prod' && $productId == $row['Short-Code'])
                {
                    $productElement = $row['Element'];
                    break(1);
                }
            }

            foreach($LabelsArray as $row)
            {
                if($productElement == $row['Element'])
                {
                    $productLabel[] = $row['en_US'];
                    break(1);
                }
            }
        }
        return implode(", ", $productLabel);

    }

    public function getProductValueFromProductShortcode($shortcode)
    {
        $productValue = '';
        $ElementsArray = $this->loadCSV("Elements");

        foreach($ElementsArray as $row)
        {
            if($row['Type'] == 'prod' && $shortcode == $row['Short-Code'])
            {
                $productValue = $row['eTrigue Value'];
                break(1);
            }
        }
        return $productValue;
    }

    public function getProductElementFromProductShortcode($shortcode)
    {
        $productElement = '';
        $ElementsArray = $this->loadCSV("Elements");

        foreach($ElementsArray as $row)
        {
            if($row['Type'] == 'prod' && $shortcode == $row['Short-Code'])
            {
                $productElement = $row['Element'];
                break(1);
            }
        }
        return $productElement;
    }

    protected function _getOwnerLabelFromOwnerShortcode($shortcode)
    {
        $ownersArray = $this->loadCSV('Owners');

        $owner = '';
        foreach($ownersArray as $own)
        {
            if($shortcode == $own['Short-Code'])
            {
                $owner = $own['Label'];
                break(1);
            }
        }
        return $owner;
    }

    protected function _getOwnerValueFromOwnerShortcode($shortcode)
    {
        $ownerValue = '';
        $ElementsArray = $this->loadCSV("Elements");

        foreach($ElementsArray as $row)
        {
            if($row['Type'] == 'owner' && $shortcode == $row['Short-Code'])
            {
                $ownerValue = $row['eTrigue Value'];
                break(1);
            }
        }
        return $ownerValue;
    }

    public function getChannelTactics()
    {
        $form_profiles = $this->loadCSV('Elements');
        $ctArray = array();

        foreach($form_profiles as $row)
        {
            if($row['Type'] == 'ct')
            {
                $ctArray[$row['Element']] = $row['eTrigue Value'];
            }
        }

        return $ctArray;
    }

    public function getChannelTacticValueFromLabel($label)
    {
        $ElementsArray = $this->loadCSV("Elements");
        $LabelsArray = $this->loadCSV("Form-Labels");

        $channelElement = '';
        $channelValue = '';

        foreach($LabelsArray as $row)
        {
            if($label == $row['en_US'])
            {
                $channelElement = $row['Element'];
                break(1);
            }
        }

        foreach($ElementsArray as $row)
        {
            if($channelElement == $row['Element'])
            {
                $channelValue = $row['eTrigue Value'];
                break(1);
            }
        }

        return $channelValue;
    }

    public function getChannelTacticLabelFromValue($value)
    {
        $ElementsArray = $this->loadCSV("Elements");
        $LabelsArray = $this->loadCSV("Form-Labels");

        $channelElement = '';
        $channelLabel = '';

        foreach($ElementsArray as $row)
        {
            if($value == $row['eTrigue Value'])
            {
                $channelElement = $row['Element'];
                break(1);
            }
        }

        foreach($LabelsArray as $row)
        {
            if($channelElement == $row['Element'])
            {
                $channelLabel = $row['en_US'];
                break(1);
            }
        }

        return $channelLabel;
    }

    protected function _getLanguageLabelFromLanguageValue($value)
    {
        $languages = $this->loadCSV('Languages');

        $language = '';
        foreach($languages as $lang)
        {
            if($lang['Element'] == $value)
            {
                $language = $lang['English Label'];
                break(1);
            }
        }

        return $language;
    }

    public function getChannelTacticValueFromElement($element)
    {
        $ElementsArray = $this->loadCSV("Elements");
        $channelValue = '';

        foreach($ElementsArray as $row)
        {
            if($element == $row['Element'])
            {
                $channelValue = $row['eTrigue Value'];
                break(1);
            }
        }

        return $channelValue;
    }

    public function getSegmentFromSolution($sol)
    {
        $solMapArray = $this->loadCSV("Sol-Map");
        $segment = '';

        foreach($solMapArray as $row)
        {
            if($row['Element'] === $sol)
            {
                $segment = $row['Mapped Segment'];
            }
        }

        return $segment;
    }

    public function getDivisionFromSolution($sol)
    {
        $solMapArray = $this->loadCSV("Sol-Map");
        $division = '';

        foreach($solMapArray as $row)
        {
            if($row['Element'] === $sol)
            {
                $division = $row['Mapped Division'];
            }
        }

        return $division;
    }

    protected function _getFormLabelFromFormId($formId)
    {
        $formProfilesArray = $this->loadCSV("Form-ID");

        $formLabel = '';
        foreach($formProfilesArray as $row)
        {
            if($formId == $row['Form ID'])
            {
                $formLabel = $row['Label'];
                break(1);
            }
        }

        return $formLabel;
    }

    protected function _getChannelTacticFromValue($value)
    {
        $ElementsArray = $this->loadCSV("Elements");
        $FormLabelsArray = $this->loadCSV("Form-Labels");

        $channel = '';
        foreach($ElementsArray as $row)
        {
            if($row['Type'] == 'ct' && $value == $row['eTrigue Value'])
            {
                foreach($FormLabelsArray as $label)
                {
                    if($row['Element'] == $label['Element'])
                    {
                        $channel = $label['en_US'];
                        break(1);
                    }
                }
                break(1);
            }
        }

        return $channel;
    }

    protected function _getOriginFromOriginId($originId)
    {
        $ElementsArray = $this->loadCSV("Elements");

        $origin = '';
        foreach($ElementsArray as $row)
        {
            if($row['Type'] == 'orig' && $originId == $row['eTrigue Value'])
            {
                $origin = $row['Element'];
                break(1);
            }
        }

        return $origin;
    }

    protected function _getApplicationLabelFromApplicationValue($value)
    {
        $ElementsArray = $this->loadCSV("Elements");

        $app = '';
        foreach($ElementsArray as $row)
        {
            if($row['Type'] == 'app' && $value == $row['eTrigue Value'])
            {
                $app = $row['Element'];
                break(1);
            }
        }

        return $app;
    }

    protected function _getProductLabelFromProductShortcode($shortcode)
    {
        $ElementsArray = $this->loadCSV("Elements");
        $formLabelsArray = $this->loadCSV('Form-Labels');

        $prod = '';
        foreach($ElementsArray as $row)
        {
            if($row['Type'] == 'prod' && $shortcode == $row['Short-Code'])
            {
                foreach($formLabelsArray as $frow)
                {
                    if($row['Element'] == $frow['Element'])
                    {
                        $prod = $frow['en_US'];
                        break(1);
                    }
                }
                break(1);
            }
        }
        return $prod;
    }

    protected function _getLabelFromElement($element)
    {
        $labelsArray = $this->loadCSV("Form-Labels");

        $label = '';
        foreach($labelsArray as $row)
        {
            if($row['Element'] === $element)
                $label = $row['en_US'];
        }

        return $label;
    }

}