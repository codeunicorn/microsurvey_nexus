<?php
/**
 * Created by JetBrains PhpStorm.
 * Author: Joseph Santos
 * Date: 6/4/14
 * Time: 6:13 PM
 * Description: 
 */

class User extends NexusBase {

    const DATABASE_TABLE = '_users';
    const ALLOWED_ATTEMPTS = 5;

    private $id, $username, $password, $role, $doesExist, $isAuthenticated, $last_logged_in, $last_login_attempt, $attempts_since_last_login;

    public function __construct($userInfo = array())
    {
        parent::__construct();

        $this->id = isset($userInfo['id']) ? $userInfo['id'] : null;
        $this->username = isset($userInfo['username']) ? $userInfo['username'] : null;
        $this->password = isset($userInfo['password']) ? $userInfo['password'] : null;
        $this->role = isset($userInfo['role']) ? $userInfo['role'] : null;
        $this->doesExist = false;
        $this->isAuthenticated = false;
    }

    public function fetchUser($username)
    {
        $sql = "SELECT * FROM ". self::DATABASE_TABLE ." WHERE `username` = :username";
        $params = array(':username' => $username);

        try {
            $results = $this->ExecAndFetch($sql, $params);
        }
        catch(Exception $e)
        {
            $results = $e->getMessage();
            error_log($results);
        }

        if(is_array($results))
        {
            $this->username = isset($results['username']) ? $results['username'] : null;
            $this->password = isset($results['password']) ? $results['password'] : null;
            $this->role = isset($results['role']) ? $results['role'] : null;
            $this->doesExist = isset($results['username']) ? true : false;
            $this->isAuthenticated = false;
            $this->last_logged_in = isset($results['last_logged_in']) ? $results['last_logged_in'] : null;
            $this->last_login_attempt = isset($results['last_login_attempt']) ? $results['last_login_attempt'] : null;
            $this->attempts_since_last_login = isset($results['attempts_since_last_login']) ? $results['attempts_since_last_login'] : 0;
        }
    }

    public function authenticate_password($password)
    {
        // validate password
        $this->isAuthenticated = validate_password($password, $this->password);

        // update the timestamps
        $this->_updateTimestamps();

        return $this->isAuthenticated();
    }

    private function _updateTimestamps()
    {
        // first let's get the difference between the current login attempt and last login attempt
        $hours = null;
        $now = new DateTime();
        $current_login_attempt = $now->format('Y-m-d H:s:i');

        if(!is_null($this->last_login_attempt))
        {
            $last_login_attempt = new DateTime($this->last_login_attempt);
            $diff = $last_login_attempt->diff($now);
            $hours = $diff->h + ($diff->days * 24);
        }

        // if it's been more than 24 hours since the last login attempt, let's reset the attempts_since_last_login variable
        if($hours > 24)
            $this->attempts_since_last_login = 1; // 1, since this attempt counts as one
        else
            $this->attempts_since_last_login += 1;

        // if the number of attempts since the last login is more than 5, we don't authenticate even if the password is correct
        if($this->attempts_since_last_login > 5)
        {
            $this->isAuthenticated = false;
        }

        // if we're authenticated and attempts since last login is less than 5, reset
        if($this->isAuthenticated && $this->attempts_since_last_login <= 5)
        {
            $this->last_logged_in = $current_login_attempt;
            $this->attempts_since_last_login = 0;
        }

        // we always update the last login attempt
        $this->last_login_attempt = $current_login_attempt;

        // update database
        $sql = "UPDATE _users SET
                    last_logged_in = :last_logged_in,
                    last_login_attempt = :last_login_attempt,
                    attempts_since_last_login = :attempts_since_last_login
                WHERE username = :username";
        $params = array(
            ':last_logged_in' => $this->last_logged_in,
            ':last_login_attempt' => $this->last_login_attempt,
            ':attempts_since_last_login' => $this->attempts_since_last_login,
            ':username' => $this->username
        );
        try {
            $results = $this->ExecAndFetchAll($sql, $params);
        }
        catch(Exception $e)
        {
            $results = $e->getMessage();
        }
        return $results;
    }

    public function isAuthenticated()
    {
        return $this->isAuthenticated;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function isBanned()
    {
        return $this->attempts_since_last_login >= self::ALLOWED_ATTEMPTS;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function getNumberOfAttempts()
    {
        return $this->attempts_since_last_login;
    }

    public function getAllUsers($type = '')
    {
        $sql = "SELECT * FROM ". self::DATABASE_TABLE ." WHERE role LIKE :role";
        $params = array(
            ':role' => !empty($type) ? $type : '%'
        );
        try {
            $results = $this->ExecAndFetchAll($sql, $params);
        }
        catch(Exception $e)
        {
            $results = $e->getMessage();
        }

        return $results;

    }
}