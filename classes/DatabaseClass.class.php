<?php
/**
 * Database Object management file
 * 
 * All classes should extend this class
 * 
 */

abstract class DatabaseClass
{
	const __NEW = 0;
	const __OPERATION_ADD = 1;
	const __OPERATION_EDIT = 2;
	const __DB_NULL = -999999;
	
	protected $Db;
	protected $LoggerObj;

	private $InternalStatement;
	
	/**
	 * Public constructor for the class. Simply creates the database connection
	 */
	public function __construct()
	{
		$this->Db = Db::Get(Array('MySQL'));

        // we'll define our internal root and external root if it hasn't already been defined
        defined('NEXUS_INTERNAL_ROOT') or define('NEXUS_INTERNAL_ROOT', realpath(dirname(dirname(__FILE__))) .'/');
        defined('NEXUS_DATA_DIR') or define('NEXUS_DATA_DIR', realpath(dirname(dirname(__FILE__))) .'/data/');

        // define our csv data and their locations here so when we want to load it,
        // all we have to do is call loadCSV - ie. $this->loadCSV('Ban-List');
        $this->files = array(
            'Elements'          => NEXUS_DATA_DIR . 'Elements.csv',
            'Filter-Presets'    => NEXUS_DATA_DIR . 'Filter-Presets.csv',
            'Form-ID'           => NEXUS_DATA_DIR . 'Form-ID.csv',
            'Form-Profiles'     => NEXUS_DATA_DIR . 'Form-Profiles.csv',
            'Form-Labels'       => NEXUS_DATA_DIR . 'Form-Labels.csv',
            'Elements'          => NEXUS_DATA_DIR . 'Elements.csv',
            'Seg-Div'           => NEXUS_DATA_DIR . 'Seg-Div.csv',
            'Seg-App'           => NEXUS_DATA_DIR . 'Seg-App.csv',
            'Select-Group'      => NEXUS_DATA_DIR . 'Select-Group.csv',
            'Product-Group'     => NEXUS_DATA_DIR . 'Product-Group.csv',
            'Email-Labels'      => NEXUS_DATA_DIR . 'Email-Labels.csv',
            'Owners'            => NEXUS_DATA_DIR . 'Owners.csv',
            'map-LGS'           => NEXUS_DATA_DIR . 'map-LGS.csv',
            'map-APE'           => NEXUS_DATA_DIR . 'map-APE.csv',
            'map-MIC'           => NEXUS_DATA_DIR . 'map-MIC.csv',
            'map-GMP'           => NEXUS_DATA_DIR . 'map-GMP.csv',
            'Languages'         => NEXUS_DATA_DIR . 'Languages.csv',
            'Sub-Regions'       => NEXUS_DATA_DIR . 'Sub-Regions.csv',
            'Origin-Redir'      => NEXUS_DATA_DIR . 'Origin-Redir.csv',
            'Sol-Map'           => NEXUS_DATA_DIR . 'Sol-Map.csv',
            'Ban-List'          => NEXUS_DATA_DIR . 'Ban-List.csv',
            'ctfix'             => NEXUS_DATA_DIR . 'CTFIX.csv',
        );

        // we'll store our instance of File_CSV_DataSource which is used a lot to load csv files
        $this->csv = new File_CSV_DataSource();
	}

    /**
     * Loads file into our File_CSV_DataSource object.
     * Checks if the passed filename exists in our $data array.
     * Throws an exception if it doesn't exist in our $data array (not allowed)
     *
     * @param $fileName
     * @throws Exception
     */
    public function loadCSV($fileName, $columns = array())
    {
        if(array_key_exists($fileName, $this->files))
        {
            $this->csv->load($this->files[$fileName]);
            return $this->csv->connect($columns);
        }
        else
        {
            error_log("loadCSV: file '{$fileName}' could not be found");
            throw New Exception("File, '{$fileName}.csv', could not be found");
        }
    }
	
	public final function SelectFoundRows()
	{
		return $this->Db->Db->query('SELECT FOUND_ROWS()')->fetchColumn();
	}
	public final function SelectIdentity()
	{
		return $this->Db->Db->query('SELECT LAST_INSERT_ID()')->fetchColumn();
	}
	public final function SelectUtcTimestamp()
	{
		return $this->Db->Db->query('SELECT UTC_TIMESTAMP()')->fetchColumn();
	}
	public final function ExecAndFetchAll($Sql, $Params, $FetchStyle = PDO::FETCH_ASSOC)
	{
		$Statement = $this->Db->Db->prepare($Sql);
		$Statement->execute($Params);
		$Result = $Statement->fetchAll($FetchStyle);
		$Statement->closeCursor();
		unset($Statement);
		return $Result;
	}
	public final function ExecAndFetch($Sql, $Params, $FetchStyle = PDO::FETCH_ASSOC)
	{
		$Statement = $this->Db->Db->prepare($Sql);
		$Statement->execute($Params);
		$Result = $Statement->fetch($FetchStyle);
		$Statement->closeCursor();
		unset($Statement);
		return $Result;
	}
	public final function ExecNonQuery($Sql, $Params)
	{
		$Statement = $this->Db->Db->prepare($Sql);
		$Statement->execute($Params);
		$RowCount = $Statement->rowCount();
		$Statement->closeCursor();
		unset($Statement);
		return $RowCount;
	}
	public final function Execute($Sql, $Params)
	{
		$this->InternalStatement = $this->Db->Db->prepare($Sql);
		$this->InternalStatement->execute($Params);
	}
	public final function Fetch($FetchStyle = PDO::FETCH_ASSOC)
	{
		if (isset($this->InternalStatement))
			return $this->InternalStatement->fetch($FetchStyle);
		return false;
	}
	public final function ExecuteEnd()
	{
		if (isset($this->InternalStatement))
		{
			$this->InternalStatement->closeCursor();
			unset($this->InternalStatement);
		}
	}
	public function BeginTrans()
	{
		$this->Db->Db->beginTransaction();
	}
	public function RollbackTrans()
	{
		$this->Db->Db->rollBack();
	}
	public function CommitTrans()
	{
		$this->Db->Db->commit();
	}
	
	protected function ProcessWhereClause($FilterMappings, $FilterSqlOperators, $Filters, &$WhereClause, &$Params, $GlobalLikeClause = false)
	{
		if ($Filters && is_array($Filters))
		{
			foreach($Filters as $FilterName => $FilterValue)
			{
				if (($FilterValue !== false) && (isset($FilterMappings[$FilterName])))
				{
					if (strlen($WhereClause) > 0)
						$WhereClause .= ' AND ';
					
					$ParamName = sprintf(':%s', strtolower($FilterName));
					$VPartsCount = count($FilterValue);
					if ($VPartsCount == 1)
					{
						if (is_array($FilterValue))
							$FilterValue = $FilterValue[0];

						if ($FilterValue != self::__DB_NULL)
						{
							/*This is a single value filter	*/
							$SqlOp = isset($FilterSqlOperators[$FilterName]) ? $FilterSqlOperators[$FilterName] : '=';
							if ($SqlOp == 'LIKE') 
							{
								if ($GlobalLikeClause)
									$FilterValue = '%' . $FilterValue . '%';
								else
									$FilterValue .= '%';
							}
							$WhereClause .= sprintf('%s %s %s', $FilterMappings[$FilterName], $SqlOp, $ParamName);
							$Params[$ParamName] = $FilterValue;
						}
						else
						{
							$WhereClause .= sprintf('(%s IS NULL OR %s = null)', $FilterMappings[$FilterName], $FilterMappings[$FilterName]);
						}
					}
					else
					{
						/*Multi valued filters translate into OR joined sql conditions*/
						$MultiCondition = '';
						for($i = 0; $i < $VPartsCount; $i++)
						{
							if ($FilterValue[$i] != self::__DB_NULL)
							{
								$ParamName = sprintf(':%s%d', strtolower($FilterName), $i);
								if (strlen($MultiCondition) > 0)
									$MultiCondition .= ' OR ';
								
								$MultiCondition .= sprintf('(%s %s %s)', $FilterMappings[$FilterName], (isset($FilterSqlOperators[$FilterName]) ? $FilterSqlOperators[$FilterName] : '='), $ParamName);
								$Params[$ParamName] = (isset($FilterSqlOperators[$FilterName]) && ($FilterSqlOperators[$FilterName] == 'LIKE') ? $FilterValue[$i] . '%' : $FilterValue[$i]);
							}
							else
							{
								if (strlen($MultiCondition) > 0)
									$MultiCondition .= ' OR ';
								$MultiCondition .= sprintf('(%s IS NULL OR %s = null)', $FilterMappings[$FilterName], $FilterMappings[$FilterName]);
							}
						}
						$WhereClause .= '(' . $MultiCondition . ')';
					}
				}
			}
		}
	}
	function GetAppTimezone()
	{
		return 0;
	}

}
?>
