<?php
/**
 * Created by PhpStorm.
 * Author: Joseph Santos
 * Date: 7/7/14
 * Time: 9:11 AM
 * Description: 
 */

class HighCharts extends NexusBase {

    public function __contruct()
    {
        parent::__construct();
    }

    public function getStackedAreaDashboard()
    {
        $dateTime = new DateTime('-30 days');
        $sql = 'SELECT * FROM form_submissions WHERE date_submitted > :date_submitted AND origin_label in(:origin1, :origin2)';
        $params = array(
            ':date_submitted' => $dateTime->format('Y-m-d H:s:i'),
            ':origin1' => 'leicaus',
            ':origin2' => 'ape'
        );

        $return = array();

        try {
            $results = $this->ExecAndFetchAll($sql, $params);

            $return = array();

            $d = array();
            for($i = 30; $i > 0; $i--)
                $d[date("m/d", strtotime('-'. $i .' days'))] = 0;

            $count = array(
                'leicaus' => $d,
                'ape' => $d
            );

            $return['xAxis'] = array(
                'categories' => array_keys($d),
                'tickmarkPlacement' => 'on',
                'title' => array('text' => 'Date'),
                'tickInterval' => 2
            );


            $tz = new DateTimeZone('America/Los_Angeles');
            foreach($results as $key => $row)
            {
                $myDate = new DateTime($row['date_submitted'], $tz);

                if(!isset($count[$row['origin_label']][$myDate->format('m/d')]))
                    $count[$row['origin_label']][$myDate->format('m/d')] = 0;
                else
                    $count[$row['origin_label']][$myDate->format('m/d')] += 1;
            }

            foreach($count as $name => $countByDate)
            {
                $return['series'][] = array(
                    'name' => $name,
                    'data' => array_values($countByDate)
                );
            }
        }
        catch(Exception $e)
        {
            $return = $e->getMessage();
            error_log($return);
        }

        return $return;

    }

    public function getHeatMapDashboard()
    {
        $dateTime = new DateTime('-28 days');
        $sql = 'SELECT * FROM form_submissions WHERE date_submitted > :date_submitted AND origin_label in(:origin1, :origin2)ORDER BY date_submitted DESC';
        $params = array(
            ':date_submitted' => $dateTime->format('Y-m-d H:s:i'),
            ':origin1' => 'leicaus',
            ':origin2' => 'ape'
        );

        $return = array();
        try {
            $results = $this->ExecAndFetchAll($sql, $params);

            $myData = array();

            $tz = new DateTimeZone('America/Los_Angeles');
            $dateNow = new DateTime('now', $tz);

            foreach($results as $key => $row)
            {
                $myDate = new DateTime($row['date_submitted'], $tz);
                $monthDay = $myDate->format("m/d");
                $dayOfWeek = $myDate->format("l");

                if($key == 0 && $monthDay < $dateNow->format("m/d"))
                {
                    $count = 1;
                    $keepGoing = true;
                    while($keepGoing)
                    {
                        $dateNow->sub(new DateInterval('P1D'));

                        if($monthDay < $dateNow->format("m/d"))
                        {
                            $myData[$dateNow->format("m/d")][$dateNow->format("l")] = 0;
                        }
                        else{
                            $keepGoing = false;
                        }

                        $count++;
                    }

                }

                $myData[$monthDay][$dayOfWeek] = isset($myData[$monthDay][$dayOfWeek]) ? $myData[$monthDay][$dayOfWeek] + 1 : 1;

            }

            $xAxis = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
            $yAxis = array();
            $data = array();
            $leicaus = array();
            $yValue = 0;
            $previousXindex = null;
            $previousYindex = 0;

            foreach($myData as $myDate => $myArr)
            {
                foreach($myArr as $myDay => $myCount)
                {
                    $xvalue = array_search($myDay, $xAxis);

                    if($xvalue !== false)
                    {
                        // we're in a different week if the current day is greater than the previous day
                        if($xvalue > $previousXindex && $previousXindex !== null)
                        {
                            // if we skipped a day of the week
                            if($previousXindex !== 0)
                            {
                                for($i = $previousXindex - 1; $i >= 0; $i--)
                                {
                                    $data[] = array($i, $yValue, 0);
                                }
                            }

                            $previousYindex = $yValue;
                            $yValue += 1;
                        }

                        // if we skipped a day of the week but we're still in the same week
                        if($previousXindex !== null && $xvalue !== $previousXindex - 1 && $previousYindex == $yValue)
                        {
                            for($j = $previousXindex - 1; $j < $xvalue; $j++)
                            {
                                if($j >= 0)
                                    $data[] = array($j, $yValue, 0);
                            }
                        }

                        // finally, store the value for this date
                        $data[] = array($xvalue, $yValue, $myCount);

                        $previousXindex = $xvalue;
                    }
                }
            }

            //var_dump($myData);


            for($i = 0; $i < 35; $i++)
            {
                if(date("l", strtotime('-'. $i .' days')) == 'Sunday')
                {
                    $yAxis[] = date("m/d", strtotime('-'. $i .' days'));
                }
            }

            $return['yAxis'] = array(
                'categories' => $yAxis,
                'title' => array('text' => 'Week of')
            );

            $return['series'][] = array(

                'name' => 'Submission count',
                'borderWidth' => 1,
                'dataLabels' => array(
                    'enabled' => true,
                    'color' => 'black',
                    'style' => array(
                        'textShadow' => 'none',
                        'HcTextStroke' => null
                    ),
                ),
                'data' => $data
            );


        }
        catch(Exception $e)
        {
            $return = $e->getMessage();
            error_log($return);
        }

        return $return;
    }

    public function getDoughnutChartDashboard() {

        $dateTime = new DateTime('-90 days');
        $sql = 'SELECT * FROM form_submissions WHERE date_submitted > :date_submitted AND origin_label in(:origin1, :origin2)';
        $params = array(
            ':date_submitted' => $dateTime->format('Y-m-d H:s:i'),
            ':origin1' => 'leicaus',
            ':origin2' => 'ape'
        );

        $return = array();

        try {
            $results = $this->ExecAndFetchAll($sql, $params);





        }
        catch(Exception $e)
        {
            $return = $e->getMessage();
            error_log($return);
        }

        return $return;


    }
} 