<?php
/**
 * Created by JetBrains PhpStorm.
 * Author: Joseph Santos
 * Date: 9/27/13
 * Time: 3:21 AM
 * Description: This is our main Nexus class that will be the main interface for all of the Nexus variables
 *
 *
 * The important part to keep note of is the profile variables that are created when the function loadProfiles() is called.
 * These profile variables are stored in the $data object as $data->profile. To standardize the profile variables, they will
 * each have the following properties:
 *
 *      code -  this stores the actual values in the spreadsheet cells for the particular variable. For instance, in Form-Profiles.csv,
 *              the variable 'form_preamble1' will have either 'TRUE' or 'FALSE' for its code. Note that the code can be a string instead
 *              of 'TRUE' or 'FALSE'.
 *
 *      label - this is the label, the actual html text the user sees on the webpage. This value is grabbed from Form-Labels.csv
 *
 *      value - this is the form input value, if the variable is an input element (i.e. <input type='text' value='this is the value'>)
 *
 *      type -  the type specifies the type of variable this is, from Elements.csv. Note that this is not the same as the 'type'
 *              of input element as in the example above.
 *
 *      shortcode - the shortcode for the variable. Only used for certain variables.
 *
 */

class Nexus extends NexusBase{

    /**
     * The mexus data. This is the main object the app uses
     */
    protected $data;

    /*
     * This will simulate global variables that template files and form submissions use.
     * This is not accessible from outside this class.
     */
    protected $globals;

    // we need this to be backwards compatible with eTrigue and v2.2, so we're gonna map the new variables to the old variables
    protected $mappings;

    /**
     *  Constructor
     */
    public function __construct()
    {
        parent::__construct();

        // initialize our profiles to an empty object
        $this->data = new stdClass;

        $this->globals = array();

        $this->mappings = array(
            'x_owner'   =>  'espUsers'
        );
    }

    /**
     * Magic function - Returns the requested variable from $profile if it exists.
     *                  Returns null if it doesn't exist.
     * @param $var
     */
    public function __get($var)
    {
        $ret = null;

        if(isset($this->data->$var))
            $ret = $this->data->$var;
        else if(isset($this->data->profile->$var))
            $ret = $this->data->profile->$var;

        return $ret;
    }

    public function loadProfiles(Forms $form = null)
    {
        // First we load our Form-ID.csv to get our defaults
        $this->_getDefaultsFromFormID($form);

        // Check if overrides were passed in via GET - if so, overwrite values and set override flags for later
        $this->_getOverridesFromGET();

        // Check if overrides are passed from POST submissions
        $this->_getOverridesFromPOST($form);

        // let's load our language info
        $this->_loadLanguage();

        // load form profile
        $this->_loadFormProfiles($form);

        // let's load our profile values
        $this->_loadProfileValues();

        // let's load our profile labels
        $this->_loadProfileLabels();

        // load autofill values from additional GET parameters
        $this->_loadAutofillValues();

        // load our select dropdown data
        $this->_loadDropdowns();
    }

    public function loadSelect($name)
    {
        $data = array();

        if(array_key_exists('Form-Labels', $this->files))
        {
            $language = 'Label:'. $this->data->lang->value;
            $selectArray = $this->loadCSV('Form-Labels', array('Element Name', 'Type', $language));
            $selectArrayLength = count($selectArray);

            switch($name)
            {
                // only get the seg
                case 'x_udf_division146__ss' :
                case 'x_udf_main_application121__ss' :

                    // load the Seg-Div.csv file or Seg-App.csv file
                    $filename = $name == 'x_udf_main_application121__ss' ? 'Seg-App' : 'Seg-Div';
                    $segChildArray = $this->loadCSV($filename);

                    foreach($selectArray as $akey => $row)
                    {
                        // We only grab the correct $type entries
                        if($row['Type'] == 'Seg')
                        {
                            $data[$akey] = array(
                                //'code'      =>  $row['Code'],
                                'name'      =>  $row[$language],
                                //'value'     =>  $row['eTrigueID'],
                                //'selected'  =>  $this->data->seg->code == $row['Code'] ? true : false,
                                'id'        =>  'x_udf_primary_market_segment122__ss'
                            );

                            // we grab the mapping between this 'Seg' and its children in Seg-Div.csv or Seg-App.csv
                            $seg = $row['Element Name'];
                            foreach($segChildArray as $segChild)
                            {
                                if($segChild['Value'] == 'Code')
                                {
                                    $data[$akey]['code'] = $segChild[$seg];
                                    $data[$akey]['selected'] = $this->data->seg->code == $segChild[$seg] ? true : false;
                                }

                                if($segChild['Value'] == 'eTrigue ID')
                                {
                                    $data[$akey]['value'] = $segChild[$seg];
                                }

                                if($segChild['Value'] != 'Product Group' && $segChild['Value'] != 'Code' && $segChild['Value'] != 'eTrigue ID')
                                {

                                    if(!empty($segChild[$seg]))
                                    {
                                        // let's get its children
                                        // map our way to fill out the rest of the children info
                                        $found = false;
                                        $childCounter = $akey;

                                        while(!$found && $childCounter < $selectArrayLength)
                                        {
                                            if($segChild['Value'] == $selectArray[$childCounter]['Element Name'])
                                            {
                                                $data[$akey]['children'][] = array(
                                                    'name'      =>  $selectArray[$childCounter]['Element Name'],
                                                    //'code'      =>  $selectArray[$childCounter]['Code'],
                                                    'value'     =>  $selectArray[$childCounter]['eTrigueID'],
                                                    //'selected'  =>  $this->data->div->code == $selectArray[$childCounter]['Code'] ? true : false,
                                                    'id'        =>  $name
                                                );

                                                $found = true;
                                            }
                                            $childCounter++;
                                        }
                                    }
                                }
                            }

                            // if we only have 1 children, set it as selected
                            if(isset($data[$akey]['children']) && count($data[$akey]['children']) == 1)
                            {
                                $data[$akey]['children'][0]['selected'] = true;
                            }


                        }
                    }
                    break;
            }
        }

        return json_encode($data);
    }

    /**
     * create variables from nexus profile. Used for Wrapper files
     */
    public function createVariablesFromProfile()
    {
        // special case for back, language, querystring, form_profile
        $arr['back'] = $this->data->back->code;
        $arr['lang'] = $this->data->lang->code;
        $arr['querystring'] = $this->data->querystring->code;
        $arr['querystring_code'] = $this->data->querystring->code;
        $arr['form_profile'] = $this->data->prof->code;
        $arr['form_label'] = $this->data->prof->label;

        $arr['adwords_code'] = $this->data->adwords->code;
        $arr['adwords_id'] = $this->data->adwords->id;
        $arr['adwords_label'] = $this->data->adwords->label;

        foreach($this->data->form as $key => $value){$arr['form_'. $key] = $value;}
        foreach($this->data->seg as $key => $value){$arr['segment_'. $key] = $value;}
        foreach($this->data->div as $key => $value){$arr['division_'. $key] = $value;}
        foreach($this->data->app as $key => $value){$arr['app_'. $key] = $value;}
        foreach($this->data->prod as $key => $value){$arr['product_'. $key] = $value;}
        foreach($this->data->orig as $key => $value){$arr['origin_'. $key] = $value;}
        foreach($this->data->prof as $key => $value){$arr['profile_'. $key] = $value;}
        foreach($this->data->owner as $key => $value){$arr['owner_'. $key] = $value;}
        foreach($this->data->ct as $key => $value){$arr['ct_'. $key] = $value;}

        if(isset($this->data->form->code)){ $arr['form'] = $this->data->form->code; }
        if(isset($this->data->seg->code)){ $arr['segment'] = $this->data->seg->code; }
        if(isset($this->data->div->code)){ $arr['division'] = $this->data->div->code; }
        if(isset($this->data->app->code)){ $arr['app'] = $this->data->app->code; }
        if(isset($this->data->prod->shortcode)){ $arr['product'] = $this->data->prod->shortcode; }
        if(isset($this->data->orig->code)){ $arr['origin'] = $this->data->orig->code; }
        if(isset($this->data->owner->code)){ $arr['owner'] = $this->data->owner->code; }

        if(isset($this->data->profile) && !empty($this->data->profile))
        {
            foreach($this->data->profile as $profileName => $profile)
            {
                if(isset($profile->code) && $profile->code && isset($profile->value) && $profile->value !== 'TRUE' && $profile->value !== 'FALSE')
                    $arr[$profileName] = $profile->value;

                if(isset($profile->code) && $profile->code && isset($profile->label) && $profile->label !== 'TRUE' && $profile->label !== 'FALSE')
                    $arr[$profileName.'_label'] = $profile->label;
            }
        }

        $this->globals = $arr;
        $this->globals['ip'] = Utility::GrabIP();
        $this->globals['referer'] = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
        $this->globals['date'] = date('Ymd');
        unset($this->globals['querystring_codes']);

        return $arr;
    }

    /**
     * Checks if a parameter is required
     *
     * @param $var
     * @return bool
     */
    public function isRequired($var)
    {
        $requiredArray = explode("||", $this->data->profile->config_required_list->code);
        return ((in_array($var, $requiredArray) || $var == 'x_emailaddress') && (isset($this->data->profile->config_required->code) && $this->data->profile->config_required->code));
    }

    public function loadEmailLabels($language = 'en_US', $product = '')
    {
        $this->csv->load($this->files["Email-Labels"]);
        $data = $this->csv->connect2(array($language));
        $englishData = $language !== 'en_US' ? $this->csv->connect2(array('en_US')) : array();
        $returnData = array();

        if(!empty($product))
        {
            $match = false;
            foreach($data as $key => $arr)
            {
                if($arr['Element'] == $product) {

                    foreach($arr as $arrKey => $arrVal)
                    {
                        if(empty($arrVal) && isset($englishData[$key]) && !empty($englishData[$key]))
                            $returnData[$arrKey] = $englishData[$key];
                        else
                            $returnData[$arrKey] = $arrVal;
                    }

                    $match = true;
                    break (1);
                }
            }

            if(!$match)
                $returnData = $data;
        }
        else
            $returnData = array();

        return $returnData;
    }

    /**
     * This method displays the flag in html format in the given language input.
     * Languages are taken from Languages.csv. Check Languages.csv for list of languages available.
     *
     * Use: Method can be used three ways, depending in the parameter.
     *
     * 1.   ALL LANGUAGES MODE - Empty parameter displays all languages.
     *
     * 2.   INCLUSION MODE - Languages can be specified in a comma-separated string.
     *      i.e. getFlags('en_US,fr_FR') will use only English and French.
     *
     * 3.   EXCLUSION MODE - Languages can be excluded with a minus sign in front of the language.
     *      i.e. getFlags('-en_US') will use all languages except English.
     *
     *      Note that all languages must have a minus sign in order to be in exclusion mode.
     *      A mix of languages without minus signs and with minus signs will effectively turn it into inclusion mode.
     *      i.e. getFlags('en_US,-fr_FR') will display only English language.
     *
     * @param string $languages
     */
    public function getFlags($languages = 'all')
    {
        // let's load our javascripts needed
        $returnHtml = '';

        $page = $_SERVER['PHP_SELF']."?". $this->data->querystring->code;
        $languagesArray = $this->loadCSV( 'Languages' );


        $returnHtml .= '<select id="demo-htmlselect-basic">';


        // ALL LANGUAGE MODE
        if( $languages === 'all' )
        {
            foreach( $languagesArray as $lang )
            {
                $selected = $lang['Element'] == $this->data->lang->code ? 'selected="selected"' : '';
                $returnHtml .= '<option data-imagesrc="http://nexus.microsurvey.com/images/flag-'. $lang['Element'].'.png" value="lang='.  $lang['Element'] .'" '. $selected .'>'. $lang["Alt Label"] .'</option>';
            }
        }

        // EXCLUSION OR INCLUSION MODE
        else
        {
            // let's check each language if it has a minus sign in front (exclusion mode)
            $mode = -1;  // we'll use 0 for inclusion, -1 for exclusion. Exclusion by default

            $inputLanguages = explode(",", $languages);
            $includeArray = array();
            $excludeArray = array();

            foreach($inputLanguages as $lang)
            {
                // if even just one language has no minus sign, we're in inclusion mode
                if(substr(trim($lang), 0, 1) !== '-')
                {
                    $mode = 0;  // switch to inclusion mode
                    $includeArray[] = trim($lang);
                }
                else
                {
                    // let's remove the negative sign
                    $realLang = substr(trim($lang), 1);
                    $excludeArray[] = $realLang;
                }
            }

            // INCLUSION MODE
            if($mode == 0)
            {
                foreach($includeArray as $includeLang)
                {
                    foreach($languagesArray as $lang)
                    {
                        if($lang['Element'] == $includeLang)
                        {
                            $selected = $lang['Element'] == $this->data->lang->code ? 'selected="selected"' : '';
                            $returnHtml .= '<option data-imagesrc="http://nexus.microsurvey.com/images/flag-'. $lang['Element'].'.png" value="lang='. $lang['Element'] .'" '. $selected .'>'. $lang["Alt Label"] .'</option>';
                        }
                    }
                }
            }

            // EXCLUSION MODE
            if($mode == -1)
            {
                foreach($excludeArray as $excludeLang)
                {
                    foreach($languagesArray as $lang)
                    {
                        if($lang['Element'] == $excludeLang)
                        {
                            $selected = $lang['Element'] == $this->data->lang->code ? 'selected="selected"' : '';
                            $returnHtml .= '<option data-imagesrc="http://nexus.microsurvey.com/images/flag-'. $lang['Element'].'.png" value="lang='. $lang['Element'] .'" '. $selected .'>'. $lang["Alt Label"] .'</option>';
                        }
                    }
                }
            }
        }

        $returnHtml .= '</select>';

        return $returnHtml;

    }

    public function addToGlobals($post = array())
    {
        foreach($post as $key => $val)
        {
            if(is_array($post[$key]))
            {
                foreach($post[$key] as $pkey => $pval)
                {
                    $this->globals[$post[$key][$pkey]] = $pval;
                }
            }
            else
                $this->globals[$key] = $val;
        }
    }

    public function getGlobals()
    {
        return $this->globals;
    }

    public function replaceAtSignWithHTML($full_string)
    {
        $count = substr_count($full_string, "@");
        while ($count>0){
            $start_pos = strpos($full_string,"@");
            $full_string = substr_replace($full_string, "&#64;", $start_pos, 1);
            $count--;
        }
        return $full_string;
    }

    public function getRedirectLink($origin, $variable)
    {
        // load the column we need from the Origin-Redir.csv file
        $redirectArray = $this->loadCSV('Origin-Redir');

        // default link url
        $link = 'http://www.microsurvey.com';

        try{

            $key = false;

            foreach($redirectArray as $rkey => $valArray)
            {
                if($valArray['Element'] == $origin)
                {
                    $key = $rkey;
                    break;
                }
            }

            if($key === false)
                throw new Exception("Origin '$origin' does not exist in Origin-Redir.csv");

            $row = $redirectArray[$key];

            if(!isset($row[$variable]))
                throw new Exception("Column '$variable' does not exist in Origin-Redir.csv");

            $link = $row[$variable];

        }
        catch(Exception $e)
        {
            echo $e->getMessage();
            echo '<pre>';
            echo $e->getTraceAsString();
            echo '</pre>';

            error_log('redir.php error: '. $e->getMessage());
        }

        return $link;
    }

    public function mapTerritoryAndCountryToOwner($config_territory_map, $country, $state, $city, $debugMode = false)
    {
        $territoryMap = NEXUS_INTERNAL_ROOT.'data/'. $config_territory_map .'.csv';

        // let's initialize owner to false
        $owner_shortcode = false;

        if(!empty($country) && file_exists($territoryMap))
        {
            // Let's get the mapping
            $map_data = $this->loadCSV($config_territory_map);
            $map_headers = $this->getHeaders();

            // build the segment column header so we know what column to look down
            $seg_column_header = $this->data->seg->code ." (Div:". $this->data->div->code .")";

            if(!in_array($seg_column_header, $map_headers))
            {
                foreach($map_headers as $header)
                {
                    if(strpos($header, $this->data->seg->code) !== false)
                    {
                        $seg_column_header = $header;
                        break;
                    }
                }
            }

            if($debugMode){error_log(" Map: ". $config_territory_map . "  country: ". $country ."  state: ". $state ."  city: ". $city ."  header: ". $seg_column_header);}

            $subregion = "";

            // Get the regions that have sub-regions (level 2 children).
            foreach ($map_data as $key => $region) {

                $theOwner = $region[$seg_column_header];

                // assign owner by country
                if ($region['Country/Region/Sub-Region'] == $country && !empty($country) && !empty($theOwner)) {
                    $owner_shortcode = $theOwner;
                }

                // assign owner by state
                if($state == $region['Code'] && $region['Level'] == 1 && !empty($theOwner)) {

                    $owner_shortcode = $theOwner;
                }

                // assign owner by subregion
                if($key + 1 < count($map_data) && $region['Level'] == 1 && $map_data[$key + 1]['Level'] == 2 && $state == $region['Code']) {

                    // Check if the supplied state value has children (sub-regions) in the map, if so load sub-region csv
                    // Why? No point in loading and parsing the sub-regions if a state-level match is all that is possible
                    $subregion_data = $this->loadCSV("Sub-Regions");
                    foreach ($subregion_data as $subreg) {

                        if ($subreg['Country'] == $country && $subreg['Region'] == $state && $subreg['City/Town/Municipality'] == $city) {

                            $subregion = $subreg['Sub-Region'];
                            break;
                        }
                    }
                }

                if($region['Country/Region/Sub-Region'] == $subregion && !empty($theOwner)) {
                    $owner_shortcode = $theOwner;
                }
            }
        }

        return $owner_shortcode;
    }

    // returns the dropdown data in json object format
    public function getDropdowns()
    {
        return json_encode($this->data->dropdowns);
    }

    public function getPrivacyPolicy() {
        $privacyPolicyContent =  json_decode(file_get_contents("http://assets.leica-geosystems.us/wp/wp-json/wp/v2/pages/3?origin=". $this->data->orig->code));
        return trim(preg_replace('/\s+/', ' ', $privacyPolicyContent->content->rendered));
    }

    /**
     * Checks if a variable is overridden
     *
     * @param $var
     * @return bool
     */
    public function isOverridden($var)
    {
        return isset($this->data->$var->override) && $this->data->$var->override == true ? true : false;
    }

    /**
     * Checks if input IP is in our banned list
     *
     * @param $ip
     * @return bool
     */
    public function isBanned($ip)
    {
        // load our ban list csv file
        $ban_list = $this->loadCSV('Ban-List');

        return in_array($ip, $ban_list, true);
    }

    public function isOwnerOverridden( )
    {
        return isset($this->data->owner->override) && $this->data->owner->override == true;
    }

    public function isOriginOverridden( )
    {
        return isset( $this->data->orig->override ) && $this->data->orig->override == true;
    }

    public function isMapOverridden( )
    {
        return isset( $this->data->map->override ) && $this->data->map->override == true;
    }

    public function isOriginOverrideSameAsDefault( )
    {
        if( ! $this->isOriginOverridden( ) )
            return false;

        $formIdArray = $this->loadCSV('Form-ID');

        foreach($formIdArray as $frow)
        {
            if($this->data->form->code == $frow['Form ID'])
            {
                if($this->data->orig->code == $frow['Default Origin'])
                    return true;
            }
        }

        return false;
    }

    /**
     * Sets our Access Control Header
     */
    public function setAccessControl( )
    {
        if( isset( $_SERVER['HTTP_ORIGIN'] ) )
        {
            // our list of sites to allow access
            $sites = array(

                // live sites
                'microsurvey.com',
                'store.microsurvey.com',
                'nexus.microsurvey.com',
                'assets.microsurvey.com',
                'leica-geosystems.us',
                'nexus.leica-geosystems.us',
                '3ddisto.leica-geosystems.us',
                'bimlearningcenter.com',
                'allenprecision.com',
                'leicageosystemsdares.com',
                'lets.becaptivated.com',
                'constructrealityxyz.com',
     		    'ims.ddev.oomphcloud.com',
		        'ims.leica-geosystems.com',
                'gis.leica-geosystems.us',
                'puresurveying.com',

                // local sites
                'microsurvey.local',
                'nexus.microsurvey.local',
                'microsurvey.staging',
                'nexus.microsurvey.staging'
            );

            // let's get only the domain part of the host url
            require_once(NEXUS_INTERNAL_ROOT ."scripts/effectiveTLDs.inc.php");
            require_once(NEXUS_INTERNAL_ROOT ."scripts/regDomain.inc.php");
            $domain = getRegisteredDomain(parse_url($_SERVER['HTTP_ORIGIN'], PHP_URL_HOST));

            // if our site matches the HTTP_ORIGIN's path, allow the site
            if( in_array( $domain, $sites ) ) {
                header( 'Access-Control-Allow-Origin: '. $_SERVER['HTTP_ORIGIN'] );
                header( 'Access-Control-Allow-Methods: POST, GET, OPTIONS' );
                header( 'Access-Control-Max-Age: 1000' );
                header( 'Access-Control-Allow-Headers: Content-Type' );
            }
        }
    }

    public function getChannelTacticValueFromCookie($cookieValue)
    {
        $ElementsArray = $this->loadCSV("Elements");
        $channelValue = '';

        foreach($ElementsArray as $row)
        {
            if($row['Type'] == 'ct' && ($cookieValue == $row['Element'] || $cookieValue == $row['eTrigue Value'] || $cookieValue == $row['Short-Code']))
            {
                $channelValue = $row['eTrigue Value'];
                break(1);
            }
        }

        return $channelValue;
    }

    public function getMappedValuesFromPreset($preset)
    {
        $presets = $this->loadCSV('Filter-Presets');
        $ret = array(
            'map'   => '',
            'origin'    => '',
            'segment'   => '',
            'division'  =>  ''
        );

        foreach($presets as $pre)
        {
            if($pre['Preset'] == $preset)
            {
                $ret['map'] = $pre['Map'];
                $ret['origin'] = $pre['Filter Origin'];
                $ret['segment'] = $pre['Filter Segment'];
                $ret['division'] = $pre['Filter Division'];

                break(1);
            }
        }

        return $ret;
    }


    public function getFormIdRow($formId)
    {
        $ret = array();

        // load our form-ID.csv file and set defaults
        $formIdArray = $this->loadCSV('Form-ID');

        $foundFormCode = false;
        foreach($formIdArray as $row)
        {
            if($row['Form ID'] == $formId)
            {
                $ret = $row;
                break 1; // if found, break out of current loop
            }
        }

        return $ret;
    }

    private function _getDefaultsFromFormID($form)
    {
        // set up our default profiles
        $this->_setDefaultObjects();

        // Load common vars that are reused
        $this->_loadCommonVars($form);

        // load our form-ID.csv file and set defaults
        $formIdArray = $this->loadCSV('Form-ID');

        $foundFormCode = false;
        foreach($formIdArray as $row)
        {
            if($row['Form ID'] == $this->data->form->code)
            {
                $this->data->seg->code = $row['Default Segment'];
                $this->data->div->code = $row['Default Division'];
                $this->data->app->code = '';                            // placeholder
                $this->data->orig->code = $row['Default Origin'];
                $this->data->prof->code = $row['Form Profile'];
                $this->data->prof->label = $row['Label'];
                $this->data->prod->code = $row['Default Product'];
                $this->data->owner->code = $row['Default Owner'];
                $this->data->status->code = $row['Default Status'];
                $this->data->crm->code = $row['Status Controls'];
                $this->data->gatracking->code = $row['GA Tracking'];
                $this->data->ct->code = $row['Default Channel Tactic'];
                $this->data->map->code = $row['Territory Map'];

                // special case for adwords
                $this->data->adwords->code = isset($row['Adwords']) ? $row['Adwords'] : null;
                $this->data->adwords->id = isset($row['Adwords ID']) ? $row['Adwords ID'] : null;
                $this->data->adwords->label = isset($row['Adwords Label']) ? $row['Adwords Label'] : null;

                // special profile variables not in Form-Profiles.csv
                $this->data->notification_sms->code = $row['Notification SMS'];
                $this->data->notification_email->code = $row['Notification Email'];
                $this->data->responder_email->code = $row['Responder Email'];



                $foundFormCode = true;
                break 1; // if found, break out of current loop
            }
        }

        // let's give default values of false if form code is not found
        // and quit with an error
        if(! $foundFormCode)
        {
            $this->data->form->code = false;
            $this->data->seg->code = false;
            $this->data->div->code = false;
            $this->data->app->code = false;
            $this->data->orig->code = false;
            $this->data->prof->code = false;
            $this->data->prod->code = false;
            $this->data->owner->code = false;
            $this->data->ct->code = false;
            $this->data->map->code = false;

            $e = new ErrorChecker();
            $e->set_error_message( "<div class='form-error'>Error Code: 204. Missing form profile.</div>" );
            die($e->get_error_message());
        }
    }

    /**
     *  Check if overrides were passed in via GET requests.
     *  If so, overwrite values and set override flags for later
     */
    private function _getOverridesFromGET()
    {
        $overrides_array = array("form","owner","seg","div","app","orig","prof","prod","map","lang","country","state","city","back", "ct", "customer", "bootstrap", "multiple");
        $querystring_array = array();

        foreach ($overrides_array as $val) {

            if( ! isset($this->data->$val ) ) {
                $this->data->$val = new stdClass( );
                $this->data->$val->code = null;
                $this->data->$val->override = null;
                $this->data->$val->type = null;
            }
            if( isset($this->data->$val ) ) { if($this->data->$val->code == "FALSE" ){ $this->data->$val->code = false; } }

            if(isset($_GET[$val]) && !empty($_GET[$val])){

                // special case for origin. If the supplied origin is the same as the default from the form profile, don't override
                if($val != 'orig' || ( $val == 'orig' && $_GET[$val] != $this->data->orig->code) )
                {
                    $this->data->$val->code = $_GET[$val];
                    $this->data->$val->override = true;
                    $this->data->$val->type = $val;
                }

                // add to querystring array that we're going to pass back
                $querystring_array[$val] = $_GET[$val];
            }
        }

        // if OWNER is NOT overriden
        if( ! $this->isOwnerOverridden( ) )
        {
            // is origin overridden?
            if( $this->isOriginOverridden( ) ) // Yes
            {
                // Lookup owner from first origin match from "Owners"
                $this->_setOwnerFromFirstOriginMatch();
            }
        }

        // It's possible the origin code will be passed in as text string [[sig_address3]].
        // In which case, we simply want it to change to 'microsurvey' before we proceed.
        if($this->data->orig->code == "[[sig_address3]]"){$this->data->orig->code = "microsurvey";}

        // for any other $_GET variables we just want to pass along in the querystring
        $querystring_array = array_merge($_GET, $querystring_array);

        // create querystring
        $this->data->querystring->code = http_build_query($querystring_array);
    }

    private function _setOwnerFromFirstOriginMatch()
    {
        $account_data = $this->loadCSV('Owners');

        // loop through the Accounts data, to do a reverse lookup, and pick up the first origin match - override as owner
        foreach ($account_data as $i => $val) {
            if ($this->data->orig->code == $val['Origin']) {
                $this->data->owner->code = $val['Short-Code'];
                break;
            }
        }
    }

    /*
     * This function handles overrides from form submission data
     */
    private function _getOverridesFromPOST($form)
    {
        if( $form instanceof Forms )
        {
            $_REQUEST = $form->getFormData();
        }

        foreach($_REQUEST as $key => $value)
        {
            if(is_array($_REQUEST[$key]))
            {
                foreach($_REQUEST[$key] as $rkey => $rval)
                {
                    $_REQUEST[$key][$rkey] = urldecode($rval);
                }
            }
            else {
                $_REQUEST[$key] = urldecode($value);
            }
        }
        $this->addToGlobals($_REQUEST);


        // Check if there are any POST results from Seg or Div - specifically, if there are any, then the
        // form author may have included the seg or seg+div dropdowns, allowing the user to choose AND
        // OVERRIDE any defaults supplied by the GET requests
        $elementsArray = $this->loadCSV('Elements');
        foreach ($elementsArray as $row) {

            if ($row['Type'] == "seg") {

                if(isset($_REQUEST['x_udf_primary_market_segment122__ss']) && $_REQUEST['x_udf_primary_market_segment122__ss'] == $row['eTrigue Value'])
                {
                    $this->data->seg->code = $row['Element'];
                    $this->data->seg->override = true;
                }
            }

            if ($row['Type'] == "div")
            {
                if (isset($_REQUEST['x_udf_division146__ss']) && $_REQUEST['x_udf_division146__ss'] == $row['eTrigue Value'])
                {
                    $this->data->div->code = $row['Element'];
                    $this->data->div->override = true;
                }
            }

            if ($row['Type'] == "ct")
            {
                if (isset($_REQUEST['x_udf_channel___tactic400__ss']) && $_REQUEST['x_udf_channel___tactic400__ss'] == $row['eTrigue Value'])
                {
                    $this->data->ct->code = $row['Element'];
                    $this->data->ct->override = true;
                }
            }

            if ($row['Type'] == "app")
            {
                if (isset($_REQUEST['x_udf_main_application121__ss']) && $_REQUEST['x_udf_main_application121__ss'] == $row['eTrigue Value'])
                {
                    $this->data->app->code = $row['Element'];
                    $this->data->app->override = true;
                }
            }

            if ($row['Type'] == "prod")
            {
                if (isset($_REQUEST['x_udf_form_product149']) && $_REQUEST['x_udf_form_product149'] == $row['Short-Code'])
                {
                    $this->data->prod->code = $row['Short-Code'];
                    $this->data->app->override = true;
                }
            }

            if($row['Type'] == "form-select-owners") {

                if(isset($_REQUEST['x_owner_override']) && !empty($_REQUEST['x_owner_override']))
                {
                    $this->data->owner->code = $_REQUEST['x_owner_override'];
                    $this->data->owner->override = true;
                }

            }
        }

        // hack for language
        if(isset($_REQUEST['x_udf_form_language302'])){
            $this->data->lang->code = $_REQUEST['x_udf_form_language302'];
            $this->data->lang->value = $_REQUEST['x_udf_form_language302'];
        }

        // get status overrides from any of the dropdowns under select-group.csv
        $this->_getStatusOverrideFromSelectGroup($form);
    }

    private function _getStatusOverrideFromSelectGroup($form)
    {
        // If the form isn't even a Utility form in the first place, it doesn't need to make this check.
        if(strtolower($this->data->status->code) !== 'utility' || $form instanceof Forms)
            return;

        $elementsArray = $this->loadCSV("Elements");
        $selectGroupArray = $this->loadCSV("Select-Group");

        /*
         * This array will hold the select dropdown variable and their value and override lookup array in the following format:
         *
         * Ex:  array(
         *          'x_udf_ext_purchase_question405' => array(
         *
         *                          'value'     => 'Within a month',
         *                          'override'    => array('select_purchase_time1')
         *                  )
         *      )
         *
         */

        $lookupArray = array();

        foreach($elementsArray as $elemRow)
        {
            if($elemRow['Type'] == 'form-input-select' && isset($_REQUEST[$elemRow['Element']]))
            {
                $variable = $elemRow['Element'];
                $value = $_REQUEST[$variable];

                // add our variable to our lookup array
                $lookupArray[$variable]['value'] = $value;

                // get our
                $selectGroupHeader = $elemRow['Short-Code'];

                foreach($selectGroupArray as $selRow)
                {
                    // look for the overrides
                    if(strtolower($selRow[$selectGroupHeader]) == 'override')
                    {
                        $lookupArray[$variable]['override'][] = $selRow['Element'];
                    }
                }
            }
        }

        // now loop through the Elements array again to look up the overrides' values
        foreach($elementsArray as $elemRow)
        {
            if($elemRow['Type'] == 'form-select-option')
            {
                foreach($lookupArray as $lookRow)
                {
                    if(isset($lookRow['override']) && in_array($elemRow['Element'], $lookRow['override']) && $elemRow['eTrigue Value'] == $lookRow['value'])
                    {
                        // override! set data->status to '' so notification email is sent out
                        $this->data->status->code = '';

                        // if we override, no need to go further since the notification email will already be sent out just quit the loop
                        break(2);
                    }
                }
            }
        }

    }

    /**
     *  Returns array of products based on the input group name
     *
     * @param $group    Pipeline delimited string
     * @return array
     */
    private function _getProductsFromGroup($group)
    {
        // our product array that we'll return
        $productArray = array();

        // lets load our Product-Group.csv file
        $productGroupCsv = $this->loadCSV('product-group', array_merge(array('Product', 'eTrigue ID'), explode("||", $group)));

        foreach($productGroupCsv as $row)
        {
            foreach($row as $header => $value)
            {
                if($header != 'Product' && $value == 'TRUE')
                {
                    $productArray[] = array(
                        'name'      =>  $row['Product'],
                        'value'     =>  $row['eTrigue ID']
                    );
                }
            }
        }

        return $productArray;
    }

    private function _loadProfileLabels()
    {
        //load Form-Labels.csv
        $lang = $this->data->lang->value;
        $inputArr = $lang !== 'en_US' ? array('Element', 'en_US', $lang) : array('Element', $lang);
        $formLabelsArray = $this->loadCSV('Form-Labels', $inputArr);
        $multiple = isset($this->data->multiple->code) ? $this->data->multiple->code : '';

        // iterate through each Form-Labels.csv rows
        foreach($formLabelsArray as $row)
        {
            $element = array_key_exists($row['Element'], $this->mappings) ? $this->mappings[$row['Element']] : $row['Element'];
            $theLabel = $lang !== 'en_US' && empty($row[$lang]) ? $row['en_US'] : $row[$lang];

            // match it up with the corresponding elements in $this->data
            $hasMatch = false;
            foreach($this->data as $className => $obj)
            {
                // if it matches with $this->data->profile.
                // this is the profile array, which has a different format
                if($className == 'profile' && isset($obj->$element))
                {
                    @$obj->$element->label = $theLabel;
                    $hasMatch = true;
                }

                // for everything else in $this->data (seg, div, lang, etc)
                else if($className != 'profile' && isset($obj->code) && $obj->code === $element)
                {
                    @$obj->label = $theLabel;
                    $hasMatch = true;
                }
            }

            // special case for x_show_products
            if(isset($this->data->profile->x_show_products) && $this->data->profile->x_show_products->code !== false)
            {
                $labelsArray = explode("||", $this->data->profile->x_show_products->label);

                foreach($labelsArray as $key => $label)
                {
                    if($label == $element)
                    {
                        $labelsArray[$key] = $row[$lang];
                    }
                }

                @$this->data->profile->x_show_products->label = implode('||', $labelsArray);
            }

            // if there's no match, add it to our profile array
            if(! $hasMatch)
            {
                @$this->data->profile->$element->code = $lang !== 'en_US' && empty($row[$lang]) ? $row['en_US'] : $row[$lang];
                @$this->data->profile->$element->label = $lang !== 'en_US' && empty($row[$lang]) ? $row['en_US'] : $row[$lang];
                @$this->data->profile->$element->type = $lang !== 'en_US' && empty($row[$lang]) ? $row['en_US'] : $row[$lang];
                @$this->data->profile->$element->value = $lang !== 'en_US' && empty($row[$lang]) ? $row['en_US'] : $row[$lang];
            }
        }

        // set empty labels for profile elements that doesn't have a label set
        foreach($this->data->profile as $row)
        {
            if(!isset($row->label))
                $row->label = '';
        }

    }

    private function _loadProfileValues()
    {
        //load Elements.csv
        $elementsArray = $this->loadCSV('Elements');

        // these are special variables for x_show_products only
        $valuesArray = array();
        $typeArray = array();
        $shortcodeArray = array();

        // iterate through each Elements.csv rows
        foreach($elementsArray as $row)
        {
            $element = array_key_exists($row['Element'], $this->mappings) ? $this->mappings[$row['Element']] : $row['Element'];

            // match it up with the corresponding elements in $this->data
            foreach($this->data as $className => $obj)
            {
                // if it matches with $this->data->profile.
                // this is the profile array, which has a different format
                if($className == 'profile' && isset($obj->$element))
                {
                    if(!empty($row['eTrigue Value']))
                        @$obj->$element->value = $row['eTrigue Value'];

                    if(!empty($row['Type']) && !isset($obj->$element->type))
                        @$obj->$element->type = $row['Type'];

                    if(!empty($row['Short-Code']))
                        @$obj->$element->shortcode = $row['Short-Code'];
                }

                // special case for seg if it was overridden
                else if($className != 'profile' && isset($obj->code) && !empty($obj->code) && isset($obj->type) && $obj->type == $row['Type'])
                {
                    // check if it matches Short-Code OR Element OR eTrigue Value
                    if($obj->code == $row['Short-Code'] || $obj->code == $element || $obj->code == $row['eTrigue Value'])
                    {
                        // rewrite the prod code with the actual element
                        @$obj->code = $element;

                        if(!empty($row['eTrigue Value']))
                            @$obj->value = $row['eTrigue Value'];

                        if(!empty($row['Type']))
                            @$obj->type = $row['Type'];

                        if(!empty($row['Short-Code']))
                            @$obj->shortcode = $row['Short-Code'];
                    }
                }
            }

            // special case for x_show_products
            if(isset($this->data->profile->x_show_products))
            {
                if($this->data->profile->x_show_products->code !== false)
                {
                    $valuesArr = explode("||", $this->data->profile->x_show_products->value);

                    foreach($valuesArr as $key => $value)
                    {
                        if($value == $element)
                        {
                            if(!empty($row['eTrigue Value']) && !in_array($row['eTrigue Value'], $valuesArray))
                                $valuesArray[] = $row['eTrigue Value'];

                            if(!empty($row['Type']) && !in_array($row['Type'], $typeArray))
                                $typeArray[] = $row['Type'];

                            if(!empty($row['Short-Code']) && !in_array($row['Short-Code'], $shortcodeArray))
                                $shortcodeArray[] = $row['Short-Code'];
                        }
                    }
                }
                else
                {
                    if(isset($this->prod->code) && $this->prod->code == $row['Element'])
                    {
                        $prodElem = $row['Element'];
                        $prodValue = $row['eTrigue Value'];
                        @$this->data->profile->$prodElem->code = true;
                        @$this->data->profile->$prodElem->type = 'form-hidden';
                        @$this->data->profile->$prodElem->value = $prodValue;
                    }
                }
            }
        }

        if(isset($this->data->profile->x_show_products))
        {
            @$this->data->profile->x_show_products->value = implode("||", $valuesArray);
            @$this->data->profile->x_show_products->type = implode("||", $typeArray);
            @$this->data->profile->x_show_products->shortcode = implode("||", $shortcodeArray);
        }

        // special case for back
        if(isset($this->data->profile->config_back) && isset($this->data->back) && !empty($this->data->back->code))
        {
            @$this->data->profile->config_back->value = $this->data->back->code;
        }

        // set code as value for profile elements that doesn't have a value set
        foreach($this->data->profile as $row)
        {
            if(!isset($row->value))
            {
                $row->value = $row->code;
            }
        }
    }

    /**
     * Loads the Languages.csv sheet into our Nexus object.
     */
    private function _loadLanguage()
    {
        $languagesArray = $this->loadCSV('Languages');

        // first let's try to load the language based on IP address
        $countryFromIP = Utility::ipToCountryCode($_SERVER['REMOTE_ADDR']);
        $foundCountry = false;

        if(!$this->data->lang->code)
        {
            foreach($languagesArray as $row)
            {
                if(substr_compare($row['Element'], $countryFromIP, -2, 2) == 0)
                {
                    $this->data->lang->code = $row['Element'];

                    // add it's flag
                    $this->data->lang->flag = $row['Element'];

                    // replace the code with its mapping
                    $this->data->lang->value = $row['Map To'];

                    // add its label
                    $this->data->lang->label = $row['English Label'];

                    // add it's native label
                    $this->data->lang->native = $row['Alt Label'];

                    $foundCountry = true;

                    // break out of our foreach loop
                    break(1);
                }
            }
        }

        // if we can't find the language by IP, load english language by default
        if(!$foundCountry)
        {
            // if language is not set, set it as english
            if(!$this->data->lang->code){$this->data->lang->code = "en_US";}

            foreach($languagesArray as $row)
            {
                if($this->data->lang->code == $row['Element'])
                {
                    // add it's flag
                    $this->data->lang->flag = $row['Element'];

                    // replace the code with its mapping
                    $this->data->lang->value = $row['Map To'];

                    // add its label
                    $this->data->lang->label = $row['English Label'];

                    // add it's native label
                    $this->data->lang->native = $row['Alt Label'];

                    // break out of our foreach loop
                    break(1);
                }
            }
        }


    }

    /**
     *  Loads the Form-Profiles.csv data into $this->data->profiles object
     *  for the corresponding profile specified in Form-ID.csv
     */
    private function _loadFormProfiles($form)
    {
        // let's load Form-Profiles.csv for the profile we loaded into
        // $this->data->prof->code from the Form-ID.csv file
        $profile = $this->data->prof->code;
        $formProfilesArray = $this->loadCSV('Form-Profiles', array('Element', $profile));

        // Seg and Div shows up twice on the spreadsheet, so we need a flag to tell which one we're looking at
        $firstSeg = true;
        $firstDiv = true;

        // we go through each row and check if it's not equal to 'false'
        foreach($formProfilesArray as $row)
        {
            $element = array_key_exists($row['Element'], $this->mappings) ? $this->mappings[$row['Element']] : $row['Element'];

            // special case for seg and div and app
            if($element === 'x_udf_primary_market_segment122__ss')
            {
                if($firstSeg && strtolower($row[$profile]) != 'true' && strtolower($row[$profile]) != 'false')
                {
                    $this->data->profile->$element = new StdClass();
                    $this->data->profile->$element->code = $row[$profile];
                    $this->data->seg->isFirst = true;
                }
                else if(!$firstSeg && !isset( $this->data->profile->$element))
                {
                    $this->data->profile->$element = new StdClass();
                    $this->data->profile->$element->code = strtolower($row[$profile]) === 'true' ? true : false;
                    $this->data->seg->isFirst = false;
                }

                $firstSeg = false;
            }
            else if($element === 'x_udf_division146__ss')
            {
                if($firstDiv && strtolower($row[$profile]) != 'true' && strtolower($row[$profile]) != 'false')
                {
                    $this->data->profile->$element = new StdClass();
                    $this->data->profile->$element->code = $row[$profile];
                }
                else if(!$firstDiv && !isset( $this->data->profile->$element))
                {
                    $this->data->profile->$element = new StdClass();
                    $this->data->profile->$element->code = strtolower($row[$profile]) === 'true' ? true : false;
                }

                $firstDiv = false;
            }
            else if($element === 'x_country')
            {
                $this->data->profile->$element = new StdClass();
                $this->data->profile->$element->code = strtolower($row[$profile]) !== 'false' ? $row[$profile] : false;
            }
            else if(strtolower($row[$profile]) === 'true')
            {
                $this->data->profile->$element = new StdClass();
                $this->data->profile->$element->code = true;
            }
            else if(strtolower($row[$profile]) === 'false')
            {
                $this->data->profile->$element = new StdClass();
                $this->data->profile->$element->code = false;

                /**
                 * When the product checkboxes/dropdown is not used (hidden field only):
                 *
                 *  In these instances, the product element short-code was given in the querystring
                 *  OR there is a default product set for the form ID. In which case, the field needs to stay hidden,
                 *  but ALSO you’ll need to create & hide the corresponding product subscription,
                 *  and have its value set to the corresponding value for that product.
                 *
                 *  E.g. if we add ‘cad’ to the querystring, not only is “cad” added as a value to the hidden x_udf_form_product149,
                 *  but also, a hidden field x_udf_sub_prod_cad332__ms will be created, and the value 226 given.
                 */
                if($element == 'x_show_products')
                {
                    $this->data->profile->x_udf_form_product149 = new stdClass();
                    $this->data->profile->x_udf_form_product149->code = true;
                    $this->data->profile->x_udf_form_product149->label = '[[product_code]]';
                    $this->data->profile->x_udf_form_product149->value = '[[product_shortcode]]';
                    $this->data->profile->x_udf_form_product149->type = 'form-hidden';

                }
            }
            else
            {
                $this->data->profile->$element = new StdClass();
                $this->data->profile->$element->code = $row[$profile];
            }

            // special case for those that don't have 'TRUE' or 'FALSE'
            if(strtolower($row[$profile]) != 'true' && strtolower($row[$profile]) != 'false' && isset($this->data->profile->$element->code))
            {
                $this->data->profile->$element->label = $row[$profile];
                $this->data->profile->$element->value = $row[$profile];

                // special case for segment and division
                if($element === 'x_udf_primary_market_segment122__ss' || $element === 'x_udf_division146__ss')
                {
                    $this->data->profile->$element->type = 'form-hidden';
                }

                // special case for products
                if($element == 'x_show_products')
                {
                    $profileArray = explode("||", $row[$profile]);

                    foreach($profileArray as $key => $val)
                    {
                        $profileArray[$key] = 'product_group_'. $val;
                    }

                    $this->data->profile->$element->label = implode("||", $profileArray);
                    $this->data->profile->$element->value = implode("||", $profileArray);
                }
            }

            if( $form instanceof Forms )
            {
                $formData = $form->getFormData();
                $postedData = json_decode($formData['posted_data'], true);

                if( array_key_exists( $element, $postedData ) )
                {
                    $this->data->profile->$element->value = $postedData[$element];
                }

                if( array_key_exists( $element, $formData ) )
                {
                    $this->data->profile->$element->value = $formData[$element];
                }

            }

        }

        /*
         * special profiles not seen in Form-Profiles.csv
         */
        $this->data->profile->config_territory_map = new StdClass();
        $this->data->profile->notification_sms = new StdClass();
        $this->data->profile->notification_email = new StdClass();
        $this->data->profile->responder_email = new StdClass();

        // special case for config_territory_map if map is overridden (and only if owner is not overridden)
        $this->data->profile->config_territory_map->code = $this->data->map->code;
        $this->data->profile->config_territory_map->type = 'form-special';

        $this->data->profile->notification_sms->code = $this->data->notification_sms->code;
        $this->data->profile->notification_sms->type = 'form-special';

        $this->data->profile->notification_email->code = $this->data->notification_email->code;
        $this->data->profile->notification_email->type = 'form-special';

        $this->data->profile->responder_email->code = $this->data->responder_email->code;
        $this->data->profile->responder_email->type = 'form-special';

        $this->data->profile->referrer = new stdClass();
        $this->data->profile->referrer->code = true;
        $this->data->profile->referrer->val = isset($_GET['referrer']) && !empty($_GET['referrer']) ? $_GET['referrer'] : (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
        $this->data->profile->referrer->value = isset($_GET['referrer']) && !empty($_GET['referrer']) ? $_GET['referrer'] : (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $_SERVER['REQUEST_SCHEME'] ."://". $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
        $this->data->profile->referrer->type = 'form-hidden';

    }

    private function _loadDropdowns()
    {
        // this array will hold all of our dropdown data
        $this->data->dropdowns = array();

        // load our Segment-Division or Segment-Application dropdowns
        $this->_loadSegment();

        // load our Product dropdowns
        $this->_loadProducts();

        // load our Solutions dropdowns
        $this->_loadSolutions();

        // load our territories
        $this->_loadTerritory();

        // load our language flags
        $this->_loadFlags();

        // load the rest of our dropdowns from Select-Group.csv
        $this->_loadSelectGroups();

        // load bath geo harvester dropdown
        $this->_loadBatchGeoHarvester();

    }

    private function _getOwnersWithOrigin($origin = '') {

        $ret = array("" => "");

        $owners = $this->loadCSV('Owners');

        foreach($owners as $owner){

            if($owner['Origin'] == $origin) {

                $val = $owner['Short-Code'];
                $ret[$val] = $owner['Label'];

            }

        }

        return $ret;

    }

    public function getMappings()
    {
        return array(
            'form'          =>  'espFormID',

            // first name mappings
            'first'         =>  'x_firstname',
            'first_name'    =>  'x_firstname',

            // last name mappings
            'last'          =>  'x_lastname',
            'last_name'     =>  'x_lastname',
            'surname'       =>  'x_lastname',

            // job title mappings
            'title'         =>  'x_title',
            'job_title'     =>  'x_title',
            'role'          =>  'x_title',
            'rank'          =>  'x_title',

            // phone mappings
            'phone'         =>  'x_phone',
            'phone_number'  =>  'x_phone',
            'telephone'     =>  'x_phone',

            // country mappings
            'country'       =>  'x_country',

            // state mappings
            'state'         =>  'x_state',
            'state/prov'    =>  'x_state',
            'state/province' => 'x_state',
            'state_/_province' =>  'x_state',
            'province'      =>  'x_state',

            // email address mappings
            'email'         =>  'x_emailaddress',
            'email_address' =>  'x_emailaddress',
            'e-mail'        =>  'x_emailaddress',

            // company mappings
            'company'       =>  'x_companyname',
            'company_name'  =>  'x_companyname',
            'organization'  =>  'x_companyname',
            'organisation'  =>  'x_companyname',

            // address mappings
            'address'       =>  'x_address',
            'address1'      =>  'x_address',

            // address2 mappings
            'address2'      =>  'x_address2',

            // city mappings
            'city'          =>  'x_city',

            // zip mappings
            'zip'           =>  'x_zip',
            'zip/postal_code' =>  'x_zip',
            'postal_code'   =>  'x_zip',
            'zip_/_postal_code' =>  'x_zip',
            'postal/zip'    =>  'x_zip',
            'zip_code'      =>  'x_zip',

            // comments mappings
            'comments'      =>  'x_udf_comments126',
            'comment'       =>  'x_udf_comments126',
            'questions_and_comments'    =>  'x_udf_comments126',

            // dealer mappings - "Referrer", "Dealer", "Dealer/Reseller", "Secondary Source", "Source"
            'dealer'        =>  'x_udf_ext_dealer375',
            'dealer_contact' => 'x_udf_dealer_contact465',
            'dealer_phone'  => 'x_udf_dealer_phone466',
            'referrer'      =>  'x_udf_ext_dealer375',
            'dealer/reseller'   =>  'x_udf_ext_dealer375',
            'secondary_source'  =>  'x_udf_ext_dealer375',
            'source'        =>  'x_udf_ext_dealer375',

            // additional mappings
            'preset' => 'preset',
            'owner'         =>  'owner',
            'origin'        =>  'origin',
            'origin_code'   =>  'origin',
            'primary_market_segment' => 'segment',
            'division'      =>  'division',
            'customer'      =>  'x_udf_extcustomer',
            'purchase_time' =>  'x_udf_extpurchasetime',
            'contact'       =>  'x_udf_ext_contact_question441',
            'product'       =>  'x_udf_form_product149',
            'ct'            =>  'x_udf_channel___tactic400__ss',
            'serial'        =>  'x_udf_ext_serial_number377',
            'lang'          =>  'config_language',
            'event'         =>  'x_udf_event443',
            'x_udf_ext_hds_question__2_435' => 'x_udf_ext_hds_question__2_435',
            'x_udf_main_application121__ss' => 'x_udf_main_application121__ss',
            'x_udf_ext_purchase_question405' => 'x_udf_ext_purchase_question405',
            'x_udf_ext_contact_question441' => 'x_udf_ext_contact_question441',
            'x_udf_ext_surv_ground_question442' => 'x_udf_ext_surv_ground_question442',
            'x_udf_extcustomer' => 'x_udf_extcustomer',
            'x_udf_ext_hds_question_cyclone451__ms' => 'x_udf_ext_hds_question_cyclone451__ms',
            'x_udf_ext_hds_question_cyclone_2452__ms' => 'x_udf_ext_hds_question_cyclone_2452__ms'
        );
    }

    private function _loadAutofillValues()
    {
        $mappings = $this->getMappings();

        foreach($mappings as $key => $nexusVariable)
        {
            if(isset($_GET[$key]))
            {
                $value = urldecode($_GET[$key]);

                // special case for ct
                if($key == 'ct')
                {
                    $this->data->profile->$nexusVariable->value = $this->getChannelTacticValueFromCookie($value);

                    // now reload the code and labels
                    $this->data->profile->$nexusVariable->code = $this->_getChannelTacticFromValue($this->data->profile->$nexusVariable->value);
                    $this->data->profile->$nexusVariable->label = $this->getChannelTacticLabelFromValue($this->data->profile->$nexusVariable->value);
                }

                else if( $key == 'owner' )
                {
                    $o = new Owner();
                    $owner = $o->getOwnerByShortcode($value);
                    $this->data->profile->x_udf_form_owner148->value = $owner['owner_shortcode'];
                    $this->data->profile->espUsers->value = $owner['owner_id'];
                }

                // for everything else
                else if(isset($this->data->profile->$nexusVariable))
                {
                    $this->data->profile->$nexusVariable->value = $value;
                    $this->data->profile->$nexusVariable->override = true;
                }

            }
        }
    }

    private function _loadAutofillValuesFromCookies()
    {
        $mappings = array(
            'nexus_ct'      =>  'x_udf_channel___tactic400__ss'
        );
        foreach($mappings as $key => $nexusVariable)
        {
            if(isset($_COOKIE) && isset($_COOKIE[$key]) && isset($this->data->profile->$nexusVariable))
            {
                $this->data->profile->$nexusVariable->value = $this->getChannelTacticValueFromCookie(urldecode($_COOKIE[$key]));
                $this->data->profile->$nexusVariable->code = $this->_getChannelTacticFromValue($this->data->profile->$nexusVariable->value);
                $this->data->profile->$nexusVariable->label = $this->getChannelTacticLabelFromValue($this->data->profile->$nexusVariable->value);
            }
        }
    }

    private function _loadSegment()
    {
        // if our profile doesn't have segments, just return
        if(!isset($this->data->profile->x_udf_primary_market_segment122__ss) || (isset($this->data->profile->x_udf_primary_market_segment122__ss) && $this->data->profile->x_udf_primary_market_segment122__ss->code == false))
            return;

        // this array will hold our select data
        $returnData = array();

        // load our Elements.csv file for our seg, div, and application data
        $elementsArray = $this->loadCSV('Elements');

        // $data will hold just segment, division, and application data from Elements.csv
        $data = array();

        $multiple = isset($this->data->multiple->code) ? $this->data->multiple->code : '';

        // loop through the Elements.csv file and store in $data array
        foreach($elementsArray as $row)
        {
            if($row['Type'] == 'seg' || $row['Type'] == 'div' || $row['Type'] == 'app')
            {
                $arr = array(
                    'id'            =>  $row['Type'] == 'seg' ? 'x_udf_primary_market_segment122__ss'. $multiple : ($row['Type'] == 'div' ? 'x_udf_division146__ss'.$multiple : 'x_udf_main_application121__ss'.$multiple),
                    'name'          =>  $row['Type'] == 'seg' ? 'x_udf_primary_market_segment122__ss' : ($row['Type'] == 'div' ? 'x_udf_division146__ss' : 'x_udf_main_application121__ss'),
                    'code'          =>  $row['Element'],
                    'type'          =>  'select',
                    'attribute'     =>  $row['Type'],
                    'shortcode'     =>  $row['Short-Code'],
                    'value'         =>  $row['eTrigue Value'],
                );

                if($row['Type'] == 'seg')
                {
                    $arr['selected'] = $this->data->profile->x_udf_primary_market_segment122__ss->value === $row['Short-Code'] ? true :  false;
                }

                if($row['Type'] == 'div')
                {
                    $arr['selected'] = $this->data->profile->x_udf_division146__ss->value === $row['Short-Code'] ? true :  false;
                }

                if($row['Type'] == 'app')
                {
                    $arr['selected'] = $this->data->profile->x_udf_main_application121__ss->value === $row['Short-Code'] ? true :  false;
                }

                $data[] = $arr;
            }

            // load our first dropdown option for segment, div, and app into our data
            if($row['Element'] == 'select_first_dropdown_option')
            {
                if(isset($this->data->seg->isFirst) && ! $this->data->seg->isFirst)
                {
                    array_unshift($data, array(
                        'id'        =>  'x_udf_primary_market_segment122__ss'.$multiple,
                        'name'      =>  'x_udf_primary_market_segment122__ss',
                        'code'      =>  $row['Element'],
                        'attribute' =>  'seg',
                        'type'      =>  'select',
                        'value'     =>  '',
                        'shortcode' =>  '',
                        'selected'  => isset($this->data->seg->override) && $this->data->seg->override == true ? false : true
                    ));
                }

                array_unshift($data, array(
                    'id'        =>  'x_udf_division146__ss'.$multiple,
                    'name'      =>  'x_udf_division146__ss',
                    'code'      =>  $row['Element'],
                    'attribute' =>  'div',
                    'type'      =>  'select',
                    'value'     =>  '',
                    'shortcode' =>  '',
                    'selected'  => isset($this->data->div->override) && $this->data->div->override == true ? false : true
                ));

                array_unshift($data, array(
                    'id'        =>  'x_udf_main_application121__ss'.$multiple,
                    'name'      =>  'x_udf_main_application121__ss',
                    'code'      =>  $row['Element'],
                    'attribute' =>  'application',
                    'type'      =>  'select',
                    'value'     =>  '',
                    'selected'  => true
                ));
            }
        }

        // load Form-Labels.csv to get our labels
        $formLabelsArray = $this->loadCSV('Form-Labels', array('Element', 'en_US', $this->data->lang->value));
        foreach($formLabelsArray as $frow)
        {
            foreach($data as $dkey => $drow)
            {
                if($drow['code'] == $frow['Element'])
                {
                    $data[$dkey]['label'] = empty($frow[$this->data->lang->value]) ? $frow['en_US'] : $frow[$this->data->lang->value];
                }
            }
        }

        // let's add our segment data into our return array
        foreach($data as $row)
        {
            if($row['attribute'] == 'seg' &&
                ( (isset($this->data->seg->isFirst) && ! $this->data->seg->isFirst) ||
                    ( (isset($this->data->seg->isFirst) &&$this->data->seg->isFirst) && $this->data->seg->value == $row['value'] )))
                $returnData[] = $row;
        }

        // load Seg-Div.csv or Seg-App.csv (or both)
        $loadWhatArray = array();
        if(isset($this->data->profile->x_udf_division146__ss) && $this->data->profile->x_udf_division146__ss->code == true){ $loadWhatArray[] = 'Seg-Div'; }
        if(isset($this->data->profile->x_udf_main_application121__ss) && $this->data->profile->x_udf_main_application121__ss->code == true){ $loadWhatArray[] = 'Seg-App'; }

        // if we're loading div or applications
        foreach($loadWhatArray as $loadWhat)
        {
            $firstOption = null;
            $optionArray = array();

            $segChildArray = $this->loadCSV($loadWhat);

            foreach($segChildArray as $row)
            {
                $segChild = $row['Element'];

                // first let's get our child data
                foreach($data as $drow)
                {
                    // get our child
                    if($drow['code'] == $segChild)
                    {
                        $child = $drow;
                    }

                    // special case for first select option for the div child
                    if($loadWhat == 'Seg-Div' && $drow['code'] == 'select_first_dropdown_option' && $drow['attribute'] == 'div')
                    {
                        $firstOption = $drow;
                        $firstOption['disabled'] = true;
                    }

                    // special case for first select option for the app child
                    if($loadWhat == 'Seg-App' && $drow['code'] == 'select_first_dropdown_option' && $drow['attribute'] == 'application')
                    {
                        $firstOption = $drow;
                        $firstOption['disabled'] = true;
                    }

                }

                // then we cross reference with the Seg-Div or Seg-App row for this child
                foreach($row as $segment => $value)
                {
                    if($segment != 'Element' && strtolower($value) == 'true')
                    {
                        // then add the child in our $returnData array for this segment
                        foreach($returnData as $rkey => $rrow)
                        {
                            if($rrow['code'] == $segment)
                            {
                                if(!in_array($segment, $optionArray))
                                {
                                    if(isset($this->data->seg->isFirst) && ! $this->data->seg->isFirst)
                                    {
                                        $returnData[$rkey]['children'][] = $firstOption;
                                        $optionArray[] = $segment;
                                    }
                                    else
                                    {
                                        $returnData[] = $firstOption;
                                        $optionArray[] = $segment;
                                    }
                                }

                                if(isset($this->data->seg->isFirst) &&  ! $this->data->seg->isFirst)
                                {
                                    $returnData[$rkey]['children'][] = $child;

                                    // add our javascript onchange event
                                    $returnData[$rkey]['js'] = array('onclick' => array('function' => 'reloadSelect'));
                                }
                                else
                                    $returnData[] = $child;
                            }
                        }
                    }
                }
            }
        }
        $this->data->dropdowns = array_merge($this->data->dropdowns, $returnData);
    }

    private function _loadProducts()
    {
        // if our profile doesn't have products, just return
        if(!isset($this->data->profile->x_show_products) || (isset($this->data->profile->x_show_products) && $this->data->profile->x_show_products->code == false))
            return;

        // this array will hold our select data
        $returnData = array();

        $multiple = isset($this->data->multiple->code) ? $this->data->multiple->code : '';

        // this will hold our individual product array
        $type = isset($this->data->profile->config_product_select_type) && $this->data->profile->config_product_select_type->code == 'multi' ? 'checkbox' : 'select';
        $product = array('code' => null, 'value' => null, 'label' => null, 'name' => null, 'shortcode' => '', 'id' => 'x_show_products'.$multiple, 'type' => $type, 'attribute' => 'prod');
        $productLabel = array('code' => null, 'value' => '', 'label' => null, 'name' => null, 'shortcode' => '', 'id' => 'x_show_products'.$multiple, 'type' => $type, 'attribute' => 'prod', 'disabled' => true);

        //check if there are products for this profile
        $productCodeArray = explode("||", $this->data->profile->x_show_products->code);

        // load our products-Group.csv file
        $productGroupArray = $this->loadCSV('Product-Group');

        // load our Elements.csv and get our product from the short-code
        $elementsArray = $this->loadCSV('Elements');

        // load Form-Labels.csv to get the label
        $formLabelsArray = $this->loadCSV('Form-Labels', array('Element', 'en_US', $this->data->lang->value));

        $isFirstOptionLoaded = false;
        $firstOption = null;
        $productLabelsLoaded = array();

        // get our label from $formLabelsArray
        foreach($formLabelsArray as $frow)
        {
            // let's add our first option, if needed
            if($frow['Element'] == 'select_first_dropdown_option' && $type == 'select' && !$isFirstOptionLoaded)
            {
                $firstOption = array(
                    'id'        =>  'x_show_products'.$multiple,
                    'name'      =>  'x_show_products',
                    'code'      =>  'select_first_dropdown_option',
                    'type'      =>  'select',
                    'attribute' =>  'prod',
                    'value'     =>  '',
                    'label'     =>  empty($frow[$this->data->lang->value]) ? $frow['en_US'] : $frow[$this->data->lang->value],
                    'selected'  =>  isset($this->data->prod->override) && $this->data->prod->override == true ? false : true,
                    'disabled'  =>  true,
                    'shortcode' =>  ''
                );
                $isFirstOptionLoaded = true;

                if($this->data->profile->config_product_select_type->code == 'single')
                    $returnData[] = $firstOption;

                if($this->data->profile->config_product_select_type->code == 'multi')
                {
                    foreach($productCodeArray as $productCode)
                    {
                        $firstOption['id'] = 'x_show_products'.$multiple.'_'. $productCode;
                        $firstOption['name'] = 'x_show_products_'. $productCode;
                        $returnData[] = $firstOption;
                    }
                }
            }
        }

        $productsToAdd = array();

        foreach($productCodeArray as $productCode)
        {
            foreach($productGroupArray as $prow)
            {
                // let's construct our product
                if(strtolower($prow[$productCode]) == "true")
                {
                    $productsToAdd[] = array('shortcode' => $prow['Element'], 'productcode' => $productCode);
                }
            }
        }

        // if product is overriden, add it to our productsToAdd array if it's not already there
        if(isset($this->data->prod->override) && $this->data->prod->override == true)
        {
            if(!in_array($this->data->prod->shortcode, $productsToAdd))
                $productsToAdd[] = array('shortcode' => $this->data->prod->shortcode, 'productcode' => $this->data->prod->shortcode);
        }

        foreach($productsToAdd as $prodToAdd)
        {
            $product['shortcode'] = $prodToAdd['shortcode'];
            $product['js'] = array('onclick' => array('function' => 'addProductToEtrigueList', 'params' => $type));
            $product['id'] = $this->data->profile->config_product_select_type->code == 'single' ? 'x_show_products'.$multiple : 'x_show_products'.$multiple.'_'. strtolower($prodToAdd['productcode']);

            $hasProductLabel = false;

            // get our code and values from $elementsArray
            foreach($elementsArray as $erow)
            {
                if($erow['Short-Code'] == $product['shortcode'])
                {
                    $product['code'] = $erow['Element'];
                    $product['value'] = $erow['eTrigue Value'];
                    $product['name'] = $erow['Element'];

                    if($this->data->prod->code == $erow['Element'])
                        $product['selected'] = true;
                    else
                        $product['selected'] = false;
                }

                if($erow['Short-Code'] == $prodToAdd['productcode'])
                {
                    $hasProductLabel = true;

                    $productLabel['code'] = $erow['Element'];
                    $productLabel['name'] = $this->data->profile->config_product_select_type->code == 'single' ? 'x_show_products' : $erow['Element'];
                    $productLabel['id'] = $this->data->profile->config_product_select_type->code == 'single' ? 'x_show_products'.$multiple : 'x_show_products'.$multiple.'_'. strtolower($prodToAdd['productcode']);
                }
            }

            // get our label from $formLabelsArray
            foreach($formLabelsArray as $frow)
            {
                if($frow['Element'] == $product['code'])
                {
                    $product['label'] = empty($frow[$this->data->lang->value]) ? $frow['en_US'] : $frow[$this->data->lang->value];
                }

                if($frow['Element'] == $productLabel['code'] && $hasProductLabel)
                {
                    $label = empty($frow[$this->data->lang->value]) ? $frow['en_US'] : $frow[$this->data->lang->value];
                    $productLabel['label'] = '-----'. $label . '-----';
                }
            }

            // let's add our productlabel, if needed
            if($this->data->profile->config_product_select_type->code == 'single' && !in_array($productLabel['code'], $productLabelsLoaded) && $hasProductLabel)
            {
                $returnData[] = $productLabel;
                $productLabelsLoaded[] = $productLabel['code'];
            }

            $returnData[] = $product;
        }

        $this->data->dropdowns = array_merge($this->data->dropdowns, $returnData);
    }

    private function _loadSolutions() {
        if(!isset($this->data->profile->x_solution) || (isset($this->data->profile->x_solution) && $this->data->profile->x_solution->code == false))
        {
            return;
        }

        // this array will hold our select data
        $returnData = array();
        $solMapArray = $this->loadCSV("Sol-Map");

        foreach($solMapArray as $row) {
            $returnData[] = array(
                'code'      => $row['Element'],
                'segment'   => $this->getSegmentValueFromSegment($row['Mapped Segment']),
                'type'      => 'sol',
                'div'       => $this->getDivValueFromDiv($row['Mapped Division'])
            );
        }

        $this->data->dropdowns = array_merge($this->data->dropdowns, $returnData);
    }

    private function _loadFlags() {

        if(empty($this->data->profile->config_territory_map->code))
            return;

        $this->data->flags = new stdClass();

        $flags = array();

        // load our map profiles
        $territoryArray = $this->loadCSV($this->data->profile->config_territory_map->code);

        // load our languages
        $languageArray = $this->loadCSV('Languages');

        // cross reference each country in the territory array with the language array
        foreach($languageArray as $lang)
        {
            $language = $lang['Element'];

            foreach($territoryArray as $territory)
            {
                if($territory['Level'] == 0) // country
                {
                    $countryCode = $territory['Code'];

                    if(substr_compare($language, $countryCode, -2, 2) == 0 && !in_array($language, $flags)) // there's a match
                    {
                        $flags[] = $language;
                    }
                }
            }
        }

        $this->data->flags->code = implode(",", $flags);
    }

    private function _loadTerritory()
    {
        // if our profile doesn't have territories, just return
        if(!isset($this->data->profile->x_country) || (isset($this->data->profile->x_country) && $this->data->profile->x_country->code == false))
            return;

        $returnArray = array();

        // load our territory map
        $territoryString = $this->data->profile->x_country->code;
        $element = 'Country/Region/Sub-Region';
        $territoryArray = $this->loadCSV($territoryString, array($element, 'Code', 'ISO-3', 'Level'));

        $parentKey = 0;
        $childKey = 0;
        $foundDefaultCountry = false;
        $hasSelectOptionForLevel0 = false;
        $hasSelectOptionForLevel1 = 0;
        $hasSelectOptionForLevel2 = 0;

        $multiple = isset($this->data->multiple->code) ? $this->data->multiple->code : '';

        foreach($territoryArray as $territory)
        {
            foreach($territory as $key => $val)
            {
                $territory[$key] = utf8_encode($val);
            }

            // if this is a territory 0, it's a top level
            switch($territory['Level'])
            {
                case 0:

                    if(!$hasSelectOptionForLevel0)
                    {
                        $returnArray[] = array(
                            'code' => '',
                            'value' => '',
                            'label' => 'Select',
                            'shortcode' => '',
                            'iso2' => '',
                            'iso3' => '',
                            'name' => 'x_country',
                            'id' => 'x_country'.$multiple,
                            'type' => 'select',
                            'selected' => true,
                            'disabled' => true,
                            'js' => array('onclick' => array('function' => 'reloadSelect'))
                        );

                        $hasSelectOptionForLevel0 = true;
                    }


                    if($this->data->country->code == $territory[$element])
                        $foundDefaultCountry = true;

                    // assign the attributes
                    $returnArray[] = array(
                        'code' => $territory[$element],
                        'value' => $territory[$element],
                        'label' => $territory[$element],
                        'shortcode' => $territory['Code'],
                        'iso2' => $territory['Code'],
                        'iso3' => $territory['ISO-3'],
                        'name' => 'x_country',
                        'id' => 'x_country'.$multiple,
                        'type' => 'select',
                        'selected' => $this->data->country->code === $territory[$element] ? true : false,
                        'js' => array('onclick' => array('function' => 'reloadSelect'))
                    );

                    // store the key into $parentKey
                    end($returnArray);
                    $parentKey = key($returnArray);

                    break;

                case 1:

                    if($hasSelectOptionForLevel1 < $parentKey)
                    {
                        $returnArray[$parentKey]['children'][] = array(
                            'code' => '',
                            'value' => '',
                            'label' => 'Select',
                            'shortcode' => '',
                            'iso2' => '',
                            'iso-3' => '',
                            'id' => 'x_state'.$multiple,
                            'type' => 'select',
                            'selected' => true,
                            'disabled' => true
                        );

                        $hasSelectOptionForLevel1 = $parentKey;
                    }

                    // assign the attributes as children of the parent
                    $returnArray[$parentKey]['children'][] = array(
                        'code' => $territory[$element],
                        'value' => $territory['Code'],
                        'label' => $territory[$element],
                        'shortcode' => $territory['Code'],
                        'iso2' => $territory['Code'],
                        'iso3' => $territory['ISO-3'],
                        'id' => 'x_state'.$multiple,
                        'type' => 'select',
                        'selected' => $this->data->state->code == $territory['Code'] ? true : false
                    );

                    // store the key into $childKey
                    end($returnArray[$parentKey]['children']);
                    $childKey = key($returnArray[$parentKey]['children']);

                    break;

                case 2:

                    if($hasSelectOptionForLevel2 < $childKey)
                    {
                        $returnArray[$parentKey]['children'][$childKey]['children'][] = array(
                            'code' => '',
                            'value' => '',
                            'label' => 'Select',
                            'shortcode' => '',
                            'iso2' => '',
                            'iso3' => '',
                            'id' => 'x_city'.$multiple,
                            'type' => 'select',
                            'selected' => true,
                            'disabled' => true
                        );

                        $hasSelectOptionForLevel2 = $childKey;
                    }

                    // assign the attributes as children of the child
                    $returnArray[$parentKey]['children'][$childKey]['children'][] = array(
                        'code' => $territory[$element],
                        'value' => $territory[$element],
                        'label' => $territory[$element],
                        'shortcode' => $territory['Code'],
                        'iso2' => $territory['Code'],
                        'iso3' => $territory['ISO-3'],
                        'id' => 'x_city'.$multiple,
                        'type' => 'select',
                        'selected' => $this->data->city->code == $territory[$element] ? true : false
                    );
            }

        }

        // if we didn't find the default country, set the first country as selected
        if( ! $foundDefaultCountry )
        {
            foreach( $returnArray as $key => $row )
            {
                if( $row['name'] == 'x_country' )
                {
                    $returnArray[$key]['selected'] = true;
                    break;
                }
            }
        }

        $this->data->dropdowns = array_merge($this->data->dropdowns, $returnArray);
    }

    private function _loadSelectGroups()
    {
        $returnArray = array();

        // load our Select-Group.csv file
        $selectGroupArray = $this->loadCSV('Select-Group');

        // load our Elements.csv and to get our code and values
        $elementsArray = $this->loadCSV('Elements');

        // load Form-Labels.csv to get the label
        $formLabelsArray = $this->loadCSV('Form-Labels', array('Element', 'en_US', $this->data->lang->value));

        $multiple = isset($this->data->multiple->code) ? $this->data->multiple->code : '';

        foreach($selectGroupArray as $srow)
        {
            // this will hold our individual select object
            $selectObj = array('code' => null, 'value' => null, 'label' => null, 'shortcode' => null, 'id' => null, 'name' => null, 'type' => 'select', 'attribute' => null, 'selected' => false);

            // now we iterate through the row values to get all the 'TRUE' cells
            foreach($srow as $sKey => $sValue)
            {
                $lowercaseValue = strtolower($sValue);

                if($sKey != 'Element' && ($lowercaseValue == "true" || $lowercaseValue == "override" || $lowercaseValue == "inclusive"))
                {
                    $selectObj['code'] = $srow['Element'];
                    $selectObj['attribute'] = $sKey;

                    if($srow['Element'] === 'select_first_dropdown_option')
                    {
                        $selectObj['disabled'] = true;
                        $selectObj['selected'] = true;
                    }

                    if($lowercaseValue == 'override')
                    {
                        $selectObj['class'] = 'status_type';
                        $selectObj['data'][] = array('name' => 'utility', 'value' => 'override');
                        $selectObj['js'] = array('onclick' => array('function' => 'checkStatus'));
                    }

                    if($lowercaseValue == 'inclusive')
                    {
                        $selectObj['class'] = 'status_type';
                        $selectObj['data'][] = array('name' => 'utility', 'value' => 'inclusive');
                        $selectObj['js'] = array('onclick' => array('function' => 'checkStatus'));
                    }

                    if($sKey == 'select_solution')
                    {
                        $selectObj['js'] = array('onclick' => array('function' => 'loadSolution'));
                    }

                    // get our code and values from $elementsArray
                    foreach($elementsArray as $erow)
                    {
                        if($erow['Short-Code'] == $sKey)
                        {
                            $selectObj['id'] = $erow['Element'].$multiple;
                            $selectObj['name'] = $erow['Element'];
                            $selectObj['shortcode'] = $erow['Short-Code'];
                            $selectObj['type'] = $erow['Type'];
                        }

                        if($erow['Element'] == $srow['Element'])
                        {
                            $selectObj['value'] = $erow['eTrigue Value'];

                            // override default selected option
                            $profileName = $selectObj['id'];
                            if( isset($this->data->profile->$profileName->override)) {
                                if ($this->data->profile->$profileName->override === true &&
                                    mb_strtolower($this->data->profile->$profileName->value) == mb_strtolower($selectObj['value'])
                                ) {
                                    $selectObj['selected'] = 'selected';
                                }
                            }

                        }

                    }

                    // get our label from $formLabelsArray
                    foreach($formLabelsArray as $frow)
                    {
                        if($frow['Element'] == $selectObj['code'])
                        {
                            $selectObj['label'] = empty($frow[$this->data->lang->value]) ? $frow['en_US'] : $frow[$this->data->lang->value];
                            break(1);
                        }
                    }

                    // if the label is blank, set it as the value
                    if(is_null($selectObj['label']))
                    {
                        $selectObj['label'] = $selectObj['value'];
                    }

                    // special case for x_solution: if the value is blank and the label is not, set the value as the label
                    // also check for special case like 'select_solution8||select_solution9||select_solution10'
                    if($selectObj['id'] === 'x_solution'.$multiple)
                    {
                        if(empty($selectObj['value']) && !empty($selectObj['label'])) {
                            $selectObj['value'] = $selectObj['label'];
                        }

                        // check if x_solution is special format (i.e. 'sol_lasers||sol_hds_bim||sol_hds_psg')
                        if(isset($this->data->profile->x_solution) && $this->data->profile->x_solution->code !== true)
                        {
                            $solutionsArray = explode("||", $this->data->profile->x_solution->code);

                            // check the value or Element if it's in the solutionsArray,
                            // if not, invalidate its code and value so it won't be added
                            if(
                                ! in_array($selectObj['value'], $solutionsArray) &&
                                ! in_array($selectObj['code'], $solutionsArray) &&
                                substr($selectObj['value'], 0, 3) !== "---" &&        // kind of a hack
                                $selectObj['code'] !== 'select_first_dropdown_option' // another hack
                            ) {
                                $selectObj = null;
                            }
                        }
                    }

                    // special case for x_udf_extheardof, x_solution and empty values, set the label as dash dividers
                    if(($selectObj['id'] === 'x_udf_extheardof'.$multiple || $selectObj['id'] === 'x_solution'.$multiple) && substr($selectObj['value'], 0, 3) === "---" && $selectObj['code'] !== 'select_first_dropdown_option')
                    {
                        $selectObj['value'] = '';
                        $selectObj['disabled'] = true;
                    }

                    // only add it if the value is not empty
                    if(!is_null($selectObj) && !is_null($selectObj['code']) && !empty($selectObj['value']) || (empty($selectObj['value']) && ($selectObj['id'] === 'x_udf_extheardof' || $selectObj['id'] === 'x_solution')) || $selectObj['code'] === 'select_first_dropdown_option')
                        $returnArray[] = $selectObj;
                }
            }
        }

        //point to end of the array
        end($returnArray);

        //fetch key of the last element of the array.
        $lastElementKey = key($returnArray);

        // let's go through the returnArray and take out empty headers
        foreach($returnArray as $key => $rrow)
        {
            // if this row is disabled (it's a header header)
            if(isset($rrow['disabled']) && $rrow['disabled']) {

                // check the previous key and if it's also a header, remove that header
                if(isset($returnArray[$key - 1]) && isset($returnArray[$key - 1]['disabled']) && $returnArray[$key - 1]['disabled'] && $returnArray[$key - 1]['code'] !== 'select_first_dropdown_option') {
                    unset($returnArray[$key - 1]);
                }

                // if this is the last key and it's a header, remove this row
                if($key == $lastElementKey && $rrow['disabled']) {
                    unset($returnArray[$key]);
                }

            }
        }

        $this->data->dropdowns = array_merge($this->data->dropdowns, $returnArray);
    }

    private function _loadBatchGeoHarvester()
    {
        // let's create an array that will hold the profile variables that will be used
        $profileVariableUsed = array();

        // if x_udf_extheardof is used, add it to our array
        if((isset($this->data->profile->x_udf_extheardof) && $this->data->profile->x_udf_extheardof->code != false))
            $profileVariableUsed['x_udf_extheardof'] = 'select_heard_of';

        // if x_udf_ext_dealer375 is used, add it to our array
        if((isset($this->data->profile->x_udf_ext_dealer375) && $this->data->profile->x_udf_ext_dealer375->code != false))
            $profileVariableUsed['x_udf_ext_dealer375'] = 'select_dealer';

        // if our array is empty, just return
        if(empty($profileVariableUsed))
            return;

        $multiple = isset($this->data->multiple->code) ? $this->data->multiple->code : '';

        // if we've already run this function, it should be stored in session, so use that instead
        if(isset($_SESSION['batchgeo_harvester']))
        {
            $batchGeoArray = json_decode($_SESSION['batchgeo_harvester'], true);
            $this->data->dropdowns = array_merge($this->data->dropdowns, $batchGeoArray);

            if(isset($_REQUEST['resetBatchSession']) && $_REQUEST['resetBatchSession'] === 'true')
            {
                unset($_SESSION['batchgeo_harvester']);
            }

            return;
        }

        //Load the $html into a DOMDocument object
        $map_url = 'http://batchgeo.com/map/kml/187954ad3ffb32715a464513590089b8';

        $returnArray = array();

        if (($response_xml_data = file_get_contents($map_url))===false){
            error_log("Error fetching XML");
        } else {

            libxml_use_internal_errors(true);
            $data = simplexml_load_string($response_xml_data);

            if (!$data) {
                error_log("Error fetching XML");

                foreach(libxml_get_errors() as $error) {
                    error_log("\t", $error->message);
                }
            } else {

                foreach($profileVariableUsed as $profileVariable => $placeholderName)
                {
                    // run the code below if this is our first time running it
                    $selectObj = array('code' => null, 'value' => null, 'label' => null, 'shortcode' => null, 'id' => $profileVariable.$multiple, 'type' => 'select', 'attribute' => $placeholderName);

                    // let's iterate through our Placemarks
                    if(isset($data->Document->Placemark))
                    {
                        foreach($data->Document->Placemark as $count => $placemark)
                        {
                            $businessName = isset($placemark->name) ? trim($placemark->name) : '';
                            $country = '';
                            $state = '';

                            foreach($placemark->ExtendedData->Data as $data)
                            {
                                if(strtolower((string) $data['name']) == 'country')
                                    $country = (string) $data->value;

                                if(strtolower((string) $data['name']) == 'state/prov')
                                    $state = (string) $data->value;
                            }
/*
                            if(isset($placemark->description))
                            {
                                $dom = new DOMDocument();
                                $dom->loadHTML($placemark->description);

                                // load xpath
                                $xpath = new DOMXPath($dom);

                                // get our <div id="data">
                                $dataTabs = $xpath->query("//div//text()");

                                foreach ( $dataTabs as $counter => $dataTabNode ) {

                                    // look for the string 'Country:'. If found, country should be the next string
                                    if($dataTabNode->nodeValue == 'Country:' && isset($dataTabs->item($counter + 1)->nodeValue))
                                        $country = trim($dataTabs->item($counter + 1)->nodeValue);

                                    // look for the string 'State/Prov:'. If found, country should be the next string
                                    if($dataTabNode->nodeValue == 'State/Prov:' && isset($dataTabs->item($counter + 1)->nodeValue))
                                        $state = trim($dataTabs->item($counter + 1)->nodeValue);
                                }
                            }
*/
                            $value = $country;
                            $value .= !empty($state) ? ', '. $state : $state;
                            $value .= !empty($country) ? ': ' . $businessName : $businessName;


                            $selectObj['code'] = $placeholderName .'_batchgeo_'. $count;
                            $selectObj['value'] = $value;
                            $selectObj['label'] = $value;
                            $selectObj['attribute'] = $placeholderName .'_batchgeo';

                            $returnArray[] = $selectObj;

                        }
                    }
                }
            }

        }

        //store our batchgeo data in session for later use
        if(!isset($_REQUEST['resetBatchSession']))
        {
            $_SESSION['batchgeo_harvester'] = json_encode($returnArray);
        }

        if(isset($_REQUEST['resetBatchSession']) && $_REQUEST['resetBatchSession'] === 'true')
        {
            unset($_SESSION['batchgeo_harvester']);
        }

        // merge it with our dropdowns
        $this->data->dropdowns = array_merge($this->data->dropdowns, $returnArray);

        return $returnArray;
    }

    /**
     * Create back url (used in javascript code)
     *
     * @return string
     */
    private function _createBackUrl()
    {
        $back = 'javascript:history.back()'; // default

        // if back var was supplied, use the back variable
        if(isset($_GET['back']) && !empty($_GET['back'])) {
            $back = $_GET['back'];
        }

        // from the form, if no back var was supplied, use a history.back(-1)
        else if((!isset($_GET['back']) || ( isset($_GET['back']) && empty($_GET['back']))) && (!isset($_GET['status']) || (isset($_GET['status']) && $_GET['status'] !== 'confirmation'))) {
            $back = 'javascript:history.back()';
        }

        // From the CONFIRMATION page, if no back var was supplied,  use a history.back(-2)
        else if((!isset($_GET['back']) || ( isset($_GET['back']) && empty($_GET['back']))) && isset($_GET['status']) && $_GET['status'] === 'confirmation')
        {
           $back = 'javascript:history.back(-2)';
        }

        else if (isset($_SERVER['HTTP_REFERER'])) {
            if (!strstr($_SERVER['HTTP_REFERER'], $_SERVER['PHP_SELF'])) {
                $ref_array=explode("?",$_SERVER['HTTP_REFERER']);
                $back = $ref_array[0];
                $_GET['back'] = $back;  // Reverse-define so it's picked up by the override detection, below
            }
        }

        return $back;
    }

    /**
     * Just instantiates objects properly to get rid of php warnings with having to supress them
     */
    private function _setDefaultObjects()
    {
        $objectArray = array('querystring', 'confirmation', 'back', 'form', 'lang', 'country', 'state', 'city', 'owner', 'seg', 'div', 'app', 'orig', 'prof', 'prod', 'map', 'profile', 'status', 'crm', 'gatracking', 'adwords', 'ct', 'notification_sms', 'notification_email', 'responder_email');
        foreach($objectArray as $o)
        {
            $this->data->$o = new stdClass();

            if($o !== 'profile')
                $this->data->$o->type = $o;
        }
    }

    private function _loadCommonVars($form)
    {
        if( $form instanceof Forms )
        {
            $this->data->confirmation->code = false;
            $this->data->form->code = $form->form_id;
            $this->data->lang->code = $form->x_udf_form_language302;
            $this->data->country->code = $form->x_country;
            $this->data->state->code = $form->x_state;
            $this->data->city->code = $form->x_city;
            $this->data->owner->code = $form->x_udf_form_owner148;
        }
        else
        {
            // Load common vars that are reused
            $this->data->confirmation->code = isset($_GET['status']) && $_GET['status']=="confirmation" ? true : false;
            $this->data->form->code = isset($_POST['form']) ? $_POST['form'] : (isset($_GET['form']) ? $_GET['form'] : false );
            $this->data->lang->code = isset($_POST['lang']) ? $_POST['lang'] : (isset($_GET['lang']) ? $_GET['lang'] : false);
            $this->data->country->code = isset($_POST['x_country']) ? $_POST['x_country'] : (isset($_GET['x_country']) ? $_GET['x_country'] : Utility::getDefaultCountry($_SERVER['REMOTE_ADDR']));
            $this->data->state->code = isset($_POST['x_state']) ? $_POST['x_state'] : (isset($_GET['x_state']) ? $_GET['x_state'] : "");
            $this->data->city->code = isset($_POST['x_city']) ? $_POST['x_city'] : (isset($_GET['x_city']) ? $_GET['x_city'] : "");
            $this->data->owner->code = "FALSE";
        }

        // Create back URL
        $this->data->back->code = $this->_createBackUrl();
    }

    /**
     * @return string
     *
     * This is the form html output produced by the Nexus
     */
    public function render($multiple = '')
    {
        // create our new form object
        $formId = "etrReg{$multiple}";
        $form = new Form($formId);

        // let's set the name attribute
        $form->setAttribute('name', $formId);

        // let's give it the class 'nexus'
        $form->setAttribute('class', 'nexus');

        // set the action attribute (blank because we're using js to submit the form)
        $form->setAttribute('action', '');

        // we'll prevent loading of these js scripts
        $form->configure(array(
            "prevent" => array("bootstrap", "focus", "font_awesome", "awesome_bootstrap_checkbox")
        ));

        // set the form id as a hidden field
        $form->addElement(new Element_Hidden("espFormID", $this->data->form->code));

        // This condition is to check if the core was simply reloaded as part of the confirmation routine (e.g., "Thank You" page)
        // If false, generate the form as normal - else print out only the postamble verbiage
        if (! $this->data->confirmation->code)
        {
            $hasPreamble = false;
            $hasFormList = false;
            $hasFieldSet = false;
            $segmentSelect = null;

            foreach ($this->data->profile as $kname => $element)
            {
                // Redefine the elements eThank you for your interest in each loop, grabbing the value & label from the proper profile
                // and swap out any [[var]] instances for both the value and label
                $name = $kname;  // e.g., "x_country"
                $element->value = $this->insertStrings($element->value);  // e.g., "FALSE" or "country_auto_all"
                $element->label = $this->insertStrings($element->label);
                if(isset($element->type)){ $element->type = $this->insertStrings($element->type); } else{ $element->type = null; }
                if ($element->value == "FALSE" || empty($element->value)) {$element->value = false;}

                // If this element is in the array of required fields, then set the label prefix
                $isRequired = $this->isRequired($name) ? true : false;
                $input_class = $isRequired ? "required-entry" : "";

                switch($element->type)
                {
                    // config elements
                    case 'form-config':

                        // Print out any preamble strings
                        if (strpos($name, "config_preamble") !== FALSE) {
                            if ($element->code) {
                                $form->addElement(new Element_HTML("<div class='form-preamble " . $name . "'>" . $element->label . "</div>"));
                            }
                        }
                        // Legend (if applicable) & start of containing div
                        if (strpos($name, "config_legend") !== FALSE) {

                            if(!$hasFieldSet)
                            {
                                $form->addElement(new Element_HTML("<div class='fieldset'>"));
                                $hasFieldSet = true;
                            }

                            if ($element->code)
                                $form->addElement(new Element_HTML("<h2 class='legend'>". $element->label ."</h2>"));
                        }

                        if(strpos($name, "config_language") !== FALSE)
                        {
                            if($element->code)
                            {
                                $languageFlags = isset($this->data->flags->code) ? $this->data->flags->code : 'all';

                                $form->configure(array('include' => array('jQueryUI')));

                                $form->addElement(new Element_HTML("<div class='header-languages' style='float:right;margin:-52px 0 0 0;text-align:right;
    background-color: #fff;padding: 0 15px;'>"));
                                $form->addElement(new Element_HTML($this->getFlags($languageFlags)));
                                $form->addElement(new Element_HTML("</div>"));
                            }
                        }

                        break;

                    // Hidden elements
                    case 'form-hidden' :

                        // config is done. Start our formlist
                        if(!$hasFormList){
                            $form->addElement(new Element_HTML("<ul class='form-list'>"));
                            $hasFormList = true;
                        }

                        if ($element->code)
                        {
                            $form->addElement( new Element_Hidden( $name, $element->value, array('id' => $name . $multiple) ) );
                        }

                        break;

                    case 'form-input-text' :
                    case 'form-input-pass' :

                        // Start the field container, and print out the label
                        if ($element->code) {

                            $form->addElement(new Element_HTML("<li class='fields' id='".$name . $multiple."_fieldpair'>"));

                            $form->addElement(new Element_Textbox($element->label, $name, array(
                                'id' => $name . $multiple,
                                'class' => $input_class . ' input-text',
                                'value' => $element->value,
                                'title' => $element->label,
                                "required" => $isRequired,
                                "autocomplete" => "off"
                            )));

                            $form->addElement(new Element_HTML("</li>"));
                        }

                        break;

                    case 'form-input-select' :
                    case 'form-input-special' :

                        if ($element->code) {

                            $form->addElement(new Element_HTML("<li class='fields' id='".$name . $multiple."_fieldpair'>"));

                            // add the select fields
                            $form->addElement(new Element_Select(
                                    $element->label,
                                    $name,
                                    array(),
                                    array
                                    (
                                        "class" => $input_class,
                                        "id" => $name . $multiple,
                                        'value' => $element->value,
                                        "title" => $element->label,
                                        "required" => $isRequired
                                    )
                                )
                            );

                            $form->addElement(new Element_HTML("</li>"));
                        }

                        break;

                    // special case for owners select field
                    case 'form-select-owners' :

                        if ($element->code) {

                            $form->addElement(new Element_HTML("<li class='fields' id='".$name . $multiple."_fieldpair'>"));

                            // add the select fields
                            $form->addElement(new Element_Select(
                                    $element->label,
                                    $name,
                                    $this->_getOwnersWithOrigin($this->data->orig->code),
                                    array
                                    (
                                        "class" => $input_class,
                                        "id" => $name . $multiple,
                                        'value' => $element->value,
                                        "title" => $element->label,
                                        "required" => $isRequired
                                    )
                                )
                            );

                            $form->addElement(new Element_HTML("</li>"));

                        }

                        break;

                    case 'form-input-textarea' :
                        if ($element->code) {

                            $form->addElement(new Element_HTML("<li class='fields' id='".$name . $multiple."_fieldpair'>"));

                            $form->addElement(new Element_Textarea($element->label, $name, array(
                                "id" => $name . $multiple,
                                'class' => $input_class,
                                'value' => $element->value,
                                'title' => $element->label,
                                "required" => $isRequired
                            )));

                            $form->addElement(new Element_HTML("</li>"));
                        }
                        break;

                    case 'form-input-checkbox' :
                    case 'form-input-checkbox-multi' :
                        if ($element->code) {

                            $tmpId = $name == 'x_udf_ext_hds_question_cyclone451__ms'
                                || $name == 'x_udf_ext_hds_question_cyclone_2452__ms'
                                || $name == 'x_udf_area_of_interest490'
                                ? $name . $multiple : $name . $multiple."_fieldpair";

                            $tmpLabel = '';
                            $tmpLabel = $name == 'x_udf_ext_hds_question_cyclone451__ms' ? 'What module(s) do you need? (select all that apply):' : $tmpLabel;
                            $tmpLabel = $name == 'x_udf_ext_hds_question_cyclone_2452__ms' ? 'What type of work do you need to do? (select all that apply):' : $tmpLabel;
                            $tmpLabel = $name == 'x_udf_area_of_interest490' ? 'Select your areas of interest:' : $tmpLabel;

                            if($name == 'x_udf_ext_hds_question_cyclone451__ms' ||
                                 $name == 'x_udf_ext_hds_question_cyclone_2452__ms' ||
                                 $name == 'x_udf_area_of_interest490') {
                                $form->addElement(new Element_HTML("<li class='fields' id='".$tmpId."'><label id='".$tmpId."_label' class='control-label'>". $tmpLabel ."</label></li>"));
                            }
                            else {
                                $form->addElement(new Element_HTML("<li class='fields' id='".$tmpId."'>"));
                                $form->addElement(new Element_Checkbox(
                                    $element->label,
                                    $name,
                                    array($element->value => ''),
                                    array(
                                        'id' => $name . $multiple,
                                        'class' => $input_class . ' checkbox',
                                        'value' => $element->value,
                                        'title' => '',
                                        "required" => $isRequired
                                    )
                                ));
                                $form->addElement(new Element_HTML("</li>"));
                            }
                        }

                        break;

                    // this is a date field
                    case 'form-input-date':

                        if ($element->code) {

                            $form->addElement(new Element_HTML("<li class='fields' id='".$name . $multiple."_fieldpair'>"));

                            $form->addElement(new Element_jQueryUIDate($element->label, $name, array(
                                "id" => $name . $multiple,
                                'class' => $input_class,
                                'value' => $element->value,
                                'title' => $element->label,
                                "required" => $isRequired
                            )));

                            $form->addElement(new Element_HTML("</li>"));
                        }

                        break;

                    // this is a special field for the product checkboxes
                    case 'product-group':

                        if ($element->code) {

                            // there could be multiple labels for this product in a pipeline-delimited string
                            $productLabelArray = explode("||", $element->label);
                            $productCode = explode("||", $element->code);
                            $containerType = $this->data->profile->config_product_select_type->code;

                            switch($containerType)
                            {
                                case 'multi' :

                                    $form->addElement(new Element_HTML("<li class='fields' id='".$name."_fieldpair'>"));

                                    foreach($productLabelArray as $key => $productLabel)
                                    {
                                        $form->addElement(new Element_HTML("
                                <div id='{$name}{$multiple}_". $productCode[$key] ."_container'>
                                    <div style='vertical-align: top; display: inline-block;'>
                                        <label id='{$name}{$multiple}_label' class='control-label'>{$productLabel}</label>
                                    </div>
                                    <div id ='{$name}{$multiple}_{$productCode[$key]}' name='{$name}' style='display: inline-block;'></div>
                                </div>"
                                        ));
                                    }

                                    $form->addElement(new Element_HTML("</li>"));

                                    break;

                                case 'group' :

                                    foreach($productLabelArray as $key => $productLabel)
                                    {
                                        $form->addElement(new Element_HTML("<li class='fields' id='{$name}{$multiple}_". $productCode[$key] ."_fieldpair'>"));

                                        $form->addElement(new Element_HTML("
                                        <label id='".$name . $multiple."_{$productCode[$key]}_label' class='control-label'>{$productLabel}</label>
                                        <select id ='{$name}{$multiple}_{$productCode[$key]}' name='{$name}_{$productCode[$key]}' style='display: inline-block;'></select>
                                "
                                        ));
                                    }

                                    break;

                                case 'single' :

                                    $form->addElement(new Element_HTML("<li class='fields' id='".$name . $multiple."_fieldpair'>"));

                                    $form->addElement(new Element_HTML("
                                    <label id='".$name . $multiple."_label' class='control-label' for='{$name}'>Product</label>
                                    <select id ='{$name}{$multiple}' name='{$name}' style='display: inline-block;'></select>"
                                    ));

                                    $form->addElement(new Element_HTML("</li>"));

                                    break;
                            }
                        }

                        break;

                    case 'form-button' :

                        // There must be exactly one button, and it must be the last visible element - this condition completes the containing divs started in the legend.
                        $form->addElement(new Element_HTML("</ul></div><div class='buttons-set'>"));

                        if (isset($this->data->profile->config_required->code) && $this->data->profile->config_required->code) {
                            $form->addElement(new Element_HTML("<p class='required'>* ".$this->data->profile->config_required_list->label."</p>"));
                        }

                        if (isset($this->data->profile->config_back->code) && $this->data->profile->config_back->code) {
                            $form->addElement(new Element_HTML("<p class='back-link'><a href='". $this->data->profile->config_back->value ."' class='back-link'><small>&laquo; </small>".$this->data->profile->config_back->label."</a></p>"));
                        }

                        $form->addElement(new Element_HTML("<button type='button' value='".$element->label."' title='".$element->label."' class='button' id='etrSubmit". $multiple ."'><span><span>".$element->label."</span></span></button>"));
                        $form->addElement(new Element_HTML("<div id='form-results{$multiple}' class='form-results'>&nbsp;</div>"));
                        break;

                    default:
                        break;

                } // end switch

            } // end foreach

        } else {

            // As mentioned above, this part only runs if we have a 'confirmation' status
            foreach ($this->data->profile as $kname => $element) {

                $name = $kname;
                $element->value = $this->insertStrings($element->value);  // e.g., "FALSE" or "country_auto_all"
                $element->label = $this->insertStrings($element->label);
                $element->type = $this->insertStrings($element->type);
                if ($element->value == "FALSE" || empty($element->value)) {$element->value = false;}

                // Print out any postamble strings
                if ($element->code && (strpos($name, "config_postamble") !== FALSE || strpos($name, "config_success") !== FALSE)) {
                    $form->addElement(new Element_HTML("<div class='form-postamble'>".$element->label."</div>"));
                }
            }
        }

        return $form->render(true);
    }

    /**
     * @return string
     *
     * This is the bootstrap form html output produced by the Nexus
     */
    public function renderBootstrap($multiple = '')
    {
        // create our new form object
        $formId = "etrReg{$multiple}";
        $form = new Form($formId);

        $form->configure(array(
            "view" => new View_Vertical
        ));

        // let's set the name attribute
        $form->setAttribute('name', $formId);

        // let's give it the class 'nexus'
        $form->setAttribute('class', 'nexus');

        // let's set the role attribute
        $form->setAttribute('role', 'form');

        // set the action attribute (blank because we're using js to submit the form)
        $form->setAttribute('action', '');

        // set the form id as a hidden field
        $form->addElement(new Element_Hidden("espFormID", $this->data->form->code));

        // bootstrap columns
        $col = isset($_GET['col']) && is_numeric($_GET['col']) ?  intval($_GET['col']) : 8;
        $labelcol = 12 - $col;

        // This condition is to check if the core was simply reloaded as part of the confirmation routine (e.g., "Thank You" page)
        // If false, generate the form as normal - else print out only the postamble verbiage
        if (! $this->data->confirmation->code)
        {
            $hasPreamble = false;
            $hasFormList = false;
            $hasFieldSet = false;
            $segmentSelect = null;

            foreach ($this->data->profile as $kname => $element)
            {
                // Redefine the elements eThank you for your interest in each loop, grabbing the value & label from the proper profile
                // and swap out any [[var]] instances for both the value and label
                $name = $kname;  // e.g., "x_country"
                $element->value = isset($element->value) ? $this->insertStrings($element->value) : '';  // e.g., "FALSE" or "country_auto_all"
                $element->label = isset($element->label) ? $this->insertStrings($element->label) : '';
                $element->type = isset($element->type) ? $this->insertStrings($element->type) : '';
                if ($element->value == "FALSE" || empty($element->value)) {$element->value = false;}

                // If this element is in the array of required fields, then set the label prefix
                $isRequired = $this->isRequired($name) ? true : false;
                $input_class = $isRequired ? "required-entry form-control" : "form-control";

                switch($element->type)
                {
                    // config elements
                    case 'form-config':

                        // Print out any preamble strings
                        if (strpos($name, "config_preamble") !== FALSE) {
                            if ($element->code) {
                                $form->addElement(new Element_HTML("<div class='form-preamble " . $name . "'>" . $element->label . "</div>"));
                            }
                        }
                        // Legend (if applicable) & start of containing div
                        if (strpos($name, "config_legend") !== FALSE) {

                            if(!$hasFieldSet)
                            {
                                $form->addElement(new Element_HTML("<div class='fieldset'>"));
                                $hasFieldSet = true;
                            }

                            if ($element->code)
                                $form->addElement(new Element_HTML("<h2 class='legend'>". $element->label ."</h2>"));
                        }

                        if(strpos($name, "config_language") !== FALSE)
                        {
                            if($element->code)
                            {
                                $languageFlags = isset($this->data->flags->code) ? $this->data->flags->code : 'all';

                                $form->addElement(new Element_HTML("<div class='header-languages' style='float:right;margin:-52px 0 0 0;text-align:right;
    background-color: #fff;padding: 0 15px;'>"));
                                $form->addElement(new Element_HTML($this->getFlags($languageFlags)));
                                $form->addElement(new Element_HTML("</div>"));
                            }
                        }

                        break;

                    // Hidden elements
                    case 'form-hidden' :

                        if ($element->code)
                        {
                            $form->addElement( new Element_Hidden( $name, $element->value, array('id' => $name . $multiple) ) );
                        }

                        break;

                    case 'form-input-text' :
                    case 'form-input-pass' :

                        // Start the field container, and print out the label
                        if ($element->code) {

                            // div form-group
                            $form->addElement(new Element_HTML("<div id='".$name.$multiple."_fieldpair' class='form-group col-md-{$col}'>"));

                            // label
                            // if required
                            if($isRequired) {
                                $form->addElement(new Element_HTML("<label id='".$name.$multiple."_label' class='control-label required'>". $element->label ."<em class='label_em' style='color:#eb340a;'>* </em></label>"));
                            }
                            else {
                                $form->addElement(new Element_HTML("<label id='".$name.$multiple."_label' class='control-label'>". $element->label ."</label>"));

                            }

                            // input
                            $form->addElement(new Element_Textbox(null, $name, array(
                                'id' => $name . $multiple,
                                'value' => $element->value,
                                'title' => $element->label,
                                "class" => $input_class,
                                "required" => $isRequired,
                                "autocomplete" => "off"
                            )));

                            // close divs
                            $form->addElement(new Element_HTML("</div>"));
                        }

                        break;

                    case 'form-input-select' :
                    case 'form-input-special' :

                        if ($element->code) {

                            // div form-group
                            $form->addElement(new Element_HTML("<div id='".$name.$multiple."_fieldpair' class='form-group col-md-{$col}'>"));

                            // label
                            // if required
                            if($isRequired) {
                                $form->addElement(new Element_HTML("<label id='".$name.$multiple."_label' class='control-label required'>". $element->label ."<em class='label_em' style='color:#eb340a;'>* </em></label>"));
                            }
                            else {
                                $form->addElement(new Element_HTML("<label id='".$name.$multiple."_label' class='control-label'>". $element->label ."</label>"));
                            }

                            // add the select fields
                            $form->addElement(new Element_Select(
                                    null,
                                    $name,
                                    array(),
                                    array
                                    (
                                        "class" => $input_class,
                                        "id" => $name . $multiple,
                                        'value' => $element->value,
                                        "title" => $element->label,
                                        "required" => $isRequired
                                    )
                                )
                            );

                            // close divs
                            $form->addElement(new Element_HTML("</div>"));
                        }

                        break;

                    // special case for owners select field
                    case 'form-select-owners' :

                        if ($element->code) {

                            // div form-group
                            $form->addElement(new Element_HTML("<div id='".$name.$multiple."_fieldpair' class='form-group col-md-{$col}'>"));

                            // label
                            // if required
                            if($isRequired) {
                                $form->addElement(new Element_HTML("<label id='".$name.$multiple."_label' class='control-label required'>". $element->label ."<em class='label_em' style='color:#eb340a;'>* </em></label>"));
                            }
                            else {
                                $form->addElement(new Element_HTML("<label id='".$name.$multiple."_label' class='control-label'>". $element->label ."</label>"));
                            }

                            // add the select fields
                            $form->addElement(new Element_Select(
                                    null,
                                    $name,
                                    $this->_getOwnersWithOrigin($this->data->orig->code),
                                    array
                                    (
                                        "class" => $input_class,
                                        "id" => $name . $multiple,
                                        'value' => $element->value,
                                        "title" => $element->label,
                                        "required" => $isRequired
                                    )
                                )
                            );

                            // close divs
                            $form->addElement(new Element_HTML("</div>"));
                        }

                        break;

                    case 'form-input-textarea' :
                        if ($element->code) {

                            // div form-group - For any .form-group that may text area field,
                            // please default these containers to col-md-12 regardless of set col variable
                            $form->addElement(new Element_HTML("<div id='".$name.$multiple."_fieldpair' class='form-group col-md-12'>"));

                            // label
                            // if required
                            if($isRequired) {
                                $form->addElement(new Element_HTML("<label id='".$name.$multiple."_label' class='control-label required'>". $element->label ."<em class='label_em' style='color:#eb340a;'>* </em></label>"));
                            }
                            else {
                                $form->addElement(new Element_HTML("<label id='".$name.$multiple."_label' class='control-label'>". $element->label ."</label>"));
                            }

                            $form->addElement(new Element_Textarea(null, $name, array(
                                "id" => $name . $multiple,
                                'class' => $input_class,
                                'value' => $element->value,
                                'title' => $element->label,
                                "required" => $isRequired
                            )));

                            // close divs
                            $form->addElement(new Element_HTML("</div>"));
                        }
                        break;

                    case 'form-input-checkbox' :
                    case 'form-input-checkbox-multi' :

                        if ($element->code) {

                            $tmpId = $name == 'x_udf_ext_hds_question_cyclone451__ms'
                            || $name == 'x_udf_ext_hds_question_cyclone_2452__ms'
                            || $name == 'x_udf_area_of_interest490'
                                ? $name . $multiple : $name . $multiple."_fieldpair";

                            $additional_class = $name == 'x_udf_privacy_policy_version143' ? 'checkbox-success' : '';

                            // div form-group
                            $form->addElement(new Element_HTML("<div id='".$tmpId."_fieldpair'><div id='".$tmpId."' class='form-group col-md-12 checkbox ". $additional_class ."'>"));

                            $tmpLabel = '';
                            $tmpLabel = $name == 'x_udf_ext_hds_question_cyclone451__ms' ? 'What module(s) do you need? (select all that apply):' : $tmpLabel;
                            $tmpLabel = $name == 'x_udf_ext_hds_question_cyclone_2452__ms' ? 'What type of work do you need to do? (select all that apply):' : $tmpLabel;
                            $tmpLabel = $name == 'x_udf_area_of_interest490' ? 'Select your areas of interest:' : $tmpLabel;

                            if($name != 'x_udf_ext_hds_question_cyclone451__ms' &&
                                $name !== 'x_udf_ext_hds_question_cyclone_2452__ms' &&
                                $name !== 'x_udf_area_of_interest490') {

                                $form->addElement(new Element_Checkbox(
                                    null,
                                    $name,
                                    array($element->value => ''),
                                    array(
                                        'id' => $name . $multiple,
                                        'class' => 'checkbox',
                                        'value' => $element->value,
                                        'title' => '',
                                        "required" => $isRequired
                                    )
                                ));

                                // label, if required
                                if($isRequired) {
                                    $form->addElement(new Element_HTML("<label id='".$name.$multiple."_label' for='". $name.$multiple ."' class='control-label required'><em class='label_em' style='color:#eb340a;'>* </em>".$element->label."</label>"));
                                }
                                else {
                                    $form->addElement(new Element_HTML("<label id='".$name.$multiple."_label' for='". $name.$multiple ."' class='control-label'>".$element->label."</label>"));
                                }
                            }
                            else {
                                $form->addElement(new Element_HTML("<div id='".$name.$multiple."_label'>".$tmpLabel."</div>"));
                            }

                            // close divs
                            $form->addElement(new Element_HTML("</div></div>"));

                        }

                        break;

                    // this is a date field
                    case 'form-input-date':

                        if ($element->code) {

                            $form->addElement(new Element_HTML("<div id='".$name.$multiple."_fieldpair' class='form-group col-md-{$col}'>"));

                            // label
                            // if required
                            if($isRequired) {
                                $form->addElement(new Element_HTML("<label id='".$name.$multiple."_label' class='control-label required'>". $element->label ."<em class='label_em' style='color:#eb340a;'>* </em></label>"));
                            }
                            else {
                                $form->addElement(new Element_HTML("<label id='".$name.$multiple."_label' class='control-label'>". $element->label ."</label>"));

                            }



                            $form->addElement(new Element_jQueryUIDate(null, $name, array(
                                "id" => $name . $multiple,
                                'class' => $input_class,
                                'value' => $element->value,
                                'title' => $element->label,
                                "required" => $isRequired
                            )));

                            $form->addElement(new Element_HTML("</div>"));
                        }

                        break;

                    // this is a special field for the product checkboxes
                    case 'product-group':

                        if ($element->code) {

                            // there could be multiple labels for this product in a pipeline-delimited string
                            $productLabelArray = explode("||", $element->label);
                            $productCode = explode("||", $element->code);
                            $containerType = $this->data->profile->config_product_select_type->code == 'multi' ? 'div' : 'select';

                            switch($this->data->profile->config_product_select_type->code)
                            {
                                case 'multi' :

                                    $form->addElement(new Element_HTML("<div class='fields' id='".$name . $multiple."_fieldpair'>"));

                                    foreach($productLabelArray as $key => $productLabel)
                                    {
                                        $form->addElement(new Element_HTML("
                                <div id='{$name}{$multiple}_". $productCode[$key] ."_container'>
                                    <div style='vertical-align: top; display: inline-block;'>
                                        <label id='{$name}{$multiple}_label' class='control-label'>{$productLabel}</label>
                                    </div>
                                    <div id ='{$name}{$multiple}_{$productCode[$key]}' name='{$name}' style='display: inline-block;' class='col-md-{$col}'></div>
                                </div>"
                                        ));
                                    }

                                    $form->addElement(new Element_HTML("</div>"));

                                    break;

                                case 'group' :

                                    foreach($productLabelArray as $key => $productLabel)
                                    {
                                        // div form-group
                                        $form->addElement(new Element_HTML("<div id='{$name}{$multiple}_". $productCode[$key] ."_fieldpair' class='form-group col-md-{$col}'>"));

                                        // label
                                        // if required
                                        if($isRequired) {
                                            $form->addElement(new Element_HTML("<label id='".$name.$multiple."_{$productCode[$key]}_label' class='control-label required'>{$productLabel}<em class='label_em' style='color:#eb340a;'>* </em></label>"));
                                        }
                                        else {
                                            $form->addElement(new Element_HTML("<label id='".$name.$multiple."_{$productCode[$key]}_label' class='control-label'>{$productLabel}</label>"));
                                        }

                                        $form->addElement(new Element_HTML("<select id ='{$name}{$multiple}_{$productCode[$key]}' name='{$name}_{$productCode[$key]}' style='display: inline-block;'></select>"));

                                        // close divs
                                        $form->addElement(new Element_HTML("</div>"));
                                    }

                                    break;

                                case 'single' :

                                    // div form-group
                                    $form->addElement(new Element_HTML("<div id='".$name.$multiple."_fieldpair' class='form-group col-md-{$col}'>"));

                                    // label
                                    // if required
                                    if($isRequired) {
                                        $form->addElement(new Element_HTML("<label id='".$name.$multiple."_label' class='control-label required'>". $element->label ."<em class='label_em' style='color:#eb340a;'>* </em></label>"));
                                    }
                                    else {
                                        $form->addElement(new Element_HTML("<label id='".$name.$multiple."_label' class='control-label'>". $element->label ."</label>"));
                                    }

                                    $form->addElement(new Element_HTML("<select id ='{$name}{$multiple}' name='{$name}' class='form-control'></select>"));

                                    // close divs
                                    $form->addElement(new Element_HTML("</div>"));

                                    break;
                            }
                        }

                        break;

                    case 'form-button' :

                        // There must be exactly one button, and it must be the last visible element - this condition completes the containing divs started in the legend.
                        $form->addElement(new Element_HTML("</div>"));


                        if (isset($this->data->profile->config_required->code) && $this->data->profile->config_required->code) {
                            $form->addElement(new Element_HTML("<p class='required'>* ".$this->data->profile->config_required_list->label."</p>"));
                        }

                        $form->addElement(new Element_HTML("<div class='buttons-set col-md-12'>"));

                        if (isset($this->data->profile->config_back->code) && $this->data->profile->config_back->code) {
                            $form->addElement(new Element_HTML("<p class='back-link'><a href='". $this->data->profile->config_back->value ."' class='back-link'><small>&laquo; </small>".$this->data->profile->config_back->label."</a></p>"));
                        }

                        $form->addElement(new Element_HTML("<button type='button' value='".$element->label."' title='".$element->label."' class='button btn btn-danger' onClick='nexus". $multiple .".checkData();' id='etrSubmit". $multiple ."'><span><span>".$element->label."</span></span></button>"));
                        $form->addElement(new Element_HTML("<div id='form-results{$multiple}' class='form-results'>&nbsp;</div>"));
                        break;

                    default:
                        break;

                } // end switch

            } // end foreach

        } else {

            // As mentioned above, this part only runs if we have a 'confirmation' status
            foreach ($this->data->profile as $kname => $element) {

                $name = $kname;
                $element->value = $this->insertStrings($element->value);  // e.g., "FALSE" or "country_auto_all"
                $element->label = $this->insertStrings($element->label);
                $element->type = $this->insertStrings($element->type);
                if ($element->value == "FALSE" || empty($element->value)) {$element->value = false;}

                // Print out any postamble strings
                if ($element->code && (strpos($name, "config_postamble") !== FALSE || strpos($name, "config_success") !== FALSE)) {
                    $form->addElement(new Element_HTML("<div class='form-postamble'>".$element->label."</div>"));
                }
            }
        }

        return $form->render(true);
    }
}
