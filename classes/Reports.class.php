<?php
/**
 * Created by JetBrains PhpStorm.
 * Author: Joseph Santos
 * Date: 5/26/14
 * Time: 1:07 PM
 * Description: 
 */

class Reports extends NexusBase{

    const DATABASE_TABLE = 'form_submissions';

    private $allowedOrigins;

    private $exclude;

    /**
     *  Constructor
     */
    public function __construct()
    {
        parent::__construct();

        // we're going to exclude these from our search
        $this->exclude = array(
            'email'             =>  array('test%', 'JarebTest%', 'JosephTest%', '""'),
            'first_name'        =>  array('test%', 'JarebTest%', 'JosephTest%', '""'),
            'last_name'         =>  array('test%', 'JarebTest%', 'JosephTest%', '""'),
            'posted_data'       =>  array('""','[]')
        );

        $this->allowedOrigins = array(
            'nexus.leica-geosystems.us' => array('allow' => array('ape', 'leicaus', 'microsurvey'), 'default' => 'leicaus'),
            'nexus.microsurvey.com' => array('deny' => array(), 'default' => ' microsurvey'),
            'nexus.microsurvey.local' => array('deny' => array(), 'default' => 'microsurvey')
        );
    }

    private function _loadOrigins()
    {
        $elements = $this->loadCSV('Elements');

        $filter = array();

        // if our host is in our allowedOrigins array, perform additional checks
        if( array_key_exists($_SERVER['HTTP_HOST'], $this->allowedOrigins))
        {
            $filter = $this->allowedOrigins[$_SERVER['HTTP_HOST']];
        }

        $arr = array();
        foreach($elements as $elem)
        {
            if($elem['Type'] == 'orig')
            {
                if(empty($filter))
                {
                    $arr[] = $elem;
                }
                else if(isset($filter['allow']) && (in_array($elem['Element'], $filter['allow']) || empty($filter['allow'])))
                {
                    $arr[] = $elem;
                }
                else if(isset($filter['deny']) && !in_array($elem['Element'], $filter['deny']))
                {
                    $arr[] = $elem;
                }

                if(isset($filter['default']) && $filter['default'] == $elem['Element'])
                    $arr['defaultValue'] = $elem['eTrigue Value'];
            }
        }

        return $arr;
    }

    private function _loadForms()
    {
        $forms = $this->loadCSV('Form-ID');

        $arr = array();
        foreach($forms as $form)
        {
            $arr[] = $form;
        }

        return $arr;
    }

    private function _loadOwners()
    {
        $owners = $this->loadCSV('Owners');
        $arr = array();
        foreach($owners as $own)
        {
            $arr[] = array(
                'Element' => $own['Element'],
                'Short-Code' => $own['Short-Code'],
                'Label' => $own['Label'],
                'Value' => $this->_getOwnerValueFromOwnerShortcode($own['Short-Code']),
                'Map'   =>  'map-'. $own['Map']
            );
        }

        return $arr;
    }

    private function _loadSegments()
    {
        $segments = $this->loadCSV('Seg-Div');
        $elements = $this->loadCSV('Elements');

        $arr = array();
        foreach($elements as $elem)
        {
            if($elem['Type'] == 'seg')
            {
                foreach($segments as $seg)
                {
                    if($seg[$elem['Element']] == 'TRUE')
                    {
                        $elem['div'][] = $seg['Element'];
                    }
                }

                $arr[] = $elem;
            }
        }

        return $arr;
    }

    private function _loadDivisions()
    {
        $elements = $this->loadCSV('Elements');

        $arr = array();
        foreach($elements as $elem)
        {
            if($elem['Type'] == 'div')
            {
                $arr[] = $elem;
            }
        }

        return $arr;
    }

    private function _loadChannels()
    {
        $elements = $this->loadCSV('Elements');
        $labels = $this->loadCSV('Form-Labels');

        $arr = array();
        foreach($elements as $elem)
        {
            if($elem['Type'] == 'ct')
            {
                foreach($labels as $label)
                {
                    if($label['Element'] == $elem['Element'])
                    {
                        $elem['Label'] = $label['en_US'];
                    }
                }

                $arr[] = $elem;
            }
        }

        return $arr;
    }

    private function _loadPresets()
    {
        $presets = $this->loadCSV('Filter-Presets');

        $arr = array();

        foreach($presets as $preset)
        {
            $arr[$preset['Preset']]['origin'][] = $preset['Filter Origin'] == 'All' ? 'All' : $this->getOriginValueFromOrigin($preset['Filter Origin']);

            $arr[$preset['Preset']]['seg'][] = $preset['Filter Segment'] == 'All' ? 'All' : $this->getSegmentValueFromSegment($preset['Filter Segment']);

            $arr[$preset['Preset']]['div'][] = $preset['Filter Division'] == 'All' ? 'All' : $this->getDivValueFromDiv($preset['Filter Division']);

            $arr[$preset['Preset']]['map'][] = $preset['Map'];

        }

        foreach($arr as $key => $innerArr)
        {
            foreach($innerArr as $ikey => $iarr)
            {
                $arr[$key][$ikey] = array_keys(array_flip($iarr));
            }
        }

        return $arr;
    }


    private function _loadMap() {

        // our return array
        $arr = array();

        // get our maps
        foreach($this->files as $fileName => $fileLocation) {

            if(strpos($fileName,'map') !== false) {

                $arr[$fileName] = array();

                // load our map array
                $theMap = $this->loadCSV($fileLocation);

                // iterate through our $maps array
                foreach ($theMap as $map) {

                    foreach($map as $mapKey => $mapVal) {
                        $map[$mapKey] = utf8_encode($mapVal);
                    }

                    $arr[$fileName][] = $map;
                }

            }
        }

        return $arr;
    }

    /**
     *  returns a json object of the forms, owners, segments, and divisions
     */
    public function toJSON()
    {
        $toJSON = array(
            'origins'   =>  $this->_loadOrigins(),
            'forms'     =>  $this->_loadForms(),
            'owners'    =>  $this->_loadOwners(),
            'segments'  =>  $this->_loadSegments(),
            'divisions' =>  $this->_loadDivisions(),
            'channels'  =>  $this->_loadChannels(),
            'presets'   =>  $this->_loadPresets(),
            'countriesAndStates' =>  $this->_loadMap()
        );

        return json_encode($toJSON);
    }

    public function getPresets()
    {
        return $this->_loadPresets();
    }

    public function getOwners()
    {
        return $this->_loadOwners();
    }

    public function getDatatableColumns()
    {
        $columns = array(
            array( 'data' => 'id',                                          'title' => "ID",                                    'bVisible' => false,            "sClass" => "id datatables_column never"),
            array( 'data' => 'count',                                       'title' => "#",                                                   "sClass" => "count id_column datatables_column basic_column advanced_column opportunities_column"),
            array( 'data' => 'date_submitted',                              'title' => "Date Submitted",                                      "sClass" => "date_submitted date_submitted_column datatables_column basic_column advanced_column opportunities_column"),
            array( 'data' => 'x_udf_form_owner148',                         'title' => "Owner",                                               "sClass" => "x_udf_form_owner148 owner_column datatables_column basic_column advanced_column opportunities_column"),
            array( 'data' => 'notif_sms_html',                              'title' => "Notifications",                                       "sClass" => "notif_sms_html notifications_column datatables_column basic_column advanced_column"),
            array( 'data' => 'status_label',                                'title' => "Status",                                              "sClass" => "status_label status_column datatables_column basic_column advanced_column"),
            array( 'data' => 'x_companyname',                               'title' => "Company",                                             "sClass" => "x_companyname company_column datatables_column basic_column advanced_column opportunities_column"),
            array( 'data' => 'x_firstname',                                 'title' => "First Name",                                          "sClass" => "x_firstname first_name_column datatables_column basic_column advanced_column"),
            array( 'data' => 'x_lastname',                                  'title' => "Last Name",                                           "sClass" => "x_lastname last_name_column datatables_column basic_column advanced_column"),
            array( 'data' => 'x_country',                                   'title' => "Country",                                             "sClass" => "x_country country_column datatables_column advanced_column"),
            array( 'data' => 'x_state',                                     'title' => "State",                                               "sClass" => "x_state state_column datatables_column advanced_column"),
            array( 'data' => 'x_emailaddress',                              'title' => "Email",                                               "sClass" => "x_emailaddress email_column datatables_column basic_column advanced_column"),
            array( 'data' => 'respond_html',                                'title' => "Auto<br>Responder",                                   "sClass" => "respond_html respond_email_column datatables_column advanced_column"),
            array( 'data' => 'form_label',                                  'title' => "Form / ID",                                           "sClass" => "form_label form_column datatables_column advanced_column"),
            array( 'data' => 'x_udf_origin_code114__ss_label',              'title' => "Origin",                                              "sClass" => "x_udf_origin_code114__ss_label origin_column datatables_column advanced_column"),
            array( 'data' => 'preset',                                      'title' => "Segment",                                             "sClass" => "preset preset_column datatables_column basic_column opportunities_column"),
            array( 'data' => 'x_udf_primary_market_segment122__ss_label',   'title' => "Primary Market Segment",                              "sClass" => "x_udf_primary_market_segment122__ss_label segment_column datatables_column advanced_column"),
            array( 'data' => 'x_udf_division146__ss_label',                 'title' => "Division Override",                                   "sClass" => "x_udf_division146__ss_label division_column datatables_column advanced_column"),
            array( 'data' => 'x_udf_channel___tactic400__ss_label',         'title' => "Channel & Tactic",                                    "sClass" => "x_udf_channel___tactic400__ss_label channel_tactic_column datatables_column basic_column advanced_column opportunities_column"),
            array( 'data' => 'x_udf_form_product149_label',                 'title' => "Product",                                             "sClass" => "x_udf_form_product149_label datatables_column advanced_column"),
            array( 'data' => 'x_udf_event443',                              'title' => "Event",                                               "sClass" => "x_udf_event443_label datatables_column advanced_column"),
            array( 'data' => 'x_udf_extcustomer',                           'title' => "Customer",                                            "sClass" => "x_udf_extcustomer customer_column datatables_column basic_column advanced_column opportunities_column"),
            array( 'data' => 'x_udf_ext_dealer375',                         'title' => "Referrer<br>/ Source",                                "sClass" => "x_udf_ext_dealer375 dealer_column datatables_column advanced_column"),
            array( 'data' => 'created_date',                                'title' => "Opportunity Creation Date",                           "sClass" => "created_date opportunity_creation_date_column datatables_column opportunities_column"),
            array( 'data' => 'amount',                                      'title' => "Amount",                                              "sClass" => "amount amount_column datatables_column opportunities_column"),
            array( 'data' => 'close_date',                                  'title' => "Close Date",                                          "sClass" => "close_date close_date_column datatables_column opportunities_column"),
            array( 'data' => 'stage',                                       'title' => "Stage",                                               "sClass" => "stage stage_column datatables_column opportunities_column"),
            array( 'data' => 'probability',                                 'title' => "Probability",                                         "sClass" => "probability probability_column datatables_column opportunities_column"),
            array( 'data' => 'effective_value',                             'title' => "Effective Value",                                         "sClass" => "effective_value effective_value_column datatables_column opportunities_column"),

            array( 'data' => 'auth_key',                                    'title' => "auth_key",                              'bVisible' => false,            "sClass" => "auth_key datatables_column never"),
            array( 'data' => 'form_id',                                     'title' => "form_id",                               'bVisible' => false,            "sClass" => "form_id datatables_column never"),
            array( 'data' => 'x_owner',                                     'title' => "Owner",                                 'bVisible' => false,            "sClass" => "x_owner datatables_column basic_column advanced_column opportunities_column"),
            array( 'data' => 'status',                                      'title' => "status",                                'bVisible' => false,            "sClass" => "status datatables_column never"),
            array( 'data' => 'x_udf_primary_market_segment122__ss',         'title' => "Primary Market Segment",                'bVisible' => false,            "sClass" => "x_udf_primary_market_segment122__ss datatables_column advanced_column"),
            array( 'data' => 'x_udf_division146__ss',                       'title' => "Division Override",                     'bVisible' => false,            "sClass" => "x_udf_division146__ss datatables_column advanced_column"),
            array( 'data' => 'x_udf_channel___tactic400__ss',               'title' => "Channel & Tactic",                      'bVisible' => false,            "sClass" => "x_udf_channel___tactic400__ss datatables_column basic_column advanced_column opportunities_column"),
            array( 'data' => 'x_udf_form_language302_label',                'title' => "x_udf_form_language302_label",          'bVisible' => false,            "sClass" => "x_udf_form_language302_label datatables_column never"),
            array( 'data' => 'x_udf_ext_last_topic364',                     'title' => "x_udf_ext_last_topic364",               'bVisible' => false,            "sClass" => "x_udf_ext_last_topic364 datatables_column never"),
            array( 'data' => 'x_udf_extindustry',                           'title' => "x_udf_extindustry",                     'bVisible' => false,            "sClass" => "x_udf_extindustry datatables_column never"),
            array( 'data' => 'x_udf_extlicensesreq',                        'title' => "x_udf_extlicensesreq",                  'bVisible' => false,            "sClass" => "x_udf_extlicensesreq datatables_column never"),
            array( 'data' => 'x_udf_extpurchasetime',                       'title' => "x_udf_extpurchasetime",                 'bVisible' => false,            "sClass" => "x_udf_extpurchasetime datatables_column never"),
            array( 'data' => 'x_udf_ext_purchase_question405',              'title' => "x_udf_ext_purchase_question405",        'bVisible' => false,            "sClass" => "x_udf_ext_purchase_question405 datatables_column never"),
            array( 'data' => 'x_udf_ext_hds_question_cyclone451__ms',       'title' => "x_udf_ext_hds_question_cyclone451__ms", 'bVisible' => false,            "sClass" => "x_udf_ext_hds_question_cyclone451__ms datatables_column never"),
            array( 'data' => 'x_udf_ext_hds_question_cyclone_2452__ms',     'title' => "x_udf_ext_hds_question_cyclone_2452__ms", 'bVisible' => false,          "sClass" => "x_udf_ext_hds_question_cyclone_2452__ms datatables_column never"),
            array( 'data' => 'x_udf_hds_cyclone_question_3459',             'title' => "x_udf_hds_cyclone_question_3459",       'bVisible' => false,            "sClass" => "x_udf_hds_cyclone_question_3459 datatables_column never"),
            array( 'data' => 'x_udf_subscription_start_date460',            'title' => "x_udf_subscription_start_date460",      'bVisible' => false,            "sClass" => "x_udf_subscription_start_date460 datatables_column never"),
            array( 'data' => 'x_udf_ext_bim_hds_question462__ss',           'title' => "x_udf_ext_bim_hds_question462__ss",     'bVisible' => false,            "sClass" => "x_udf_ext_bim_hds_question462__ss datatables_column never"),
            array( 'data' => 'x_udf_ext_interest401',                       'title' => "x_udf_ext_interest401",                 'bVisible' => false,            "sClass" => "x_udf_ext_interest401 datatables_column never"),
            array( 'data' => 'x_udf_main_application121__ss_label',         'title' => "x_udf_main_application121__ss_label",   'bVisible' => false,            "sClass" => "x_udf_main_application121__ss_label datatables_column never"),
            array( 'data' => 'x_city',                                      'title' => "x_city",                                'bVisible' => false,            "sClass" => "x_city datatables_column never"),
            array( 'data' => 'x_udf_extheardof',                            'title' => "x_udf_extheardof",                      'bVisible' => false,            "sClass" => "x_udf_extheardof datatables_column never"),
            array( 'data' => 'x_solution',                                  'title' => "x_solution",                            'bVisible' => false,            "sClass" => "x_solution datatables_column never"),
            array( 'data' => 'x_address',                                   'title' => "x_address",                             'bVisible' => false,            "sClass" => "x_address datatables_column never"),
            array( 'data' => 'x_address2',                                  'title' => "x_address2",                            'bVisible' => false,            "sClass" => "x_address2 datatables_column never"),
            array( 'data' => 'x_zip',                                       'title' => "x_zip",                                 'bVisible' => false,            "sClass" => "x_zip datatables_column never"),
            array( 'data' => 'x_fax',                                       'title' => "x_fax",                                 'bVisible' => false,            "sClass" => "x_fax datatables_column never"),
            array( 'data' => 'x_phone',                                     'title' => "x_phone",                               'bVisible' => false,            "sClass" => "x_phone datatables_column never"),
            array( 'data' => 'x_phone2',                                    'title' => "x_phone2",                              'bVisible' => false,            "sClass" => "x_phone2 datatables_column never"),
            array( 'data' => 'x_title',                                     'title' => "x_title",                               'bVisible' => false,            "sClass" => "x_title datatables_column never"),
            array( 'data' => 'x_udf_employees130',                          'title' => "x_udf_employees130",                    'bVisible' => false,            "sClass" => "x_udf_employees130 datatables_column never"),
            array( 'data' => 'x_udf_ext_dealer375',                         'title' => "x_udf_ext_dealer375",                   'bVisible' => false,            "sClass" => "x_udf_ext_dealer375 datatables_column never"),
            array( 'data' => 'x_udf_ext_serial_number377',                  'title' => "x_udf_ext_serial_number377",            'bVisible' => false,            "sClass" => "x_udf_ext_serial_number377 datatables_column never"),
            array( 'data' => 'x_udf_extjobfunction',                        'title' => "x_udf_extjobfunction",                  'bVisible' => false,            "sClass" => "x_udf_extjobfunction datatables_column never"),
            array( 'data' => 'x_url',                                       'title' => "x_url",                                 'bVisible' => false,            "sClass" => "x_url datatables_column never"),
            array( 'data' => 'x_udf_comments126',                           'title' => "x_udf_comments126",                     'bVisible' => false,            "sClass" => "x_udf_comments126 datatables_column never"),
            array( 'data' => 'x_show_products',                             'title' => "x_show_products",                       'bVisible' => false,            "sClass" => "x_show_products datatables_column never"),
            array( 'data' => 'x_udf_event443',                              'title' => "x_udf_event443",                        'bVisible' => false,            "sClass" => "x_udf_event443 datatables_column never")
        );

        return json_encode($columns);
    }

    public function getDatatableData($array = array())
    {
        $returnData = array();

        foreach($array['columns'] as $key => $col)
        {
            $name = $col['name'];
            $val = $col['data'];
            $array[$name] = $val;
        }

        // remove some data we don't need
        if(isset($array['columns']))
            unset($array['columns']);
        if(isset($array['search']))
            unset($array['search']);
        if(isset($array['order']))
            unset($array['order']);

        $query = $this->_buildDatatablesQuery($array);

        try {
            $results = $this->ExecAndFetchAll($query['sql'], $query['params']);
        }
        catch(Exception $e)
        {
            $results = $e->getMessage();
            error_log($results);
        }

        $type = isset($array['type']) ? $array['type'] : 'basic';

        $opportunitiesCount = 0;
        $basicCount = 0;
        $effectiveValue = 0;
        $total = count($results);
        $recordsFiltered = 0;

        foreach( $results as $key => $result )
        {
            // format date submitted
            $result['date_submitted'] = substr($result['date_submitted'], 0, -3);

            // add additional data from posted_data
            $postedData = json_decode($result['posted_data'], true);

            // remove posted_data, we don't actually need to return it
            unset($result['posted_data']);

            // special variable event and x_udf_event443
            $result['x_udf_event443'] = isset($postedData['x_udf_event443']) ? $postedData['x_udf_event443'] : '';

            // get cyclone labels
            $form = new Forms();
            if(!empty($result['ext_hds_question_cyclone451__ms'])){
                $newCyclone1 = $form->getCycloneLabelsFromValues($result['ext_hds_question_cyclone451__ms']);
                $result['ext_hds_question_cyclone451__ms'] = $newCyclone1;
            }

            if(!empty($result['ext_hds_question_cyclone_2452__ms'])){
                $newCyclone2 = $form->getCycloneLabelsFromValues($result['ext_hds_question_cyclone_2452__ms']);
                $result['ext_hds_question_cyclone_2452__ms'] = $newCyclone2;
            }

            if($array['length'] == -1)
            {
                $result['count'] = $array['start'] + $recordsFiltered + 1;
                $returnData[] = $result;
                $recordsFiltered++;
            }
            else if($key >= $array['start'] && $key < $array['start'] + $array['length'])
            {
                $result['count'] = $array['start'] + $recordsFiltered + 1;
                $returnData[] = $result;
                $recordsFiltered++;
            }
            else
            {
                $returnData[] = $result;
            }

            if(isset($result['status']) && $result['status'] != 7)
            {
                $basicCount++;
            }

            if(!empty($result['opportunity_name']) && $result['opportunity_name'] !== $result['x_companyname'] && $result['opportunity_name'] !== $result['x_firstname'] . ' ' . $result['x_lastname'])
            {
                $opportunitiesCount++;
                $effectiveValue += $result['effective_value'];
            }
        }

        return json_encode(array('data' => $returnData, 'draw' => $array['draw'], 'recordsTotal' => $total, 'recordsFiltered' => $total, 'type' => $type, 'basic_count' => $basicCount, 'opportunities_count' => $opportunitiesCount, 'total_effective_value' => number_format($effectiveValue, 2, '.', ',')));

    }

    private function _buildDatatablesQuery($array = array())
    {
        $dateRange = isset($array['date']) && !empty($array['date']) ? $array['date'] : date('F j, Y') . '   -   '. date('F j, Y');
        $from = isset($array['from']) && !empty($array['from']) ? $array['from'] : '';
        $to = isset($array['to']) && !empty($array['to']) ? $array['to'] : '';
        $status = isset($array['status']) && !empty($array['status']) ? $array['status'] : array();
        $origin = isset($array['origin']) && !empty($array['origin']) ? $array['origin'] : array();
        $seg = isset($array['seg']) && !empty($array['seg']) ? $array['seg'] : array();
        $div = isset($array['div']) && !empty($array['div']) ? $array['div'] : array();
        $map = isset($array['map']) && !empty($array['map']) ? $array['map'] : array();
        $country = isset($array['country']) && !empty($array['country']) ? $array['country'] : array();
        $state = isset($array['state']) && !empty($array['state']) ? $array['state'] : array();
        $formId = isset($array['formId']) && !empty($array['formId']) ? $array['formId'] : array();
        $owner = isset($array['owner']) && !empty($array['owner']) ? $array['owner'] : '';
        $ctIds = isset($array['channel']) && !empty($array['channel']) ? $array['channel'] : array();

        $columns = array(
            'DT_RowId',
            'id',
            'auth_key',
            'date_submitted',
            'form_id',
            'x_owner',
            'x_udf_form_owner148',
            'posted_data',
            'notif_sms_html',
            'status',
            'status_label',
            'x_companyname',
            'x_firstname',
            'x_lastname',
            'x_country',
            'x_state',
            'x_city',
            'x_address',
            'x_address2',
            'x_zip',
            'x_fax',
            'x_emailaddress',
            'x_phone',
            'x_phone2',
            'x_title',
            'respond_html',
            'form_label',
            'x_udf_comments126',
            'x_udf_form_language302_label',
            'x_udf_origin_code114__ss_label',
            'x_udf_form_product149_label',
            'x_udf_ext_serial_number377',
            'preset',
            'x_udf_primary_market_segment122__ss',
            'x_udf_primary_market_segment122__ss_label',
            'x_udf_division146__ss',
            'x_udf_division146__ss_label',
            'x_udf_channel___tactic400__ss',
            'x_udf_channel___tactic400__ss_label',
            'x_udf_extjobfunction',
            'x_udf_extcustomer',
            'x_udf_ext_dealer375',
            'x_udf_ext_last_topic364',
            'x_udf_extlicensesreq',
            'x_udf_extpurchasetime',
            'x_udf_extindustry',
            'x_udf_ext_interest401',
            'x_udf_event443',
            'x_udf_main_application121__ss_label',
            'x_udf_ext_purchase_question405',
            'x_udf_ext_hds_question_cyclone451__ms',
            'x_udf_ext_hds_question_cyclone_2452__ms',
            'x_udf_hds_cyclone_question_3459',
            'x_udf_subscription_start_date460 ',
            'x_udf_ext_bim_hds_question462__ss',
            'x_udf_employees130',
            'x_udf_extheardof',
            'x_udf_ext_dealer375',
            'x_show_products',
            'x_url',
            'x_solution',
            'created_date',
            'opportunity_name',
            'amount',
            'close_date',
            'stage',
            'probability',
            'effective_value'
        );
        
        $select = "SELECT ". implode(',', $columns) ." FROM ". self::DATABASE_TABLE;
        $whereArray = array();
        $whereOrArray = array();
        $params = array();

        if(!empty($dateRange))
        {
            $tz = new DateTimeZone('America/Los_Angeles');
            $whereArray[] = "date_submitted >= :from AND date_submitted <= :to";

            $dateArray = explode("-", $dateRange);
            $from = trim($dateArray[0]) . '00:00:00';
            $to = trim($dateArray[1]) . '23:59:59';

            $oldFrom = DateTime::createFromFormat('F j, Y H:i:s', $from);
            $oldFrom->setTimezone($tz);
            $newFrom = $oldFrom->format('Y-m-d H:i:s');

            $oldTo = DateTime::createFromFormat('F j, Y H:i:s', $to);
            $oldTo->setTimezone($tz);
            $newTo = $oldTo->format('Y-m-d H:i:s');

            $params[':from'] = $newFrom;
            $params[':to'] = $newTo;
        }
        else if(!empty($from) && !empty($to))
        {
            $tz = new DateTimeZone('America/Los_Angeles');
            $whereArray[] = "date_submitted >= :from AND date_submitted <= :to";

            $oldFrom = DateTime::createFromFormat('Y-m-d H:i:s', $from);
            $oldFrom->setTimezone($tz);
            $newFrom = $oldFrom->format('Y-m-d H:i:s');

            $oldTo = DateTime::createFromFormat('Y-m-d H:i:s', $to);
            $oldTo->setTimezone($tz);
            $newTo = $oldTo->format('Y-m-d H:i:s');

            $params[':from'] = $newFrom;
            $params[':to'] = $newTo;
        }

        if(!empty($status))
        {
            if(is_array($status))
            {
                foreach($status as $skey => $st)
                {
                    $whereOrArray['status'][] = "status = :status".$skey;
                    $params[':status'.$skey] = $st;
                }
            }
        }

        if(!empty($origin))
        {
            if(is_array($origin))
            {
                foreach($origin as $okey => $oid)
                {
                    $whereOrArray['origin'][] = "x_udf_origin_code114__ss = :x_udf_origin_code114_ss". $okey;
                    $params[':x_udf_origin_code114_ss'. $okey] = $oid;
                }
            }
            else
            {
                $whereArray[] = "x_udf_origin_code114__ss = :x_udf_origin_code114_ss";
                $params[':x_udf_origin_code114_ss'] = $origin;
            }
        }

        if(!empty($seg))
        {
            if(is_array($seg))
            {
                foreach($seg as $okey => $oid)
                {
                    $whereOrArray['seg'][] = "x_udf_primary_market_segment122__ss = :x_udf_primary_market_segment122__ss". $okey;
                    $params[':x_udf_primary_market_segment122__ss'. $okey] = $oid;
                }
            }
            else
            {
                $whereArray[] = "x_udf_primary_market_segment122__ss = :x_udf_primary_market_segment122__ss";
                $params[':x_udf_primary_market_segment122__ss'] = $seg;
            }
        }

        if(!empty($div))
        {
            if(is_array($div))
            {
                foreach($div as $okey => $oid)
                {
                    $whereOrArray['div'][] = "x_udf_division146__ss = :x_udf_division146__ss". $okey;
                    $params[':x_udf_division146__ss'. $okey] = $oid;
                }
            }
            else
            {
                $whereArray[] = "x_udf_division146__ss = :x_udf_division146__ss";
                $params[':x_udf_division146__ss'] = $div;
            }
        }

        // map filter
        if(!empty($map)) {

            if(is_array($map))
            {
                foreach($map as $okey => $oid)
                {
                    $whereOrArray['div'][] = "map = :map". $okey;
                    $params[':map'. $okey] = $oid;
                }
            }
            else
            {
                $whereArray[] = "map = :map";
                $params[':map'] = $map;
            }
        }

        // country filter
        if(!empty($country)) {
            
            if(is_array($country))
            {
                foreach($country as $okey => $oid)
                {
                    $whereOrArray['div'][] = "x_country = :x_country". $okey;
                    $params[':x_country'. $okey] = $oid;
                }
            }
            else
            {
                $whereArray[] = "x_country = :x_country";
                $params[':x_country'] = $country;
            }
        }

        if(!empty($state)) {

            if(is_array($state))
            {
                foreach($state as $okey => $oid)
                {
                    if(!empty($oid)) {
                        $whereOrArray['div'][] = "x_state = :x_state" . $okey;
                        $params[':x_state' . $okey] = $oid;
                    }
                }
            }
            else
            {
                $whereArray[] = "x_state = :x_state";
                $params[':x_state'] = $state;
            }
        }
        
        // special rules for preset
        if(isset($array['preset']) && !empty($array['preset']))
        {
            $preset = !is_array($array['preset']) ? array($array['preset']) : $array['preset'];

            foreach($preset as $key => $querystring)
            {
                $presetWhere = array();

                // parse the querystring
                parse_str($querystring, $get_array);

                if(isset($get_array['origin']))
                {
                    $presetWhere[] = 'x_udf_origin_code114__ss = :x_udf_origin_code114__ss_preset'. $key;
                    $params[':x_udf_origin_code114__ss_preset'. $key] = $get_array['origin'];

                }

                if(isset($get_array['seg']))
                {
                    $presetWhere[] = 'x_udf_primary_market_segment122__ss = :x_udf_primary_market_segment122__ss_preset'. $key;
                    $params[':x_udf_primary_market_segment122__ss_preset'. $key] = $get_array['seg'];
                }

                if(isset($get_array['div']))
                {
                    $presetWhere[] = 'x_udf_division146__ss = :x_udf_division146__ss_preset'. $key;
                    $params[':x_udf_division146__ss_preset'. $key] = $get_array['div'];
                }

                $whereOrArray['preset'][] = ' ( '. implode(' AND ', $presetWhere) .' ) ';
            }
        }

        if(!empty($ctIds))
        {
            if(is_array($ctIds))
            {
                foreach($ctIds as $okey => $oid)
                {
                    $whereOrArray['ct'][] = "x_udf_channel___tactic400__ss = :x_udf_channel___tactic400__ss". $okey;
                    $params[':x_udf_channel___tactic400__ss'. $okey] = $oid;
                }
            }
            else
            {
                $whereArray[] = "x_udf_channel___tactic400__ss = :x_udf_channel___tactic400__ss";
                $params[':x_udf_channel___tactic400__ss'] = $ctIds;
            }
        }

        if(!empty($formId))
        {
            if(is_array($formId))
            {
                foreach($formId as $fkey => $fid)
                {
                    $whereOrArray['form'][] = "form_id = :formId". $fkey;
                    $params[':formId'. $fkey] = $fid;
                }
            }
            else
            {
                $whereArray[] = "form_id = :formId";
                $params[':formId'] = $formId;
            }
        }

        if(!empty($owner))
        {
            if(is_array($owner))
            {
                foreach($owner as $okey => $oid)
                {
                    $whereOrArray['owner'][] = "x_udf_form_owner148 = :x_udf_form_owner148". $okey;
                    $params[':x_udf_form_owner148'. $okey] = $oid;
                }
            }
            else
            {
                $whereArray[] = "x_udf_form_owner148 = :x_udf_form_owner148";
                $params[':x_udf_form_owner148'] = $owner;
            }
        }

        // build exclusion list
        foreach($this->exclude as $field => $exArr)
        {
            foreach($exArr as $x)
            {
                if(!empty($x))
                    $whereArray[] = $field ." NOT LIKE '". $x. "'";
                else
                    $whereArray[] = $field ." NOT LIKE ''";
            }
        }


        $whereOr = '';
        if(!empty($whereOrArray))
        {
            foreach($whereOrArray as $orArray)
            {
                $whereOr .= " AND (" . implode(" OR ", $orArray) .")";
            }
        }

        $where = !empty($whereArray) ? " WHERE ". implode(" AND ", $whereArray) . $whereOr : "";

        $order = ' order by id DESC ';

        $sql = $select . $where . $order;

        return array('sql' => $sql, 'params' => $params);
    }

    public function getHighchartsData($name)
    {
        $data = '';

        $sql = "SELECT * FROM highcharts WHERE name = :name";
        $params = array(':name' => $name);

        try {
            $result = $this->ExecAndFetch($sql, $params);
        }
        catch(Exception $e)
        {
            $result = $e->getMessage();
            error_log($result);
        }

        // check if the data exists and if the date matches today's date
        if(isset($result['date']) && $result['date'] == date('Y-m-d'))
        {
            $data = $result['data'];
        }
        else
        {
            $method = 'get'. $name;

            if(method_exists($this, $method))
                $data = json_encode($this->$method());

            $sql = "INSERT INTO highcharts (name, date, data)
                VALUES(:name, :date, :data)
                ON DUPLICATE KEY UPDATE date = :date, data=:data";
            $params = array(
                ':name' => $name,
                ':date' => date('Y-m-d'),
                ':data' => $data
            );
            $this->Execute($sql, $params);
        }

        return $data;
    }

    public function getStackedAreaBySegmentDashboard()
    {
        $dateTime = new DateTime('-30 days');
        $select = 'SELECT * FROM form_submissions
                    WHERE date_submitted >= :date_submitted
                    AND origin_label in(:origin1, :origin2)
                    AND channel_tactic != ""
                    AND status < :status';
        $params = array(
            ':date_submitted' => $dateTime->format('Y-m-d H:s:i'),
            ':origin1' => 'leicaus',
            ':origin2' => 'ape',
            ':status'  => 2     // rejected
        );

        // build exclusion list
        foreach($this->exclude as $field => $exArr)
        {
            foreach($exArr as $x)
            {
                $whereArray[] = $field ." NOT LIKE '". $x. "'";
            }
        }

        $where = !empty($whereArray) ? " AND ". implode(" AND ", $whereArray) . (!empty($whereOrArray) ? " AND (" . implode(" OR ", $whereOrArray) .")" : "") : "";

        $sql = $select . $where;

        try {

            $results = $this->ExecAndFetchAll($sql, $params);
            $return = array();

            $d = array();
            for($i = 30; $i >= 0; $i--)
                $d[date("m/d", strtotime('-'. $i .' days'))] = 0;

            $count = array();
            $presets = array();

            // get our segments
            $presetsArray = $this->loadCSV("Filter-Presets");

            foreach($presetsArray as $elem)
            {
                if(!isset($count[$elem['Preset']]) && ($elem['Filter Origin'] === 'leicaus' || $elem['Filter Origin'] === 'ape'))
                {
                    $count[$elem['Preset']] = $d;
                    $presets[$elem['Preset']] = $elem['Preset'];
                }
            }

            $return['xAxis'] = array(
                'categories' => array_keys($d),
                'tickmarkPlacement' => 'on',
                'title' => array('text' => 'Date'),
                'tickInterval' => 2
            );

            $tz = new DateTimeZone('America/Los_Angeles');
            foreach($results as $key => $row)
            {
                $myDate = new DateTime($row['date_submitted']);
                $theDate = $myDate->format('m/d');
                $postedData = json_decode($row['posted_data'], true);

                if(isset($postedData['x_udf_primary_market_segment122__ss']) && !empty($postedData['x_udf_primary_market_segment122__ss']))
                {
                    $primarySegment = $postedData['x_udf_primary_market_segment122__ss'];
                    $primaryDivision = isset($postedData['x_udf_division146__ss']) ? $postedData['x_udf_division146__ss'] : '';
                    $origin = $row['origin_label'];

                    $segment = $this->getSegmentFromSegmentId($primarySegment);
                    $division = $this->getDivFromDivValue($primaryDivision);
                    $preset = $this->getPresetFromSegmentAndDivision($segment, $division, $origin);

                    if(!empty($preset))
                    {
                        if(!isset($count[$preset][$theDate]))
                            $count[$preset][$theDate] = 0;
                        else
                            $count[$preset][$theDate] += 1;
                    }
                }
            }

            foreach($count as $name => $countByDate)
            {
                $return['series'][] = array(
                    'name' => $presets[$name],
                    'data' => array_values($countByDate)
                );
            }
        }
        catch(Exception $e)
        {
            $return = $e->getMessage();
            error_log($return);
        }



        return $return;

    }

    public function getStackedAreaByOriginDashboard()
    {
        $dateTime = new DateTime('-30 days');
        $select = 'SELECT * FROM form_submissions
                    WHERE date_submitted > :date_submitted
                    AND origin_label in(:origin1, :origin2)
                    AND channel_tactic != ""
                    AND status < :status';
        $params = array(
            ':date_submitted' => $dateTime->format('Y-m-d H:s:i'),
            ':origin1' => 'leicaus',
            ':origin2' => 'ape',
            ':status'  => 2     // rejected
        );

        // build exclusion list
        foreach($this->exclude as $field => $exArr)
        {
            foreach($exArr as $x)
            {
                $whereArray[] = $field ." NOT LIKE '". $x. "'";
            }
        }

        $where = !empty($whereArray) ? " AND ". implode(" AND ", $whereArray) . (!empty($whereOrArray) ? " AND (" . implode(" OR ", $whereOrArray) .")" : "") : "";

        $sql = $select . $where;

        try {
            $results = $this->ExecAndFetchAll($sql, $params);

            $return = array();

            $d = array();
            for($i = 30; $i > 0; $i--)
                $d[date("m/d", strtotime('-'. $i .' days'))] = 0;

            $count = array(
                'leicaus' => $d,
                'ape' => $d
            );

            $return['xAxis'] = array(
                'categories' => array_keys($d),
                'tickmarkPlacement' => 'on',
                'title' => array('text' => 'Date'),
                'tickInterval' => 2
            );


            $tz = new DateTimeZone('America/Los_Angeles');
            foreach($results as $key => $row)
            {
                $myDate = new DateTime($row['date_submitted'], $tz);

                if(!isset($count[$row['origin_label']][$myDate->format('m/d')]))
                    $count[$row['origin_label']][$myDate->format('m/d')] = 0;
                else
                    $count[$row['origin_label']][$myDate->format('m/d')] += 1;
            }

            foreach($count as $name => $countByDate)
            {
                $return['series'][] = array(
                    'name' => $name,
                    'data' => array_values($countByDate)
                );
            }
        }
        catch(Exception $e)
        {
            $return = $e->getMessage();
            error_log($return);
        }

        return $return;

    }

    public function getPieChartByEffectiveValueDashboard()
    {
        $dateTime = new DateTime('-180 days');
        $select = 'SELECT * FROM form_submissions
                    WHERE date_submitted > :date_submitted
                    AND channel_tactic != ""';
        $params = array(
            ':date_submitted' => $dateTime->format('Y-m-d H:s:i'),
        );

        // build exclusion list
        foreach($this->exclude as $field => $exArr)
        {
            foreach($exArr as $x)
            {
                $whereArray[] = $field ." NOT LIKE '". $x. "'";
            }
        }

        $where = !empty($whereArray) ? " AND ". implode(" AND ", $whereArray) . (!empty($whereOrArray) ? " AND (" . implode(" OR ", $whereOrArray) .")" : "") : "";

        $sql = $select . $where;

        try {
            $results = $this->ExecAndFetchAll($sql, $params);

            $return = array(
                'series' => array(
                    array(
                        'name' => 'Channel Tactic',
                        'colorByPoint'  => 'true',
                        'data' => array()
                    )
                ),
                'drilldown' => array(
                    'series' => array()
                )
            );

            foreach($results as $res)
            {
                if(!empty($res['channel_tactic']) && !empty($res['opportunities']))
                {
                    $opportunities = json_decode($res['opportunities'], true);
                    $channelTactic = $this->_getChannelTacticFromValue($res['channel_tactic']);
                    $ctArray = explode(": ", $channelTactic);
                    $numLevels = count($ctArray);
                    $parentsArray = array();

                    // iterate through the channel tactic levels
                    foreach($ctArray as $ckey => $ct)
                    {
                        switch($ckey)
                        {
                            // if we're at the top level, look in our series array
                            case 0 :
                                $isInSeries = false;

                                // if it's already in our series array, just update the effective value
                                foreach($return['series'][0]['data'] as $skey => $s)
                                {
                                    if(isset($s['name']) && $ct == $s['name'])
                                    {
                                        $isInSeries = true;
                                        $return['series'][0]['data'][$skey]['y'] += $opportunities['Effective Value'];
                                        break;
                                    }
                                }

                                // if it's not in our series array, add it
                                if(!$isInSeries)
                                {
                                    $ctData = array(
                                        'name' => $ct,
                                        'visible' => true,
                                        'y' => $opportunities['Effective Value']
                                    );

                                    // does this have a drilldown?
                                    if($ckey < $numLevels - 1)
                                        $ctData['drilldown'] = $ct;

                                    $return['series'][0]['data'][] = $ctData;

                                }

                                $parentsArray[] = $ct;

                                break;

                            // if we're in a lower level, look in our drilldown series array
                            case 1:
                            case 2:
                                $parent = implode(':', $parentsArray);
                                $isParentInSeries = false;
                                $isInSeries = false;

                                // we're gonna search our drilldown series for our parent
                                foreach($return['drilldown']['series'] as $skey => $s)
                                {
                                    if($parent == $s['id']) // we've found the parent!
                                    {
                                        $isParentInSeries = true;

                                        // if it's already in our drilldown series array, just update the effective value
                                        foreach($s['data'] as $dkey => $data)
                                        {
                                            if($ct == $data['name'])
                                            {
                                                $isInSeries = true;
                                                $return['drilldown']['series'][$skey]['data'][$dkey]['y'] += $opportunities['Effective Value'];
                                                break(2);
                                            }
                                        }

                                        // if we find the parent but the ct is not in our series, add it
                                        if(!$isInSeries)
                                        {
                                            $ctData =  array(
                                                'name' => $ct,
                                                'y' => $opportunities['Effective Value']
                                            );

                                            // does this have a drilldown?
                                            if($ckey < $numLevels - 1)
                                                $ctData['drilldown'] = $parent .':'. $ct;

                                            $return['drilldown']['series'][$skey]['data'][] = $ctData;
                                        }
                                    }
                                }

                                // if the parent isn't in our series array, add it and the ct data
                                if(!$isParentInSeries)
                                {
                                    $ctData =  array(
                                        'name' => $ct,
                                        'y' => $opportunities['Effective Value']
                                    );

                                    // does this have a drilldown?
                                    if($ckey < $numLevels - 1)
                                        $ctData['drilldown'] = $parent . ':'. $ct;

                                    $parentData = array(
                                        'id' => $parent,
                                        'name' => $parent,
                                        'visible' => true,
                                        'data' => array($ctData)
                                    );

                                    $return['drilldown']['series'][] = $parentData;

                                }

                                $parentsArray[] = $ct;
                                break;
                        }
                    }
                }
            }
        }
        catch(Exception $e)
        {
            $return = $e->getMessage();
            error_log($return);
        }

        return $return;
    }

    public function getHeatMapDashboard()
    {
        $dateTime = new DateTime('-28 days');
        $select = 'SELECT * FROM form_submissions WHERE date_submitted > :date_submitted AND origin_label in(:origin1, :origin2)ORDER BY date_submitted DESC';
        $params = array(
            ':date_submitted' => $dateTime->format('Y-m-d H:s:i'),
            ':origin1' => 'leicaus',
            ':origin2' => 'ape'
        );

        // build exclusion list
        foreach($this->exclude as $field => $exArr)
        {
            foreach($exArr as $x)
            {
                $whereArray[] = $field ." NOT LIKE '". $x. "'";
            }
        }

        $where = !empty($whereArray) ? " AND ". implode(" AND ", $whereArray) . (!empty($whereOrArray) ? " AND (" . implode(" OR ", $whereOrArray) .")" : "") : "";

        $sql = $select . $where;

        $return = array();
        try {
            $results = $this->ExecAndFetchAll($sql, $params);

            $myData = array();

            $tz = new DateTimeZone('America/Los_Angeles');
            $dateNow = new DateTime('now', $tz);

            foreach($results as $key => $row)
            {
                $myDate = new DateTime($row['date_submitted'], $tz);
                $monthDay = $myDate->format("m/d");
                $dayOfWeek = $myDate->format("l");

                if($key == 0 && $monthDay < $dateNow->format("m/d"))
                {
                    $count = 1;
                    $keepGoing = true;
                    while($keepGoing)
                    {
                        $dateNow->sub(new DateInterval('P1D'));

                        if($monthDay < $dateNow->format("m/d"))
                        {
                            $myData[$dateNow->format("m/d")][$dateNow->format("l")] = 0;
                        }
                        else{
                            $keepGoing = false;
                        }

                        $count++;
                    }

                }

                $myData[$monthDay][$dayOfWeek] = isset($myData[$monthDay][$dayOfWeek]) ? $myData[$monthDay][$dayOfWeek] + 1 : 1;

            }

            $xAxis = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
            $yAxis = array();
            $data = array();
            $leicaus = array();
            $yValue = 0;
            $previousXindex = null;
            $previousYindex = 0;

            foreach($myData as $myDate => $myArr)
            {
                foreach($myArr as $myDay => $myCount)
                {
                    $xvalue = array_search($myDay, $xAxis);

                    if($xvalue !== false)
                    {
                        // we're in a different week if the current day is greater than the previous day
                        if($xvalue > $previousXindex && $previousXindex !== null)
                        {
                            // if we skipped a day of the week
                            if($previousXindex !== 0)
                            {
                                for($i = $previousXindex - 1; $i >= 0; $i--)
                                {
                                    $data[] = array($i, $yValue, 0);
                                }
                            }

                            $previousYindex = $yValue;
                            $yValue += 1;
                        }

                        // if we skipped a day of the week but we're still in the same week
                        if($previousXindex !== null && $xvalue !== $previousXindex - 1 && $previousYindex == $yValue)
                        {
                            for($j = $previousXindex - 1; $j < $xvalue; $j++)
                            {
                                if($j >= 0)
                                    $data[] = array($j, $yValue, 0);
                            }
                        }

                        // finally, store the value for this date
                        $data[] = array($xvalue, $yValue, $myCount);

                        // error_log("x:". $xvalue .' , y: '. $yValue);

                        $previousXindex = $xvalue;
                    }
                }
            }

            //var_dump($myData);


            for($i = 0; $i < 35; $i++)
            {
                if(date("l", strtotime('-'. $i .' days')) == 'Sunday')
                {
                    $yAxis[] = date("m/d", strtotime('-'. $i .' days'));
                }
            }

            $return['yAxis'] = array(
                'categories' => $yAxis,
                'title' => array('text' => 'Week of')
            );

            $return['series'][] = array(

                'name' => 'Submission count',
                'borderWidth' => 1,
                'dataLabels' => array(
                    'enabled' => true,
                    'color' => 'black',
                    'style' => array(
                        'textShadow' => 'none',
                        'HcTextStroke' => null
                    ),
                ),
                'data' => $data
            );


        }
        catch(Exception $e)
        {
            $return = $e->getMessage();
            error_log($return);
        }

        return $return;
    }

    public function getHighMapByRegionDashboard() {

        $dateTime = new DateTime('-90 days');
        $select = 'SELECT * FROM form_submissions
                    WHERE date_submitted > :date_submitted
                    AND origin_label in(:origin1, :origin2)
                    AND channel_tactic != ""
                    AND status < :status';
        $params = array(
            ':date_submitted' => $dateTime->format('Y-m-d H:s:i'),
            ':origin1' => 'leicaus',
            ':origin2' => 'ape',
            ':status'  => 2     // rejected
        );

        // build exclusion list
        foreach($this->exclude as $field => $exArr)
        {
            foreach($exArr as $x)
            {
                $whereArray[] = $field ." NOT LIKE '". $x. "'";
            }
        }

        $where = !empty($whereArray) ? " AND ". implode(" AND ", $whereArray) . (!empty($whereOrArray) ? " AND (" . implode(" OR ", $whereOrArray) .")" : "") : "";

        $sql = $select . $where;

        $return = array();
        $data = array();

        try {
            $results = $this->ExecAndFetchAll($sql, $params);

            foreach($results as $key => $row)
            {
                $postData = json_decode($row['posted_data'], true);

                if(isset($postData['x_country']) && !empty($postData['x_country']))
                {
                    if(isset($data[$postData['x_country']]))
                        $data[$postData['x_country']] += 1;
                    else
                        $data[$postData['x_country']] = 0;
                }
            }

            foreach($data as $country => $count)
            {
                $return[0][] = array(
                    'code' => $country,
                    'value' => $count
                );
            }

        }
        catch(Exception $e)
        {
            $return = $e->getMessage();
            error_log($return);
        }

        return $return;

    }


    public function getHighMapByStateDashboard() {

        $dateTime = new DateTime('-90 days');
        $select = 'SELECT * FROM form_submissions
                    WHERE date_submitted > :date_submitted
                    AND origin_label in(:origin1, :origin2)
                    AND channel_tactic != ""
                    AND status < :status';
        $params = array(
            ':date_submitted' => $dateTime->format('Y-m-d H:s:i'),
            ':origin1' => 'leicaus',
            ':origin2' => 'ape',
            ':status'  => 2     // rejected
        );

        // build exclusion list
        foreach($this->exclude as $field => $exArr)
        {
            foreach($exArr as $x)
            {
                $whereArray[] = $field ." NOT LIKE '". $x. "'";
            }
        }

        $where = !empty($whereArray) ? " AND ". implode(" AND ", $whereArray) . (!empty($whereOrArray) ? " AND (" . implode(" OR ", $whereOrArray) .")" : "") : "";

        $sql = $select . $where;

        $return = array();
        $data = array();

        try {
            $results = $this->ExecAndFetchAll($sql, $params);

            foreach($results as $key => $row)
            {
                $postData = json_decode($row['posted_data'], true);

                if(isset($postData['x_country']) && isset($postData['x_state']) && $postData['x_country'] == 'USA' && !empty($postData['x_state']))
                {
                    if(isset($data[$postData['x_state']]))
                        $data[$postData['x_state']] += 1;
                    else
                        $data[$postData['x_state']] = 0;
                }
            }

            foreach($data as $state => $count)
            {
                $return[0][] = array(
                    'code' => $state,
                    'value' => $count
                );
            }

        }
        catch(Exception $e)
        {
            $return = $e->getMessage();
            error_log($return);
        }

        return $return;

    }

    public function getBarChartByCategoryDashboard() {
        /*
            series: [{
                name: 'John',
                data: [5, 3, 4, 7, 2, 5, 3, 4, 7, 2, 7, 2]
            }, {
                name: 'Jane',
                data: [2, 2, 3, 2, 1, 2, 2, 3, 2, 1, 2, 1]
            }, {
                name: 'Joe',
                data: [3, 4, 4, 2, 5, 3, 4, 4, 2, 5, 3, 2]
            }]
         */
        $select = 'SELECT * FROM form_submissions
                    WHERE date_submitted >= :date_submitted
                    AND origin_label in(:origin1, :origin2)
                    AND channel_tactic != ""
                    AND status < :status';
        $params = array(
            ':date_submitted' => date("Y-m-d 00:00:00", strtotime("-11 months")),
            ':origin1' => 'leicaus',
            ':origin2' => 'ape',
            ':status'  => 2     // rejected
        );

        // build exclusion list
        foreach($this->exclude as $field => $exArr)
        {
            foreach($exArr as $x)
            {
                $whereArray[] = $field ." NOT LIKE '". $x. "'";
            }
        }

        $where = !empty($whereArray) ? " AND ". implode(" AND ", $whereArray) . (!empty($whereOrArray) ? " AND (" . implode(" OR ", $whereOrArray) .")" : "") : "";

        $sql = $select . $where;

        $return = array();
        try {
            $results = $this->ExecAndFetchAll($sql, $params);
            /**
             *  Category
             */
            $catFormsArray = array();

            foreach($results as $key => $row)
            {
                $postData = json_decode($row['posted_data'], true);
                $channel_tactic = $row['channel_tactic'];

                if(!empty($channel_tactic))
                {
                    $theDate = new DateTime($row['date_submitted']);
                    $month = $theDate->format('M');
                    $channel_tactic_label = $this->getChannelTacticLabelFromValue($channel_tactic);

                    if(!array_key_exists($channel_tactic_label, $catFormsArray))
                    {
                        $catFormsArray[$channel_tactic_label] = array(
                            date("M", strtotime("-11 months")) => 0,
                            date("M", strtotime("-10 months")) => 0,
                            date("M", strtotime("-9 months")) => 0,
                            date("M", strtotime("-8 months")) => 0,
                            date("M", strtotime("-7 months")) => 0,
                            date("M", strtotime("-6 months")) => 0,
                            date("M", strtotime("-5 months")) => 0,
                            date("M", strtotime("-4 months")) => 0,
                            date("M", strtotime("-3 months")) => 0,
                            date("M", strtotime("-2 months")) => 0,
                            date("M", strtotime("-1 months")) => 0,
                            date("M", strtotime("-0 months")) => 0,
                        );
                    }

                    $catFormsArray[$channel_tactic_label][$month] += 1;
                }

            }

            /*
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            }
            */

            foreach($catFormsArray as $cat => $data)
            {
                $return['series'][] = array('name' => $cat, 'data' => array_values($data));
                $return['xAxis']['categories'] = array_keys($data);
            }
        }
        catch(Exception $e)
        {
            $return = $e->getMessage();
            error_log($return);
        }

        return $return;
    }

    public function getBarChartByCategoryDashboardLegacy() {
        /*
            series: [{
                name: 'John',
                data: [5, 3, 4, 7, 2, 5, 3, 4, 7, 2, 7, 2]
            }, {
                name: 'Jane',
                data: [2, 2, 3, 2, 1, 2, 2, 3, 2, 1, 2, 1]
            }, {
                name: 'Joe',
                data: [3, 4, 4, 2, 5, 3, 4, 4, 2, 5, 3, 2]
            }]
         */
        $dateTime = new DateTime('first day of January this year');
        $select = 'SELECT * FROM form_submissions
                    WHERE date_submitted >= :date_submitted
                    AND origin_label in(:origin1, :origin2)
                    AND channel_tactic != ""
                    AND status < :status';
        $params = array(
            ':date_submitted' => $dateTime->format('Y-m-d H:s:i'),
            ':origin1' => 'leicaus',
            ':origin2' => 'ape',
            ':status'  => 2     // rejected
        );

        // build exclusion list
        foreach($this->exclude as $field => $exArr)
        {
            foreach($exArr as $x)
            {
                $whereArray[] = $field ." NOT LIKE '". $x. "'";
            }
        }

        $where = !empty($whereArray) ? " AND ". implode(" AND ", $whereArray) . (!empty($whereOrArray) ? " AND (" . implode(" OR ", $whereOrArray) .")" : "") : "";

        $sql = $select . $where;

        $return = array();
        try {
            $results = $this->ExecAndFetchAll($sql, $params);
            /**
             *  Category
             */
            $catFormsArray = array();

            foreach($results as $key => $row)
            {
                $postData = json_decode($row['posted_data'], true);
                $channel_tactic = $row['channel_tactic'];

                if(!empty($channel_tactic))
                {
                    $theDate = new DateTime($row['date_submitted']);
                    $month = $theDate->format('m');
                    $channel_tactic_label = $this->getChannelTacticLabelFromValue($channel_tactic);

                    if(!array_key_exists($channel_tactic_label, $catFormsArray))
                    {
                        $catFormsArray[$channel_tactic_label] = array(
                            '01' => 0,
                            '02' => 0,
                            '03' => 0,
                            '04' => 0,
                            '05' => 0,
                            '06' => 0,
                            '07' => 0,
                            '08' => 0,
                            '09' => 0,
                            '10' => 0,
                            '11' => 0,
                            '12' => 0,
                        );
                    }

                    $catFormsArray[$channel_tactic_label][$month] += 1;
                }

            }

            foreach($catFormsArray as $cat => $data)
            {
                $return['series'][] = array('name' => $cat, 'data' => array_values($data));
            }
        }
        catch(Exception $e)
        {
            $return = $e->getMessage();
            error_log($return);
        }

        return $return;
    }

    public function getDonutChartByCategoryDashboard() {

        $dateTime = new DateTime('-90 days');
        $select = 'SELECT * FROM form_submissions WHERE date_submitted >= :date_submitted AND origin_label in(:origin1, :origin2)';
        $params = array(
            ':date_submitted' => $dateTime->format('Y-m-d H:s:i'),
            ':origin1' => 'leicaus',
            ':origin2' => 'ape'
        );

        // build exclusion list
        foreach($this->exclude as $field => $exArr)
        {
            foreach($exArr as $x)
            {
                $whereArray[] = $field ." NOT LIKE '". $x. "'";
            }
        }

        $where = !empty($whereArray) ? " AND ". implode(" AND ", $whereArray) . (!empty($whereOrArray) ? " AND (" . implode(" OR ", $whereOrArray) .")" : "") : "";

        $sql = $select . $where;

        try {
            $results = $this->ExecAndFetchAll($sql, $params);

            /**
             *  Category
             */
            $catFormsArray = array();

            foreach($results as $key => $row)
            {
                $postData = json_decode($row['posted_data'], true);
                $category = isset($postData['x_udf_source_category399__ss']) && !empty($postData['x_udf_source_category399__ss']) ? $postData['x_udf_source_category399__ss'] : $this->_getDefaultCategoryForFormId($row['form_id']);

                if(isset($catFormsArray[$category]))
                {
                    if(isset($catFormsArray[$category]['forms'][$row['form_id']]))
                    {
                        $catFormsArray[$category]['y'] += 1;
                        $catFormsArray[$category]['forms'][$row['form_id']] += 1;
                        $catFormsArray[$category]['total'] +=1;
                    }
                    else
                    {
                        $catFormsArray[$category]['y'] += 1;
                        $catFormsArray[$category]['forms'][$row['form_id']] = 0;
                        $catFormsArray[$category]['total'] +=1;
                    }

                }
                else
                {
                    $catFormsArray[$category]['forms'][$row['form_id']] = 0;
                    $catFormsArray[$category]['y'] = 0;
                    $catFormsArray[$category]['total'] = 0;
                    $catFormsArray[$category]['visible'] = true;
                    $catFormsArray[$category]['name'] = $category;
                }
            }


            $colorArray = array('#f45b5b', '#8085e9', '#8d4654');
            foreach($catFormsArray as $myArr)
            {
                $color = '';

                if(!empty($colorArray))
                {
                    $randomColorKey = array_rand($colorArray, 1);
                    $color = $colorArray[$randomColorKey];
                    unset($colorArray[$randomColorKey]);
                }
                else
                {
                    $color = '#'. $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
                }

                $returnCategory[] = array(
                    'name'  =>  $myArr['name'],
                    'y'     =>  $myArr['y'],
                    'color' =>  $color,
                    'visible' => $myArr['visible']
                );

                foreach($myArr['forms'] as $key => $myformCount)
                {
                    if($myformCount > 0)
                    {
                        $returnForms[] = array(
                            'name'  =>  $this->_getFormLabelFromFormId($key),
                            'y'     =>  $myformCount,
                            'color' =>  $this->adjustColorLightenDarken($color, -50),
                            'visible' => $myArr['visible']
                        );
                    }
                }
            }

            $return = array($returnCategory);

        }
        catch(Exception $e)
        {
            $return = $e->getMessage();
            error_log($return);
        }

        return $return;


    }

    private function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    /**
    * @param $color_code
    * @param int $percentage_adjuster
    * @return array|string
    * @author Jaspreet Chahal
    */
    private function adjustColorLightenDarken($color_code,$percentage_adjuster = 0) {
        $percentage_adjuster = round($percentage_adjuster/100,2);
        if(is_array($color_code)) {
            $r = $color_code["r"] - (round($color_code["r"])*$percentage_adjuster);
            $g = $color_code["g"] - (round($color_code["g"])*$percentage_adjuster);
            $b = $color_code["b"] - (round($color_code["b"])*$percentage_adjuster);

            return array("r"=> round(max(0,min(255,$r))),
                "g"=> round(max(0,min(255,$g))),
                "b"=> round(max(0,min(255,$b))));
        }
        else if(preg_match("/#/",$color_code)) {
            $hex = str_replace("#","",$color_code);
            $r = (strlen($hex) == 3)? hexdec(substr($hex,0,1).substr($hex,0,1)):hexdec(substr($hex,0,2));
            $g = (strlen($hex) == 3)? hexdec(substr($hex,1,1).substr($hex,1,1)):hexdec(substr($hex,2,2));
            $b = (strlen($hex) == 3)? hexdec(substr($hex,2,1).substr($hex,2,1)):hexdec(substr($hex,4,2));
            $r = round($r - ($r*$percentage_adjuster));
            $g = round($g - ($g*$percentage_adjuster));
            $b = round($b - ($b*$percentage_adjuster));

            return "#".str_pad(dechex( max(0,min(255,$r)) ),2,"0",STR_PAD_LEFT)
            .str_pad(dechex( max(0,min(255,$g)) ),2,"0",STR_PAD_LEFT)
            .str_pad(dechex( max(0,min(255,$b)) ),2,"0",STR_PAD_LEFT);
        }
    }
}