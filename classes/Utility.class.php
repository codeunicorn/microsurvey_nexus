<?php
class Utility extends DatabaseClass
{
    // constructor
    public function __construct()
    {
        parent::__construct();
    }

    // grabs the user's actual IP address
    public static function GrabIP()
    {
        $_REMOTE_ADDR = $_SERVER['REMOTE_ADDR'];

        //patch for the LB IP adress screwing UP the forwarded for header
        if (substr($_REMOTE_ADDR, 0, 3) == '10.') //this means it was sent thru the LB
        {
            $Addrs = explode(',', (isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']));
            $_REMOTE_ADDR = trim(array_pop($Addrs));

            if (count($Addrs) > 0)
                $_HTTP_X_FORWARDED_FOR = implode(',', $Addrs);
            else
                $_HTTP_X_FORWARDED_FOR = '';
        }

        //now determine the real IP address
        //assume we have a proxy in front and try to get the forwarded for
        $Ip = isset($_HTTP_X_FORWARDED_FOR) ? $_HTTP_X_FORWARDED_FOR : '';

        //if the forwarded for is a list of IPs, grab from the left until we find a non private IP
        if (strpos($Ip, ',') !== FALSE)
        {
            $List = explode(',', $Ip);
            $Ip = trim(array_shift($List));
            while (!self::IsValidIP($Ip) && (count($List) > 0))
                $Ip = trim(array_shift($List));
        }

        //test if we got so far a normal IP. if not, fall back to getting the REMOTE_ADDR parameter
        if (($Ip == '') || !self::IsValidIP($Ip))
            $Ip = $_REMOTE_ADDR;

        return $Ip;
    }

    public static function IsValidIP($IP)
    {
        //first run thru the php built in filter val function
        $IsValid = filter_var($IP, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_RES_RANGE | FILTER_FLAG_NO_PRIV_RANGE) !== false;

        //if valid so far, test against 127.* range. This is not caught by filter_val
        if ($IsValid)
            $IsValid = substr($IP, 0, 4) != '127.';

        return $IsValid;
    }

    /**
     * Locates the visitor's country via IP address (Minified, and this is not my code. See readme.txt in /inc folder)
     */
    public static function iptocountry($ip)
    {
        $two_letter_country_code = "CA";
        $numbers = preg_split( "/\./", $ip);

        include(NEXUS_INTERNAL_ROOT ."inc/ip_files/".$numbers[0].".php");
        $code=($numbers[0] * 16777216) + ($numbers[1] * 65536) + ($numbers[2] * 256) + ($numbers[3]);

        foreach($ranges as $key => $value)
        {
            if($key<=$code)
            {
                if($ranges[$key][0]>=$code)
                {
                    $two_letter_country_code = $ranges[$key][1];
                    break;
                }
            }
        }

        $countries = array(
            "US" => "USA",
            "MX" => "Mexico",
            "CA" => "Canada",
            "AU" => "Australia",
            "BR" => "Brazil",
            "NL" => "Netherlands",
            "GB" => "United Kingdom",
            "IE" => "Ireland (Eire)"
        );
        $default_country = array_key_exists($two_letter_country_code, $countries) ? $countries[$two_letter_country_code] : "Canada";
        return $default_country;
    }

    public static function ipToCountryCode($ip)
    {
        $two_letter_country_code = "US";
        $numbers = preg_split( "/\./", $ip);

        if( file_exists( NEXUS_INTERNAL_ROOT ."inc/ip_files/".$numbers[0].".php" ) ) {
            include(NEXUS_INTERNAL_ROOT . "inc/ip_files/" . $numbers[0] . ".php");
            $code = ($numbers[0] * 16777216) + ($numbers[1] * 65536) + ($numbers[2] * 256) + ($numbers[3]);

            foreach ($ranges as $key => $value) {
                if ($key <= $code) {
                    if ($ranges[$key][0] >= $code) {
                        $two_letter_country_code = $ranges[$key][1];
                        break;
                    }
                }
            }
        }
        return $two_letter_country_code;
    }

    public static function getDefaultCountry($ip)
    {
        $countries = array(
            "US" => "USA",
            "MX" => "Mexico",
            "CA" => "Canada",
            "AU" => "Australia",
            "BR" => "Brazil",
            "NL" => "Netherlands",
            "GB" => "United Kingdom",
            "IE" => "Ireland (Eire)"
        );
        $two_letter_country_code = Utility::ipToCountryCode($ip);
        $default_country = array_key_exists($two_letter_country_code, $countries) ? $countries[$two_letter_country_code] : "Canada";

        // override if on local server
        if($ip === '127.0.0.1')
            $default_country = 'USA';

        return $default_country;
    }

    /*
     * These functions can be used in wrappers - querystring merging and manipulation
     */
    public static function removeQuerystring($url, $key) {
        $url = preg_replace('/(.*)(\?|&)' . $key . '=[^&]+?(&)(.*)/i', '$1$2$4', $url . '&');
        $url = substr($url, 0, -1);
        return $url;
    }

// http://stackoverflow.com/questions/7356555/better-way-to-replace-query-string-value-in-a-given-url
    public static function mergeQuerystring($url = null,$query = null,$recursive = false)
    {
        if($url == null)
            return false;
        if($query == null)
            return $url;
        $url_components = parse_url($url);
        if(empty($url_components['query']))
            return $url . $query;
        parse_str($url_components['query'],$original_query_string);
        parse_str(parse_url($query,PHP_URL_QUERY),$merged_query_string);
        if($recursive == true)
            $merged_result = array_merge_recursive($original_query_string,$merged_query_string);
        else
            $merged_result = array_merge($original_query_string,$merged_query_string);

        return str_replace($url_components['query'],http_build_query($merged_result),$url);
    }

// Function for basic field validation (present and neither empty nor only white space
    public static function IsNullOrEmptyString($question){
        return (!isset($question) || trim($question)==='');
    }

    public static function dump($x, $die = false)
    {
        if(isset($_GET['debug']) && class_exists('ChromePhp'))
            ChromePhp::log($x);
        else
            var_dump($x);

        if($die)
            die();
    }

    // logger function
    public static function logMessage($message, $filename = 'default_log', $logOnlyOnDebugMode = false)
    {
        // always log, or when $logOnlyOnDebugMode is true, debug must also be passed as a url querystring
        if(!$logOnlyOnDebugMode || ($logOnlyOnDebugMode && isset($_GET['debug'])))
        {
            $baseDir = dirname(__DIR__);
            $logfile = $baseDir . '/logs/'. $filename .'.log';
            $thefile = fopen($logfile, 'a+');
            fwrite($thefile, "(". date('Y-m-d H:i:s') .") ". $message ."\n");
            fclose($thefile);
        }
    }

    /**
     * gets the dial Code for the selected country
     * @param string $country  The lookup country
     * @return string $dialCode  The dial code for input country
     * @access public
     */
    public function getDialCode($country = 'United States')
    {
        $sql = 'SELECT `prefix_code` FROM `sms_int_dial_code`
                WHERE country = :country';
        $params = array(
            ':country' => $country
        );
        try {
            $data = $this->ExecAndFetch( $sql, $params );
        }
        catch( exception $e ) {
            $data = $e;
        }

        // default dial code is US (+1)
        $dialCode = '1';
        if(!empty($data))
            $dialCode = $data['prefix_code'];

        // remove the '+' from the dial code
        $dialCode = str_replace('+', '', $dialCode);

        return $dialCode;
    }
}
