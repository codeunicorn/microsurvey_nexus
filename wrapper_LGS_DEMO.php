<?php

// Demo Nexus v2.0 - Jareb Coupe 2013
// Demo Nexus v2.2 - Joseph Santos 2014
// ### Form Builder (core) ###
// The Leica Geosystems NAFTA demo wrapper

// Common initialization
require_once('common_init.php');
$page = $_SERVER['PHP_SELF']."?".$querystring;

?>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Leica Geosystems Demo Nexus</title>
<link rel="icon" href="http://gis.leica-geosystems.us/media/favicon/websites/6/IDR_MAINFRAME.ico" type="image/x-icon" />
<link rel="shortcut icon" href="http://gis.leica-geosystems.us/media/favicon/websites/6/IDR_MAINFRAME.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/fortis/default/css/styles.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/fortis/gis/css/skin.css" media="all" />
<script type="text/javascript" src="scripts/prototype/prototype.js"></script>
<script type="text/javascript" src="scripts/prototype/validation.js"></script>
<script type="text/javascript" src="scripts/varien/form.js"></script>
<script type="text/javascript" src="scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript">jQuery.noConflict();</script>

<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/base/default/css/styles-ie.css" media="all" />
<![endif]-->
<!--[if lt IE 7]>
<script type="text/javascript" src="http://gis.leica-geosystems.us/js/lib/ds-sleight.js"></script>
<script type="text/javascript" src="http://gis.leica-geosystems.us/skin/frontend/base/default/js/ie6.js"></script>
<![endif]-->
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/fortis/default/css/styles-ie-all.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/fortis/default/css/skin-ie-all.css" media="all" />
<![endif]-->
<!--[if lte IE 7]>
<link rel="stylesheet" type="text/css" href="http://gis.leica-geosystems.us/skin/frontend/fortis/default/css/styles-ie7.css" media="all" />
<![endif]-->

<style>
label {
	width: 200px;
	text-align: right;
	padding-right: 15px;
}
input, select, textarea {
	margin-left: 12px;
}
.fields {
	text-align: left;
}
.form-results {
	float: right;
	margin: 9px 8px 0 0;
}
body {
	margin: 0;
	padding: 0;
	text-align: left;
}
.header-container {
	background-image: url('http://gis.leica-geosystems.us/skin/frontend/fortis/default/images/img/header-gray-l.png');
}
.header-container2 {
	background: url('http://gis.leica-geosystems.us/skin/frontend/fortis/default/images/img/top-bg-plus.png') center 0 no-repeat;
}
.header {
	width: 960px;
	margin: 0 auto;
	padding: 0;
	height: 126px;
	position: relative;
}
.header-top {
	height: 40px;
}
.header-mid {
	height: 86px;
}
.nav {
	width: 960px;
	margin: 0 auto;
	padding: 0;
	height: 51px;
}
.navbar-bg {
	background-image: url('http://gis.leica-geosystems.us/skin/frontend/fortis/default/images/img/navbar-red.png');
}
.navbar-left {
	background-position: 0 -51px;
	width: 8px;
	height: 51px;
	float: left;
}
.navbar {
	background-position: top left;
	background-repeat: repeat-x;
	float: left;
	width: 944px;
	height: 41px;
	padding-top: 10px;
}
.navbar-right {
	background-position: 100% -51px;
	width: 8px;
	height: 51px;
	float: left;
}
.header-text {
	width: 300px;
	color: #333;
	margin: 6px 0 0 10px;
	font-family: 'Open Sans', sans-serif;
	font-weight: 600;
	font-size: 14px;
	text-transform: uppercase;
	float: left;
}
.show-home-img {
	width: 40px;
	height: 41px;
	padding: 0;
	background: url('http://gis.leica-geosystems.us/skin/frontend/fortis/default/images/img/pix.png') 0 -335px no-repeat;
	text-indent: -9999px;
	overflow: hidden;
	float: left;
	border: none;
	text-decoration: none;
	text-align: left;
	list-style: none;
}
.show-home-img:hover {
	background-position: -46px -335px;
}
.nav-home-link {
	height: 41px;
	display: inline-block;
	list-style: none;
	float: left;
	text-align: left;
}
.footer-container {
	margin: 0 auto;
	padding: 0;
	background: url('http://gis.leica-geosystems.us/skin/frontend/fortis/default/images/img/footer-gray-l.png');
	height: 154px;
}
.footer {
	width: 960px;
	margin: 0 auto;
	padding: 0;
	text-align: center;
}
.footer-text {
	font-size: 11px;
	display: inline-block;
	width: 505px;
	margin: 86px auto 0;
	line-height: 15px;
	text-align: center;
	color: #555;
	line-height: 15px;
}
.sidebar {
	background: url(images/wrapper-images/MIC-demo/rightbar.png) 0 0 no-repeat;
	margin: 11px 0 0 -1px;
}
.sidebar-top {
	padding: 10px 20px 15px 17px;
	border-left: 1px #fff solid;
	border-right: 1px #fff solid;
}
.sidebar-bottom {
	margin: 0 0 0 2px;
	border-left: 1px #fff solid;
	border-right: 1px #fff solid;
}
.form-preamble, .form-postamble {
	margin: 17px 0 17px 0;
}
</style>
</head>

<body class="customer-account-create">
<div class="wrapper">
  <div class="header-container">
    <div class="header-container2">
      <div class="header">
        <div class="header-top"> </div>
        <div class="header-mid"> <a href="" class="logo"><img src="http://gis.leica-geosystems.us/skin/frontend/fortis/gis/images/logo.png" border="0"></a> </div>
      </div>
      <div class="nav-container">
        <div class="nav">
          <div class="navbar-bg navbar-left"></div>
          <div class="navbar-bg navbar">
            <div class="nav-home-link"><a class="show-home-img" href="<?php echo $config_back; ?>"><?php echo isset($config_back_label) ? $config_back_label : ''; ?></a></div>
            <div class="form-search"></div>
            <div class="header-text"><?php echo isset($config_navbar1_label) ? $config_navbar1_label : '';?></div>
          </div>
          <div class="navbar-bg navbar-right"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="main-container col2-right-layout">
    <div class="main">
      <div class="breadcrumbs">
        <ul>
          <li class="home"><a href="<?php echo $config_back; ?>"><?php echo isset($config_back_label) ? $config_back_label : ''; ?></a></li>
        </ul>
      </div>
      <div class="col-main">
        <div class="account-create">
          <?php require_once('core.php'); ?>
        </div>
      </div>
      <div class="col-right">
        <div class="sidebar">
          <div class="sidebar-top">
            <div class="block block-layered-nav" style="margin-bottom: 12px; margin-top: 10px;">
              <div class="block-title"><strong><span><?php echo isset($config_sidebar_header2_label) ? $nexus->insertStrings($config_sidebar_header2_label) : ''; ?></span></strong></div>
            </div>
            <span><?php echo isset($config_sidebar_content2_label) ? $nexus->insertStrings($config_sidebar_content2_label) : ''; ?></span> </div>
          <div class="sidebar-bottom">
          <a href="http://uas.leica-geosystems.us/"><img src="http://www.leica-geosystems.us/images/new/base/UAS-microsite_main.jpg" width="212" height="88"></a><BR>
          <a href="http://www.leica-geosystems.us/en/Building-Information-Modeling-BIM_104309.htm"><img src="http://www.leica-geosystems.us/images/new/base/BIM_Field_Trip_231x96.gif" width="212" height="88"></a><BR>
          <a href="http://www.leica-geosystems.us/en/Leica-ScanStation-C10_79411.htm"><img src="http://www.leica-geosystems.us/images/new/base/Leica_ScanStation_C10_TV_Series_231x95.jpg" width="212" height="88"></a><BR>          
          
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-container">
    <div class="footer">
      <div class="footer-text">© 2014 Leica Geosystems. All Rights Reserved.</div>
    </div>
  </div>
</div>
</body>
</html>