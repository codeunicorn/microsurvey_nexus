<?php
// Demo Nexus v2.2 - Joseph Santos, Jareb Coupe 2015
// Leica Geosystems eBook wrapper
// Common initialization
require_once('common_init.php');
$page = $_SERVER['PHP_SELF']."?".$querystring;
//Create array from querystring
parse_str($querystring, $queryOutput);
//Define variables
$img = isset($queryOutput['img']) ? $queryOutput['img'] : '';
$t1 = isset($queryOutput['t1']) ? $queryOutput['t1'] : '';
$t2 = isset($queryOutput['t2']) ? $queryOutput['t2'] : '';
$dl = isset($queryOutput['dl']) ? $queryOutput['dl'] : '';
$ex = isset($queryOutput['ex']) ? $queryOutput['ex'] : '';
$bh = isset($queryOutput['bh']) ? $queryOutput['bh'] : '';
$status = isset($queryOutput['status']) ? $queryOutput['status'] : '';
?>
<!doctype html>
<html lang="en"><head>
<meta charset="UTF-8">
<meta name="description" content="<?php echo $t1; ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="icon" href="http://assets.leica-geosystems.us/wrapper/ebook/favicon.ico">
<!--<link href="http://assets.leica-geosystems.us/wrapper/ebookv2/style/templatev2.css" rel="stylesheet">!-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $t1; ?></title>
<style>
body {
	margin:0; 
	background-color:#fff; 
	font-family:Helvetica, Arial, sans-serif;
}
@media (min-width: 1200px) {
  .container {
    width: 960px !important;
   }
}
@media (min-width: 992px) {
  .container {
    width: 960px !important;
   }
 }
#header-container {
	height:580px;
	background-image:url(http://assets.leica-geosystems.us/wrapper/ebookv2/images/bg_header/<?php echo $img; ?>.jpg);
	background-repeat:no-repeat;
	background-size:cover;
	padding-top:40px;
}
#header-container h1 {
	font-size:32px;
	color:#fff;
	text-align:center;
	letter-spacing:1px;
	padding:100px 20px 0;
text-shadow: 0px 4px 3px rgba(0,0,0,0.4),
             0px 8px 13px rgba(0,0,0,0.1),
             0px 18px 23px rgba(0,0,0,0.1);

}
#header-logo {
	height:56px;
	float:left;
	width:114px;	
	background-color:#fff;
	padding:7px;
	text-align:center;
	background-image:url(http://assets.leica-geosystems.us/wrapper/ebookv2/images/leica-logo.png);
	background-size:auto 40px;
	background-position:center;
	background-repeat:no-repeat;
}
nav {
	height:56px;
	background: rgb(0, 0, 0); /* The Fallback */
    background: rgba(0, 0, 0, 0.4); 
}

.nav-menu {
  list-style: none;
  text-align: center;
  padding: 0;
  margin: 0;
  float:right;
  
  overflow: auto;
  margin: 0 auto;
}
.nav-menu li {
	float:left;
	line-height: 56px;
	height: 65px;
	margin-left:15px;
}
.nav-menu li.active a span, .nav-menu li a:hover span {
	width:0;
	height:0;
	border-left: 5px solid transparent;
	border-right: 5px solid transparent;	
	border-bottom: 5px solid #fff;
	position:absolute;
	bottom:0;
	left:40%;
}
.nav-menu a {
	color:#fff;
	font-size:16px;
	text-transform:uppercase;
	text-decoration: none;
	display: block;
	transition: .3s color;
	position:relative;
}
.nav-menu a:hover, .nav-menu a:active, .nav-menu a:focus {
  text-decoration:none;
  color:#ddd;
}
 
.nav-menu a.active {
  background-color: #fff;
  color: #444;
  cursor: default;
}

.contact-box {
	text-align: center;
}
.contact-box .contact-button {
    background-color: #ff1831;
	background-image:url(http://assets.leica-geosystems.us/wrapper/ebookv2/images/arrow_down.png);
	background-position:center;
	background-repeat:no-repeat;
    font-size: 30px;
    height: 88px;
    line-height: 60px;
    margin: -44px auto auto;
    position: relative;
    vertical-align: middle;
    width: 88px;
	border-radius:50%;
	color:#fff;
}
.contact-box .contact-button:hover {
    color: #ffffff;
}
#download-form {
	padding:35px 0 50px;
}
#contact-form {
	padding:35px 0 50px;
	text-align: left;
}
#contact-container h2 {
	font-size:19px;
	color:#000;
	text-align: center;
	text-transform:uppercase;
	margin-bottom:30px;
	line-height:29px;
	letter-spacing:1px;
}
#content-container {
	padding: 40px 0;
	background-color:#ff1831;
	color:#fff;
}
#content-container h3 {
	color:#fff;
	font-size:25px;
	line-height:35px;
	padding:0 0 5px;
	letter-spacing:1px;
	text-align:center;
}
#content-container h4 {
	font-size:16px;
	letter-spacing:1px;
}
#content-container .ebook img {
	
}
#content-container .ebook {
	text-align:center;
}
#footer-container {
	height:90px;
	font-size:12px;
	color:#808080;
}
footer .leica-logo {
	background:url(http://assets.leica-geosystems.us/wrapper/ebookv2/images/leica_logo.png) center right no-repeat #FFF;
	width:195px;
	height:45px;
	float:right;
	margin:23px 30px 23px 0;
}
footer .hexagon-logo {
	background:url(http://assets.leica-geosystems.us/wrapper/ebookv2/images/hexagon_logo.png) center right no-repeat #FFF;
	width:82px;
	height:44px;
	float:left;
	margin:23px 42px 0 -15px;
}
footer .copyright {
	padding-top:38px;
	font-size:12px;
	color:#808080;
}
.buttons-set {
	clear:both;
}
#etrSubmit {
    font-size: 14px;
    font-weight: bold;
    text-transform: uppercase;
	
}

label {
	color:#808080;
	font-weight:normal !important;

}
label em {
	margin-left: 2px;
}
.bullet-icon {
	width:40px;
	height:40px;
	background-position:center;
	background-repeat:no-repeat;
	background-color: #fff;
	border-radius:50%;
	margin:4px 0 0;
	float:right;
}
.bullet-text {
	color:#fff;
	padding-bottom:5px;
}
.required-text {
	color:#ee0033;
	padding-bottom:28px;
}
/* Bootstrap Overrides */

.btn-primary {
	border: none !important;
	background:#ee0033 url("http://assets.leica-geosystems.us/wrapper/ebookv2/images/arrow_right.png") no-repeat !important;
	padding-right: 26px !important;
	text-transform:uppercase;
	
}
.btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .btn-primary:active {
	border: none !important;
	background:#d80f25 url("http://assets.leica-geosystems.us/wrapper/ebookv2/images/arrow_right.png") no-repeat !important;
}
#contact-form .btn-primary {
	background-position:75px center !important;
}
#contact-form .btn-primary:hover, #contact-form .btn-primary:focus, #contact-form .btn-primary:active, #contact-form .btn-primary.active, #contact-form .btn-primary:active {
	background-position:75px center !important;
}
.download-button .btn-primary {
	background-position:140px center !important;
}
.download-button .btn-primary:hover, .download-button .btn-primary:focus, .download-button .btn-primary:active, .download-button .btn-primary.active, .download-button .btn-primary:active {
	background-position:140px center !important;
}

.form-group.col-md-12.checkbox {
	padding-bottom:23px;
}
.form-group textarea.form-control {
	height:60px;
}
.has-error .form-control, .has-error .checkbox, .has-error .checkbox-inline, .has-error .control-label, .has-error .help-block, .has-error .radio, .has-error .radio-inline, .has-error.checkbox label, .has-error.checkbox-inline label, .has-error.radio label, .has-error.radio-inline label {
	color: #ee0033 !important;
}
.has-error .form-control {
	border-color: #ee0033 !important;
}
/* Markup Overrides */
p.required {
	display:none;
}
#x_udf_ext_hds_question_cyclone_2452__ms, #x_udf_ext_hds_question_cyclone451__ms {
	padding: 6px 15px 6px 15px;
	border: 1px solid #ccc;
	border-radius: 4px;
	width: 96.3%;
	margin-left: 15px;
}

#x_udf_privacy_policy_version143_fieldpair {
	margin-left: 15px;	
}
 @media (min-width: 750px) { 
 	#header-container h1 {
		font-size:48px;
	}
	#content-container h3 {
		font-size:30px;
	}
	.bullet-points {
		padding-top:50px;
	}
  }
</style>
<script>var _etgq=_etgq||[];_etgq.push(['_etg_o',979]);(function(e,t,r,i,g){g=e.createElement(t);i=e.getElementsByTagName(t)[0];g.async=true;g.src=r;i.parentNode.insertBefore(g,i);})(document,'script','//trk.etrigue.com/etriguelive.js');</script>
</head>
<body>
	<div id="header-container">
    	<nav>
        	<div class="container">
                 <div id="header-logo"></div>
                 <ul class="nav-menu">
                 	<?php
						foreach (array_combine($queryOutput['nt'], $queryOutput['nl']) as $navText => $navLink) {
							echo '<li><a href="'.$navLink.'">'.$navText.'<span></span></a></li>';
						}
					?>
                 </ul>
             </div>
        </nav>
        <header>
        	<div class="container">
            	<div class="row">
                	<div class="col-md-10 col-md-offset-1">
        				<h1><?php echo $t1; ?></h1>	
                    </div>
                </div>
            </div>
        </header>
    </div>
    <div id="contact-container">
        <div class="container">
            <div class="contact-box">
                <div class="expanded-contact-form">
                    <a class="btn contact-button expand-form expanded"></a>
                    <?php if ($status=="confirmation") { ?>
                        <div id="download-form" class="col-md-10 col-md-offset-1">
                        	<div class="row">
                            	<div class="col-md-12">
                                    <h2>Thank you. We'll be in contact with you shortly.</h2>
                                    
                                </div>
                            </div>
                        </div>
                        <?php require_once('core.php'); ?>
                    <?php } else { ?>
                        <div id="contact-form" class="col-md-10 col-md-offset-1">
                        	<div class="row">
                            	<div class="col-md-12">
                            		<h2>Fill out the form below to be added to the Cyclone Cloud field trial waiting list. We will contact you as soon as a space is available. Please note: Cyclone Cloud is currently available for field trial in North America only.</h2>
                            	</div>
                            </div>
                            <div class="row">
                            	<div class="col-md-12">
                            		<div class="required-text">* Required Fields</div>
                            	</div>
                            </div>
                            <div class="row">
								<?php require_once('core.php'); ?>
                            </div>
                        </div>
                    <?php } ?>	
                
                </div>
            </div>
        </div>
    </div>
    <div id="content-container">
    	<div class="container">
        	<?php if (!empty($bh)) { ?> 
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <h3><?php echo $bh; ?></h3>
                    </div>
                </div>
            <?php } ?>
        	<div class="row">
            	<div class="col-md-5 col-md-offset-1">
                	<div class="ebook">
                    	<img alt="<?php echo $t1; ?>" src="http://assets.leica-geosystems.us/wrapper/ebookv2/images/book/<?php echo $img; ?>.png" />
                    </div>
                </div>
                <div class="col-md-5 bullet-points">
                    <?php
						$i = 0;
						foreach (array_combine($queryOutput['bs'], $queryOutput['bt']) as $bulletSub => $bulletText) {
							$i++;
					?>
                    <?php 
						if (!empty($bulletSub)) {
					?>
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-2">
                                <?php echo '<h4>'.$bulletSub.'</h4>'; ?>
                            </div>
                        </div>
					<?php
						}
					?>
                    <div class="row">
                    	<div class="col-xs-2">
                        	<div class="bullet-icon" style="background-image:url(http://assets.leica-geosystems.us/wrapper/ebookv2/images/bullets/<?php echo $img.'-icon'.$i.'.png'; ?>)"></div>
                        </div>
                        <div class="col-xs-10">
                        	<p class="bullet-text"><?php echo $bulletText ?></p>
                        </div>
                    </div>
                    <?php
						}
					?>
                </div>
            </div>
        </div>
    </div>
	<?php if (!empty($ex)) { ?> 
        <div id="ex-container">             
        <?php 
            $exRL = 'http://assets.leica-geosystems.us/wrapper/ebookv2/ex/'.$_GET['ex'].'.html';
            echo file_get_contents($exRL); 
        ?>
        </div>
    <?php } ?>
    <div id="footer-container">
    	<footer>
        	<div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="hexagon-logo"></div>
                        <p class="copyright">© Copyright <?php echo date("Y");?> Leica Geosystems - Part of Hexagon</p>
                    </div>
                    <div class="col-sm-6">
                        <div class="leica-logo"></div>	
                    </div>
                </div>
            </div>
        </footer>
    </div>
<script>
	nQuery('#x_emailaddress_fieldpair').insertAfter(nQuery('#x_lastname_fieldpair'));
</script>
</body>
</html>